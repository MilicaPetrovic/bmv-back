Članovi tima BMV
    1. Violeta Erdelji, SW 32/2019
    2. Milica Petrović, SW 46/2019
    3. Bane Gerić, SW 47/2019


Back end aplikacija: https://gitlab.com/MilicaPetrovic/bmv-back.git    
        - pokreće se kao spring boot projekat i radi na portu 8080
Front end aplikacija: https://gitlab.com/MilicaPetrovic/bmv-front.git  
        - pre pokretanja odraditi npm install pa pokrenuti projekat komandom npm start, aplikacija radi na portu 4200

Za pokretanje aplikacije potrebno je prvo pokrenuti back end projekat, a zatim front end projekat i skriptu za simulaciju.

Obavezno nakon pokretanja back end aplikacije na linku https://localhost:8080/uber/h2  dozvoliti korišćenje self-signed certificate  kako bi aplikacija imla pristup bazi.

Skripta za simulaciju se nalazi u okviru back end projekta, u folderu locust.
Za pokretanje locust skripte je neophodno imati instaliran python.
Nakon toga, potrebno je instalirati pip alat koji će omogućiti instalaciju eksternih biblioteka u lokalni virutelni environment.

Kreiranje i aktivacija virtual environment-a:

Windows:
  virtualenv env
  env\Scripts\activate

Nakon aktivacije virtualenv okruženja, instalirati biblioteke:
  pip3 install locust
  pip3 install requests

Locust skriptu pokrenuti komandom:
	locust -f putanja_do_skripte/uber_simulation.py --headless -u 7 -r 1 --run-time 30m

Početna stranica se nalazi na linku: https://localhost:4200/auth/login (omogućiti korišćenje https-a)


Pokretanje testova
1) E2E
	Za pokretanje e2e testova podesiti putanju do Chrome i Edge webdrivera koji odgovaraju browserima na računaru na kom se pokreću testovi u e2e/tests/BaseTest.java fajlu.
	Pokrenuti back end ponovo da bi podaci bili odgovarajući, zatim pokrenuti testove koji se nalaze u e2e/tests folderu.
    Pre pokretanja EndDrivingTest klase, pokrenuti skriptu za simulaciju.
2) REST
	Za pokretanje REST integracionih tesova pokrenuti testove u controller folderu.
	Za pokretanje servis jediničnih testova pokrenuti testove u service folderu.
3) Front test
	Za pokretanje testova na frontu pokrenuti komandu ng test
	
