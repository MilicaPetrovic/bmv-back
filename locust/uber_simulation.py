import requests

from locust import HttpUser, task, between, events
from random import randrange

drivers = [4, 5, 6, 9, 10, 11, 12]  # id-ovi drivera


@events.test_start.add_listener
def on_test_start(environment, **kwargs):
    # requests.delete('http://localhost:8080/uber/simulation')
    # requests.delete('http://localhost:8080/uber/vehicle/type')
    pass


class QuickstartUser(HttpUser):
    host = 'https://localhost:8080/uber'
    wait_time = between(0.5, 0.1)

    def on_start(self):
        if len(drivers) > 0:  # + str(drivers.pop(0))
            self.load(str(drivers.pop(0)))

    @task
    def update_vehicle_coordinates(self):
        if self.is_null == False:
            if len(self.coordinates) > 0 and self.driving_to_start_point == True and self.end_to_start_point == False:
                new_coordinate = self.coordinates.pop(0)
                print("new coordinate")
                print(new_coordinate)

                print("driver id")
                print(self.driver_id)

                self.client.put(f"/vehicle/type/{self.driver_id}/{self.driving_id}", json={
                    'latitude': new_coordinate[0],
                    'longitude': new_coordinate[1]
                })

            elif len(self.coordinates) > 0 and self.driving_the_route == True and self.end_route == False and (
                    self.state == 1 or self.state == 2):
                new_coordinate = self.coordinates.pop(0)
                print("new coordinate")
                print(new_coordinate)

                print("driver id")
                print(self.driver_id)

                self.client.put(f"/vehicle/type/{self.driver_id}/{self.driving_id}", json={
                    'latitude': new_coordinate[0],
                    'longitude': new_coordinate[1]
                })

            else:
                if self.driving_to_start_point == True and self.end_to_start_point == False and (
                        self.state == 6 or self.state == 2):
                    self.end_ride_to_passenger()
                elif self.driving_the_route == True and self.end_route == False:
                    self.end_ride()
                    print(" yovem krajjjjjjjjjjjjjjjjjjjjj")
                    print("state")
                    print(self.state)
                    print("eeeeeeee nnnnn dddd   rrrrroute")
                    print(self.end_route)

                elif self.driving_to_start_point == True and self.end_to_start_point == True:
                    self.get_state_for_driving()
                    if (self.state == 2 and self.is_called == True):
                        self.departure = self.stations[0]
                        self.destination = self.stations[1]
                        self.visited_stations.append(0)
                        self.driving_to_start_point = False
                        self.driving_the_route = True
                        self.end_route = False
                        print(' Da li stignes dovde? ')
                        self.get_new_coordinates(randrange(1, 11))

                        print(self.coordinates)
                        print(" ///////////////  USLI SU PUTNICI   ////////////////")
                    elif (self.state == 4 or self.state == 3 or self.state == 5):  # ako je prekinuta prethodna voznja
                        self.coordinates = []
                        print("  nooooooooooovooooooooo")
                        self.load(self.driver_id)
                elif self.driving_the_route == True and self.end_route == True:  # ako je zavrsena prethodna voznja
                    self.get_state_for_driving()
                    print("state  qwqwqw")
                    print(self.state)
                    print("drivir idd")
                    print(self.driver_id)
                    if (self.state == 4 or self.state == 3 or self.state == 5):
                        self.coordinates = []
                        print("  nooooooooooovooooooooo")
                        self.load(self.driver_id)

        elif (self.is_null == True):
            print("moyeeeeeeeeeeee")
            self.load(self.driver_id)

    def get_new_coordinates(self, probability):  # legs[0] je prva ruta, a onda idu dalje
        response = requests.get(
            f'https://routing.openstreetmap.de/routed-car/route/v1/driving/{self.departure["lng"]},{self.departure["lat"]};{self.destination["lng"]},{self.destination["lat"]}?geometries=geojson&overview=false&alternatives=true&steps=true')
        self.routeGeoJSON = response.json()
        self.coordinates = []
        if len(self.routeGeoJSON['routes'][0]['legs']) >= 2 and probability < 5:
            index = 1
        else:
            index = 0
        for step in self.routeGeoJSON['routes'][0]['legs'][index]['steps']:
            self.coordinates = [*self.coordinates, *step['geometry']['coordinates']]
        print(self.coordinates)

    def end_ride_to_passenger(self):
        self.client.get(f"/simulation/come/{self.driving_id}")
        self.end_to_start_point = True

    def end_ride(self):
        if len(self.stations) - len(self.visited_stations) > 1:
            self.departure = self.stations[len(self.visited_stations)]
            self.destination = self.stations[len(self.visited_stations) + 1]
            self.visited_stations.append(0)
            self.get_new_coordinates(randrange(1, 11))
        else:
            self.end_route = True
            self.client.get(f"/simulation/end/{self.driving_id}")

    def load(self, driver_id):
        try:
            self.simulation_dto = self.client.get('/simulation/driver/' + str(driver_id), verify=False, headers={
                "Content-Type": "application/json"
            }).json()

            #  print(self.simulation_dto)
            # print(self.simulation_dto['isPaid'])
            self.is_called = False

            self.driving_id = self.simulation_dto['id']
            self.driver_id = self.simulation_dto['simulationVehicleDTO']['driverId']
            self.state = self.simulation_dto['drivingState']
            self.stations = self.simulation_dto['stations']
            print("stations")
            print(self.stations)

            if self.simulation_dto['isPaid'] == True:
                self.is_null = False
                print("evooooo")
                print(self.simulation_dto['simulationVehicleDTO']['driverId'])
                self.driving_to_start_point = True
                self.driving_the_route = False
                self.end_route = False
                self.end_to_start_point = False

                print("longitude")
                print(self.simulation_dto['simulationVehicleDTO']['latitude'])  # ovo je hak
                print("latitude")
                print(self.simulation_dto['simulationVehicleDTO']['longitude'])

                self.departure = {"lng": self.simulation_dto['simulationVehicleDTO']['latitude'],
                                  "lat": self.simulation_dto['simulationVehicleDTO']['longitude']}
                self.destination = self.simulation_dto['coordinates'][0]  # ovde ne valja

                print("stations")
                print(self.stations)
                self.visited_stations = []
                self.get_new_coordinates(10)

            else:
                self.is_null = False
                self.driving_to_start_point = False
                self.driving_the_route = True
                self.end_route = False
                self.end_to_start_point = False
             #   self.departure = self.simulation_dto['stations'][0] # ovde stavi adresu vozaca da bi se samo nastavilo
                self.departure = {"lng": self.simulation_dto['simulationVehicleDTO']['latitude'],
                                  "lat": self.simulation_dto['simulationVehicleDTO']['longitude']}
                self.destination = self.simulation_dto['stations'][1]
                self.visited_stations = []
                self.visited_stations.append(0)

                self.get_new_coordinates(randrange(1, 11))

        except:
            self.is_null = True
            self.driver_id = driver_id

        print(" LOAD ")
        print(driver_id)

    def get_state_for_driving(self):
        self.simulation_dto = self.client.get('/simulation/state/' + str(self.driving_id), verify=False, headers={
            "Content-Type": "application/json"
        }).json()
        self.state = self.simulation_dto['drivingState']
        self.is_called = True
