INSERT INTO CITY (NAME, POST_NUMBER) VALUES ('Novi Sad', 21000);

INSERT INTO role (name) VALUES ('ROLE_ADMIN');
INSERT INTO role (name) VALUES ('ROLE_REGISTERED_USER');
INSERT INTO role (name) VALUES ('ROLE_DRIVER');


------ RU 1 --------- 123    id 1
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Marko', 'Markovic', 'evioletta121@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'mm', '+381246548', false, true, false, 0, 'bmv-back\src\main\resources\static\photos\registered_user_1\ocean-3605547_1920.jpg', 5000, 0);
insert into user_role (user_id, role_id) values (1, 2);

------ RU 2 --------- 123    id 2
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, verification_code, tokens, version)
values ('RU', 'Ivan', 'Ivancevic', 'ev@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'ii', '+381246548', false, true, false, 0, '7jps0ncytrwyqCGmvDAUlcRXII8fsAlLY6o9ITnAaWvdq1NauNiiYhDnYKbnR79m', 5000, 0);
insert into user_role (user_id, role_id) values (2, 2);

------ A 1 --------- 123     id 3
insert into users (type, name, surname, email, password, username, phone, blocked, version)
values ('A', 'Jovan', 'Jovanovic', 'jj@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'jj', '+381246548', false, 0);
insert into user_role (user_id, role_id) values (3, 1);


-- adresses
INSERT INTO address (longitude, latitude) values(45.239467, 19.822798);  -- 1 Jevrs
INSERT INTO address (longitude, latitude) values(45.254550, 19.842580);  -- 2
INSERT INTO address (longitude, latitude) values(45.254400, 19.842490);  -- 3
INSERT INTO address (longitude, latitude) values(45.254400, 19.842490);  -- 4
INSERT INTO address (longitude, latitude) values(45.2561725, 19.8467566);  -- 5
INSERT INTO address (longitude, latitude) values(45.2561725, 19.8467566);  -- 6
INSERT INTO address (longitude, latitude) values(45.24638, 19.8342);  -- 7
INSERT INTO address (longitude, latitude) values(45.258800, 19.849280);  -- 8
INSERT INTO address (longitude, latitude) values(45.247220, 19.841160);  -- 9
INSERT INTO address (longitude, latitude) values(45.247220, 19.841160);  -- 10
INSERT INTO address (longitude, latitude) values(45.240879, 19.828400);  -- 11
INSERT INTO address (longitude, latitude) values(45.240879, 19.828400);  -- 12
INSERT INTO address (longitude, latitude) values(45.240879, 19.828400);  -- 13 -- D1
INSERT INTO address (longitude, latitude) values(45.247220, 19.841160);  -- 14 -- D1


--type vehicle
INSERT INTO vehicle_type (name, price) values ('karavan', 120);
INSERT INTO vehicle_type (name, price) values ('dzip', 200);
INSERT INTO vehicle_type (name, price) values ('terenski', 300);

--vehicle 1
INSERT INTO vehicle(type_id, name, label, year_of_production, color, baby_friendly, pet_friendly, number_of_seats) values
(1, 'Reno', 'NS 021 FT', 2010, 'silver', true, false, 2);

--vehicle 2
INSERT INTO vehicle(type_id, name, label, year_of_production, color, baby_friendly, pet_friendly, number_of_seats) values
(1, 'Tojota', 'NS 022 TO', 2014, 'black', true, true, 3);

-- vehicle 3
INSERT INTO vehicle(type_id, name, label, year_of_production, color, baby_friendly, pet_friendly, number_of_seats) values
(1, 'Golf', 'NS 333 FT', 2010, 'gold', true, false, 2);

-- vehicle 4
INSERT INTO vehicle(type_id, name, label, year_of_production, color, baby_friendly, pet_friendly, number_of_seats) values
(1, 'BMW', 'NS 015 PM', 2003, 'blue', false, false, 3);

-- vehicle 5
INSERT INTO vehicle(type_id, name, label, year_of_production, color, baby_friendly, pet_friendly, number_of_seats) values
(1, 'Skoda', 'NS 030 AS', 2007, 'red', false, true, 2);

-- vehicle 6
INSERT INTO vehicle(type_id, name, label, year_of_production, color, baby_friendly, pet_friendly, number_of_seats) values
(1, 'Fiat', 'NS 040 WQ', 2008, 'white', true, true, 2);

-- vehicle 7
INSERT INTO vehicle(type_id, name, label, year_of_production, color, baby_friendly, pet_friendly, number_of_seats) values
(1, 'Mini moris', 'NS 041 JK', 2010, 'rose', true, true, 2);


------ D 1 --------- 123   id 4
insert into users (type, name, surname, email, password, username, phone, blocked, driver_state, vehicle, current_address, version) values
('D', 'Lazar', 'Lazarevic', 'll@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'll', '+381246548', false, 1, 3, 13, 0);
insert into user_role (user_id, role_id) values (4, 3);
insert into time_period (start_date_time, end_date_time, active_hour, estimated) values
(TIMESTAMPADD('MINUTE', -60, NOW()), null, 4, false);   -- 1


------ D 2  ------ 123     id 5
insert into users (type, name, surname, email, password, username, phone, blocked, driver_state, vehicle, current_address, version) values
 ('D', 'Olivera', 'Oliverovic', 'oo@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'oo', '+381246548', false, 0, 1, 1, 0);
insert into user_role (user_id, role_id) values (5, 3);
insert into time_period (start_date_time, active_hour, estimated) values
 (TIMESTAMPADD('MINUTE', -120, NOW()), 5, false);   -- 2


------- D 3 ------ 123     id 6
insert into users (type, name, surname, email, password, username, phone, blocked, driver_state, vehicle, current_address, version) values
 ('D', 'Petar', 'Petrovic', 'pp@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'pp', '+381246748', false, 1, 2, 3, 0);
insert into user_role (user_id, role_id) values (6, 3);
insert into time_period (start_date_time, active_hour, estimated) values
 (TIMESTAMPADD('MINUTE', -30, NOW()), 6, false);   -- 3


------ A 2 ------- 123     id 7
insert into users (type, name, surname, email, password, username, phone, blocked, version)
values ('A', 'Peda', 'Petrovic', 'pp@ex.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'peda', '+381246548', false, 0);
insert into user_role (user_id, role_id) values (7, 1);

------ RU 3 ------ 123     id 8
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, tokens, version)
values ('RU', 'Jovana', 'Jovanovic', 'jj@ex.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'jovi', '+381246548', false, true,false, 0, 5000,0);
insert into user_role (user_id, role_id) values (8, 2);


------ D4 ------   123     id 9
insert into users (type, name, surname, email, password, username, phone, blocked, driver_state, vehicle, current_address, version) values
 ('D', 'Vasilije', 'Vasiljevic', 'vv@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'vv', '+381246748', false, 0, 4, 5, 0);
insert into user_role (user_id, role_id) values (9, 3);
insert into time_period (start_date_time, active_hour, estimated) values
 (TIMESTAMPADD('MINUTE', -180, NOW()), 9, false);   -- 4


------ D5 ------   123     id 10
insert into users (type, name, surname, email, password, username, phone, blocked, driver_state, vehicle, current_address, version) values
 ('D', 'Dusan', 'Dusanic', 'dd@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'dd', '+381246748', false, 0, 5, 7, 0);
insert into user_role (user_id, role_id) values (10, 3);
insert into time_period (start_date_time, active_hour, estimated) values
 (TIMESTAMPADD('MINUTE', -10, NOW()), 10, false);   -- 5


------ D6 ------   123     id 11
insert into users (type, name, surname, email, password, username, phone, blocked, driver_state, vehicle, current_address, version) values
 ('D', 'Nenad', 'Nenadic', 'nn@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'nn', '+381246748', false, 1, 6, 9, 0);
insert into user_role (user_id, role_id) values (11, 3);
insert into time_period (start_date_time, end_date_time, active_hour, estimated) values
 (PARSEDATETIME('2023-01-03 03:00:00','yyyy-MM-dd HH:mm:ss'), (PARSEDATETIME('2023-01-03 03:29:00','yyyy-MM-dd HH:mm:ss')), 11, false);   -- 6
insert into time_period (start_date_time, active_hour, estimated) values
 (TIMESTAMPADD('MINUTE', -20, NOW()), 11, false);   -- 7


 ------ D7 ------- 123 id 12
 insert into users (type, name, surname, email, password, username, phone, blocked, driver_state, vehicle, current_address, version) values
 ('D', 'Aleksa', 'Aleksic', 'aa@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'aa','+381246748', false, 2, 7, 11,0);
 insert into user_role (user_id, role_id) values (12, 3);


------ RU 4 ------ 123     id 13
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, tokens, version)
values ('RU', 'Dzon', 'Dzon', 'dz@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'dzoni', '+381246548', false, true,false, 0, 0, 0);
insert into user_role (user_id, role_id) values (13, 2);

------ RU 5 ------ 123     id 14
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, tokens, version)
values ('RU', 'Ted', 'Mosby', 'ted@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'ted', '+381146548', false, true,false, 0, 5000, 0);
insert into user_role (user_id, role_id) values (14, 2);


------ RU 6 ------ 123     id 15
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, tokens, version)
values ('RU', 'Lili', 'Lili', 'lili@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'lili', '+381246548', false, true,false, 0, 5000, 0);
insert into user_role (user_id, role_id) values (15, 2);


------ RU 7 ------ 123     id 16
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, tokens, version)
values ('RU', 'Robin', 'Robin', 'robin@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'robin', '+381246548', false, true,false, 0, 5000, 0);
insert into user_role (user_id, role_id) values (16, 2);

------ RU 8 ------ 123     id 17
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, tokens, version)
values ('RU', 'Marsal', 'Marsal', 'marsal@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'marsal', '+381246548', false, true,false, 0, 5000, 0);
insert into user_role (user_id, role_id) values (17, 2);

------ RU 9 --------- 123    id 18
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, verification_code, tokens, version)
values ('RU', 'Gavro', 'Gavric', 'gg@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'gg', '+381246548', false, false, false, 0, '7jps0ncytrwyqCGmvDAUlcRXII8fsAlLY6o9ITnAaWvdq1NauNiiYhDnYKbnR79m', 0, 0);
insert into user_role (user_id, role_id) values (18, 2);


----- driving 1  -- paid
insert into time_period (start_date_time, end_date_time, estimated)   -- 8
values (TIMESTAMPADD('DAY', -1, NOW()), TIMESTAMPADD('DAY', -0.9, NOW()), true);
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (true, true, 1);
insert into route (distance, time) values (15, 15);
insert into path (route) values (1);
insert into address (longitude, latitude, name, path_id) values (45.246479, 19.834240,'Gogoljeva 22 Novi Sad', 1); --15
insert into address (longitude, latitude, name, path_id) values (45.264370, 19.822000,'Bulevar Jase Tomica 12 Novi Sad', 1); --16
insert into address (longitude, latitude, route_id) values (45.264370, 19.822000, 1); --17

insert into driving (driver_id, price, state, driving_time_id, order_time, chosen_additional_services_id, path_id, future, notification_sent)
values (4, 800, 6, 8, TIMESTAMPADD('DAY', -1, NOW()), 1, 1, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (400, true, 1, 18, true);


----- driving 2  -- current
insert into time_period (start_date_time, end_date_time, estimated)   -- 9
values (TIMESTAMPADD('DAY', -2, NOW()), TIMESTAMPADD('DAY', -1.8, NOW()), true);
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (true, false, 1); --2
insert into route (distance, time) values (1, 1);
insert into path (route) values (2);
insert into address (longitude, latitude, name, path_id) values (45.246479, 19.834240,'Gogoljeva 22 Novi Sad', 2); --18
insert into address (longitude, latitude, name, path_id) values (45.264370, 19.822000,'Bulevar Jase Tomica 12 Novi Sad', 2); --19
insert into address (longitude, latitude, route_id) values (45.264370, 19.822000, 2); --20

insert into driving (driver_id, price, state, driving_time_id, order_time, chosen_additional_services_id, path_id, future, notification_sent)
values (6, 520, 2, 9, TIMESTAMPADD('DAY', -2.01, NOW()), 2,2, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (520, true, 2, 2, true);



------ driving 3  -- za njega trazim drivera  made
insert into time_period (start_date_time, end_date_time, estimated)  -- 10
values (TIMESTAMPADD('DAY', -7, NOW()), TIMESTAMPADD('DAY', -6.9, NOW()), true);
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, true, 1); --3
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (3);
insert into address (longitude, latitude, name, path_id) values (45.246479, 19.834240,'Gogoljeva 22 Novi Sad', 3);--21
insert into address (longitude, latitude, name, path_id) values (45.264370, 19.822000,'Bulevar Jase Tomica 12 Novi Sad', 3);--22
insert into address (longitude, latitude, route_id) values (45.264370, 19.822000, 3);--23
insert into driving (price, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (550, 0, 10, TIMESTAMPADD('DAY', -7, NOW()), 3, 3, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (260, true, 3, 1, true);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (260, true, 3, 2, true);
-- insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (260, true, 3, 3, true);    -- za testiranje broja sedista


----- driving 4   -- made
insert into time_period (start_date_time, end_date_time, estimated)  -- 11
values (TIMESTAMPADD('DAY', -5, NOW()), TIMESTAMPADD('DAY', -5, NOW()), true);  -- 11
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (4);
insert into address (longitude, latitude, name, path_id) values (45.2464547, 19.8342626,'Gogoljeva 22 Novi Sad', 4);--24
insert into address (longitude, latitude, name, path_id) values (45.245906, 19.8353987,'Gogoljeva 52 Novi Sad', 4);--25
insert into address (longitude, latitude, route_id) values (45.24638, 19.8342, 4);--26
insert into address (longitude, latitude, route_id) values (45.24611, 19.8348, 4);--27
insert into address (longitude, latitude, route_id) values (45.24591, 19.8354, 4);--28
insert into address (longitude, latitude, route_id) values (45.24591, 19.8354, 4);--29
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, true, 1); --4
insert into driving (price, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (550, 0, 10, TIMESTAMPADD('DAY', -4.9, NOW()), 4, 4, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (260, false, 4, 1, true);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (260, false, 4, 2, true);
-- insert into driving_payment (price, paid,driving_id, client_id) values (260, true, 3, 3);    -- za testiranje broja sedista


------ driving 5  -- paid
insert into time_period (start_date_time, end_date_time, estimated)   -- 12
values (TIMESTAMPADD('DAY', -3, NOW()), TIMESTAMPADD('DAY', -3, NOW()), true);
insert into route (distance, time) values (10.31, 10);
insert into path (route) values (5);
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, false, 1);
insert into address (longitude, latitude, name, path_id) values (45.2537745, 19.8411572,'Jevrejska 10 Novi Sad', 5);--30
insert into address (longitude, latitude, name, path_id) values (45.2531858, 19.8393187,'Jevrejska 26 Novi Sad', 5);--31


insert into address (longitude, latitude, route_id) values (45.25373, 19.8412, 5);--32
insert into address (longitude, latitude, route_id) values (45.25361, 19.84094, 5);--33
insert into address (longitude, latitude, route_id) values (45.25358, 19.84085, 5);--34
insert into address (longitude, latitude, route_id) values (45.25336, 19.8403, 5);--35
insert into address (longitude, latitude, route_id) values (45.25315, 19.83978, 5);--36
insert into address (longitude, latitude, route_id) values (45.25302, 19.83945, 5);--37
insert into address (longitude, latitude, route_id) values (45.25302, 19.83945, 5);--38


insert into driving (driver_id, price, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (11, 900, 6, 12, TIMESTAMPADD('DAY', -2.9, NOW()), 5, 5, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (900, true, 5, 1, true);

--- warning note
insert into warning_note (text, admin_id, user_id) values ('hoppa hoppa', 3, 1);




------------------------------------------------ Driving history -------------------------------------
----- driving 6  -- successful
insert into time_period (start_date_time, end_date_time, estimated) --13
values (PARSEDATETIME('2022-11-03 18:57:58','yyyy-MM-dd HH:mm:ss'), PARSEDATETIME('2022-11-03 19:57:58', 'yyyy-MM-dd HH:mm:ss'), false);
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (true, true, 1);--6
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (6);
insert into address (longitude, latitude, name, path_id) values (45.2464547, 19.8342626,'Gogoljeva 22 Novi Sad', 6);--
insert into address (longitude, latitude, name, path_id) values (45.245906, 19.8353987,'Gogoljeva 52 Novi Sad', 6);--
insert into driving (driver_id, price, state, driving_time_id, order_time, chosen_additional_services_id, path_id, future, notification_sent)
values (4, 800, 4, 13, PARSEDATETIME('2022-11-03 18:40:58','yyyy-MM-dd HH:mm:ss'), 6, 6, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (400, true, 6, 1, true);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (400, true, 6, 2, true);
insert into notification (date, type, driving_id, client_id) values
    (PARSEDATETIME('2022-11-03 18:40:58','yyyy-MM-dd HH:mm:ss'), 0, 1, 2);
insert into rating (driver_rating, text, vehicle_rating, client_id, driving_id) values (4, 'Okeeej', 5, 1, 6);
--insert into report (client_id, driver_id, driving_id) values (1, 4, 1);

----- driving 7  -- successful
insert into time_period (start_date_time, end_date_time, estimated) --14
values (PARSEDATETIME('2022-01-03 15:02:58','yyyy-MM-dd HH:mm:ss'), PARSEDATETIME('2022-01-03 15:20:58', 'yyyy-MM-dd HH:mm:ss'), false);
insert into route (distance, time) values (10, 9);
insert into path (route) values (7);
insert into address (longitude, latitude, name, path_id) values (45.2464547, 19.8342626,'Gogoljeva 22 Novi Sad', 7);--
insert into address (longitude, latitude, name, path_id) values (45.245906, 19.8353987,'Gogoljeva 52 Novi Sad', 7);--
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (true, false, 1);--7
insert into driving (driver_id, price, state, driving_time_id, order_time, chosen_additional_services_id, path_id, future, notification_sent)
values (6, 520, 4, 14, PARSEDATETIME('2022-01-03 14:57:58','yyyy-MM-dd HH:mm:ss'), 7, 7, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (520, true, 7, 1, true);
insert into rating (driver_rating, text, vehicle_rating, client_id, driving_id) values (3, 'huh', 4, 1, 7);


------ driving 8  -- rejected
insert into time_period (start_date_time, end_date_time, estimated) --15
values (PARSEDATETIME('2022-04-03 15:16:58','yyyy-MM-dd HH:mm:ss'), PARSEDATETIME('2022-04-03 15:34:58', 'yyyy-MM-dd HH:mm:ss'), true);
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, true, 1);--8
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (8);--8
insert into address (longitude, latitude, name, path_id) values (45.246479, 19.834240,'Gogoljeva 22 Novi Sad', 8);
insert into address (longitude, latitude, name, path_id) values (45.264370, 19.822000,'Bulevar Jase Tomica 12 Novi Sad', 8);
insert into address (longitude, latitude, route_id) values (45.264370, 19.822000, 8);
insert into driving (driver_id, price, state, driving_time_id, order_time, path_id, chosen_additional_services_id, rejection_text, future, notification_sent)
values (4, 550, 3, 14, PARSEDATETIME('2022-04-03 12:22:58','yyyy-MM-dd HH:mm:ss'), 8, 8, 'ne moguuuu', false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (260, false, 8, 1, true);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (260, false, 8, 2, true);


----- driving 9   -- refused
insert into time_period (start_date_time, end_date_time, estimated)
values (PARSEDATETIME('2022-01-03 15:04:58','yyyy-MM-dd HH:mm:ss'), PARSEDATETIME('2022-01-03 15:24:58', 'yyyy-MM-dd HH:mm:ss'), true);
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, true, 1);
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (9);--9
insert into address (longitude, latitude, name, path_id) values (45.246479, 19.834240,'Gogoljeva 22 Novi Sad', 9);
insert into address (longitude, latitude, name, path_id) values (45.264370, 19.822000,'Bulevar Jase Tomica 12 Novi Sad', 9);
insert into address (longitude, latitude, route_id) values (45.264370, 19.822000, 9);
insert into driving (price, state, driving_time_id, order_time, chosen_additional_services_id, path_id, future, notification_sent)
values (630, 5, 15, PARSEDATETIME('2022-01-03 10:22:00','yyyy-MM-dd HH:mm:ss'), 9, 9, false, 0);
insert into driving_payment (price, paid,driving_id, client_id,confirmed) values (630, false, 9, 1, true);



------- driving 10 --- future
insert into time_period (start_date_time, end_date_time, estimated)   -- 17
values (TIMESTAMPADD('DAY', -2, NOW()), TIMESTAMPADD('DAY', -2, NOW()), true);
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, false, 1);
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (10);--10
insert into address (longitude, latitude, name, path_id) values (45.246479, 19.834240,'Gogoljeva 22 Novi Sad', 10);
insert into address (longitude, latitude, name, path_id) values (45.264370, 19.822000,'Bulevar Jase Tomica 12 Novi Sad', 10);
insert into address (longitude, latitude, route_id) values (45.264370, 19.822000, 10);
insert into driving (price, state, driving_time_id, order_time, chosen_additional_services_id, path_id, future, notification_sent)
values (630, 5, 17, TIMESTAMPADD('DAY', -2, NOW()), 10, 10, true, 0);
insert into driving_payment (price, paid,driving_id, client_id,confirmed) values (630, false, 10, 2, true);


--------------- Messages ---------------
insert into message (text, date_time, sender_id, receiver_id) values
    ('Moj prvi tekst', PARSEDATETIME('2022-12-03 18:57:58','yyyy-MM-dd HH:mm:ss'), 1, null);
insert into message (text, date_time, sender_id, receiver_id) values
    ('Drugi user', PARSEDATETIME('2022-12-05 18:57:58','yyyy-MM-dd HH:mm:ss'), 2, null);
insert into message (text, date_time, sender_id, receiver_id) values
    ('Admin answers', PARSEDATETIME('2022-12-03 20:57:58','yyyy-MM-dd HH:mm:ss'), 3, 1);


insert into notification (date, type, driving_id, client_id) values
    (PARSEDATETIME('2022-12-28 15:57:58','yyyy-MM-dd HH:mm:ss'), 1, 1, 1);

insert into notification (date, type, driving_id, client_id) values
    (PARSEDATETIME('2022-12-28 16:20:58','yyyy-MM-dd HH:mm:ss'), 2, 1, 1);


-------  favourite route  -----
insert into favourite_route (client_id, route_id) values (1, 5);

--------------------------- end driving e2e ------------------
------ RU 10 --------- 123    id 19
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, verification_code, tokens, version)
values ('RU', 'Jamie', 'Fraser', 'jamie@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'jamie', '+381246548', false, true, false, 0, '7jps0ncytrwyqCGmvDAUlcRXII8fsAlLY6o9ITnAaWvdq1NauNiiYhDnYKbnR79m', 5000, 0);
insert into user_role (user_id, role_id) values (19, 2);

------ RU 11 --------- 123    id 20
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, verification_code, tokens, version)
values ('RU', 'Olivia', 'Fraser', 'olivia@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'olivia', '+381246548', false, true, false, 0, '7jps0ncytrwyqCGmvDAUlcRXII8fsAlLY6o9ITnAaWvdq1NauNiiYhDnYKbnR79m', 5000, 0);
insert into user_role (user_id, role_id) values (20, 2);

------ driving   -- paid
insert into time_period (start_date_time, end_date_time, estimated)   -- 18
values (TIMESTAMPADD('MINUTE', 0, NOW()), TIMESTAMPADD('MINUTE', 10, NOW()), true);
insert into route (distance, time) values (10.31, 10); --11
insert into path (route) values (11);
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, false, 1);
insert into address (longitude, latitude, name, path_id) values (45.2381685, 19.8169544,'Cirila i Metodija, 11, Telep, Novi Sad, Vojvodina, Serbia,', 11);--30
insert into address (longitude, latitude, name, path_id) values (45.2390038	, 19.820394,'Cirila i Metodija, 20, Telep, Novi Sad, Vojvodina, Serbia,	', 11);--31


--insert into address (longitude, latitude, route_id) values (45.23824, 19.81693, 11);--32
insert into address (longitude, latitude, route_id) values (45.23825, 19.81693, 11);--33
insert into address (longitude, latitude, route_id) values (45.23845, 19.81791, 11);--34
insert into address (longitude, latitude, route_id) values (45.23855, 19.81838, 11);--35
insert into address (longitude, latitude, route_id) values (45.23864, 19.81882, 11);--36
insert into address (longitude, latitude, route_id) values (45.23876, 19.81945, 11);--37
insert into address (longitude, latitude, route_id) values (45.23891, 19.82042, 11);--38
insert into address (longitude, latitude, route_id) values (45.23891, 19.82042, 11);--38

insert into driving (driver_id, price, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (5, 300, 6, 18, TIMESTAMPADD('MINUTE', -10, NOW()), 11, 11, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (300, true, 11, 19, true);

insert into favourite_route (client_id, route_id) values (19, 5);


insert into driving (driver_id, price, state, driving_time_id, order_time, chosen_additional_services_id, path_id, future, notification_sent)
values (4, 800, 6, 8, TIMESTAMPADD('MINUTE', -5, NOW()), 1, 1, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (800, true, 12, 20, true);

insert into notification (date, type, driving_id, client_id) values
    (TIMESTAMPADD('MINUTE', -5, NOW()), 6, 12, 20);