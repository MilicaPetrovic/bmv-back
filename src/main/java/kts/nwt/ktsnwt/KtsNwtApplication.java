package kts.nwt.ktsnwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.File;
import java.io.IOException;
@EnableScheduling
@SpringBootApplication
public class KtsNwtApplication {

	public static String IMAGE_DIR;
	public static void main(String[] args) throws IOException {
		IMAGE_DIR = new File(".").getCanonicalPath() + "\\src\\main\\resources\\static\\photos\\";
		SpringApplication.run(KtsNwtApplication.class, args);
	}

}
