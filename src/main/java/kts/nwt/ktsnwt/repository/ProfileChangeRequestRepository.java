package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.requests.ProfileChangeRequest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProfileChangeRequestRepository extends JpaRepository<ProfileChangeRequest, Long> {

    @Query("select u from ProfileChangeRequest u where u.done = false")
    List<ProfileChangeRequest> findAll(PageRequest of);

    @Query("select count(u) from ProfileChangeRequest u where u.done = false")
    int getProfileChangeRequestCount();
}
