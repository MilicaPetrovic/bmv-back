package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.feedback.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {
    List<Report> findByDriving_Id(Long id);

}
