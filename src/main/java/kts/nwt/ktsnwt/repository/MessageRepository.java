package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.communication.Message;
import kts.nwt.ktsnwt.beans.users.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
    @Query("select distinct m.sender from Message m where m.receiver is null")// join fetch m.sender
    List<User> findDistinctUserByReceiverAdmin();

    @Query("select m from Message m where m.sender.username = ?1")//or (m.receiver != null and m.receiver.username = ?1)
    List<Message> getMessagesByUserIsSender(String username);

    @Query("select m from Message m where m.receiver.username = ?1")
    List<Message> getMessagesByUserIsReceiver(String username);
}
