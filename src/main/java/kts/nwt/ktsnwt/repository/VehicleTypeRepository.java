package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.driving.VehicleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VehicleTypeRepository extends JpaRepository<VehicleType, Long> {
    Optional<VehicleType> findByIdIs(Long id);

}
