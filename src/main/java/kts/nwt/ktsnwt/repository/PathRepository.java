package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.location.Path;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PathRepository extends JpaRepository<Path, Long> {

}
