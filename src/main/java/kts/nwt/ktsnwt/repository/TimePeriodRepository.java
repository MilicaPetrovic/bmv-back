package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.driving.TimePeriod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimePeriodRepository extends JpaRepository<TimePeriod, Long> {

}
