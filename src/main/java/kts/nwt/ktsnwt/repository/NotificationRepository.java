package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.communication.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<Notification,Long> {

    @Query("select n from Notification n where n.client.username = ?1")
    List<Notification> findByClient_Username(String username);

    @Query("select n from Notification n where n.client.username = ?1 and n.date >= ?2 order by n.date desc")
    List<Notification> findByClient_UsernameAndDateIsGreaterThanEqual(String username, LocalDateTime date);

    @Query("select n from Notification n where n.driving.driver.username = ?1 and n.date >= ?2 and n.type = 6 order by n.date desc")
    List<Notification> findByDriver_UsernameAndDateIsGreaterThanEqual(String username, LocalDateTime date);
}
