package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.driving.TimePeriod;
import kts.nwt.ktsnwt.beans.driving.Vehicle;
import kts.nwt.ktsnwt.beans.users.Driver;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;
import java.util.List;
import java.util.Optional;

@Repository
public interface DriverRepository extends JpaRepository<Driver, Long> {
    @Query("select u from User u where u.role.id = 3 and u.driverState=0 and u.blocked=false")
    List<Driver> getActiveDrivers();


    @Lock(LockModeType.PESSIMISTIC_READ)
    @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value ="2")})
    @Query("select u from User u where u.role.id = 3 and u.driverState=1 and blocked = false")
    List<Driver> getDrivingDrivers();

    @Query("select u from User u where u.role.id = 3 and u.driverState=2 and blocked = false")
    List<Driver> getInactiveDrivers();

    @Query("select count(u) from Driver u")
    int getDriversCount();

    @Query("select u from Driver u")
    List<Driver> findAll(PageRequest of);

    //@Query("select d from Driver d where d.username = ?1")
    //Driver findByUsername(String username);

    @Query("select d.vehicle from Driver d where d.username = ?1")
    Vehicle findVehicleByUsername(String username);

    Optional<Driver> findByUsername(String username);
    @Query("select u from Driver u where u.id=?1")
    Optional<Driver> findDriverById(Long id);

    @Query("select d.activeHours from Driver d where d.username = ?1")
    List<TimePeriod> findWorkingPeriodsByUsername(String username);


}
