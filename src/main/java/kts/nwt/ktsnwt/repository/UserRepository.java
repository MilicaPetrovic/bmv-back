package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;



@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    User findByEmail(String email);


    @Query("select u from User u where u.id = ?1 and u.role.id = ?2")
    Driver findByDriverId(Long id, long driverRole);


    @Query("select u from User u inner join fetch u.role where u.id = ?1 ")
    Optional<User> findById(Long id);

    List<User> findByRole_Id(Long id);

    @Query("select u.username from User u where u.role.id = 2")
    List<String> findAllRegisteredUsers();

    @Query("select u from User u where u.username = ?1 and u.blocked = false")
    Optional<User> findByUsernameNotBlocked(String username);

}
