package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.feedback.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {
    Optional<Rating> findByClient_IdAndDriving_Id(Long id, Long id1);

    @Query("select Avg(r.driverRating) from Rating r where r.driving.driver.id = ?1")
    Optional<Double> getRatingOfDriver(Long id);


    @Query("select Avg(r.vehicleRating) from Rating r where r.driving.driver.id = ?1")
    Optional<Double> getRatingOfVehicle(long driverId);
}
