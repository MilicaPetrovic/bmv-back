package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.driving.ChosenAdditionalServices;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChosenAdditionalServicesRepository extends JpaRepository<ChosenAdditionalServices, Long> {
}
