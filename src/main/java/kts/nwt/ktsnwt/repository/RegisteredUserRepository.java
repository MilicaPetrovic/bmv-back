package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RegisteredUserRepository extends JpaRepository<RegisteredUser, Long> {
    Optional<RegisteredUser> findByUsername(String username);

    @Query("select r from RegisteredUser r where r.activated = true")
    List<RegisteredUser> findByIfActivated(PageRequest of);

    @Query("select count(r) from RegisteredUser r where r.activated = ?1")
    long countByActivated(boolean activated);

    //List<RegisteredUser> findByActivatedIs(boolean activated, PageRequest of);

    List<RegisteredUser> findByActivated(boolean activated, Pageable pageable);

}
