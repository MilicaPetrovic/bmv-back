package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.driving.DrivingPayment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DrivingPaymentRepository extends JpaRepository<DrivingPayment, Long> {

}
