package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.location.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RouteRepository extends JpaRepository<Route, Long> {

}
