package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.feedback.WarningNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WarningNoteRepository extends JpaRepository<WarningNote, Long> {
    @Query("select w from WarningNote w join fetch w.admin where w.user.id = ?1")
    List<WarningNote> findByUserIdWithAdmin(Long username);


}
