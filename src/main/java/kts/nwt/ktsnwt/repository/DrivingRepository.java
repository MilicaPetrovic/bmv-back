package kts.nwt.ktsnwt.repository;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.driving.DrivingPayment;
import kts.nwt.ktsnwt.beans.driving.DrivingState;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface DrivingRepository extends JpaRepository<Driving, Long> {

    @Query("select d from Driving d inner join fetch d.passengers passengers where passengers.registeredUser.id = ?1 order by d.orderTime desc")
    List<Driving> getDrivingsByClient(Long id, PageRequest pageRequest);

    @Query("select d from Driving d inner join fetch d.passengers passengers where passengers.registeredUser.id = ?1")
    List<Driving> getDrivingsByClientPaged(Long id, PageRequest of);

    @Query("select d from Driving d where d.driver.username = ?1")
    List<Driving> getDrivingsByDriver(String username, PageRequest of);

    @Query("select d from Driving d where d.driver.username = ?1")
    List<Driving> getDrivingsByDriver(String username);

    @Query("select d from Driving d inner join fetch d.passengers passengers where d.id = ?1")
    Optional<Driving> getDrivingWithPassengers(Long drivingId);

    @Query("select count(d) from Driving d inner join d.passengers passengers where passengers.registeredUser.username = ?1")
    int getCountOfDrivingsByClient(String username);

    @Query("select count(d) from Driving d where d.driver.username = ?1")
    int getCountOfDrivingsByDriver(String username);

    @Query("select d from Driving d where d.driver.id = ?1")
    List<Driving> getDrivingsByDriverPaged(Long id, PageRequest of);

    Optional<Driving> findDrivingById(Long id);

    @Query("select d.passengers from Driving d where d.id = ?1")
    List<DrivingPayment> findUsersByDrivingId(long drivingId);

    @Query("select d from Driving d where d.state = 2")
    List<Driving> getCurrentDrivings();

    @Query("select d from Driving d inner join fetch d.passengers passengers where passengers.registeredUser.username = ?1 and (d.state = 0 or d.state = 1 or d.state = 2 or d.state = 6) order by d.orderTime")
    List<Driving> getActiveDrivingsByClientsUsername(String id);


    @Query("select d from Driving d inner join fetch d.driver driver where driver.username = ?1 and (d.state = 0 or d.state = 1 or d.state = 2)")
    List<Driving> getActiveDrivingsByDriversUsername(String username);

    @Query("select d from Driving d inner join fetch d.passengers passengers where passengers.registeredUser.username = ?1")
    List<Driving> getDrivingsByClientsUsername(String id);


    @Query("select d from Driving d where d.state = 1")
    List<Driving> getAcceptedDriving();

    @Query("select d from Driving d where d.driver.id = ?1 and (d.state = 1 or d.state = 2)")
    Driving getAcceptedOrCurrentDrivingByDriverId(long driverId);


    @Query("""
            select d from Driving d inner join d.passengers passengers
            where passengers.registeredUser.username = ?1 and d.state = ?2 and d.drivingTime.startDateTime >= ?3 and d.drivingTime.startDateTime <= ?4
            order by d.orderTime""")
    List<Driving> getDrivingsByClientsUsernameInPeriod(String username, DrivingState drivingState, LocalDateTime startDateTime, LocalDateTime endDateTime);


    @Query("""
            select d from Driving d
            where d.driver.username = ?1 and d.state = ?2 and d.drivingTime.startDateTime >= ?3 and d.drivingTime.startDateTime <= ?4
            order by d.orderTime""")
    List<Driving> getDrivingsByDriverInPeriod(String username, DrivingState successful, LocalDateTime atStartOfDay, LocalDateTime atStartOfDay1);


    @Query("select d from Driving d where d.driver.id = ?1 and (d.state = 6 or d.state = 2)")
    Optional<Driving> getPaidOrCurrentDrivingByDriverId(Long driverId);

    @Query("""
            select d from Driving d
            where d.state = ?1 and d.drivingTime.startDateTime > ?2 and d.drivingTime.startDateTime < ?3
            order by d.orderTime""")
    List<Driving> getDrivingsByInPeriod(DrivingState successful, LocalDateTime atStartOfDay, LocalDateTime atStartOfDay1);

    @Query("select d from Driving d where d.future = ?1 and d.driver is null and d.drivingTime.startDateTime > ?2 and d.drivingTime.startDateTime < ?3")
    List<Driving> getFutureDrivingFromPeriodWithoutDriver(boolean future, LocalDateTime start, LocalDateTime end);

    @Query("select d from Driving d where d.future = ?1 and d.drivingTime.startDateTime > ?2 and d.drivingTime.startDateTime < ?3 and d.state != 5")
    List<Driving> getFutureDrivingFromPeriod(boolean b, LocalDateTime start, LocalDateTime end);

    @Query("select d from Driving d where d.driver.id = ?1 order by d.orderTime")
    List<Driving> getDrivingsByDriverId(Long driverId);
}
