package kts.nwt.ktsnwt.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import kts.nwt.ktsnwt.auth.RestAuthenticationEntryPoint;
import kts.nwt.ktsnwt.auth.TokenAuthenticationFilter;
import kts.nwt.ktsnwt.auth.TokenUtils;
import kts.nwt.ktsnwt.service.security.UberUserDetailsService;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private UberUserDetailsService userService;

    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userService)
                .passwordEncoder(new BCryptPasswordEncoder());
    }

    @Autowired
    private TokenUtils tokenUtils;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()

                .exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint).and()

                .authorizeRequests()
                .antMatchers("/resources/**").permitAll()

                .antMatchers("/uber").permitAll()
                .antMatchers("/reset/password/**").permitAll()
                //.antMatchers("/driving/**").permitAll()//skloniti posle logina

                .antMatchers("/driver/**").permitAll()
                .antMatchers("/get/driver").permitAll() // skloniti posle

                .antMatchers("/h2/**").permitAll()

                .antMatchers("/auth/register").permitAll()

                //.antMatchers("/notify").permitAll()
                .antMatchers("/socket").permitAll()
                .antMatchers("/socket/**").permitAll()

                .antMatchers("/verify/**").permitAll()
                .antMatchers("/auth/login").permitAll()
                .antMatchers("/auth/socialLogin").permitAll()
                .antMatchers("/auth/logout").permitAll()

                //funkcije u kontoleru
                .antMatchers("/simulation/**").permitAll()
                .antMatchers("/vehicle/type/**").permitAll()

                .anyRequest().authenticated().and()

                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and()

                .cors().and()
                .addFilterBefore(new TokenAuthenticationFilter(tokenUtils, userService), BasicAuthenticationFilter.class);

        // zbog jednostavnosti primera ne koristimo Anti-CSRF token (https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.html)
        http.csrf().disable();
        http.headers().frameOptions().disable();
    }
}
