package kts.nwt.ktsnwt.controller.feedback;

import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.dto.feedback.ClientDrivingRatingDTO;
import kts.nwt.ktsnwt.exception.*;
import kts.nwt.ktsnwt.service.feedback.interfaces.ClientDrivingRatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/client/driving")
public class ClientDrivingRatingController {

    @Autowired
    private ClientDrivingRatingService clientDrivingRatingService;

    @PostMapping(value = "/rating")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<StatusResponseDTO> ratingDriving(@RequestBody ClientDrivingRatingDTO ratingDTO){
        StatusResponseDTO responseDTO;
        try {
            String loggedin = getLoggedInUser();
            clientDrivingRatingService.leaveRating(ratingDTO, loggedin);
            responseDTO = new StatusResponseDTO(true);
        } catch (NoUserFoundException | NotInTimeException | CostumNotFoundException | NotValidException | AlreadyInUse | NoMatchingException e) {
            e.printStackTrace();
            responseDTO = new StatusResponseDTO(e, e.getCode());

        }catch (Exception e){
            e.printStackTrace();
            UnexpectedException ue = new UnexpectedException("Something happened");
            responseDTO = new StatusResponseDTO(ue, ue.getCode());
        }
        return ResponseEntity.ok(responseDTO);
    }

    private String getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }}
