package kts.nwt.ktsnwt.controller.chat;

import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.chat.FriendsListDTO;
import kts.nwt.ktsnwt.dto.chat.MessageDTO;
import kts.nwt.ktsnwt.dto.chat.NotifyMessageReceivedDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.service.chat.interfaces.ChatService;
import kts.nwt.ktsnwt.service.chat.interfaces.MessageService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.websocket.OnMessage;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/chat")
public class ChatController {

    @Autowired
    private ChatService chatService;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping(value = "/friends")
    @PreAuthorize("hasRole('ADMIN') or hasRole('REGISTERED_USER') or hasRole('DRIVER')")
    public ResponseEntity<List<FriendsListDTO>> getFriends(){
        String username = getLoggedInUser();
        List<FriendsListDTO> res = new ArrayList<>();
        try {
            List<User> users = chatService.getFriends(username);
            for (User u : users){
                res.add(new FriendsListDTO(u));
            }
        } catch (NoUserFoundException e) {
            return new ResponseEntity(res, HttpStatus.NOT_FOUND);
        }
        //ResponseEntity re = new ResponseEntity();
        //
        return ResponseEntity.status(HttpStatus.OK).body(res);
        //return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping(value = "/messages/{withUsername}")//username - ulogovani korisnik
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<MessageDTO>> getMessagesAdmin(@PathVariable String withUsername){
        String loggedInUsername = getLoggedInUser();
        List<MessageDTO> res = new ArrayList<>();
        if (withUsername.equals("admin"))
            res = messageService.getMessagesWithAdmins(loggedInUsername);
        else
            res = messageService.getMessagesWithUser(withUsername);
        return ResponseEntity.ok(res);
    }

    @GetMapping(value = "/messages")//username - ulogovani korisnik
    @PreAuthorize("hasRole('REGISTERED_USER') or hasRole('DRIVER')")
    public ResponseEntity<List<MessageDTO>> getMessagesRegisteredUser(  ){
        String loggedInUsername = getLoggedInUser();
        List<MessageDTO> res = new ArrayList<>();
        res = messageService.getMessagesWithAdmins(loggedInUsername);
        return ResponseEntity.ok(res);
    }

    @PutMapping(value = "/send/message/admin")
    @PreAuthorize("hasRole('ADMIN')")
    @OnMessage
    public ResponseEntity<String> sendMessageAdmin(@RequestBody MessageDTO messageDTO){
        String loggedInUsername = getLoggedInUser();
        String initFromId = messageDTO.getFromId(); // init: admin
        messageDTO.setFromId(loggedInUsername);// jj
        try {
            User from = messageService.sendMessage(messageDTO);
            FriendsListDTO user = new FriendsListDTO(from);//(new FriendsListDTO()).setFriendsListDTOToSender(from);
            messageDTO.setFromId(initFromId);
            NotifyMessageReceivedDTO response = new NotifyMessageReceivedDTO(user, messageDTO);
            String dest = "/topic/" + messageDTO.getToId();
            template.convertAndSend(dest, response);
            User to = userService.findByUsername(messageDTO.getToId());
            String dest2 = "/topic/admin";
            template.convertAndSend(dest2, new NotifyMessageReceivedDTO(new FriendsListDTO(to), messageDTO));
            return new ResponseEntity<>("", HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = "/send/message")
    @PreAuthorize("hasRole('REGISTERED_USER') or hasRole('DRIVER')")
    public ResponseEntity<String> sendMessage(@RequestBody MessageDTO messageDTO){
        //System.out.println("SEND MESSAGE");
        String loggedInUsername = getLoggedInUser();
        if (!messageDTO.getFromId().equals(loggedInUsername)) {
            //System.out.println("HACKING");
            return new ResponseEntity<>("Hacking here?", HttpStatus.BAD_REQUEST);
        }
        try {
            //System.out.println("SENDING....");
            User from = messageService.sendMessage(messageDTO);
            FriendsListDTO user = new FriendsListDTO(from);//(new FriendsListDTO()).setFriendsListDTOToSender(from);
            NotifyMessageReceivedDTO response = new NotifyMessageReceivedDTO(user, messageDTO);
            String dest = "/topic/" + messageDTO.getToId();
            template.convertAndSend(dest, response);
            return new ResponseEntity<>("", HttpStatus.OK);
        } catch (NoUserFoundException e) {
            //System.out.println("FAIIIL");
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    private String getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }
}