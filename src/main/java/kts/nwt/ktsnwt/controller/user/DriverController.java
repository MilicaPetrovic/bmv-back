package kts.nwt.ktsnwt.controller.user;

import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.dto.location.MapPointDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.StillHasActiveDrivingException;
import kts.nwt.ktsnwt.service.user.DriverServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/driver", produces = MediaType.APPLICATION_JSON_VALUE)
public class DriverController {

    @Autowired
    private DriverServiceImpl driverService;

    @GetMapping(value="/driving")
    public ResponseEntity<ArrayList<MapPointDTO>> getDrivingDrivers(){
        ArrayList<MapPointDTO> addresses = new ArrayList<MapPointDTO>();
        ArrayList<Driver> activeDrivers = driverService.getDrivingDrivers();
        for (Driver d : activeDrivers) {
     //       System.out.println(d.getDriverState().toString() + ' ' + d.getUsername());
            addresses.add(new MapPointDTO(d.getCurrentAddress().getLatitude(), d.getCurrentAddress().getLongitude()));
        }
        return new ResponseEntity<>(addresses, HttpStatus.OK);
    }

    @GetMapping(value="/inactive")
    public ResponseEntity<ArrayList<MapPointDTO>> getInactiveDriver(){
        ArrayList<MapPointDTO> addresses = new ArrayList<MapPointDTO>();
        ArrayList<Driver> inactiveDrivers = driverService.getInactiveDrivers();
        for (Driver d : inactiveDrivers) {
            addresses.add(new MapPointDTO(d.getCurrentAddress().getLatitude(), d.getCurrentAddress().getLongitude()));
        }
        return new ResponseEntity<>(addresses, HttpStatus.OK);
    }

    @GetMapping(value="/active")
    public ResponseEntity<ArrayList<MapPointDTO>> getActiveDrivers(){
        ArrayList<MapPointDTO> addresses = new ArrayList<MapPointDTO>();
        ArrayList<Driver> activeDrivers = driverService.getActiveDrivers();
        for (Driver d : activeDrivers) {
      //      System.out.println(d.getDriverState().toString() + ' ' + d.getUsername());
            addresses.add(new MapPointDTO(d.getCurrentAddress().getLatitude(), d.getCurrentAddress().getLongitude()));
        }
    //    System.out.println(addresses.get(0).getLat());
        return new ResponseEntity<>(addresses, HttpStatus.OK);
    }

    @GetMapping(value = "/active/driver")
    public ResponseEntity<Boolean> getDriverStatus() throws NoUserFoundException {
        String username = getLoggedInUser();
        return new ResponseEntity<Boolean>(driverService.getIfActive(username), HttpStatus.OK);
    }

    @GetMapping(value = "/change/status")
    public ResponseEntity<StatusResponseDTO> changeDriverStatus(){
        String username = getLoggedInUser();
        try {
            driverService.changeStatus(username);
            return new ResponseEntity<>(new StatusResponseDTO(true), HttpStatus.OK);
        } catch (StillHasActiveDrivingException | NoUserFoundException e) {
            return new ResponseEntity<>(new StatusResponseDTO(e, e.getCode()), HttpStatus.OK);
        }
    }


    private String getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }

}
