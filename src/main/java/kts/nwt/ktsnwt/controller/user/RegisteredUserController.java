package kts.nwt.ktsnwt.controller.user;

import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotValidException;
import kts.nwt.ktsnwt.service.user.interfaces.FavouriteRouteService;
import kts.nwt.ktsnwt.service.user.interfaces.RegisteredUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping(value = "client")
public class RegisteredUserController {

    @Autowired
    private FavouriteRouteService routeService;

    @Autowired
    private RegisteredUserService userService;

    @GetMapping("/toggle/route/{drivingId}")
    public ResponseEntity<StatusResponseDTO> toggleFavouriteRouteWithRoute(@PathVariable int drivingId){
        String username = getLoggedInUser();
        try {
            routeService.toggleFavouriteRoute(username, drivingId);
        } catch (NoUserFoundException | NotValidException e) {
            e.printStackTrace();
            return ResponseEntity.ok(new StatusResponseDTO(e, e.getCode()));
        }
        return new ResponseEntity<>(new StatusResponseDTO(true), HttpStatus.OK);
    }

    @PutMapping("/buy/tokens")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<StatusResponseDTO> buyTokens(@RequestBody int tokens, Principal userP) {
        try {
            userService.addTokens(userP.getName(), tokens);
        } catch (NoUserFoundException e) {
            e.printStackTrace();
            return ResponseEntity.ok(new StatusResponseDTO(e, e.getCode()));
        }
        return new ResponseEntity<>(new StatusResponseDTO(true), HttpStatus.OK);
    }

    private String getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }

}
