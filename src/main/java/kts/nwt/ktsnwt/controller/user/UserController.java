package kts.nwt.ktsnwt.controller.user;

import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.dto.user.UserProfileDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.Principal;
import java.util.ArrayList;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping(value = "/get")
    @PreAuthorize("hasAnyRole('ROLE_REGISTERED_USER', 'ROLE_ADMIN', 'ROLE_DRIVER')")
    public ResponseEntity<UserProfileDTO> getUser(Principal userP) throws NoUserFoundException {
        User user = userService.findByUsername(userP.getName());
        UserProfileDTO dto = userService.getUserProfileDTO(user);
        return ResponseEntity.ok(dto);
    }

    @PutMapping("/change/photo")
    @PreAuthorize("hasAnyRole('ROLE_REGISTERED_USER', 'ROLE_ADMIN', 'ROLE_DRIVER')")
    public ResponseEntity<InputStreamResource> changeProfilePicture(@RequestPart("files") MultipartFile[] multiPartFiles, Principal userP) throws IOException, NoUserFoundException {
        String profilePhotoPath = userService.changeProfilePhoto(multiPartFiles, userP.getName());
        File file = new File(profilePhotoPath);
        return new ResponseEntity<>(new InputStreamResource(Files.newInputStream(file.toPath())), HttpStatus.OK);
    }

    @PutMapping("/update")
    @PreAuthorize("hasAnyRole('ROLE_REGISTERED_USER', 'ROLE_ADMIN', 'ROLE_DRIVER')")
    public ResponseEntity<StatusResponseDTO> updateUser(@RequestBody UserProfileDTO dto, Principal userP) {
        try {
            User user = userService.findByUsername(userP.getName());
            boolean isUpdated = userService.updateUser(dto, user);
            if (!isUpdated)
                return new ResponseEntity<>(new StatusResponseDTO(true, "Zahtev za promenu podataka je poslat adminu."), HttpStatus.OK);
            return new ResponseEntity<>(new StatusResponseDTO(true, "Podaci su uspešno izmenjeni"), HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(new StatusResponseDTO(false), HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping(value = "/username/all/registered")
    @PreAuthorize("hasAnyRole('ROLE_REGISTERED_USER', 'ROLE_ADMIN', 'ROLE_DRIVER')")
    public ResponseEntity<ArrayList<String>> getRegisteredUsersWithoutLoggedIn(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return new ResponseEntity<>(this.userService.getAllRegisteredUsers(userDetails.getUsername()), HttpStatus.OK);
    }

    @GetMapping(value = "/get/{username}")
    @PreAuthorize("hasAnyRole('ROLE_REGISTERED_USER', 'ROLE_ADMIN', 'ROLE_DRIVER')")
    public ResponseEntity<UserProfileDTO> getUserByUsername(@PathVariable String username) throws NoUserFoundException {
        User user = userService.findByUsername(username);
        UserProfileDTO dto = userService.getUserProfileDTO(user);
        return ResponseEntity.ok(dto);
    }

    @GetMapping(value = "/get/photo/{username}")
    @PreAuthorize("hasAnyRole('ROLE_REGISTERED_USER', 'ROLE_ADMIN', 'ROLE_DRIVER')")
    public ResponseEntity<InputStreamResource> getProfilePhotoByUsername(@PathVariable String username) throws NoUserFoundException, IOException {
        User user = userService.findByUsername(username);
        String path = user.getImage();
        if (path == null || path.equals(""))
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        File file = new File(path);
        return new ResponseEntity<>(new InputStreamResource(Files.newInputStream(file.toPath())), HttpStatus.OK);
    }

    @GetMapping(value = "/get/social/photo/{username}")
    @PreAuthorize("hasAnyRole('ROLE_REGISTERED_USER', 'ROLE_ADMIN', 'ROLE_DRIVER')")
    public ResponseEntity<String> getSocialProfilePhotoByUsername(@PathVariable String username) throws NoUserFoundException {
        User user = userService.findByUsername(username);
        String path = user.getImage();
        if (path == null || path.equals(""))
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(path, HttpStatus.OK);
    }

}
