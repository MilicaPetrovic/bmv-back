package kts.nwt.ktsnwt.controller;

import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.exception.CostumNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ResponseExceptionHandler {

    @ExceptionHandler(IndexOutOfBoundsException.class)
    protected ResponseEntity<StatusResponseDTO> handleIndexOutOfBoundException(IndexOutOfBoundsException ex){
        ex.printStackTrace();
        StatusResponseDTO s = new StatusResponseDTO(ex, -1);
        return new ResponseEntity<StatusResponseDTO>(s, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NullPointerException.class)
    protected ResponseEntity<StatusResponseDTO> handleNullPointerException(NullPointerException ex){
        ex.printStackTrace();
        StatusResponseDTO s = new StatusResponseDTO(ex, -2);
        return new ResponseEntity<>(s, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CostumNotFoundException.class)
    protected ResponseEntity handleNullPointerException(CostumNotFoundException ex){
        ex.printStackTrace();
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
}
