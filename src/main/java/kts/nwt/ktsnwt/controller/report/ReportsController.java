package kts.nwt.ktsnwt.controller.report;

import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.dto.report.InconsistencyDTO;
import kts.nwt.ktsnwt.dto.report.PeriodDTO;
import kts.nwt.ktsnwt.dto.report.ReportDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.service.report.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping(value = "report")
public class ReportsController {

    @Autowired
    private ReportService reportService;

    @PostMapping(value = "/client")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<ReportDTO> getReportsForClientInPeriod(@RequestBody PeriodDTO periodDTO){
        System.out.println("******************************************* Report - client *********************************");
        String username = getLoggedInUser();
        System.out.println(username);
        System.out.println(periodDTO.getStartString());
        System.out.println(periodDTO.getStart());
        System.out.println(periodDTO.getEndString());
        System.out.println(periodDTO.getEnd());
        LocalDate currentEnd = periodDTO.getEnd().plusDays(1);
        periodDTO.setEnd(currentEnd);
        System.out.println(periodDTO.getEnd());
        ReportDTO reportDTO = reportService.getReportByClient(username, periodDTO.getStart(), currentEnd);
        return new ResponseEntity<>(reportDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/driver")
    @PreAuthorize("hasRole('DRIVER')")
    public ResponseEntity<ReportDTO> getReportsForDriverInPeriod(@RequestBody PeriodDTO periodDTO){
        System.out.println("******************************************* Report - driver *********************************");
        String username = getLoggedInUser();
        System.out.println(username);
        LocalDate currentEnd = periodDTO.getEnd().plusDays(1);
        periodDTO.setEnd(currentEnd);
        ReportDTO reportDTO = reportService.getReportByDriver(username, periodDTO.getStart(), periodDTO.getEnd());
        return new ResponseEntity<>(reportDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ReportDTO> getReportsForUsersInPeriod(@RequestBody PeriodDTO periodDTO){
        System.out.println("******************************************* Report - admin *********************************");
        String username = getLoggedInUser();
        System.out.println(username);
        LocalDate currentEnd = periodDTO.getEnd().plusDays(1);
        periodDTO.setEnd(currentEnd);
        ReportDTO reportDTO = reportService.getReportForAll(periodDTO.getStart(), periodDTO.getEnd());
        return new ResponseEntity<>(reportDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/admin/client/{username}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ReportDTO> getReportsForClientInPeriod(@RequestBody PeriodDTO periodDTO, @PathVariable String username){
        System.out.println("******************************************* Report - admin - client *********************************");
        LocalDate currentEnd = periodDTO.getEnd().plusDays(1);
        periodDTO.setEnd(currentEnd);
        ReportDTO reportDTO = reportService.getReportByClient(username, periodDTO.getStart(), periodDTO.getEnd());
        return new ResponseEntity<>(reportDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/admin/driver/{username}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ReportDTO> getReportsForDriverInPeriod(@RequestBody PeriodDTO periodDTO, @PathVariable String username){
        System.out.println("******************************************* Report - admin - driver *********************************");
        LocalDate currentEnd = periodDTO.getEnd().plusDays(1);
        periodDTO.setEnd(currentEnd);
        ReportDTO reportDTO = reportService.getReportByDriver(username, periodDTO.getStart(), periodDTO.getEnd());
        return new ResponseEntity<>(reportDTO, HttpStatus.OK);
    }

    private String getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }

    @PostMapping(value="/inconsistency", produces = "application/json", consumes = "application/json")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<StatusResponseDTO> reportInconsistency (@RequestBody InconsistencyDTO inconsistencyDTO){
        try {
            System.out.println("   prijava nekonzistencije ------");
            System.out.println(inconsistencyDTO);
            this.reportService.reportInconsistency(inconsistencyDTO);
        }catch (NoUserFoundException e){
            return new ResponseEntity<>(new StatusResponseDTO(e, 1), HttpStatus.OK);
        }
        return new ResponseEntity<>(new StatusResponseDTO(true), HttpStatus.OK);
    }
}
