package kts.nwt.ktsnwt.controller.security;

import kts.nwt.ktsnwt.dto.TextDTO;
import kts.nwt.ktsnwt.dto.security.ChangePasswordDTO;
import kts.nwt.ktsnwt.dto.security.ResetPasswordDTO;
import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.exception.*;
import kts.nwt.ktsnwt.service.mail.interfaces.ResetPasswordMailService;
import kts.nwt.ktsnwt.service.security.interfaces.ChangePasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@CrossOrigin(origins = "https://localhost:4200")
@RequestMapping( produces = MediaType.APPLICATION_JSON_VALUE)
public class PasswordResetController {

    @Autowired
    private ResetPasswordMailService resetPasswordMailService;

    @Autowired
    private ChangePasswordService changePasswordService;

    @PostMapping("/reset/password/request")
    public ResponseEntity<StatusResponseDTO> sendMailForPasswordReset(@RequestBody TextDTO email){
        StatusResponseDTO st;
        try {
            resetPasswordMailService.sendMailForUserToResetPassword(email.getText());
            st = new StatusResponseDTO(true);
        } catch (UserNotEnabledException e) {
            e.printStackTrace();
            st = new StatusResponseDTO(e, e.getCode());
        }catch (NoUserFoundException e){
            st = new StatusResponseDTO(e, e.getCode());
        }
        catch (Exception e){
            e.printStackTrace();
            st = new StatusResponseDTO(new UnexpectedException("Unexpected exception"), -100);
        }
        return new ResponseEntity<>(st, HttpStatus.OK);
    }

    @PostMapping("/reset/password")//prebaci da bude status DTO
    public ResponseEntity<StatusResponseDTO> resetPassword(@RequestBody ResetPasswordDTO resetDTO){
        StatusResponseDTO responseDTO;
        try {
            changePasswordService.resetPassword(resetDTO);
            responseDTO = new StatusResponseDTO(true);
        } catch (NoMatchingException | NoUserFoundException | NotInTimeException e) {
            responseDTO = new StatusResponseDTO(e, e.getCode());
        } catch (Exception e) {
            e.printStackTrace();
            UnexpectedException ue = new UnexpectedException();
            responseDTO = new StatusResponseDTO(ue, ue.getCode());
        }
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @PutMapping("/user/change/password")
    @PreAuthorize("hasAnyRole('ROLE_REGISTERED_USER', 'ROLE_ADMIN', 'ROLE_DRIVER')")
    public ResponseEntity<StatusResponseDTO> changePassword(@RequestBody ChangePasswordDTO dto, Principal userP) {
        try {
            changePasswordService.changePassword(dto, userP.getName());
        } catch (NoUserFoundException | PasswordsNotMatchingException e) {
            return new ResponseEntity<>(new StatusResponseDTO(e, e.getCode()), HttpStatus.OK);
        }
        return new ResponseEntity<>(new StatusResponseDTO(true), HttpStatus.OK);
    }

}
