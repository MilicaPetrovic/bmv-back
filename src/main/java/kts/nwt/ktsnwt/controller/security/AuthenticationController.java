package kts.nwt.ktsnwt.controller.security;

import kts.nwt.ktsnwt.beans.users.*;
import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.dto.security.JwtAuthenticationRequest;
import kts.nwt.ktsnwt.dto.UserTokenState;
import kts.nwt.ktsnwt.dto.user.DriverRegisterRequest;
import kts.nwt.ktsnwt.dto.user.SocialUserDTO;
import kts.nwt.ktsnwt.dto.user.UserRegisterRequest;
import kts.nwt.ktsnwt.exception.AlreadyInUse;
import kts.nwt.ktsnwt.exception.NotValidException;
import kts.nwt.ktsnwt.exception.StillHasActiveDrivingException;
import kts.nwt.ktsnwt.service.security.interfaces.RegisterService;
import kts.nwt.ktsnwt.service.user.interfaces.DriverService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import kts.nwt.ktsnwt.auth.TokenUtils;
import kts.nwt.ktsnwt.service.security.UberUserDetailsService;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;


@RestController
@RequestMapping(value = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private UberUserDetailsService userDetailsService;

    @Autowired
    private RegisterService registerService;

    @Autowired
    private UserService userService;

    @Autowired
    private DriverService driverService;

    @PostMapping("/login")
    public ResponseEntity<UserTokenState> createAuthenticationToken(
            @RequestBody JwtAuthenticationRequest authenticationRequest, HttpServletResponse response) {

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                authenticationRequest.getEmail(), authenticationRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        User user = (User) authentication.getPrincipal();
        if (user.isRegisteredUser()) {
            RegisteredUser registeredUser = (RegisteredUser) user;
            if (!registeredUser.isActivated()) return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }
        driverService.ifUserIsDriverItBecomesActive(user);
        UserDetails userDetails = userDetailsService.loadUserByEmail(user.getEmail());
        if (userDetails == null) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

        return ResponseEntity.ok(generateToken(user));
    }

    public UserTokenState generateToken(User user){
//        driverService.ifUserIsDriverItBecomesActive(user);
        String jwt = tokenUtils.generateToken(user);  // TODO: da li username ili email
        int expiresIn = tokenUtils.getExpiredIn();
        return new UserTokenState(jwt, expiresIn);
    }

    @PostMapping("/socialLogin")
    public ResponseEntity<UserTokenState> socialLogin(
            @RequestBody SocialUserDTO socialUserDTO, HttpServletResponse response) {
        User user = userService.getSocialUser(socialUserDTO);
        driverService.ifUserIsDriverItBecomesActive(user);
        String jwt = tokenUtils.generateToken(user);
        int expiresIn = tokenUtils.getExpiredIn();
        return ResponseEntity.ok(new UserTokenState(jwt, expiresIn));
    }

    @PostMapping("/register")
    public ResponseEntity<StatusResponseDTO> addUser(@RequestBody UserRegisterRequest userRequest) {
        System.out.println("IN REGISTER CONTROLLER");
        try {
            registerService.registerClient(userRequest);
            return new ResponseEntity<>(new StatusResponseDTO(true), HttpStatus.OK);
        } catch (AlreadyInUse | NotValidException exception) {
            exception.printStackTrace();
            System.out.println(exception.getMessage());
            return new ResponseEntity<>(new StatusResponseDTO(exception, exception.getCode()), HttpStatus.OK);
        }
    }

    @GetMapping(value = "/logout")
    public ResponseEntity<StatusResponseDTO> logoutUser(Principal userP) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        try {
            driverService.ifUserIsDriverItBecomesInactive(userDetails.getUsername());
        } catch (StillHasActiveDrivingException e) {
            return new ResponseEntity<>(new StatusResponseDTO(e, e.getCode()), HttpStatus.OK);
        }
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            SecurityContextHolder.clearContext();

            return new ResponseEntity<>(new StatusResponseDTO(true), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/register/driver")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<StatusResponseDTO> addDriver(@RequestBody DriverRegisterRequest userRequest) {
        try {
            registerService.registerDriver(userRequest);
            return new ResponseEntity<>(new StatusResponseDTO(true), HttpStatus.OK);
        } catch (AlreadyInUse | NotValidException exception) {
            exception.printStackTrace();
            System.out.println(exception.getMessage());
            return new ResponseEntity<>(new StatusResponseDTO(exception, exception.getCode()), HttpStatus.OK);
        }
    }
}
