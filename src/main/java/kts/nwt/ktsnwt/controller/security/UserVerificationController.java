package kts.nwt.ktsnwt.controller.security;

import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.UserTokenState;
import kts.nwt.ktsnwt.exception.NoMatchingException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.service.security.interfaces.UserVerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/verify")
public class UserVerificationController {

    @Autowired
    private UserVerificationService userVerificationService;

    @Autowired
    private AuthenticationController authenticationController;

    @GetMapping(value = "/{username}/{code}")
    public ResponseEntity<UserTokenState> verifyAccount(@PathVariable String username, @PathVariable String code,
                                                        HttpServletResponse response) {
        try {
            User reg = userVerificationService.verificate(username, code);
            return new ResponseEntity<UserTokenState>(authenticationController.generateToken(reg), HttpStatus.OK);
        } catch (NoUserFoundException |NoMatchingException e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
