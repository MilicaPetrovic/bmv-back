package kts.nwt.ktsnwt.controller.admin;

import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.PaginationDTO;
import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.dto.VehicleDTO;
import kts.nwt.ktsnwt.dto.admin.AdminDriverDrivingHistoryDTO;
import kts.nwt.ktsnwt.dto.admin.AdminUsersDTO;
import kts.nwt.ktsnwt.dto.admin.UserDTO;
import kts.nwt.ktsnwt.dto.user.ProfileChangeRequestDTO;
import kts.nwt.ktsnwt.exception.NoProfileChangeRequestFoundException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.service.user.DriverServiceImpl;
import kts.nwt.ktsnwt.service.driving.interfaces.DriverDrivingHistoryService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "https://localhost:4200")
@RestController
@RequestMapping(value = "/admin")
public class AdminsDriverController {

    @Autowired
    private DriverServiceImpl driverService;

    @Autowired
    private DriverDrivingHistoryService driverDrivingHistoryService;

    @Autowired
    private UserService userService;

    @GetMapping(value = "/get/drivers/{page}/{row}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<PaginationDTO<AdminUsersDTO>> getDrivers(@PathVariable int page, @PathVariable int row) {
        List<AdminUsersDTO> resultList = new ArrayList<>();
        List<Driver> drivers = driverService.getDrivers(PageRequest.of(page, row));//.subList(from, until);
        for (User driver : drivers){
            resultList.add(new AdminUsersDTO(driver));
        }
        PaginationDTO<AdminUsersDTO> result = new PaginationDTO<>(driverService.getDriversCount(), resultList);
        return ResponseEntity.ok(result);

    }
    @GetMapping(value = "/get/driver/drivings/{username}/{page}/{row}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<PaginationDTO<AdminDriverDrivingHistoryDTO>> getDriversDrivings(@PathVariable String username, @PathVariable int page, @PathVariable int row) {
        List<AdminDriverDrivingHistoryDTO> resultList = new ArrayList<>();
        PaginationDTO<AdminDriverDrivingHistoryDTO> result = new PaginationDTO<>(0, resultList);
        try {
            resultList = driverDrivingHistoryService.getDrivingHistoryByDriver(username, PageRequest.of(page, row));//.subList(page, row);
            result.setTotalCount(driverDrivingHistoryService.getDrivingHistoryByDriverCount(username));
            result.setResultList(resultList);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/driver/vehicle/{username}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<VehicleDTO> getVehicle(@PathVariable String username) {
        try {
            return ResponseEntity.ok(driverService.getVehicle(username));
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/update/driver")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<StatusResponseDTO> updateDriver(@RequestBody ProfileChangeRequestDTO dto) {
        try {
            boolean isUpdated = userService.updateDriver(dto);
            if (!isUpdated)
                return new ResponseEntity<>(new StatusResponseDTO(true, "Zahtev za promenu podataka je odbijen."), HttpStatus.OK);
            return new ResponseEntity<>(new StatusResponseDTO(true, "Podaci su uspešno izmenjeni"), HttpStatus.OK);
        } catch (NoUserFoundException | NoProfileChangeRequestFoundException e) {
            return new ResponseEntity<>(new StatusResponseDTO(false), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/get/profile/change/requests/{page}/{row}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<PaginationDTO<ProfileChangeRequestDTO>> getProfileChangeRequests(@PathVariable int page, @PathVariable int row) {
        List<ProfileChangeRequestDTO> resultList = userService.getProfileChangeRequests(PageRequest.of(page, row));
        PaginationDTO<ProfileChangeRequestDTO> result = new PaginationDTO<>(userService.getProfileChangeRequestCount(), resultList);
        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/drivers")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<UserDTO>> getClients(){
        return ResponseEntity.ok(driverService.getDrivers());
    }
}
