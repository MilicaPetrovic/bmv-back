package kts.nwt.ktsnwt.controller.admin;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.feedback.WarningNote;
import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.dto.TextDTO;
import kts.nwt.ktsnwt.dto.driving.DetailedDrivingDTO;
import kts.nwt.ktsnwt.dto.admin.WarningNoteDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.feedback.interfaces.WarningsNoteService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/admin")
public class AdminsUserController {

    @Autowired
    private UserService userService;

    @Autowired
    private WarningsNoteService warningsNoteService;

    @Autowired
    private DrivingService drivingService;

    @PostMapping(value = "/add/warning/{username}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<WarningNoteDTO>> addWarningNote(@PathVariable String username, @RequestBody TextDTO textDTO){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        try {
            warningsNoteService.addWarningNot(userDetails.getUsername(), username, textDTO.getText());
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return getClientsWarningNote(username);
    }


    @GetMapping(value = "/get/user/warnings/{username}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<WarningNoteDTO>> getClientsWarningNote(@PathVariable String username){
        List<WarningNoteDTO> res = new ArrayList<>();
        try {
            List<WarningNote> warningNotes = warningsNoteService.getWarningsNoteByUsername(username);
            for (WarningNote w : warningNotes){
                res.add(new WarningNoteDTO(w));
            }
            return ResponseEntity.ok(res);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(res, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/toggle/block/{username}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<StatusResponseDTO> toggleBlockUser(@PathVariable String username){
        StatusResponseDTO responseDTO;
        try {
            userService.toggleBlockForUser(username);
            responseDTO = new StatusResponseDTO(true);
        } catch (NoUserFoundException e) {
            e.printStackTrace();
            responseDTO = new StatusResponseDTO(e, e.getCode());
        }
        return ResponseEntity.ok(responseDTO);
    }

    @GetMapping(value = "/driving/{drivingId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<DetailedDrivingDTO> getDriving(@PathVariable long drivingId){
        try {
            return ResponseEntity.ok(drivingService.getDetailedDriving(drivingId));
        } catch (NotFoundException e) {
            e.printStackTrace();
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }
}
