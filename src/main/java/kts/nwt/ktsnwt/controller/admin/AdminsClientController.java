package kts.nwt.ktsnwt.controller.admin;

import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.dto.PaginationDTO;
import kts.nwt.ktsnwt.dto.admin.AdminClientDrivingHistoryDTO;
import kts.nwt.ktsnwt.dto.admin.AdminUsersDTO;
import kts.nwt.ktsnwt.dto.admin.UserDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.service.driving.interfaces.ClientDrivingHistoryService;
import kts.nwt.ktsnwt.service.user.interfaces.RegisteredUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/admin")
public class AdminsClientController {

    @Autowired
    private RegisteredUserService registeredUserService;

    @Autowired
    private ClientDrivingHistoryService clientDrivingHistoryService;

    @GetMapping(value = "/get/clients/{page}/{row}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<PaginationDTO<AdminUsersDTO>> getClients(@PathVariable int page, @PathVariable int row){
        List<AdminUsersDTO> resultList = new ArrayList<>();
        List<RegisteredUser> registeredUsers = registeredUserService.getActivatedUsers(PageRequest.of(page, row));//.subList(from, until);
        for (RegisteredUser ru : registeredUsers){
            resultList.add(new AdminUsersDTO(ru));
        }
        PaginationDTO<AdminUsersDTO> result = new PaginationDTO<>(registeredUserService.getActivatedUsersCount(), resultList);
        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/get/client/drivings/{username}/{page}/{row}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<PaginationDTO<AdminClientDrivingHistoryDTO>> getClientsDrivings(@PathVariable String username, @PathVariable int page, @PathVariable int row){
        List<AdminClientDrivingHistoryDTO> resultList = new ArrayList<>();
        PaginationDTO<AdminClientDrivingHistoryDTO> result = new PaginationDTO<>(0, resultList);
        try {
            resultList = clientDrivingHistoryService.getDrivingHistoryByClientForAdmin(username, PageRequest.of(page, row));//.subList(page, row);
            result.setTotalCount(clientDrivingHistoryService.getDrivingHistoryByClientCount(username));
            result.setResultList(resultList);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/clients")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<UserDTO>> getClients(){
        return ResponseEntity.ok(registeredUserService.getClients());
    }
}
