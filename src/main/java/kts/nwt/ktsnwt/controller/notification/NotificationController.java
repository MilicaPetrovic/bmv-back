package kts.nwt.ktsnwt.controller.notification;

import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.dto.notification.DriverNotificationDTO;
import kts.nwt.ktsnwt.dto.notification.NotificationDTO;
import kts.nwt.ktsnwt.dto.notification.DetailedDrivingNotificationDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotValidException;
import kts.nwt.ktsnwt.exception.UnexpectedException;
import kts.nwt.ktsnwt.service.driving.interfaces.PassengersService;
import kts.nwt.ktsnwt.service.notification.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "https://localhost:4200/home")
@RestController
@RequestMapping(value = "/notification")
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private PassengersService passengersService;

    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping("/all")//vraca samo za proslih 7 dana
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<List<NotificationDTO>> getNotifications(){
        String username = getLoggedInUser();
        List<NotificationDTO> results = notificationService.getNotificationsFor(username, 7);
        return ResponseEntity.ok(results);
    }

    @GetMapping("/split/fare/{notificationId}")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<DetailedDrivingNotificationDTO> getSplitFareDataDTO(@PathVariable long notificationId){
        DetailedDrivingNotificationDTO dto = notificationService.getSplitFareNotification(notificationId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping("/split/fare/confirm/{drivingId}")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<StatusResponseDTO> confirmSplitFare(@PathVariable long drivingId){
        try {
            List<DetailedDrivingNotificationDTO> notificationDTOs = this.passengersService.confirmDriving(drivingId, getLoggedInUser());///D
            for (DetailedDrivingNotificationDTO dto : notificationDTOs) {
                String dest = "/topic/driving/" + dto.getUsername();
                template.convertAndSend(dest, dto);
            }
            return new ResponseEntity<>(new StatusResponseDTO(true), HttpStatus.OK);
        } catch (NoUserFoundException | NotValidException e) {
            return new ResponseEntity<>(new StatusResponseDTO(e, e.getCode()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new StatusResponseDTO(new UnexpectedException("Unexpected"), -100), HttpStatus.OK);
        }
    }

    @GetMapping("/driver/all")
    @PreAuthorize("hasRole('DRIVER')")
    public ResponseEntity<List<DriverNotificationDTO>> getDriverNotifications() {
        String username = getLoggedInUser();
        List<DriverNotificationDTO> results = notificationService.getNotificationsForDriver(username, 7);
        return ResponseEntity.ok(results);
    }

    @GetMapping("/driver/get/{notificationId}")
    @PreAuthorize("hasRole('DRIVER')")
    public ResponseEntity<DriverNotificationDTO> getDriverNotificationInfo(@PathVariable long notificationId) {
        DriverNotificationDTO dto = notificationService.getDriverNotification(notificationId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    private String getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }
}
