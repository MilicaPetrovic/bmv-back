package kts.nwt.ktsnwt.controller.driving;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.dto.driving.DrivingStateDTO;
import kts.nwt.ktsnwt.dto.location.MapPointDTO;
import kts.nwt.ktsnwt.dto.location.SimulationDTO;
import kts.nwt.ktsnwt.dto.notification.NewNotificationDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotHaveDrivingsForDriver;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.notification.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin(origins = "https://localhost:4200")
@RestController
@RequestMapping(value = "/simulation")
public class SimulationController {

    @Autowired
    private DrivingService drivingService;
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private NotificationService notificationService;

    @GetMapping(value = "/driver/{driverId}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<SimulationDTO> getCurrentAcceptedDriving(@PathVariable("driverId") Long driverId) {
        SimulationDTO simulationDTO = drivingService.getLastCurrentPaidDrivingForDriver(driverId);
        System.out.println(" py gadja drivera ");
        if (simulationDTO != null) {
            this.simpMessagingTemplate.convertAndSend("/topic/map-updates/new-ride", simulationDTO);
            for (MapPointDTO m : simulationDTO.getCoordinates()) {
                System.out.println(" latitude : " + m.getLat() + "  longitude :  " + m.getLng());
            }
        }
        // SimulationDTO simulationDTO = this.drivingService.getSimulationDriving(drivingId);
        return new ResponseEntity<>(simulationDTO, HttpStatus.OK);
    }

    @GetMapping(value = "come/{drivingId}", produces = "application/json")
    public void comeToPassenger(@PathVariable("drivingId") long drivingId) {
        String driverUsername = null;
        try {
            driverUsername = this.drivingService.getDriverByDrivingId(drivingId);
            System.out.println(" %%%%%%%  stigao je do putnika  %%%%%%%%% " + drivingId + " driver username " + driverUsername);
            this.simpMessagingTemplate.convertAndSend("/topic/map-updates/enable-start-driving/" + driverUsername, drivingId);
            List<NewNotificationDTO> notifications = notificationService.createDriverArrivedNotificationForDriving(drivingId);
            for (NewNotificationDTO n : notifications) {
                this.simpMessagingTemplate.convertAndSend("/topic/driving/" + n.getClient(), n);
            }
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
    }

    @GetMapping(value = "state/{drivingId}", produces = "application/json")
    public ResponseEntity<SimulationDTO> getStateForDriving(@PathVariable("drivingId") long drivingId) {
        System.out.println("  ------------------    stigne da pita za state    --------------------  " + drivingId);
        try {

            SimulationDTO simulationDTO = this.drivingService.getSimulationDriving(drivingId);
            System.out.println("state " + simulationDTO.getDrivingState() + "   id: " + simulationDTO.getId());
            return ResponseEntity.ok(simulationDTO);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping(value = "end/{drivingId}", produces = "application/json")
    public void endDriving(@PathVariable("drivingId") long drivingId) {
        System.out.println(" %%%%%%%  stigao je do kraja  %%%%%%%%% " + drivingId);
        String driverUsername = null;
        try {
            driverUsername = this.drivingService.getDriverByDrivingId(drivingId);
            this.simpMessagingTemplate.convertAndSend("/topic/map-updates/enable-end-driving/" + driverUsername, drivingId);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
    }

    @GetMapping(value = "state/last/driving", produces = "application/json")
    public ResponseEntity<DrivingStateDTO> getStateOfLastDriving() {
        String username =getLoggedInUser();
        try {
            DrivingStateDTO drivingStateDTO = this.drivingService.getStateOfLastDriving(username);
            return new ResponseEntity<>(drivingStateDTO, HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>((new DrivingStateDTO(false, false, -1)), HttpStatus.OK);
        } catch (NotHaveDrivingsForDriver e) {
            return new ResponseEntity<>((new DrivingStateDTO( false, false, -1)), HttpStatus.OK);
        }
    }

    @GetMapping(value="draw", produces="application/json")
    public ResponseEntity<SimulationDTO> getSimulationForDrawing(){
        String username = getLoggedInUser();
        System.out.println("DRAW : " + username);

        try {
            SimulationDTO simulationDTO = drivingService.getSimulationIfExist(username);
            return new ResponseEntity<>(simulationDTO, HttpStatus.OK);
        } catch (NotHaveDrivingsForDriver e) {
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }


    private String getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }

}
