package kts.nwt.ktsnwt.controller.driving;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.communication.Notification;
import kts.nwt.ktsnwt.dto.driving.MakeDrivingDTO;
import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.dto.driving.PurchaseDrivingDTO;
import kts.nwt.ktsnwt.dto.driving.RejectDrivingDTO;
import kts.nwt.ktsnwt.dto.notification.DriverNotificationDTO;
import kts.nwt.ktsnwt.dto.notification.NotificationDTO;
import kts.nwt.ktsnwt.exception.*;
import kts.nwt.ktsnwt.service.driving.interfaces.*;
import kts.nwt.ktsnwt.service.notification.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class MakeDrivingController {

    @Autowired
    private MakeDrivingService makeDrivingService;

    @Autowired
    private DriverAssignmentService driverAssignmentService;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private PurchaseDrivingService purchaseDrivingService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private DrivingService drivingService;

    @Autowired
    private DrivingFlowService drivingFlowService;


    @PostMapping(value = "/make/driving", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<StatusResponseDTO> makeDriving(@RequestBody MakeDrivingDTO dto) {
        System.out.println("Driving kontoler - make driving");
        System.out.println(dto.isFavouriteRoute());
        dto.setApplicantUsername(getLoggedInUser());
        StatusResponseDTO st;
        try {
            Pair<Long, List<Notification>> id_sendNotifications = makeDrivingService.makeDrivingAndReturnNotificationsForPassengers(dto);
            for (Notification notification : id_sendNotifications.getSecond()) {
                NotificationDTO notificationDto = new NotificationDTO(notification);
                String dest = "/topic/driving/" + notification.getClientUsername();
                template.convertAndSend(dest, notificationDto);
            }
            st = new StatusResponseDTO(true, String.valueOf(id_sendNotifications.getFirst()));
        } catch (NoUserFoundException | RegisteredUserExceptedException | NoAvailableDriverException |
                 CanNotMakeNewDrivingException | NotEnoughTokensException | InvalidFutureTimeException
                 | NoPathGivenException e) {
            st = new StatusResponseDTO(e, e.getCode());
            e.printStackTrace();
        } catch (NotFoundException e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (NullPointerException e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            st = new StatusResponseDTO(new UnexpectedException("Unexpected exception"), -100);
        }
        return ResponseEntity.ok(st);
    }

    @PostMapping(value = "/purchase/driving")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<StatusResponseDTO> purchaseDriving(@RequestBody PurchaseDrivingDTO dto) {
        try {
            boolean purchased = purchaseDrivingService.purchaseDriving(dto);
            String driver = dto.getDriver();
            if (purchased) {
                DriverNotificationDTO notificationDTO = notificationService.sendDrivingStartedNotification(dto.getDrivingId());
                String dest = "/topic/driving/" + driver;
                template.convertAndSend(dest, notificationDTO);
            }
            System.out.println("DRIVER: " + dto.getDriver());
        } catch (NotEnoughTokensException | NoUserFoundException | NotAllUsersConfirmedException |
                 NoAvailableDriverException e) {
            e.printStackTrace();
            return new ResponseEntity<>(new StatusResponseDTO(e), HttpStatus.OK);
        }
        return new ResponseEntity<>(new StatusResponseDTO(true), HttpStatus.OK);
    }

    @PostMapping("/reject/driving")
    @PreAuthorize("hasRole('DRIVER')")
    public ResponseEntity<StatusResponseDTO> rejectDriving(@RequestBody RejectDrivingDTO dto) {
        try {
            drivingService.rejectDriving(dto);
        } catch (NotFoundException e) {
            e.printStackTrace();
            return new ResponseEntity<>(new StatusResponseDTO(e, -100), HttpStatus.NOT_FOUND);
        }
        this.template.convertAndSend("/topic/map-updates/ended-ride", dto.getDrivingId());
        return new ResponseEntity<>(new StatusResponseDTO(false), HttpStatus.OK);
    }

    @PostMapping("/confirm/driving")
    @PreAuthorize("hasRole('DRIVER')")
    public ResponseEntity<StatusResponseDTO> confirmDriving(@RequestBody long drivingId) {
        try {
            drivingService.startDriving(drivingId);
        } catch (NotFoundException e) {
            e.printStackTrace();
            return new ResponseEntity<>(new StatusResponseDTO(e, -100), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new StatusResponseDTO(true), HttpStatus.OK);
    }

    private String getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }
}
