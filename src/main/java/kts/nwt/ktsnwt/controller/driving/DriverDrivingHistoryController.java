package kts.nwt.ktsnwt.controller.driving;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.dto.PaginationDTO;
import kts.nwt.ktsnwt.dto.driving.DetailedDrivingDTO;
import kts.nwt.ktsnwt.dto.driving_history.DriverDrivingHistoryDTO;
import kts.nwt.ktsnwt.exception.CostumNotFoundException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.UnauthorizedRequest;
import kts.nwt.ktsnwt.service.driving.interfaces.DriverDrivingHistoryService;
import kts.nwt.ktsnwt.service.driving.interfaces.PassengersService;
import kts.nwt.ktsnwt.service.feedback.interfaces.RatingByUserAndDrivingService;
import kts.nwt.ktsnwt.service.feedback.RatingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "https://localhost:4200")
@RestController
@RequestMapping(value = "/driving/history/driver")
public class DriverDrivingHistoryController {

    @Autowired
    private DriverDrivingHistoryService driverDrivingHistoryService;

    @Autowired
    private RatingByUserAndDrivingService ratingFromUserToDrivingService;

    @Autowired
    private RatingServiceImpl ratingService;

    @Autowired
    private PassengersService passengersService;

    @GetMapping(value = "/detail/{drivingId}")
    @PreAuthorize("hasRole('DRIVER')")
    public ResponseEntity<DetailedDrivingDTO> getDetailedDrivingHistoryForDriver(@PathVariable Long drivingId) throws CostumNotFoundException {
        String username = getLoggedInUser();
        try {
            return ResponseEntity.ok(driverDrivingHistoryService.getDetailedDrivingCheckIfUserIsDriver(username, drivingId));
        } catch (NotFoundException | NoUserFoundException e) {
            e.printStackTrace();
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }catch (UnauthorizedRequest e){
            e.printStackTrace();
            return new ResponseEntity(null, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(value = "/all/{page}/{row}")
    @PreAuthorize("hasRole('DRIVER')")
    public ResponseEntity<PaginationDTO<DriverDrivingHistoryDTO>> getDrivingHistoryForDriver(@PathVariable int page, @PathVariable int row){
        String username = getLoggedInUser();
        List<DriverDrivingHistoryDTO> res = driverDrivingHistoryService.getDrivingHistoryByDriverForDriver(username, PageRequest.of(page, row));
        return new ResponseEntity<>(new PaginationDTO<>(driverDrivingHistoryService.getDrivingHistoryByDriverCount(username),res), HttpStatus.OK);
    }


    @GetMapping(value = "/all/{page}/{row}/cena")
    @PreAuthorize("hasRole('DRIVER')")
    public ResponseEntity<PaginationDTO<DriverDrivingHistoryDTO>> getDrivingHistoryForUserSortByPrice(@PathVariable int page, @PathVariable int row){
        String loggedInUser = getLoggedInUser();
        try {
            List<DriverDrivingHistoryDTO> res = driverDrivingHistoryService.getDrivingHistoryByDriverSortedByPrice(loggedInUser, PageRequest.of(page, row));
            PaginationDTO<DriverDrivingHistoryDTO> result = new PaginationDTO<DriverDrivingHistoryDTO>(driverDrivingHistoryService.getDrivingHistoryByDriverCount(loggedInUser), res);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/all/{page}/{row}/polaziste")
    @PreAuthorize("hasRole('DRIVER')")
    public ResponseEntity<PaginationDTO<DriverDrivingHistoryDTO>> getDrivingHistoryForUserSortByDeparture(@PathVariable int page, @PathVariable int row){
        String loggedInUser = getLoggedInUser();
        try {
            List<DriverDrivingHistoryDTO> res = driverDrivingHistoryService.getDrivingHistoryByDriverSortedByDeparture(loggedInUser, PageRequest.of(page, row));
            PaginationDTO<DriverDrivingHistoryDTO> result = new PaginationDTO<DriverDrivingHistoryDTO>(driverDrivingHistoryService.getDrivingHistoryByDriverCount(loggedInUser), res);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/all/{page}/{row}/odrediste")
    @PreAuthorize("hasRole('DRIVER')")
    public ResponseEntity<PaginationDTO<DriverDrivingHistoryDTO>> getDrivingHistoryForUserSortByArrival(@PathVariable int page, @PathVariable int row){
        String loggedInUser = getLoggedInUser();
        try {
            List<DriverDrivingHistoryDTO> res = driverDrivingHistoryService.getDrivingHistoryByDriverSortedByArrival(loggedInUser, PageRequest.of(page, row));
            PaginationDTO<DriverDrivingHistoryDTO> result = new PaginationDTO<DriverDrivingHistoryDTO>(driverDrivingHistoryService.getDrivingHistoryByDriverCount(loggedInUser), res);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/all/{page}/{row}/start")
    @PreAuthorize("hasRole('DRIVER')")
    public ResponseEntity<PaginationDTO<DriverDrivingHistoryDTO>> getDrivingHistoryForUserSortByStartTime(@PathVariable int page, @PathVariable int row){
        String loggedInUser = getLoggedInUser();
        try {
            List<DriverDrivingHistoryDTO> res = driverDrivingHistoryService.getDrivingHistoryByDriverSortedByStartTime(loggedInUser, PageRequest.of(page, row));
            PaginationDTO<DriverDrivingHistoryDTO> result = new PaginationDTO<DriverDrivingHistoryDTO>(driverDrivingHistoryService.getDrivingHistoryByDriverCount(loggedInUser), res);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/all/{page}/{row}/end")
    @PreAuthorize("hasRole('DRIVER')")
    public ResponseEntity<PaginationDTO<DriverDrivingHistoryDTO>> getDrivingHistoryForUserSortByEndTime(@PathVariable int page, @PathVariable int row){
        String loggedInUser = getLoggedInUser();
        try {
            List<DriverDrivingHistoryDTO> res = driverDrivingHistoryService.getDrivingHistoryByDriverSortedByEndTime(loggedInUser, PageRequest.of(page, row));
            PaginationDTO<DriverDrivingHistoryDTO> result = new PaginationDTO<DriverDrivingHistoryDTO>(driverDrivingHistoryService.getDrivingHistoryByDriverCount(loggedInUser), res);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    private String getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }
}
