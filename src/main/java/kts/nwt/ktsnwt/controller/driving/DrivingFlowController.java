package kts.nwt.ktsnwt.controller.driving;

import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.dto.notification.NewNotificationDTO;
import kts.nwt.ktsnwt.exception.AlreadyEndedException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotValidException;
import kts.nwt.ktsnwt.exception.ShouldNotHappenException;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingFlowService;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/driving/flow")
public class DrivingFlowController {

    @Autowired
    private DrivingService drivingService;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private DrivingFlowService drivingFlowService;

    @GetMapping(value = "/end/{drivingId}")
    @PreAuthorize("hasRole('DRIVER')")
    public ResponseEntity<StatusResponseDTO> drivingEnded(@PathVariable long drivingId){
        String loggedInUser = getLoggedInUser();
        try {//TODO: format response
            NewNotificationDTO dto = drivingFlowService.drivingEnded(loggedInUser, drivingId);
            List<String> passengers = drivingService.getPassengerUsersFromDrivingId(drivingId);
            for (String u : passengers) {
                String dest = "/topic/driving/" + u;
                template.convertAndSend(dest, dto);
            }
            this.template.convertAndSend("/topic/map-updates/ended-ride", drivingId);
            return new ResponseEntity<>(new StatusResponseDTO(true), HttpStatus.OK);
        } catch (NoUserFoundException| NotValidException | AlreadyEndedException | ShouldNotHappenException e) {
            return new ResponseEntity<>(new StatusResponseDTO(e, e.getCode()), HttpStatus.OK);
        }
        //List<String> passengers = drivingService.getPassengerUsersFromDrivingId(drivingId);
    }

    private String getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }

}
