package kts.nwt.ktsnwt.controller.driving;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.driving.VehicleType;
import kts.nwt.ktsnwt.dto.driving.VehicleTypeDTO;
import kts.nwt.ktsnwt.dto.location.LatLngDTO;
import kts.nwt.ktsnwt.dto.location.SimulationVehicleDTO;
import kts.nwt.ktsnwt.dto.report.InconsistencyDTO;
import kts.nwt.ktsnwt.service.driving.VehicleTypeServiceImpl;
import kts.nwt.ktsnwt.service.user.interfaces.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/vehicle/type", produces = MediaType.APPLICATION_JSON_VALUE)
public class VehicleTypeController {

    @Autowired
    private VehicleTypeServiceImpl vehicleTypeService;
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    @Autowired
    private DriverService driverService;


    @GetMapping(value = "/all")
   // @PreAuthorize("hasAnyRole('ROLE_REGISTERED_USER', 'ROLE_ADMIN', 'ROLE_DRIVER')")
    public ResponseEntity<ArrayList<VehicleTypeDTO>> getAllVehicleTypes() {
        System.out.println("vehicle type controller je stigao");
        ArrayList<VehicleTypeDTO> vehicleTypeDTOS = new ArrayList<VehicleTypeDTO>();
        for (VehicleType vehicleType : this.vehicleTypeService.getAllVehicleTypes()) {
            vehicleTypeDTOS.add(new VehicleTypeDTO(vehicleType));
        }
        return new ResponseEntity<>(vehicleTypeDTOS, HttpStatus.OK);
    }

    @PutMapping(path = "/{driverId}/{drivingId}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<SimulationVehicleDTO> updateVehicleLocationOfDriver(@PathVariable("driverId") Long driverId, @PathVariable("drivingId") Long drivingId, @RequestBody LatLngDTO locationDTO)  {
        System.out.println(" *** python gadja promenu current adrese ***");
        System.out.println("latitude: " + locationDTO.getLatitude() + "  longitude   " + locationDTO.getLongitude() + "  driver id  " + driverId + "  driving id Č " +drivingId);
        SimulationVehicleDTO simulationVehicleDTO = this.vehicleTypeService.updateVehicleLocation(driverId, locationDTO.getLatitude(), locationDTO.getLongitude());
        this.simpMessagingTemplate.convertAndSend("/topic/map-updates/update-vehicle-position", simulationVehicleDTO);
        try {
            this.checkConsistency(driverId, drivingId, locationDTO.getLatitude(), locationDTO.getLongitude());
            this.updateTimeToCome(driverId, drivingId, locationDTO.getLatitude(), locationDTO.getLongitude());
            return new ResponseEntity<>(simulationVehicleDTO, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(simulationVehicleDTO, HttpStatus.OK);
        }

    }

    private boolean checkConsistency(long driverId, long drivingId, double latitude,  double longitude) throws NotFoundException {
        boolean check = this.vehicleTypeService.checkConsistency(driverId, drivingId, latitude, longitude);
        System.out.println("  checked: " + check);
        if (!check) {
            List<InconsistencyDTO> passengers = this.vehicleTypeService.getPassenger(driverId, drivingId);
            for (InconsistencyDTO dto : passengers) {
                System.out.println(dto.getRegisteredUsername());
                this.simpMessagingTemplate.convertAndSend("/topic/map-updates/enable-report-driving/" + dto.getRegisteredUsername(), dto);
            }
            return check;
        }
        return check;
    }

    private void updateTimeToCome(long driverId, long drivingId,double latitude,  double longitude) throws NotFoundException {
        List<InconsistencyDTO> passengers = this.vehicleTypeService.getPassenger(driverId, drivingId);
        double timeInMinutes = 0.00;
        try {
            double time = this.vehicleTypeService.getTimeToCome(driverId, drivingId, latitude, longitude);
            timeInMinutes = time/60;
            System.out.println(" update tiiiimeeeee");
            System.out.println(timeInMinutes + "minuta");
            for (InconsistencyDTO dto : passengers) {
                System.out.println(dto.getRegisteredUsername());
                this.simpMessagingTemplate.convertAndSend("/topic/map-updates/update-time/" + dto.getRegisteredUsername(), String.valueOf(timeInMinutes));
            }
        }
        catch ( IOException | InterruptedException e){
            for (InconsistencyDTO dto : passengers) {
                System.out.println(dto.getRegisteredUsername());
                this.simpMessagingTemplate.convertAndSend("/topic/map-updates/update-time/" + dto.getRegisteredUsername(), "trenutno ne možemo da ažuriramo vreme");
            }
        }
    }

    @GetMapping(path = "/vehicle/all")
    public ResponseEntity<List<SimulationVehicleDTO>> getAllVehicles() {
        System.out.println(" get all vozila");
        List<SimulationVehicleDTO> simulationVehicleDTOS = this.driverService.getAllDriverAndVehicles();
        return new ResponseEntity<>(simulationVehicleDTOS, HttpStatus.OK);
    }
}
