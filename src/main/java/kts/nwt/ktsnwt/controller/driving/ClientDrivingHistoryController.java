package kts.nwt.ktsnwt.controller.driving;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.dto.PaginationDTO;
import kts.nwt.ktsnwt.dto.driving.DetailedDrivingDTO;
import kts.nwt.ktsnwt.dto.driving_history.ClientDetailedDrivingDTO;
import kts.nwt.ktsnwt.dto.location.RouteDTO;
import kts.nwt.ktsnwt.dto.driving_history.ClientDrivingHistoryDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.UnauthorizedRequest;
import kts.nwt.ktsnwt.service.driving.interfaces.ClientDrivingHistoryService;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.feedback.RatingServiceImpl;
import kts.nwt.ktsnwt.service.feedback.interfaces.RatingByUserAndDrivingService;
import kts.nwt.ktsnwt.service.user.interfaces.FavouriteRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "https://localhost:4200")
@RestController
@RequestMapping(value = "/driving/history")
public class ClientDrivingHistoryController {

    @Autowired
    private ClientDrivingHistoryService drivingHistoryService;

    @Autowired
    private RatingByUserAndDrivingService ratingFromUserToDrivingService;

    @Autowired
    private RatingServiceImpl ratingService;

    @Autowired
    private DrivingService drivingService;

    @Autowired
    private FavouriteRouteService favouriteRouteService;

    @GetMapping(value = "/{drivingId}")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<DetailedDrivingDTO> getDetailedDrivingHistoryForUser(@PathVariable Long drivingId) {
        String username = getLoggedInUser();
        try {
            System.out.println("************************************ driving history ***********");
            ClientDetailedDrivingDTO c = drivingHistoryService.getDetailedDrivingCheckIfUserIsPassenger(username, drivingId);
            System.out.println(c.isFavourite());
            return ResponseEntity.ok(c);
        } catch (NotFoundException | NoUserFoundException e) {
            e.printStackTrace();
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }catch (UnauthorizedRequest e){
            e.printStackTrace();
            return new ResponseEntity(null, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(value = "/all/{page}/{row}")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<PaginationDTO<ClientDrivingHistoryDTO>> getDrivingHistoryForUser(@PathVariable int page, @PathVariable int row){
        String loggedInUser = getLoggedInUser();
        List<ClientDrivingHistoryDTO> res = null;
        try {
            res = drivingHistoryService.getDrivingHistoryByClient(loggedInUser, PageRequest.of(page, row));
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        PaginationDTO<ClientDrivingHistoryDTO> result = new PaginationDTO<ClientDrivingHistoryDTO>(drivingHistoryService.getDrivingHistoryByClientCount(loggedInUser), res);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/all/{page}/{row}/cena")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<PaginationDTO<ClientDrivingHistoryDTO>> getDrivingHistoryForUserSortByPrice(@PathVariable int page, @PathVariable int row){
        String loggedInUser = getLoggedInUser();
        try {
            List<ClientDrivingHistoryDTO> res = drivingHistoryService.getDrivingHistoryByClientSortedByPrice(loggedInUser, PageRequest.of(page, row));
            PaginationDTO<ClientDrivingHistoryDTO> result = new PaginationDTO<ClientDrivingHistoryDTO>(drivingHistoryService.getDrivingHistoryByClientCount(loggedInUser), res);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/all/{page}/{row}/polaziste")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<PaginationDTO<ClientDrivingHistoryDTO>> getDrivingHistoryForUserSortByDepature(@PathVariable int page, @PathVariable int row) {
        String loggedInUser = getLoggedInUser();
        try {
            List<ClientDrivingHistoryDTO> res = drivingHistoryService.getDrivingHistoryByClientSortedByDeparture(loggedInUser, PageRequest.of(page, row));
            PaginationDTO<ClientDrivingHistoryDTO> result = new PaginationDTO<ClientDrivingHistoryDTO>(drivingHistoryService.getDrivingHistoryByClientCount(loggedInUser), res);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping(value = "/all/{page}/{row}/odrediste")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<PaginationDTO<ClientDrivingHistoryDTO>> getDrivingHistoryForUserSortByArrival(@PathVariable int page, @PathVariable int row){
        String loggedInUser = getLoggedInUser();
        try {
            List<ClientDrivingHistoryDTO> res = drivingHistoryService.getDrivingHistoryByClientSortedByArrival(loggedInUser, PageRequest.of(page, row));
            PaginationDTO<ClientDrivingHistoryDTO> result = new PaginationDTO<ClientDrivingHistoryDTO>(drivingHistoryService.getDrivingHistoryByClientCount(loggedInUser), res);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/all/{page}/{row}/order")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<PaginationDTO<ClientDrivingHistoryDTO>> getDrivingHistoryForUserSortByOrderTime(@PathVariable int page, @PathVariable int row){
        String loggedInUser = getLoggedInUser();
        try {
            List<ClientDrivingHistoryDTO> res = drivingHistoryService.getDrivingHistoryByClientSortedByOrderTime(loggedInUser, PageRequest.of(page, row));
            PaginationDTO<ClientDrivingHistoryDTO> result = new PaginationDTO<ClientDrivingHistoryDTO>(drivingHistoryService.getDrivingHistoryByClientCount(loggedInUser), res);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/all/{page}/{row}/start")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<PaginationDTO<ClientDrivingHistoryDTO>> getDrivingHistoryForUserSortByStartTime(@PathVariable int page, @PathVariable int row){
        String loggedInUser = getLoggedInUser();
        try {
            List<ClientDrivingHistoryDTO> res = drivingHistoryService.getDrivingHistoryByClientSortedByStartTime(loggedInUser, PageRequest.of(page, row));
            PaginationDTO<ClientDrivingHistoryDTO> result = new PaginationDTO<ClientDrivingHistoryDTO>(drivingHistoryService.getDrivingHistoryByClientCount(loggedInUser), res);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping(value = "/all/{page}/{row}/end")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<PaginationDTO<ClientDrivingHistoryDTO>> getDrivingHistoryForUserSortByEndTime(@PathVariable int page, @PathVariable int row){
        String loggedInUser = getLoggedInUser();
        try {
            List<ClientDrivingHistoryDTO> res = drivingHistoryService.getDrivingHistoryByClientSortedByEndTime(loggedInUser, PageRequest.of(page, row));
            PaginationDTO<ClientDrivingHistoryDTO> result = new PaginationDTO<ClientDrivingHistoryDTO>(drivingHistoryService.getDrivingHistoryByClientCount(loggedInUser), res);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (NoUserFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "route/{id}")
    @PreAuthorize("hasRole('REGISTERED_USER') or hasRole('ADMIN')")
    public ResponseEntity<RouteDTO> getDrivingRoute(@PathVariable Long id){
        RouteDTO route = drivingHistoryService.getRouteByDrivingId(id);
        System.out.println("cord size " + route.getCoordinates().size());
        System.out.println("station size " + route.getStations().size());
        return new ResponseEntity<RouteDTO>(route, HttpStatus.OK);
    }

    @GetMapping(value = "favourite")
    @PreAuthorize("hasRole('REGISTERED_USER')")
    public ResponseEntity<ArrayList<RouteDTO>> getFavouriteRoutes() throws NoUserFoundException {
        String username = getLoggedInUser();
        ArrayList<RouteDTO> routes = favouriteRouteService.getFavouriteRoutesForUsername(username);
        return new ResponseEntity<>(routes, HttpStatus.OK);
    }


    private String getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }
}
