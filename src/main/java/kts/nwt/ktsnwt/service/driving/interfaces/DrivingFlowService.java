package kts.nwt.ktsnwt.service.driving.interfaces;

import kts.nwt.ktsnwt.dto.notification.NewNotificationDTO;
import kts.nwt.ktsnwt.exception.AlreadyEndedException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotValidException;
import kts.nwt.ktsnwt.exception.ShouldNotHappenException;


public interface DrivingFlowService {

    NewNotificationDTO drivingEnded(String driverUsername, long drivingId) throws NoUserFoundException, NotValidException, AlreadyEndedException, ShouldNotHappenException;
}
