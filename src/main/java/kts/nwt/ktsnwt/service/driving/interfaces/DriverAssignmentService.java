package kts.nwt.ktsnwt.service.driving.interfaces;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.exception.NoAvailableDriverException;

import java.io.IOException;
import java.time.Clock;

public interface DriverAssignmentService {
     Driving handleDriver(Clock clock, Driving driving) throws NoAvailableDriverException;

     double getActiveMinutesInThePast24Hours(Driver driver);
     void assignDriverToFutureDriving() throws NoAvailableDriverException;
}
