package kts.nwt.ktsnwt.service.driving.interfaces;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.dto.admin.AdminDriverDrivingHistoryDTO;
import kts.nwt.ktsnwt.dto.driving.DetailedDrivingDTO;
import kts.nwt.ktsnwt.dto.driving_history.DriverDrivingHistoryDTO;
import kts.nwt.ktsnwt.exception.CostumNotFoundException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.UnauthorizedRequest;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface DriverDrivingHistoryService {

    List<DriverDrivingHistoryDTO> getDrivingHistoryByDriverForDriver(String username, PageRequest of);

    Driving getDrivingByIdWithDrivingPayment(Long drivingId) throws CostumNotFoundException;

    List<AdminDriverDrivingHistoryDTO> getDrivingHistoryByDriver(String username, PageRequest of) throws NoUserFoundException;

    int getDrivingHistoryByDriverCount(String username);

    List<DriverDrivingHistoryDTO> getDrivingHistoryByDriverSortedByPrice(String loggedInUser, PageRequest of) throws NoUserFoundException;

    List<DriverDrivingHistoryDTO> getDrivingHistoryByDriverSortedByDeparture(String loggedInUser, PageRequest of) throws NoUserFoundException;

    List<DriverDrivingHistoryDTO> getDrivingHistoryByDriverSortedByArrival(String loggedInUser, PageRequest of) throws NoUserFoundException;

    List<DriverDrivingHistoryDTO> getDrivingHistoryByDriverSortedByStartTime(String loggedInUser, PageRequest of) throws NoUserFoundException;

    List<DriverDrivingHistoryDTO> getDrivingHistoryByDriverSortedByEndTime(String loggedInUser, PageRequest of) throws NoUserFoundException;

    DetailedDrivingDTO getDetailedDrivingCheckIfUserIsDriver(String username, Long drivingId) throws NoUserFoundException, UnauthorizedRequest, NotFoundException;
}
