package kts.nwt.ktsnwt.service.driving.interfaces;

import kts.nwt.ktsnwt.beans.location.Path;
import kts.nwt.ktsnwt.dto.location.MapPointDTO;
import kts.nwt.ktsnwt.exception.NoPathGivenException;

import java.util.ArrayList;

public interface PathService {
    Path save(ArrayList<MapPointDTO> chosenStations, ArrayList<MapPointDTO> coordinates, double distance, double time, boolean isFavourite, String username) throws NoPathGivenException;
}
