package kts.nwt.ktsnwt.service.driving;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.driving.ChosenAdditionalServices;
import kts.nwt.ktsnwt.beans.driving.VehicleType;
import kts.nwt.ktsnwt.repository.ChosenAdditionalServicesRepository;
import kts.nwt.ktsnwt.repository.VehicleTypeRepository;
import kts.nwt.ktsnwt.service.driving.interfaces.ChosenAdditionalServicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChosenAdditionalServicesServiceImpl implements ChosenAdditionalServicesService {

    @Autowired
    private VehicleTypeRepository vehicleTypeRepository;

    @Autowired
    private ChosenAdditionalServicesRepository chosenAdditionalServicesRepository;

    @Override
    public ChosenAdditionalServices makeChosenAdditionalServices(long vehicleTypeId, boolean petFriendly, boolean babyFriendly) throws NotFoundException {
        VehicleType vehicleType = vehicleTypeRepository.findByIdIs(vehicleTypeId).orElseThrow(
                () -> new NotFoundException("Vehicle type not found")
        );
        ChosenAdditionalServices chosenAdditionalServices = new ChosenAdditionalServices(vehicleType, petFriendly, babyFriendly);
        return chosenAdditionalServicesRepository.save(chosenAdditionalServices);
    }
}
