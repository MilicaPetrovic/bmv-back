package kts.nwt.ktsnwt.service.driving.interfaces;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.notification.DetailedDrivingNotificationDTO;
import kts.nwt.ktsnwt.exception.*;

import java.util.List;

public interface PassengersService {

    List<User> getPassangersByDriving(Driving driving);

    List<User> getPassengerByDrivingId(long drivingId) throws CostumNotFoundException;

    List<DetailedDrivingNotificationDTO> confirmDriving(long notificationId, String username) throws NoUserFoundException, NotValidException;

    Driving linkUsersInDriving(String applicantUsername, List<String> linkedUsers, Driving driving) throws NoUserFoundException, RegisteredUserExceptedException, CanNotMakeNewDrivingException, NotEnoughTokensException;
}
