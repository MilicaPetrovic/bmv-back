package kts.nwt.ktsnwt.service.driving.interfaces;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.communication.Notification;
import kts.nwt.ktsnwt.dto.driving.MakeDrivingDTO;
import kts.nwt.ktsnwt.exception.*;
import org.springframework.data.util.Pair;

import java.util.List;

public interface MakeDrivingService {
    Pair<Long, List<Notification>> makeDrivingAndReturnNotificationsForPassengers(MakeDrivingDTO dto) throws NoUserFoundException, RegisteredUserExceptedException, NoAvailableDriverException, CanNotMakeNewDrivingException, NotFoundException, NotEnoughTokensException, InvalidFutureTimeException, NoPathGivenException;
}
