package kts.nwt.ktsnwt.service.driving;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.driving.*;
import kts.nwt.ktsnwt.beans.location.Path;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.DriverState;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.driving.DetailedDrivingDTO;
import kts.nwt.ktsnwt.dto.driving.DrivingStateDTO;
import kts.nwt.ktsnwt.dto.driving.RejectDrivingDTO;
import kts.nwt.ktsnwt.exception.InvalidDrivingStateException;
import kts.nwt.ktsnwt.exception.InvalidFutureTimeException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotHaveDrivingsForDriver;
import kts.nwt.ktsnwt.repository.DriverRepository;
import kts.nwt.ktsnwt.beans.location.Address;
import kts.nwt.ktsnwt.dto.location.MapPointDTO;
import kts.nwt.ktsnwt.dto.location.SimulationDTO;
import kts.nwt.ktsnwt.dto.location.SimulationVehicleDTO;
import kts.nwt.ktsnwt.repository.DrivingPaymentRepository;
import kts.nwt.ktsnwt.repository.DrivingRepository;
import kts.nwt.ktsnwt.repository.UserRepository;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.feedback.RatingServiceImpl;
import kts.nwt.ktsnwt.service.notification.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
@Transactional
public class DrivingServiceImpl implements DrivingService {
    @Autowired
    private DrivingRepository drivingRepository;

    @Autowired
    private RatingServiceImpl ratingService;

    @Autowired
    private DriverRepository driverRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DrivingPaymentRepository drivingPaymentRepository;

    @Autowired
    private NotificationService notificationService;

    @Override
    public Driving getDrivingById(long drivingId) {
        return drivingRepository.findById(drivingId).orElse(null);
    }

    @Override
    public DetailedDrivingDTO getDetailedDriving(long drivingId) throws NotFoundException {
        Driving d = drivingRepository.findById(drivingId).orElse(null);
        if (d == null)
            throw new NotFoundException("Driving not found");
        if (d.getDriver() != null) {
            double driverRating = ratingService.driversRating(d.getDriver().getId());
            double vehicleRating = ratingService.vehicleRating(d.getDriver().getId());
            return new DetailedDrivingDTO(d, driverRating, vehicleRating);
        }
        return new DetailedDrivingDTO(d);
    }

    @Override
    public List<String> getPassengerUsersFromDrivingId(long drivingId) {
        List<DrivingPayment> dp = drivingRepository.findUsersByDrivingId(drivingId);
        List<String> res = new ArrayList<>();
        for (DrivingPayment d : dp) {
            res.add(d.getRegisteredUser().getUsername());
        }
        return res;
    }

    @Override
    public Driving save(Driving driving) {
        return drivingRepository.save(driving);
    }

    @Override
    public List<Driving> getActiveDrivingByClient(String applicantUsername) {
        return drivingRepository.getActiveDrivingsByClientsUsername(applicantUsername);
    }

    @Override
    public void rejectDriving(RejectDrivingDTO dto) throws NotFoundException {
        Driving driving = getDrivingById(dto.getDrivingId());
        if (driving == null)
            throw new NotFoundException("Driving not found");
        Driver driver = driving.getDriver();
        driver.setDriverState(DriverState.ACTIVE);
        driving.setState(DrivingState.REJECTED);
        driving.setRejectionText(dto.getReason());
        returnPaidTokens(driving);
        drivingRepository.save(driving);
        driverRepository.save(driver);
        notificationService.createDrivingRefusedNotification(driving);
    }

    private void returnPaidTokens(Driving driving) {
        for (DrivingPayment dp : driving.getPassengers()) {
            dp.returnMoneyToUser();
            userRepository.save(dp.getRegisteredUser());
            drivingPaymentRepository.save(dp);
        }
    }

    @Override
    public Driving startDriving(long drivingId) throws NotFoundException, InvalidDrivingStateException {
        Driving driving = getDrivingById(drivingId);
        if (driving == null)
            throw new NotFoundException("Driving not found");
        if (driving.getState() != DrivingState.PAID)
            throw new InvalidDrivingStateException("Inappropriate driving state");
        driving.setState(DrivingState.CURRENT);
        drivingRepository.save(driving);
        return driving;
    }

    public SimulationDTO getSimulationDriving(Long drivingId) {
        System.out.println("driving id : " + drivingId);
        Driving driving = this.drivingRepository.findDrivingById(drivingId).orElse(null);
        //   System.out.println("path: " + driving.getPath().getId());

        List<Address> points = driving.getPath().getRoute().getRoute();
        ArrayList<MapPointDTO> coordinates = new ArrayList<>();
        for (Address address : points) {
            coordinates.add(new MapPointDTO(address.getLatitude(), address.getLongitude()));
        }

        ArrayList<MapPointDTO> stations = new ArrayList<>();
        for (Address a : driving.getPath().getStations()) {
            stations.add(new MapPointDTO(a.getLatitude(), a.getLongitude()));
        }

        Vehicle vehicle = driving.getDriver().getVehicle();
        Address vehicleCurrentAddress = driving.getDriver().getCurrentAddress();
        SimulationVehicleDTO simulationVehicleDTO = new SimulationVehicleDTO(
                vehicle.getId(), vehicle.getLabel(), vehicleCurrentAddress.getLatitude(),
                vehicleCurrentAddress.getLongitude(), driving.getDriver().getId());
        boolean isPaid = driving.getState() == DrivingState.PAID;
        return new SimulationDTO(driving.getId(), coordinates, simulationVehicleDTO, driving.getState().getValue(), isPaid, stations);
    }

    @Override
    public SimulationDTO getLastCurrentPaidDrivingForDriver(Long driverId) {
        Driving driving = this.drivingRepository.getPaidOrCurrentDrivingByDriverId(driverId).orElse(null);
        System.out.println("Servis : driver id: " + driverId);
        System.out.println("repo: " + this.drivingRepository);
        if (driving == null) {
            return null;
        }
        System.out.println("   .........  nasla driving za drivera  ........   driving id: " + driving.getId());
        return this.getSimulationDriving(driving.getId());
    }

    @Override
    public String getDriverByDrivingId(long drivingId) throws NotFoundException {
        Driving driving = drivingRepository.findDrivingById(drivingId).orElse(null);
        if (driving == null) {
            throw new NotFoundException("Driving is not found");
        }
        Driver driver = driving.getDriver();
        if (driver != null) {
            return driver.getUsername();
        }
        return null;
    }

    @Override
    public Driving save(Clock clock, ChosenAdditionalServices chosenAdditionalServices, Path path, double price, double time, boolean future, int hour, int minute) throws InvalidFutureTimeException {
        if (future) {
            LocalDateTime start = LocalDateTime.now(clock).withHour(hour).withMinute(minute).withSecond(0);
            if (start.isBefore(LocalDateTime.now()))
                start.plusDays(1);
            isValidFutureTime(start, clock);
            TimePeriod drivingTime = new TimePeriod(start, start.plusMinutes((long) time), true);
            Driving driving = new Driving(clock, price, DrivingState.MADE, chosenAdditionalServices, path, drivingTime, true);
            return drivingRepository.save(driving);
        } else {
            TimePeriod drivingTime = new TimePeriod(LocalDateTime.now(clock), LocalDateTime.now(clock).plusMinutes((long) time), true);
            Driving driving = new Driving(clock, price, DrivingState.MADE, chosenAdditionalServices, path, drivingTime, false);
            return drivingRepository.save(driving);
        }
    }

    private void isValidFutureTime(LocalDateTime drivingTimeStart, Clock clock) throws InvalidFutureTimeException {
        double duration = Duration.between(drivingTimeStart, LocalDateTime.now(clock)).toMinutes();
        if (Math.abs(duration) > 300 || LocalDateTime.now(clock).isAfter(drivingTimeStart)) {
            throw new InvalidFutureTimeException("Vreme zakazivanja može biti najviše 5 sati unapred.");
        }
    }

    public Driving getPaidOrCurrentDrivingByDriverId(long driverId) {
        return this.drivingRepository.getPaidOrCurrentDrivingByDriverId(driverId).orElse(null);
    }

    @Override
    public List<Driving> getFutureDrivingFromPeriod(boolean b, LocalDateTime start, LocalDateTime end) {
        return drivingRepository.getFutureDrivingFromPeriod(b, start, end);
    }

    public DrivingStateDTO getStateOfLastDriving(String driverUsername) throws  NotHaveDrivingsForDriver {
        Driver driver = this.driverRepository.findByUsername(driverUsername).orElse(null);
        if (driver == null) {
            throw new NoUserFoundException("Driver is not found");
        }
        List<Driving> drivings = drivingRepository.getDrivingsByDriverId(driver.getId());
        if (drivings.size() == 0) {
            throw new NotHaveDrivingsForDriver("This driver dont have drivind.");
        }else{
            Driving lastDriving =  drivings.get(drivings.size() - 1);
            return compareDriverDrivingPosition(driver, lastDriving);
        }
    }

    private DrivingStateDTO compareDriverDrivingPosition(Driver driver, Driving driving){
        Address driverPosition = driver.getCurrentAddress();
        Address start = driving.getPath().getStations().get(0);
        int countStations = driving.getPath().getStations().size();
        Address end = driving.getPath().getStations().get(countStations-1);
        boolean startFlag = false;
        boolean endFlag = false;

        System.out.println("driver:  lat: " + driverPosition.getLatitude() + "     lon: " + driverPosition.getLongitude());
        System.out.println("start:  lat: " + start.getLatitude() + "     lon: " + start.getLongitude());
        System.out.println("end:  lat: " + end.getLatitude() + "     lon: " + end.getLongitude());


        if (((Math.abs(driverPosition.getLatitude()-start.getLatitude())) < 0.0001) &&
        ((Math.abs(driverPosition.getLongitude()-start.getLongitude())) < 0.0001))  {
            startFlag = true;
        } else if  (((Math.abs(driverPosition.getLatitude()-end.getLatitude())) < 0.001) &&
                ((Math.abs(driverPosition.getLongitude()-end.getLongitude())) < 0.001)) {
            endFlag = true;
        }
        return new DrivingStateDTO(startFlag, endFlag, driving.getId());
    }

    @Override
    public SimulationDTO getSimulationIfExist(String username) throws NotHaveDrivingsForDriver {
        System.out.println(" funkcija: " + username);
        User user = this.userRepository.findByUsername(username).orElse(null);
        //System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        //System.out.println(((Driver) user).getDriverState());
        if(user == null){
            throw new NoUserFoundException("User is not found");
        } else if (user instanceof Driver) {
            Driver driver = (Driver) user;
            List<Driving> drivings = drivingRepository.getDrivingsByDriverId(driver.getId());
            return this.getCurrentSimulationForDraw(drivings);
        } else if (user instanceof RegisteredUser) {
            List<Driving> drivings = drivingRepository.getActiveDrivingsByClientsUsername(username);
            return this.getCurrentSimulationForDraw(drivings);
        }
        return null;
    }

    private SimulationDTO getCurrentSimulationForDraw(List<Driving> drivings ) throws NotHaveDrivingsForDriver {
        System.out.println("DRIVINGS DRAW");
        for(Driving d : drivings){
            System.out.println(d.getState() + "  " + d.getId());
        }
        if (drivings.size() == 0) {
            throw new NotHaveDrivingsForDriver("This user dont have driving.");
        }else{
            Driving lastDriving =  drivings.get(drivings.size() - 1);
            DrivingState state = lastDriving.getState();
            if(state == DrivingState.PAID || state==DrivingState.CURRENT){
                return  getSimulationDriving(lastDriving.getId());
            }
        }
        throw new NotHaveDrivingsForDriver("This user dont have driving.");
    }
}
