package kts.nwt.ktsnwt.service.driving;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.driving.DrivingPayment;
import kts.nwt.ktsnwt.beans.driving.DrivingState;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.DriverState;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.dto.driving.PurchaseDrivingDTO;
import kts.nwt.ktsnwt.exception.NoAvailableDriverException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotAllUsersConfirmedException;
import kts.nwt.ktsnwt.exception.NotEnoughTokensException;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.driving.interfaces.PurchaseDrivingService;
import kts.nwt.ktsnwt.service.user.interfaces.DriverService;
import kts.nwt.ktsnwt.service.user.interfaces.RegisteredUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PurchaseDrivingServiceImpl implements PurchaseDrivingService {

    @Autowired
    private DrivingService drivingService;

    @Autowired
    private RegisteredUserService userService;

    @Autowired
    private DriverService driverService;

    @Override
    public boolean purchaseDriving(PurchaseDrivingDTO dto) throws NoUserFoundException, NotEnoughTokensException, NotAllUsersConfirmedException, NoAvailableDriverException {
        Driving driving = drivingService.getDrivingById(dto.getDrivingId());
        if (!driving.everybodyHasConfirmed())
            throw new NotAllUsersConfirmedException("Neko od putnika nije potvrdio vožnju.");
        if (driving.getDriver() == null && !driving.isFuture())
            throw new NoAvailableDriverException("Vozac nije pronadjen.");
        double pricePerUser = driving.getPrice() / driving.getPassengers().size();
        this.checkEnoughTokens(pricePerUser, driving.getPassengers(), driving);
        for (DrivingPayment passenger : driving.getPassengers()) {
            RegisteredUser user = passenger.getRegisteredUser();
            user.setTokens(user.getTokens() - pricePerUser);
            passenger.setPaid(true);
            userService.save(user);
        }
        driving.setState(DrivingState.PAID);
        drivingService.save(driving);
        if (!driving.isFuture())
            updateDriverState(driving.getDriver());
        return true;
    }

    private void updateDriverState(Driver u) {
        u.setDriverState(DriverState.DRIVING);
        driverService.save(u);
    }


    private void checkEnoughTokens(double price, List<DrivingPayment> passengers, Driving driving) throws NoUserFoundException, NotEnoughTokensException {
        for(DrivingPayment dp : passengers) {
            if (dp.getRegisteredUser().getTokens() < price) {
                refuseDriving(driving);
                throw new NotEnoughTokensException("Neko od putnika nema dovoljno tokena za ovu vožnju. Molimo kupite tokene.");
            }
        }
    }

    private void refuseDriving(Driving driving) {
        driving.setState(DrivingState.REFUSED);
        driving.getDriver().setDriverState(DriverState.ACTIVE);
        drivingService.save(driving);
        driverService.save(driving.getDriver());
    }
}
