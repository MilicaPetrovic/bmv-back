package kts.nwt.ktsnwt.service.driving.interfaces;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.dto.admin.AdminClientDrivingHistoryDTO;
import kts.nwt.ktsnwt.dto.driving_history.ClientDetailedDrivingDTO;
import kts.nwt.ktsnwt.dto.driving_history.ClientDrivingHistoryDTO;
import kts.nwt.ktsnwt.dto.location.RouteDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.UnauthorizedRequest;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface ClientDrivingHistoryService {
    List<ClientDrivingHistoryDTO> getDrivingHistoryByClient(String username, PageRequest pageRequest) throws NoUserFoundException;

    List<AdminClientDrivingHistoryDTO> getDrivingHistoryByClientForAdmin(String id, PageRequest of) throws NoUserFoundException;

    int getDrivingHistoryByClientCount(String username);

    RouteDTO getRouteByDrivingId(Long id);

    List<ClientDrivingHistoryDTO> getDrivingHistoryByClientSortedByPrice(String loggedInUser, PageRequest of) throws NoUserFoundException;

    List<ClientDrivingHistoryDTO> getDrivingHistoryByClientSortedByDeparture(String loggedInUser, PageRequest of) throws NoUserFoundException;

    List<ClientDrivingHistoryDTO> getDrivingHistoryByClientSortedByArrival(String loggedInUser, PageRequest of) throws NoUserFoundException;

    List<ClientDrivingHistoryDTO> getDrivingHistoryByClientSortedByOrderTime(String loggedInUser, PageRequest of) throws NoUserFoundException;

    List<ClientDrivingHistoryDTO> getDrivingHistoryByClientSortedByStartTime(String loggedInUser, PageRequest of) throws NoUserFoundException;

    List<ClientDrivingHistoryDTO> getDrivingHistoryByClientSortedByEndTime(String loggedInUser, PageRequest of) throws NoUserFoundException;

    ClientDetailedDrivingDTO getDetailedDrivingCheckIfUserIsPassenger(String username, long drivingId) throws NotFoundException, NoUserFoundException, UnauthorizedRequest;


}
