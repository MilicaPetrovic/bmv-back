package kts.nwt.ktsnwt.service.driving;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.driving.DrivingPayment;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.notification.DetailedDrivingNotificationDTO;
import kts.nwt.ktsnwt.exception.*;
import kts.nwt.ktsnwt.repository.DrivingPaymentRepository;
import kts.nwt.ktsnwt.service.driving.interfaces.DriverAssignmentService;
import kts.nwt.ktsnwt.service.driving.interfaces.DriverDrivingHistoryService;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.driving.interfaces.PassengersService;
import kts.nwt.ktsnwt.service.notification.NotificationService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class PassengersServiceImpl implements PassengersService {

    @Autowired
    private UserService userService;

    @Autowired
    private DriverDrivingHistoryService driverDrivingHistoryService;

    @Autowired
    private DrivingService drivingService;

    @Autowired
    private DrivingPaymentRepository drivingPaymentRepository;

    @Autowired
    private DriverAssignmentService driverAssignmentService;

    @Autowired
    private NotificationService notificationService;

    @Transactional
    @Override
    public List<User> getPassangersByDriving(Driving driving) {
        List<User> passangers = new ArrayList<>();
        for (DrivingPayment dp : driving.getPassengers()) {
            passangers.add(userService.findById(dp.getRegisteredUser().getId()));
        }
        return passangers;
    }

    @Transactional
    public List<User> getPassengerByDrivingId(long drivingId) throws CostumNotFoundException {
        Driving driving = driverDrivingHistoryService.getDrivingByIdWithDrivingPayment(drivingId);
        return getPassangersByDriving(driving);
    }

    @Override
    public List<DetailedDrivingNotificationDTO> confirmDriving(long drivingId, String username) throws NoUserFoundException, NotValidException {
        Driving driving = drivingService.getDrivingById(drivingId);
        User user = userService.findByUsername(username);
        if (!driving.hasPassenger(user))
            throw new NotValidException("User is not a passenger");
        DrivingPayment drivingPayment = driving.confirmPassenger(user);
        drivingPaymentRepository.save(drivingPayment);


        try {
            if (driving.everybodyHasConfirmed()) {
                driverAssignmentService.handleDriver(Clock.systemDefaultZone(), driving);
                return notificationService.makeDriverFoundNotification(driving);
            }
        } catch (NoAvailableDriverException e) {
            return notificationService.makeDriverNotFoundNotification(driving);
        }
        return new ArrayList<>();
    }

    @Override
    public Driving linkUsersInDriving(String applicantUsername, List<String> linkedUsers, Driving driving) throws NoUserFoundException, RegisteredUserExceptedException, CanNotMakeNewDrivingException, NotEnoughTokensException {
        if (linkedUsers == null)
            linkedUsers = new ArrayList<>();
        driving.addPassenger(makePaymentForUserWithCredential(applicantUsername, (linkedUsers.size() + 1), driving, false, true));

        for (String credential : linkedUsers) {
            DrivingPayment passenger = makePaymentForUserWithCredential(credential, (linkedUsers.size() + 1), driving, false, false);
            driving.addPassenger(passenger);
        }
        return driving;
    }

    private DrivingPayment makePaymentForUserWithCredential(String username, int size, Driving driving, boolean paid, boolean confirmed) throws NoUserFoundException, RegisteredUserExceptedException, CanNotMakeNewDrivingException, NotEnoughTokensException {
        User uApplicant = getUserByCredential(username);
        double price = driving.getPrice() / size;
        if (!((RegisteredUser)uApplicant).hasEnoughToken(price)){
            throw new NotEnoughTokensException("User: " + username + " does not have enough tokens");
        }
        if (!canMakeNewDriving(uApplicant.getUsername())) {
            System.out.println(" baci CanNotMakeNewDrivingException ");
            throw new CanNotMakeNewDrivingException("User can not make a new driving");
        }
        return new DrivingPayment(price, confirmed, paid, driving, (RegisteredUser) uApplicant);
    }

    private User getUserByCredential(String credential) throws NoUserFoundException, RegisteredUserExceptedException {
        User u = userService.findByUsernameNotBlocked(credential);
        if (!u.isRegisteredUser()) {
            throw new RegisteredUserExceptedException("Registered user excepted, got user: " + u.getUsername() + ", with role: " + u.getRole().toString());
        }
        return u;
    }

    private boolean canMakeNewDriving(String applicantUsername) {
        List<Driving> activeDriving = drivingService.getActiveDrivingByClient(applicantUsername);
        for (Driving d : activeDriving) {
            if (d.isStillActive()) {
                System.out.println(" can make vrati FALSE");
                return false;
            }
        }
        System.out.println(" can make vrati TRUE");
        return true;
    }
}
