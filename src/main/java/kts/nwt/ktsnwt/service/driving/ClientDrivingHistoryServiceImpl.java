package kts.nwt.ktsnwt.service.driving;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.location.Address;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.admin.AdminClientDrivingHistoryDTO;
import kts.nwt.ktsnwt.dto.driving_history.ClientDetailedDrivingDTO;
import kts.nwt.ktsnwt.dto.driving_history.ClientDrivingHistoryDTO;
import kts.nwt.ktsnwt.dto.location.MapPointDTO;
import kts.nwt.ktsnwt.dto.location.RouteDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.UnauthorizedRequest;
import kts.nwt.ktsnwt.repository.*;
import kts.nwt.ktsnwt.service.driving.interfaces.ClientDrivingHistoryService;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
@Transactional
public class ClientDrivingHistoryServiceImpl implements ClientDrivingHistoryService {
    @Autowired
    private DrivingRepository drivingRepository;

    @Autowired
    private UserService userService;

    @Autowired

    private DrivingService drivingService;


    @Override
    public List<ClientDrivingHistoryDTO> getDrivingHistoryByClient(String username, PageRequest pageRequest) throws NoUserFoundException {
        User u = userService.findByUsername(username);
        List<Driving> res = drivingRepository.getDrivingsByClient(u.getId(), pageRequest);
        List<ClientDrivingHistoryDTO> resultList = convertIntoClientDrivingHistoryDTO(res);
        return resultList;
    }

    @Override
    @Transactional
    public List<AdminClientDrivingHistoryDTO> getDrivingHistoryByClientForAdmin(String username, PageRequest of) throws NoUserFoundException {
        User user = userService.findByUsername(username);
        List<Driving> res = drivingRepository.getDrivingsByClientPaged(user.getId(), of);
        List<AdminClientDrivingHistoryDTO> resultList = new ArrayList<>();
        for (Driving d : res){
            resultList.add(new AdminClientDrivingHistoryDTO(d));
      }
        return resultList;
    }

    @Override
    public int getDrivingHistoryByClientCount(String username) {
        return drivingRepository.getCountOfDrivingsByClient(username);
    }

    @Override
    public RouteDTO getRouteByDrivingId(Long id) {
        Driving driving = drivingRepository.getOne(id);
        List<Address> routePoints = driving.getPath().getRoute().getRoute();

        ArrayList<MapPointDTO> points = new ArrayList<MapPointDTO>();
        for(Address a : routePoints){
            points.add(new MapPointDTO(a.getLatitude(), a.getLongitude()));
        }

        List<Address> path = driving.getPath().getStations();
        ArrayList<MapPointDTO> pathPoints = new ArrayList<>();
        for(Address a : path){
            pathPoints.add(new MapPointDTO(a.getLatitude(), a.getLongitude(), a.getName()));
        }
        RouteDTO routeDTO = new RouteDTO(pathPoints, points,driving.getTime(),driving.getDistance(),driving.getPrice());
        return routeDTO;
    }

    @Override
    public List<ClientDrivingHistoryDTO> getDrivingHistoryByClientSortedByPrice(String username, PageRequest of) throws NoUserFoundException {
        List<ClientDrivingHistoryDTO> result = getAllClientDrivingHistory(username);
        result.sort(new Comparator<ClientDrivingHistoryDTO>() {
            @Override
            public int compare(ClientDrivingHistoryDTO c1, ClientDrivingHistoryDTO c2) {
                return (int) (c1.getPrice() - c2.getPrice());
            }
        });
        return getRequestedPage(result, of);
    }

    @Override
    public List<ClientDrivingHistoryDTO> getDrivingHistoryByClientSortedByDeparture(String loggedInUser, PageRequest of) throws NoUserFoundException {
        List<ClientDrivingHistoryDTO> result = getAllClientDrivingHistory(loggedInUser);
        result.sort(new Comparator<ClientDrivingHistoryDTO>() {
            @Override
            public int compare(ClientDrivingHistoryDTO c1, ClientDrivingHistoryDTO c2) {
                return (c1.getDeparture().compareTo(c2.getDeparture()));
            }
        });
        return getRequestedPage(result, of);
    }

    @Override
    public List<ClientDrivingHistoryDTO> getDrivingHistoryByClientSortedByArrival(String loggedInUser, PageRequest of) throws NoUserFoundException {
        List<ClientDrivingHistoryDTO> result = getAllClientDrivingHistory(loggedInUser);
        result.sort(new Comparator<ClientDrivingHistoryDTO>() {
            @Override
            public int compare(ClientDrivingHistoryDTO c1, ClientDrivingHistoryDTO c2) {
                return (c1.getArrival().compareTo(c2.getArrival()));
            }
        });
        return getRequestedPage(result, of);
    }

    @Override
    public List<ClientDrivingHistoryDTO> getDrivingHistoryByClientSortedByStartTime(String loggedInUser, PageRequest of) throws NoUserFoundException {
        List<Driving> res = drivingRepository.getDrivingsByClientsUsername(loggedInUser);
        res.sort(new Comparator<Driving>() {
            @Override
            public int compare(Driving c1, Driving c2) {
                if (c1.getDrivingStartTime() == null)
                    return 1;
                if (c2.getDrivingStartTime() == null)
                    return -1;
                return (c1.getDrivingStartTime()).compareTo(c2.getDrivingStartTime());
            }
        });
        Collections.reverse(res);
        return getRequestedPage(convertIntoClientDrivingHistoryDTO(res), of);
    }

    @Override
    public List<ClientDrivingHistoryDTO> getDrivingHistoryByClientSortedByEndTime(String loggedInUser, PageRequest of) throws NoUserFoundException {
        //List<ClientDrivingHistoryDTO> result = getAllClientDrivingHistory(loggedInUser);
        List<Driving> res = drivingRepository.getDrivingsByClientsUsername(loggedInUser);
        res.sort(new Comparator<Driving>() {
            @Override
            public int compare(Driving c1, Driving c2) {
                if (c1.getDrivingEndTime() == null)
                    return 1;
                if (c2.getDrivingEndTime() == null)
                    return -1;
                return (c1.getDrivingEndTime()).compareTo(c2.getDrivingEndTime());
            }
        });
        Collections.reverse(res);
        return getRequestedPage(convertIntoClientDrivingHistoryDTO(res), of);
    }

    @Override
    public ClientDetailedDrivingDTO getDetailedDrivingCheckIfUserIsPassenger(String username, long drivingId) throws NotFoundException, NoUserFoundException, UnauthorizedRequest {
        //check
        Driving d = drivingService.getDrivingById(drivingId);
        User u = userService.findByUsername(username);
        if (d.hasPassenger(u))
            return new ClientDetailedDrivingDTO(drivingService.getDetailedDriving(drivingId), d, u);
        throw new UnauthorizedRequest("Unauthorized");
    }

    @Override
    public List<ClientDrivingHistoryDTO> getDrivingHistoryByClientSortedByOrderTime(String loggedInUser, PageRequest of) throws NoUserFoundException {
        List<ClientDrivingHistoryDTO> result = getAllClientDrivingHistory(loggedInUser);
        result.sort(new Comparator<ClientDrivingHistoryDTO>() {
            @Override
            public int compare(ClientDrivingHistoryDTO c1, ClientDrivingHistoryDTO c2) {
                return (c1.getOrderTime().compareTo(c2.getOrderTime()));
            }
        });
        Collections.reverse(result);
        return getRequestedPage(result, of);
    }

    private List<ClientDrivingHistoryDTO> convertIntoClientDrivingHistoryDTO(List<Driving> res) {
        List<ClientDrivingHistoryDTO> result = new ArrayList<>();
        for (Driving d : res){
            result.add(new ClientDrivingHistoryDTO(d));
        }
        return result;
    }

    private List<ClientDrivingHistoryDTO> getAllClientDrivingHistory(String username){
        List<Driving> res = drivingRepository.getDrivingsByClientsUsername(username);
        return convertIntoClientDrivingHistoryDTO(res);
    }

    private List<ClientDrivingHistoryDTO> getRequestedPage(List<ClientDrivingHistoryDTO> result, PageRequest of) {
        int from = of.getPageNumber()*of.getPageSize();
        int until = from + of.getPageSize();
        if (until > result.size())
            until = result.size();
        return result.subList(from, until);
    }
}
