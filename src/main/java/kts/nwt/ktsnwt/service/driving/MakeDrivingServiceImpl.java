package kts.nwt.ktsnwt.service.driving;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.communication.Notification;
import kts.nwt.ktsnwt.beans.driving.*;
import kts.nwt.ktsnwt.beans.location.Path;
import kts.nwt.ktsnwt.dto.driving.MakeDrivingDTO;
import kts.nwt.ktsnwt.exception.*;
import kts.nwt.ktsnwt.service.driving.interfaces.*;
import kts.nwt.ktsnwt.exception.CanNotMakeNewDrivingException;
import kts.nwt.ktsnwt.exception.NoAvailableDriverException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.RegisteredUserExceptedException;
import kts.nwt.ktsnwt.service.driving.interfaces.DriverAssignmentService;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.driving.interfaces.MakeDrivingService;
import kts.nwt.ktsnwt.service.notification.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;
import java.util.List;

@Service
public class MakeDrivingServiceImpl implements MakeDrivingService {

    @Autowired
    private ChosenAdditionalServicesService chosenAdditionalServicesService;

    @Autowired
    private DrivingService drivingService;

    @Autowired
    private PathService pathService;

    @Autowired
    private PassengersService passengersService;

    @Autowired
    private DriverAssignmentService driverAssignmentService;
    @Autowired
    private NotificationService notificationService;

    @Transactional(rollbackFor = Exception.class)
    public Pair<Long, List<Notification>> makeDrivingAndReturnNotificationsForPassengers(MakeDrivingDTO dto) throws NoUserFoundException, RegisteredUserExceptedException, NoAvailableDriverException, CanNotMakeNewDrivingException, NotFoundException, NotEnoughTokensException, InvalidFutureTimeException, NoPathGivenException {

        ChosenAdditionalServices chosenAdditionalServices = chosenAdditionalServicesService.makeChosenAdditionalServices(dto.getVehicleTypeId(), dto.isPetFriendly(), dto.isBabyFriendly());

        Path path =this.pathService.save(dto.getStations(), dto.getCoordinates(), dto.getDistance(), dto.getTime(), dto.isFavouriteRoute(), dto.getApplicantUsername());

        Driving driving = drivingService.save(Clock.systemDefaultZone(), chosenAdditionalServices, path, dto.getPrice(), dto.getTime(), dto.isFuture(), dto.getHour(), dto.getMinute());

        Driving drivingWithPassengers = passengersService.linkUsersInDriving(dto.getApplicantUsername(), dto.getLinkedUsers(), driving);

        List<Notification> notifications = notificationService.createSplitFareConfirmationNotification(drivingWithPassengers);//create notification to accepted

        if (notifications.isEmpty() && !driving.isFuture())
            driverAssignmentService.handleDriver(Clock.systemDefaultZone(), driving);

        return Pair.of(driving.getId(), notifications);
    }
}
