package kts.nwt.ktsnwt.service.driving.interfaces;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.dto.location.SimulationVehicleDTO;
import kts.nwt.ktsnwt.dto.report.InconsistencyDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;

import java.io.IOException;
import java.util.List;

public interface VehicleTypeService {
    SimulationVehicleDTO updateVehicleLocation(Long driverId, double latitude, double longitude);
    boolean checkConsistency(long driverId, long drivingId, double latitude, double longitude);
    public List<InconsistencyDTO> getPassenger(long driverId, long drivingId) throws NotFoundException;
    public double getTimeToCome(long driverId, long drivingId, double latitude, double longitude) throws IOException, InterruptedException;
}
