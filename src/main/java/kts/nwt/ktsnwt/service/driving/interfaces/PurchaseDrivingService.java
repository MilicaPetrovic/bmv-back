package kts.nwt.ktsnwt.service.driving.interfaces;

import kts.nwt.ktsnwt.dto.driving.PurchaseDrivingDTO;
import kts.nwt.ktsnwt.exception.NoAvailableDriverException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotAllUsersConfirmedException;
import kts.nwt.ktsnwt.exception.NotEnoughTokensException;

public interface PurchaseDrivingService {

    boolean purchaseDriving(PurchaseDrivingDTO dto) throws NoUserFoundException, NotEnoughTokensException, NotAllUsersConfirmedException, NoAvailableDriverException;
}
