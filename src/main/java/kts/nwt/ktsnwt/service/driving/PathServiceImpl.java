package kts.nwt.ktsnwt.service.driving;

import kts.nwt.ktsnwt.beans.location.Address;
import kts.nwt.ktsnwt.beans.location.Path;
import kts.nwt.ktsnwt.beans.location.Route;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.dto.location.MapPointDTO;
import kts.nwt.ktsnwt.exception.NoPathGivenException;
import kts.nwt.ktsnwt.repository.PathRepository;
import kts.nwt.ktsnwt.repository.RegisteredUserRepository;
import kts.nwt.ktsnwt.service.driving.interfaces.PathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
@Transactional
public class PathServiceImpl implements PathService {
    @Autowired
    private PathRepository pathRepository;
    @Autowired
    private RegisteredUserRepository registeredUserRepository;


    @Override
    public Path save(ArrayList<MapPointDTO> chosenStations, ArrayList<MapPointDTO> coordinates, double distance, double time, boolean isFavourite, String username) throws NoPathGivenException {

        Path path = new Path();

        addStations(path, chosenStations);
        addRoute(path, coordinates, distance, time);

        Path savedPath = pathRepository.save(path);

        if (isFavourite){
            RegisteredUser registeredUser = registeredUserRepository.findByUsername(username).orElse(null);
            registeredUser.addFavouriteRoute(savedPath.getRoute());//getFavouriteRoute().add(savedPath.getRoute());
            registeredUserRepository.save(registeredUser);
        }
        return savedPath;
    }

    private void addRoute(Path path, ArrayList<MapPointDTO> coordinates, double distance, double time) throws NoPathGivenException {
        Route r = new Route();
        if (coordinates.isEmpty())
            throw new NoPathGivenException("No coordinates given");
        ArrayList<Address> route = new ArrayList<>();
        for(MapPointDTO current : coordinates) {
            Address a = new Address(current.getLng(), current.getLat(), current.getName());
            a.setRoute(r);
            route.add(a);
        }
        r.setRoute(route);
        r.setTime(time);
        r.setDistance(distance);
        path.setRoute(r);
    }

    private void addStations(Path path, ArrayList<MapPointDTO> chosenStations) throws NoPathGivenException {
        ArrayList<Address> stations = new ArrayList<>();
        if (chosenStations == null)
            throw new NullPointerException("Chosen stations is null");
        if (chosenStations.isEmpty())
            throw new NoPathGivenException("No chosen station given");
        for(MapPointDTO current : chosenStations){
            Address a = new Address(current.getLng(), current.getLat(), current.getName());
            a.setPath(path);
            stations.add(a);
        }
        path.setStations(stations);
    }
}
