package kts.nwt.ktsnwt.service.driving;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.driving.*;
import kts.nwt.ktsnwt.beans.location.Address;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.dto.location.SimulationVehicleDTO;
import kts.nwt.ktsnwt.dto.report.InconsistencyDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.repository.DriverRepository;
import kts.nwt.ktsnwt.repository.DrivingRepository;
import kts.nwt.ktsnwt.repository.VehicleTypeRepository;
import kts.nwt.ktsnwt.service.driving.interfaces.VehicleTypeService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;

@Service
@Transactional
public class VehicleTypeServiceImpl implements VehicleTypeService {
    @Autowired
    private VehicleTypeRepository vehicleTypeRepository;
    @Autowired
    private DriverRepository driverRepository;

    @Autowired
    private DrivingRepository drivingRepository;

    private Map<Long, Integer> calls = new HashMap<>();

    public ArrayList<VehicleType> getAllVehicleTypes() {
        return (ArrayList<VehicleType>) vehicleTypeRepository.findAll();
    }

    @Override
    public SimulationVehicleDTO updateVehicleLocation(Long driverId, double latitude, double longitude) {
        Driver driver = this.driverRepository.findDriverById(driverId).orElseThrow(() -> {
            throw new NoUserFoundException("No not blocked user was found");
        });;
        driver.getCurrentAddress().setLatitude(latitude);
        driver.getCurrentAddress().setLongitude(longitude);
        this.driverRepository.save(driver);
        Vehicle vehicle = driver.getVehicle();
        SimulationVehicleDTO simulationVehicleDTO = new SimulationVehicleDTO(vehicle.getId(), vehicle.getLabel(),
                driver.getCurrentAddress().getLatitude(), driver.getCurrentAddress().getLongitude(), driver.getId());
        return simulationVehicleDTO;
    }

    @Override
    public boolean checkConsistency(long driverId, long drivingId, double latitude, double longitude) {
        System.out.println(" IIIII DDDDD " + drivingId);
        Driving driving = this.drivingRepository.findDrivingById(drivingId).orElse(null);
        if ((!this.calls.containsKey(driverId))) {
            this.calls.put(driverId, 0);
            return true;
        } else if (driving.getState() == DrivingState.CURRENT) {
            System.out.println("$$$$ " + driving.getState().name()  + " id driving: " + driving.getId());
                int current = this.calls.get(driverId) + 1;
                this.calls.put(driverId, current);
                List<Address> points = driving.getPath().getRoute().getRoute();
                if (points.size() <= current) {
                    Address a = points.get(points.size() - 1);
                    return compareIfNear(a, latitude, longitude);
                } else {
                    return compareIfNear(points.get(current), latitude, longitude);
                }
        }
        return true;
    }

    private boolean compareIfNear(Address a, double latitude, double longitude) {
        System.out.println("point from baza:    lat: " + a.getLatitude() + " lon" + a.getLongitude());
        System.out.println(" nova point:    lat: " + latitude + " lon: " + longitude);
        System.out.println(" razlika  lat: " + Math.abs(a.getLatitude() - latitude) + "  raylonka lloon  " +
                Math.abs(a.getLongitude() - longitude));
        if ((Math.abs(a.getLatitude() - latitude) > 0.001) || (Math.abs(a.getLongitude() - longitude) > 0.001)) {
            return false;
        }
        return true;
    }

    @Override
    public List<InconsistencyDTO> getPassenger(long driverId, long drivingId) throws NotFoundException {
        Driving driving = this.drivingRepository.findDrivingById(drivingId).orElse(null);
        if(driving == null)
            throw new NotFoundException("Driving is not found");
        List<InconsistencyDTO> inconsistencyDTOS = new ArrayList<>();
        for (DrivingPayment drivingPayment : driving.getPassengers()) {
            inconsistencyDTOS.add(new InconsistencyDTO(driverId, driving.getId(), drivingPayment.getPassengerId(),
                    drivingPayment.getPassangerUsername()));
        }
        return inconsistencyDTOS;
    }


    private double getDistanceBetweenDriverAndDeparture(double driverLat, double driverLon, Address passAddress) throws IOException, InterruptedException {
        double lat1 = driverLat;
        double long1 = driverLon;
        double lat2 = passAddress.getLatitude();
        double long2 = passAddress.getLongitude();
        System.out.println("lat 1: " + lat1 + " lon1: " + long1);
        System.out.println("lat 2: " + lat2 + " long2: " + long2);
        String key = "AmVePoOhWRnb6gZq-k_QZvydI33UHUeqxlZQ8-q2k4phoJRmoVsjN5lZLE2L0Ef5";
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://dev.virtualearth.net/REST/v1/Routes?wp.0=" + lat1 + "," + long1 + "&wp.1=" + lat2
                        + "," + long2 + "&optimize=timeWithTraffic&routeAttributes=routeSummariesOnly&distanceUnit=km&key=" + key))
                .header("Content-Type", "application/json")
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        return extractTimeToComeFromResponse(response.body());
    }


    private double extractTimeToComeFromResponse(String body) {
        System.out.println(body);

        JSONArray array = new JSONArray("[" + body + "]");
        Double time = 0.00;
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);
            JSONArray array2 = object.getJSONArray("resourceSets");
            for (int j = 0; j < array2.length(); j++) {
                JSONObject resourceObject = array2.getJSONObject(j);
                JSONArray resourceArray = resourceObject.getJSONArray("resources");
                for (int k = 0; k < resourceArray.length(); k++) {
                    JSONObject distanceObject = resourceArray.getJSONObject(k);
                    time = distanceObject.getDouble("travelDuration");
                    System.out.println("distanca dobijena pozivom apija");
                    System.out.println("distanceee:   " + time);
                }
            }
        }
        return time;
    }

    @Override
    public double getTimeToCome(long driverId, long drivingId, double latitude, double longitude) throws IOException, InterruptedException {
        Driving driving = this.drivingRepository.findDrivingById(drivingId).orElse(null);
        Address dest = driving.getPath().getStations().get(0);
        double time = getDistanceBetweenDriverAndDeparture(latitude, longitude, dest);
        return time;
    }
}
