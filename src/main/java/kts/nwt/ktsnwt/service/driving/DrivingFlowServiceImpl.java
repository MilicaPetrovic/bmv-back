package kts.nwt.ktsnwt.service.driving;

import kts.nwt.ktsnwt.beans.communication.NotificationType;
import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.driving.DrivingState;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.DriverState;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.notification.NewNotificationDTO;
import kts.nwt.ktsnwt.exception.AlreadyEndedException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotValidException;
import kts.nwt.ktsnwt.exception.ShouldNotHappenException;
import kts.nwt.ktsnwt.repository.DriverRepository;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingFlowService;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.notification.NotificationService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DrivingFlowServiceImpl implements DrivingFlowService {

    @Autowired
    private UserService userService;

    @Autowired
    private DrivingService drivingService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private DriverRepository driverRepository;

    @Override
    public NewNotificationDTO drivingEnded(String driverUsername, long drivingId) throws NoUserFoundException, NotValidException, AlreadyEndedException, ShouldNotHappenException {
        User u = userService.findByUsername(driverUsername);
        if (!u.isDriver())//da li vozac
            throw new NotValidException("Driver awaited");
        Driving driving = drivingService.getDrivingById(drivingId);
        if (!driving.hasAsDriver(u))// da li je vozac koji je dodeljen voznji
            throw new NotValidException("Not right driver");
        if (!driving.canEnded())//da li moze da postane zavrsen
            throw new NotValidException("Not valid action");
        updateDriverState((Driver) u);
        saveEndDate(driving);// save end time
        notificationService.saveRatingNotifications(driving);//Save notification for users
        return createNotificationDTO(driving);
    }

    public Driver updateDriverState(Driver u) {
        u.setDriverState(DriverState.ACTIVE);
        driverRepository.save(u);
        return u;
    }

    private NewNotificationDTO createNotificationDTO(Driving driving) {
        NewNotificationDTO notificationDTO = new NewNotificationDTO();
        notificationDTO.setDate(LocalDateTime.now());
        notificationDTO.setType(NotificationType.RATE_DRIVING.toString());
        notificationDTO.setOrderTime(driving.getOrderTime());
        notificationDTO.setDriverAssigned(true);
        notificationDTO.setDriverName(driving.getDriversName());
        notificationDTO.setDrivingId(driving.getId());
        notificationDTO.setCanMakeAnAction(true);
        return notificationDTO;
    }


    private void saveEndDate(Driving driving) {
        driving.ended(LocalDateTime.now());
        drivingService.save(driving);
    }
}
