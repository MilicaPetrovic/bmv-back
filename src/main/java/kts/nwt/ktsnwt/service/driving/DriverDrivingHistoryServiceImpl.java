package kts.nwt.ktsnwt.service.driving;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.admin.AdminDriverDrivingHistoryDTO;
import kts.nwt.ktsnwt.dto.driving.DetailedDrivingDTO;
import kts.nwt.ktsnwt.dto.driving_history.DriverDrivingHistoryDTO;
import kts.nwt.ktsnwt.exception.CostumNotFoundException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.UnauthorizedRequest;
import kts.nwt.ktsnwt.repository.*;
import kts.nwt.ktsnwt.service.driving.interfaces.DriverDrivingHistoryService;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
@Transactional
public class DriverDrivingHistoryServiceImpl implements DriverDrivingHistoryService {
    @Autowired
    private DrivingRepository drivingRepository;

    @Autowired
    private UserService userService;

    @Autowired
    TimePeriodRepository timePeriodRepository;

    @Autowired
    private DrivingPaymentRepository drivingPaymentRepository;

    @Autowired
    private ReportRepository reportsRepository;


    @Autowired
    private DrivingService drivingService;

    @Override
    public List<DriverDrivingHistoryDTO> getDrivingHistoryByDriverForDriver(String username, PageRequest of){
        List<Driving> res = drivingRepository.getDrivingsByDriver(username, of);
        return convertIntoDriverDrivingHistoryDTO(res);
    }

    @Override
    public Driving getDrivingByIdWithDrivingPayment(Long drivingId) throws CostumNotFoundException {
        Driving driving = drivingRepository.getDrivingWithPassengers(drivingId).orElse(null);
        if (driving == null)
            throw new CostumNotFoundException("Driving not found");
        if (driving.getDrivingTime() != null)
            driving.setDrivingTime(timePeriodRepository.findById(driving.getDrivingTime().getId()).orElse(null));

        driving.setReports(reportsRepository.findByDriving_Id(drivingId));
        return driving;
    }

    @Override
    public List<AdminDriverDrivingHistoryDTO> getDrivingHistoryByDriver(String username, PageRequest of) throws NoUserFoundException {
        User user = userService.findByUsername(username);
        List<Driving> drivings = drivingRepository.getDrivingsByDriverPaged(user.getId(), of);
        List<AdminDriverDrivingHistoryDTO> resultList = new ArrayList<>();
        for (Driving d : drivings){
            resultList.add(new AdminDriverDrivingHistoryDTO(d));
        }
        return resultList;
    }

    @Override
    public int getDrivingHistoryByDriverCount(String username) {
        return drivingRepository.getCountOfDrivingsByDriver(username);
    }

    @Override
    public List<DriverDrivingHistoryDTO> getDrivingHistoryByDriverSortedByPrice(String username, PageRequest of) throws NoUserFoundException {
        List<DriverDrivingHistoryDTO> result = getAllDriverDrivingHistory(username);
        result.sort(new Comparator<DriverDrivingHistoryDTO>() {
            @Override
            public int compare(DriverDrivingHistoryDTO c1, DriverDrivingHistoryDTO c2) {
                return (int) (c1.getPrice() - c2.getPrice());
            }
        });
        return getRequestedPage(result, of);
    }

    @Override
    public List<DriverDrivingHistoryDTO> getDrivingHistoryByDriverSortedByDeparture(String loggedInUser, PageRequest of) throws NoUserFoundException {
        List<DriverDrivingHistoryDTO> result = getAllDriverDrivingHistory(loggedInUser);
        result.sort(new Comparator<DriverDrivingHistoryDTO>() {
            @Override
            public int compare(DriverDrivingHistoryDTO c1, DriverDrivingHistoryDTO c2) {
                return c1.getDeparture().compareTo(c2.getDeparture());
            }
        });
        return getRequestedPage(result, of);
    }

    @Override
    public List<DriverDrivingHistoryDTO> getDrivingHistoryByDriverSortedByArrival(String loggedInUser, PageRequest of) throws NoUserFoundException {
        List<DriverDrivingHistoryDTO> result = getAllDriverDrivingHistory(loggedInUser);
        result.sort(new Comparator<DriverDrivingHistoryDTO>() {
            @Override
            public int compare(DriverDrivingHistoryDTO c1, DriverDrivingHistoryDTO c2) {
                return c1.getArrival().compareTo(c2.getArrival());
            }
        });
        return getRequestedPage(result, of);
    }

    @Override
    public List<DriverDrivingHistoryDTO> getDrivingHistoryByDriverSortedByStartTime(String loggedInUser, PageRequest of) throws NoUserFoundException {
        List<Driving> result = drivingRepository.getDrivingsByDriver(loggedInUser);
        result.sort(new Comparator<Driving>() {
            @Override
            public int compare(Driving c1, Driving c2) {
                if (c1.getDrivingStartTime() == null)
                    return 1;
                if (c2.getDrivingStartTime() == null)
                    return -1;
                return (c1.getDrivingStartTime()).compareTo(c2.getDrivingStartTime());
            }
        });
        Collections.reverse(result);
        return getRequestedPage(convertIntoDriverDrivingHistoryDTO(result), of);
    }

    @Override
    public List<DriverDrivingHistoryDTO> getDrivingHistoryByDriverSortedByEndTime(String loggedInUser, PageRequest of) throws NoUserFoundException {
        List<Driving> result = drivingRepository.getDrivingsByDriver(loggedInUser);
        result.sort(new Comparator<Driving>() {
            @Override
            public int compare(Driving c1, Driving c2) {
                if (c1.getDrivingEndTime() == null)
                    return 1;
                if (c2.getDrivingEndTime() == null)
                    return -1;
                return (c1.getDrivingEndTime()).compareTo(c2.getDrivingEndTime());
            }
        });
        Collections.reverse(result);
        return getRequestedPage(convertIntoDriverDrivingHistoryDTO(result), of);
    }

    @Override
    public DetailedDrivingDTO getDetailedDrivingCheckIfUserIsDriver(String username, Long drivingId) throws NoUserFoundException, UnauthorizedRequest, NotFoundException {
        //check
        Driving d = drivingRepository.findDrivingById(drivingId).orElse(null);
        User u = userService.findByUsername(username);
        if (d.hasAsDriver(u))
            return drivingService.getDetailedDriving(drivingId);
        throw new UnauthorizedRequest("Unauthorized");
    }


    private List<DriverDrivingHistoryDTO> convertIntoDriverDrivingHistoryDTO(List<Driving> res) {
        List<DriverDrivingHistoryDTO> result = new ArrayList<>();
        for (Driving d : res){
            result.add(new DriverDrivingHistoryDTO(d));
        }
        return result;
    }

    private List<DriverDrivingHistoryDTO> getAllDriverDrivingHistory(String username){
        List<Driving> res = drivingRepository.getDrivingsByDriver(username);
        return convertIntoDriverDrivingHistoryDTO(res);
    }

    private List<DriverDrivingHistoryDTO> getRequestedPage(List<DriverDrivingHistoryDTO> result, PageRequest of) {
        int from = of.getPageNumber()*of.getPageSize();
        int until = from + of.getPageSize();
        if (until > result.size())
            until = result.size();
        return result.subList(from, until);
    }
}
