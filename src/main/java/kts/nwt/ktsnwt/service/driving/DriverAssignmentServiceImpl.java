package kts.nwt.ktsnwt.service.driving;

import kts.nwt.ktsnwt.beans.driving.*;
import kts.nwt.ktsnwt.beans.location.Address;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.DriverState;
import kts.nwt.ktsnwt.exception.NoAvailableDriverException;
import kts.nwt.ktsnwt.repository.DriverRepository;
import kts.nwt.ktsnwt.repository.DrivingRepository;
import kts.nwt.ktsnwt.service.driving.interfaces.DriverAssignmentService;
import kts.nwt.ktsnwt.service.notification.NotificationService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

@Service
@Transactional
public class DriverAssignmentServiceImpl implements DriverAssignmentService {
    @Autowired
    DriverRepository driverRepository;
    @Autowired
    DrivingRepository drivingRepository;
    @Autowired
    NotificationService notificationService;


    public Driving handleDriver(Clock clock, Driving driving) throws NoAvailableDriverException {
        try {
            List<Driving> futureDriving = this.drivingRepository.getFutureDrivingFromPeriodWithoutDriver(true, LocalDateTime.now(clock), LocalDateTime.now(clock).plusMinutes(10));
            List<Driver> availableDrivers = this.driverRepository.getActiveDrivers();
            System.out.println("future size: " + futureDriving.size());
            System.out.println(" availanle drivers: " + availableDrivers.size());
            if (availableDrivers.size() <= futureDriving.size() && !driving.isFuture() && !availableDrivers.isEmpty()) {
                driving.setState(DrivingState.REFUSED);
                drivingRepository.save(driving);
                throw new NoAvailableDriverException("Driving can not be made");
            } else {//driving.getId()
                Driver driver = getAvailableDriver(driving);
                driver.setDriverState(DriverState.DRIVING);
                driverRepository.save(driver);
                driving.setDriver(driver);
                driving.setState(DrivingState.ACCEPTED);
                drivingRepository.save(driving);
                return driving;
            }

        } catch (Exception e) {
            e.printStackTrace();
            driving.setState(DrivingState.REFUSED);
            drivingRepository.save(driving);
            throw new NoAvailableDriverException("Driving can not be made");
        }
    }

    private Driver getAvailableDriver(Driving driving) throws IOException, InterruptedException, NoAvailableDriverException {
        Driver driver;
        //Driving driving = this.drivingRepository.getDrivingById(drivingId);
        //Driving driving = this.drivingRepository.findDrivingById(drivingId).orElse(null);
        driver = this.getActiveAvailableDriver(driving);
        System.out.println(driver);
        if (driver == null) {
            driver = this.getCurrentFastestDriver(driving);
            if (driver == null) {
                System.out.println(" Driving se odbija ");
                throw new NoAvailableDriverException("Trenutno nema slobodnog vozaca.");
            }
        }
        return driver;
    }

    private Driver getActiveAvailableDriver(Driving driving) throws IOException, InterruptedException, NoAvailableDriverException {
        List<Driver> activeDrivers = getActiveDrivers();
        if (!activeDrivers.isEmpty()) {
            activeDrivers = filterByWorkingHours(activeDrivers, driving);               // provara ako su leste vec prazne
            if (!activeDrivers.isEmpty()) {
                filterByAdditionalServicesAndNumberOfSeats(activeDrivers, driving);
                if (!activeDrivers.isEmpty()) {
                    System.out.println(" Postoje slobodni vozaci  ");
                    System.out.println(activeDrivers.size() + " slobodnih vozaca");
                    for (Driver d : activeDrivers) {
                        System.out.println(d.getUsername());
                    }
                    System.out.println(activeDrivers.get(0).getUsername());
                    return (activeDrivers.size() == 1) ? activeDrivers.get(0) : this.getNearestDriver(activeDrivers, driving);
                }
            }
        }
        return null;
    }

    private Driver getCurrentFastestDriver(Driving driving) throws NoAvailableDriverException {
        System.out.println(" Ne postoje slobodni vozaci, pa se traze ove koji vec voze");
        List<Driver> drivingDrivers = this.getDrivingDrivers();
        Driver theFastestDriver = null;
        if (!drivingDrivers.isEmpty()) {
            // provera za radne sate
            drivingDrivers = filterByWorkingHours(drivingDrivers, driving);
            if (!drivingDrivers.isEmpty()) {
                System.out.println(" curent vozaci: " + drivingDrivers.size());  // provera ako su vec prazne liste
                filterByAdditionalServicesAndNumberOfSeats(drivingDrivers, driving);
                if (!drivingDrivers.isEmpty()) {
                    theFastestDriver = this.getTheFastestDriver(drivingDrivers);
                }
            }
            return theFastestDriver;
        }
        return null;
    }

    private List<Driver> filterByWorkingHours(List<Driver> drivers, Driving driving) {
        List<Driver> result = new ArrayList<>();
        double activeMinutes = 0.00;
        for (Driver driver : drivers) {
            activeMinutes = getActiveMinutesInThePast24HoursWithDriving(driver, driving);
            System.out.println("-*-*-*-* driver : " + driver.getUsername() + "  working  Hours : " + activeMinutes);
            if (activeMinutes < 480) {
                result.add(driver);
            }
        }
        return result;
    }

    private double getActiveMinutesInThePast24HoursWithDriving(Driver driver, Driving driving) {
        double activeMinutes = getActiveMinutesInThePast24Hours(driver);
        Duration duration = Duration.between(driving.getDrivingStartTime(), driving.getDrivingEndTime());
        activeMinutes += duration.toMinutes();
        return activeMinutes;
    }


    public double getActiveMinutesInThePast24Hours(Driver driver) {
        List<TimePeriod> workingPeriods = driver.getActiveHours();

        double activeMinutes = 0.00;
        LocalDateTime currentMoment = LocalDateTime.now();
        LocalDateTime before24hours = currentMoment.minusHours(24);
        Duration duration;
        for (TimePeriod timePeriod : workingPeriods) {
            if (timePeriod.getEndDateTime() == null) {
                duration = Duration.between(timePeriod.getStartDateTime(), currentMoment);
                activeMinutes += duration.toMinutes();
            } else if (timePeriod.getEndDateTime().isAfter(before24hours) && timePeriod.getEndDateTime().isBefore(currentMoment)) {
                duration = Duration.between(timePeriod.getStartDateTime(), timePeriod.getEndDateTime());
                activeMinutes += duration.toMinutes();
            }
        }
        return activeMinutes;
    }

    private void filterByAdditionalServicesAndNumberOfSeats(List<Driver> activeDrivers, Driving driving) {
        ChosenAdditionalServices chosenAdditionalServices = driving.getChosenAdditionalServices();
        List<Driver> removeDrivers = new ArrayList<>();
        for (Driver driver : activeDrivers) {
            System.out.println(chosenAdditionalServices.getType().getId());
            System.out.println(driver.getVehicle().getType().getId());
            if (chosenAdditionalServices.isBabyFriendly() && !driver.getVehicle().isBabyFriendly())
                removeDrivers.add(driver);
            else if (chosenAdditionalServices.isPetFriendly() && !driver.getVehicle().isPetFriendly())
                removeDrivers.add(driver);
            else if (chosenAdditionalServices.getType().getId() != driver.getVehicle().getType().getId()) {
                removeDrivers.add(driver);
                System.out.println("ne odgovara tip");
                System.out.println("  :  " + driver.getUsername());
            }
             else if (driving.getPassengers().size() > driver.getVehicle().getNumberOfSeats())
                removeDrivers.add(driver);

             System.out.println("broj putnika u drivingu: " + driving.getPassengers().size());
             System.out.println("broj u sedista u kolima: " + driver.getVehicle().getNumberOfSeats());
        }
        System.out.println(" iybaceni vozaci zbog additional");
        for (Driver driver : removeDrivers) {
            System.out.println(driver.getUsername());
            activeDrivers.remove(driver);
        }
    }

    private Driver getTheFastestDriver(List<Driver> drivingDrivers) {
        LocalDateTime currentMoment = LocalDateTime.now();
        System.out.println(" now : " + currentMoment);
        System.out.println(drivingDrivers.get(0));
        Driver fastest = getFirstForComparisonFastest(drivingDrivers);
        if(fastest == null){
            return null;
        }
        System.out.println(fastest.getUsername());
        double minDuration = Double.POSITIVE_INFINITY;
        System.out.println("*** durations ***");
        for (Driver driver : drivingDrivers) {
            if(!checkLastTwoDrivings(driver.getDrivingHistory()))
                continue;
            int indexOfLast = driver.getDrivingHistory().size() - 1;
            Driving driving = driver.getDrivingHistory().get(indexOfLast);

            Duration duration = Duration.between(currentMoment, driving.getDrivingTime().getEndDateTime());
            System.out.println(duration.toMinutes());
            long minutes = Math.abs(duration.toMinutes());
            if (minutes < minDuration &&  minutes > 0) {
                minDuration = minutes;
                fastest = driver;//driving.getDriver();
                System.out.println("new minduration: " + minDuration);
                System.out.println("new fastest: " + fastest.getUsername());
            }
        }
        return fastest;
    }

    private Driver getFirstForComparisonFastest(List<Driver> drivers){
        for(Driver driver:drivers){
            List<Driving> drivings = driver.getDrivingHistory();
            if(drivings.size()==1 ){
                return driver;
            }else if(drivings.size() > 1){
                Driving last = drivings.get(drivings.size()-1);
                Driving last2 = drivings.get(drivings.size()-2);
                if((last2.getState()==DrivingState.PAID || last2.getState()==DrivingState.CURRENT) &&
                        (last.getState()==DrivingState.PAID || last.getState()==DrivingState.CURRENT))
                    continue;
            }
            return driver;
        }
        return null;
    }
    private boolean checkLastTwoDrivings(List<Driving> drivings){
        if(drivings.size()==1 ){
            return true;
        }else if(drivings.size() > 1){
            Driving last = drivings.get(drivings.size()-1);
            Driving last2 = drivings.get(drivings.size()-2);
            if((last2.getState()==DrivingState.PAID || last2.getState()==DrivingState.CURRENT) &&
            (last.getState()==DrivingState.PAID || last.getState()==DrivingState.CURRENT))
                return false;
        }
        return true;
    }


    private Driver getNearestDriver(List<Driver> activeDrivers, Driving driving) throws IOException, InterruptedException {
        System.out.println("driving id: " + driving.getId());
        Address departure = driving.getPath().getStations().get(0);
        double nearestDistance = Double.POSITIVE_INFINITY;
        Driver nearestDriver = null;
        for (Driver driver : activeDrivers) {
            double distance = getDistanceBetweenDriverAndDeparture(departure, driver.getCurrentAddress());
            System.out.println("calculated distence - " + distance);
            if (distance < nearestDistance) {
                nearestDistance = distance;
                nearestDriver = driver;
            }
        }
        return nearestDriver;
    }

    private double getDistanceBetweenDriverAndDeparture(Address departure, Address driverPosition) throws IOException, InterruptedException {
        double lat1 = departure.getLatitude();
        double long1 = departure.getLongitude();
        double lat2 = driverPosition.getLatitude();
        double long2 = driverPosition.getLongitude();
        System.out.println("lat 1: " + lat1 + " lon1: " + long1);
        System.out.println("lat 2: " + lat2 + " long2: " + long2);
        String key = "AmVePoOhWRnb6gZq-k_QZvydI33UHUeqxlZQ8-q2k4phoJRmoVsjN5lZLE2L0Ef5";
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://dev.virtualearth.net/REST/v1/Routes?wp.0=" + lat1 + "," + long1 + "&wp.1=" + lat2
                        + "," + long2 + "&optimize=timeWithTraffic&routeAttributes=routeSummariesOnly&distanceUnit=km&key=" + key))
                .header("Content-Type", "application/json")
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println("distanca dobijena pozivom apija");
        return extractDistanceFromResponse(response.body());
    }


    private double extractDistanceFromResponse(String body) {
        System.out.println(body);

        JSONArray array = new JSONArray("[" + body + "]");
        Double distance = 0.0;
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);
            JSONArray array2 = object.getJSONArray("resourceSets");
            for (int j = 0; j < array2.length(); j++) {
                JSONObject resourceObject = array2.getJSONObject(j);
                JSONArray resourceArray = resourceObject.getJSONArray("resources");
                for (int k = 0; k < resourceArray.length(); k++) {
                    JSONObject distanceObject = resourceArray.getJSONObject(k);
                    distance = distanceObject.getDouble("travelDistance");
                    System.out.println("distanceee:   " + distance);
                }
            }
        }
        return distance;
    }


    @Scheduled(initialDelayString = "${greeting.initialdelay}", fixedRateString = "${greeting.fixedrate}")
    public void assignDriverToFutureDriving() {
        System.out.println(" RADI FUTURE 0000000000000000000");
        LocalDateTime start = LocalDateTime.now().minusMinutes(10);
        LocalDateTime end = LocalDateTime.now().plusMinutes(15);
        System.out.println(start);
        System.out.println(end);
        List<Driving> drivings = this.drivingRepository.getFutureDrivingFromPeriodWithoutDriver(true, start, end);
        for (Driving d : drivings) {
            System.out.println("future driving: " + d.getId() + "   time:  " + d.getDrivingTime().getStartDateTime());
            try {
                handleDriver(Clock.systemDefaultZone(), d);
            }catch (NoAvailableDriverException e){
                returnTokens(d);
                notificationService.createDrivingRefusedNotification(d);
            }
        }
    }

    private void returnTokens(Driving driving) {
        for (DrivingPayment drivingPayment : driving.getPassengers()){
            drivingPayment.returnTokens();
            drivingPayment.setPaid(false);
        }
        drivingRepository.save(driving);
    }


    private List<Driving> getCurrentDrivings() {
        return this.drivingRepository.getCurrentDrivings();
    }

    private List<Driver> getActiveDrivers() {
        return this.driverRepository.getActiveDrivers();
    }

    private List<Driver> getDrivingDrivers() {
        return this.driverRepository.getDrivingDrivers();
    }
}
