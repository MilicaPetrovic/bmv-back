package kts.nwt.ktsnwt.service.driving.interfaces;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.driving.ChosenAdditionalServices;

public interface ChosenAdditionalServicesService {

    ChosenAdditionalServices makeChosenAdditionalServices(long vehicleTypeId, boolean petFriendly, boolean babyFriendly) throws NotFoundException;
}
