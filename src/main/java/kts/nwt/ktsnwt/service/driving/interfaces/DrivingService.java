package kts.nwt.ktsnwt.service.driving.interfaces;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.driving.ChosenAdditionalServices;
import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.location.Path;
import kts.nwt.ktsnwt.dto.driving.DetailedDrivingDTO;
import kts.nwt.ktsnwt.dto.driving.DrivingStateDTO;
import kts.nwt.ktsnwt.dto.driving.RejectDrivingDTO;
import kts.nwt.ktsnwt.dto.location.SimulationDTO;
import kts.nwt.ktsnwt.exception.InvalidFutureTimeException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotHaveDrivingsForDriver;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;

public interface DrivingService {
    Driving getDrivingById(long drivingId);

    DetailedDrivingDTO getDetailedDriving(long drivingId) throws NotFoundException;

    List<String> getPassengerUsersFromDrivingId(long drivingId);

    Driving save(Driving driving);

    Driving save(Clock clock, ChosenAdditionalServices chosenAdditionalServices, Path path, double price, double time, boolean future, int hour, int minute) throws InvalidFutureTimeException;

    List<Driving> getActiveDrivingByClient(String applicantUsername);


    void rejectDriving(RejectDrivingDTO dto) throws NotFoundException;

    Driving startDriving(long drivingId) throws NotFoundException;

    SimulationDTO getSimulationDriving(Long drivingId);


    SimulationDTO getLastCurrentPaidDrivingForDriver(Long driverId);

    String getDriverByDrivingId(long drivingId) throws NotFoundException;

    Driving getPaidOrCurrentDrivingByDriverId(long driverId);


    List<Driving> getFutureDrivingFromPeriod(boolean b, LocalDateTime start, LocalDateTime end);

    DrivingStateDTO getStateOfLastDriving(String driverUsername) throws  NotHaveDrivingsForDriver, NoUserFoundException;

    SimulationDTO getSimulationIfExist(String username) throws NotHaveDrivingsForDriver;
}
