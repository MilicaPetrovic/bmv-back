package kts.nwt.ktsnwt.service.chat.interfaces;

import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.exception.NoUserFoundException;

import java.util.List;

public interface ChatService {
    List<User> getFriends(String username) throws NoUserFoundException;

}
