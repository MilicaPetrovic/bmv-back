package kts.nwt.ktsnwt.service.chat.interfaces;

import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.chat.MessageDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;

import java.util.List;

public interface MessageService {
    List<User> getUsersAdminsHasConversationWith();
    List<MessageDTO> getMessagesWithAdmins(String loggedInUsername);

    User sendMessage(MessageDTO messageDTO) throws NoUserFoundException;

    List<MessageDTO> getMessagesWithUser(String withUsername);
}
