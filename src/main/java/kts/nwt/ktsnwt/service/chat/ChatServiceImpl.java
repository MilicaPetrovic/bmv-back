package kts.nwt.ktsnwt.service.chat;

import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.repository.RoleRepository;
import kts.nwt.ktsnwt.service.chat.interfaces.ChatService;
import kts.nwt.ktsnwt.service.chat.interfaces.MessageService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ChatServiceImpl implements ChatService {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private MessageService messageService;

    @Override
    public List<User> getFriends(String username) throws NoUserFoundException {
        User u = userService.findByUsername(username);
        List<User> res = new ArrayList<>();
        if (u.isRegisteredUser() || u.isDriver()){
            User adminMock = new User();
            adminMock.setUsername("admin");
            adminMock.setName("Admin");
            adminMock.setSurname("");
            adminMock.setRole(roleRepository.findById(1L).orElse(null));
            res.add(adminMock);
            return res;
        }else if (u.isAdmin()){
            return messageService.getUsersAdminsHasConversationWith();
        }
        return null;
    }
}
