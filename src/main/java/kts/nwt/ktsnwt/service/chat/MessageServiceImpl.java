package kts.nwt.ktsnwt.service.chat;

import kts.nwt.ktsnwt.beans.communication.Message;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.chat.MessageDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.repository.MessageRepository;
import kts.nwt.ktsnwt.service.chat.interfaces.MessageService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@Transactional
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserService userService;

    @Override
    public List<User> getUsersAdminsHasConversationWith(){
        return messageRepository.findDistinctUserByReceiverAdmin();
    }

    @Override
    public List<MessageDTO> getMessagesWithAdmins(String loggedInUsername) {
        System.out.println(loggedInUsername);
        List<Message> messages = messageRepository.getMessagesByUserIsSender(loggedInUsername);
        messages.addAll(messageRepository.getMessagesByUserIsReceiver(loggedInUsername));
        return convertMessagesToMessagesDTOSortedByDate(messages);
    }

    private List<MessageDTO> convertMessagesToMessagesDTOSortedByDate(List<Message> messages) {
        List<MessageDTO> res = new ArrayList<>();
        for (Message message : messages){
            res.add(new MessageDTO(message));
        }
        res.sort(new Comparator<MessageDTO>() {
            @Override
            public int compare(MessageDTO c1, MessageDTO c2) {
                return (c1.getDateSent()).compareTo(c2.getDateSent());
            }
        });
        return res;
    }

    @Override
    public User sendMessage(MessageDTO messageDTO) throws NoUserFoundException {
        User toUser, fromUser = userService.findByUsername(messageDTO.getFromId());
        if (messageDTO.getToId().equals("admin"))
            toUser = null;
        else
            toUser = userService.findByUsername(messageDTO.getToId());
        Message m = new Message();
        m.setDateTime(LocalDateTime.now());
        m.setSender(fromUser);
        m.setReceiver(toUser);
        m.setText(messageDTO.getMessage());
        messageRepository.save(m);
        return fromUser;
    }

    @Override
    public List<MessageDTO> getMessagesWithUser(String withUsername) {
        //List<User> admins = userService.getAdmins();
        List<Message> messages = messageRepository.getMessagesByUserIsSender(withUsername);
        messages.addAll(messageRepository.getMessagesByUserIsReceiver(withUsername));
        //for (User a : admins){
        //}
        return convertMessagesToMessagesDTOSortedByDate(messages);
    }

}
