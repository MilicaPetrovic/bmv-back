package kts.nwt.ktsnwt.service.security;

import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.exception.NoMatchingException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.repository.RegisteredUserRepository;
import kts.nwt.ktsnwt.service.security.interfaces.UserVerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserVerificationServiceImpl implements UserVerificationService {

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Override
    public User verificate(String username, String code) throws NoUserFoundException, NoMatchingException {
        RegisteredUser registeredUser = registeredUserRepository.findByUsername(username).orElse(null);
        if (registeredUser == null){
            throw new NoUserFoundException("No user with the given username found");
        }else if(registeredUser.isActivated()){
            return registeredUser;
        }else if (registeredUser.getVerificationCode().equals(code)){
            registeredUser.setActivated(true);
            registeredUserRepository.save(registeredUser);
            return registeredUser;
        }else{
            throw new NoMatchingException("Verification code is not valid");
        }
    }
}
