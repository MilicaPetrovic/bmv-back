package kts.nwt.ktsnwt.service.security;

import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.user.DriverRegisterRequest;
import kts.nwt.ktsnwt.dto.user.UserRegisterRequest;
import kts.nwt.ktsnwt.exception.AlreadyInUse;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotValidException;
import kts.nwt.ktsnwt.repository.UserRepository;
import kts.nwt.ktsnwt.service.mail.interfaces.VerificationMailService;
import kts.nwt.ktsnwt.service.security.interfaces.RegisterService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private VerificationMailService verificationMailService;

    @Override
    public boolean registerClient(UserRegisterRequest registerRequest) throws AlreadyInUse, NotValidException {
        this.register(registerRequest);
        RegisteredUser user = userService.saveClient(registerRequest);
        verificationMailService.sendVerificationMailToUser(user);
        return true;
    }

    @Override
    public boolean registerDriver(DriverRegisterRequest registerRequest) throws AlreadyInUse, NotValidException {
        this.register(registerRequest);
        return userService.saveDriver(registerRequest);
    }

    private void register(UserRegisterRequest registerRequest) throws AlreadyInUse, NotValidException {
        try {
            userService.findByUsername(registerRequest.getUsername());
            throw  new AlreadyInUse("Username is already in use");
        } catch (NoUserFoundException ignored) {
        }
        User existUserByEmail = userRepository.findByEmail(registerRequest.getEmail());

        if (existUserByEmail != null) {
            throw new AlreadyInUse("Email is already in use");
        }

        checkIfStartsWithCapitalMoreThan3LessThan20(registerRequest.getName(), "name");
        checkIfStartsWithCapitalMoreThan3LessThan20(registerRequest.getSurname(), "surname");
        checkUsername(registerRequest.getUsername());
        checkEmail(registerRequest.getEmail());
        checkPassword(registerRequest.getPassword());
        checkPhoneNumber(registerRequest.getPhoneNumber());
//        checkIfStartsWithCapitalMoreThan3LessThan20(registerRequest.getCity(), "grad");
        checkPostalNumber(registerRequest.getPostNumber());

    }

    private void checkPostalNumber(String postNumber) throws NotValidException {
        Pattern p = Pattern.compile("[0-9]{5}");//. represents single character
        Matcher m = p.matcher(postNumber);//NE RADIIIIIIIIII
        if (!m.matches())
            throw new NotValidException("Phone number is not valid");
    }

    private void checkPhoneNumber(String phoneNumber) throws NotValidException {
        Pattern p = Pattern.compile("\\+[0-9]{6,13}");//. represents single character
        Matcher m = p.matcher(phoneNumber);//NE RADIIIIIIIIII
        if (!m.matches())
            throw new NotValidException("Phone number is not valid");
    }

    private void checkPassword(String password) throws NotValidException {
        Pattern p = Pattern.compile("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@!%*?&])[A-Za-zd$@!%*?&].{7,}");//. represents single character
        Matcher m = p.matcher(password);
        if (!m.matches())
            throw new NotValidException("Password is not valid");
    }

    private void checkEmail(String email) throws NotValidException {
        Pattern p = Pattern.compile("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");//. represents single character
        Matcher m = p.matcher(email);
        if (!m.matches())
            throw new NotValidException("Email is not valid");
    }


    private void checkIfStartsWithCapitalMoreThan3LessThan20(String name, String element) throws NotValidException {
        Pattern p = Pattern.compile("[A-ZŠĆČĐŽ][a-zšđčćž]*");//. represents single character
        Matcher m = p.matcher(name);
        if (!m.matches())
            throw new NotValidException(element + " is not valid");
    }

    private void checkUsername(String username) throws NotValidException {
        Pattern p = Pattern.compile("^(?=.{4,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$");//. represents single character
        Matcher m = p.matcher(username);
        if (!m.matches())
            throw new NotValidException("Username is not valid");
    }
}
