package kts.nwt.ktsnwt.service.security.interfaces;

import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.exception.NoMatchingException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;

public interface UserVerificationService {

    User verificate(String username, String code) throws NoUserFoundException, NoMatchingException;
}
