package kts.nwt.ktsnwt.service.security;

import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.security.ChangePasswordDTO;
import kts.nwt.ktsnwt.dto.security.ResetPasswordDTO;
import kts.nwt.ktsnwt.exception.NoMatchingException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotInTimeException;
import kts.nwt.ktsnwt.exception.PasswordsNotMatchingException;
import kts.nwt.ktsnwt.service.security.interfaces.ChangePasswordService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

@Service
public class ChangePasswordServiceImpl implements ChangePasswordService {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder encoder;//?

    @Transactional(rollbackFor = Exception.class)
    public boolean resetPassword(ResetPasswordDTO passwordDTO)
            throws NoUserFoundException, NoMatchingException, NotInTimeException {
        User user = userService.findByUsername(passwordDTO.getUsername());

        if (user == null)
            throw new NoUserFoundException("No user found");//promeni za no user exception

        if (!user.getResetPasswordCode().equals(passwordDTO.getResetCode())){
            throw new NoMatchingException("Reset code for changing password is not matching");
        }

        LocalDateTime inTime = user.getResetPasswordTime().plusDays(1);
        if (!inTime.isAfter(LocalDateTime.now())){
            throw new NotInTimeException("Reset code expired");
        }

        user.setPassword(encoder.encode(passwordDTO.getNewPassword()));
        Date date = new Date();
        user.setLastPasswordResetDate(new Timestamp(date.getTime()));
        User new_user = userService.save(user);
        return true;
    }

    @Override
    public boolean changePassword(ChangePasswordDTO dto, String username) throws NoUserFoundException, PasswordsNotMatchingException {
        User user = userService.findByUsername(username);
        if (user == null)
            throw new NoUserFoundException("No user found");

        if (!encoder.matches(dto.getCurrentPassword(), user.getPassword()))
            throw new PasswordsNotMatchingException("Trenutna lozinka nije ispravna.");

        if (!dto.getNewPassword().equals(dto.getConfirmPassword()))
            throw new PasswordsNotMatchingException("Lozinke se ne poklapaju.");

        user.setPassword(encoder.encode(dto.getNewPassword()));
        Date date = new Date();
        user.setLastPasswordResetDate(new Timestamp(date.getTime()));
        userService.save(user);
        return true;
    }

}
