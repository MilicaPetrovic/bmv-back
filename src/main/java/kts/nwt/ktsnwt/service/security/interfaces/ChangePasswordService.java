package kts.nwt.ktsnwt.service.security.interfaces;

import kts.nwt.ktsnwt.dto.security.ChangePasswordDTO;
import kts.nwt.ktsnwt.dto.security.ResetPasswordDTO;
import kts.nwt.ktsnwt.exception.NoMatchingException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotInTimeException;
import kts.nwt.ktsnwt.exception.PasswordsNotMatchingException;

public interface ChangePasswordService {

    boolean resetPassword(ResetPasswordDTO passwordDTO) throws NoUserFoundException, NoMatchingException, NotInTimeException;

    boolean changePassword(ChangePasswordDTO dto, String username) throws NoUserFoundException, PasswordsNotMatchingException;
}
