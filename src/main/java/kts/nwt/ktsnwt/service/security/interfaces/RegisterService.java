package kts.nwt.ktsnwt.service.security.interfaces;

import kts.nwt.ktsnwt.dto.user.DriverRegisterRequest;
import kts.nwt.ktsnwt.dto.user.UserRegisterRequest;
import kts.nwt.ktsnwt.exception.AlreadyInUse;
import kts.nwt.ktsnwt.exception.NotValidException;

public interface RegisterService {
    boolean registerClient(UserRegisterRequest registerRequest) throws AlreadyInUse, NotValidException;

    boolean registerDriver(DriverRegisterRequest registerRequest) throws AlreadyInUse, NotValidException;
}
