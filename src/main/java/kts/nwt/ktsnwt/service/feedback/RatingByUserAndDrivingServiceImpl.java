package kts.nwt.ktsnwt.service.feedback;

import kts.nwt.ktsnwt.beans.feedback.Rating;
import kts.nwt.ktsnwt.repository.RatingRepository;
import kts.nwt.ktsnwt.service.feedback.interfaces.RatingByUserAndDrivingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingByUserAndDrivingServiceImpl implements RatingByUserAndDrivingService {

    @Autowired
    private RatingRepository ratingRepository;


    public Rating findRatingByUserToDriving(Long userId, Long drivingId) {
        return ratingRepository.findByClient_IdAndDriving_Id(userId, drivingId).orElse(null);
    }
}
