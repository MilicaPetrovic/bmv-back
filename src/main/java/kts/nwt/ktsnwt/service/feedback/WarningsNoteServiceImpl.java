package kts.nwt.ktsnwt.service.feedback;

import kts.nwt.ktsnwt.beans.feedback.WarningNote;
import kts.nwt.ktsnwt.beans.users.Admin;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.repository.WarningNoteRepository;
import kts.nwt.ktsnwt.service.feedback.interfaces.WarningsNoteService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WarningsNoteServiceImpl implements WarningsNoteService {
    @Autowired
    private WarningNoteRepository warningsNoteRepository;
    @Autowired
    private UserService userService;

    @Override
    public List<WarningNote> getWarningsNoteByUsername(String username) throws NoUserFoundException {
        User user = userService.findByUsername(username);

        return warningsNoteRepository.findByUserIdWithAdmin(user.getId());
    }

    @Override
    public void addWarningNot(String adminUsername, String username, String warningNoteText) throws NoUserFoundException {
        User admin = userService.findByUsername(adminUsername);
        User client = userService.findByUsername(username);
        WarningNote warningNote = new WarningNote();
        warningNote.setAdmin((Admin) admin);
        warningNote.setUser(client);
        warningNote.setText(warningNoteText);
        warningsNoteRepository.save(warningNote);
    }
}
