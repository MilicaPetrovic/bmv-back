package kts.nwt.ktsnwt.service.feedback.interfaces;

import kts.nwt.ktsnwt.dto.feedback.ClientDrivingRatingDTO;
import kts.nwt.ktsnwt.exception.*;

public interface ClientDrivingRatingService {
    void leaveRating(ClientDrivingRatingDTO ratingDTO, String username) throws NoUserFoundException, NotInTimeException, CostumNotFoundException, NotValidException, AlreadyInUse, NoMatchingException;
}
