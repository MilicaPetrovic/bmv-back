package kts.nwt.ktsnwt.service.feedback.interfaces;

import kts.nwt.ktsnwt.beans.feedback.Rating;

public interface RatingByUserAndDrivingService {
    Rating findRatingByUserToDriving(Long userId, Long drivingId);
}
