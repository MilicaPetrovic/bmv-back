package kts.nwt.ktsnwt.service.feedback;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.feedback.ClientDrivingRatingDTO;
import kts.nwt.ktsnwt.exception.*;
import kts.nwt.ktsnwt.repository.RegisteredUserRepository;
import kts.nwt.ktsnwt.service.driving.interfaces.DriverDrivingHistoryService;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.driving.interfaces.PassengersService;
import kts.nwt.ktsnwt.service.feedback.interfaces.ClientDrivingRatingService;
import kts.nwt.ktsnwt.service.feedback.interfaces.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ClientDrivingRatingServiceImpl implements ClientDrivingRatingService {

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private DriverDrivingHistoryService driverDrivingHistoryService;

    @Autowired
    private DrivingService drivingService;

    @Autowired
    private RatingService ratingService;

    @Autowired
    private PassengersService passengersService;

    public void leaveRating(ClientDrivingRatingDTO ratingDTO, String username) throws NoUserFoundException, NotInTimeException, CostumNotFoundException, NotValidException, AlreadyInUse, NoMatchingException {
        RegisteredUser ru = checkIfUserWasPartOfTheDriving(username, ratingDTO.getDrivingId());
        Driving driving = drivingService.getDrivingById(ratingDTO.getDrivingId());
        ratingService.saveRating(driving, ru, ratingDTO.getDriverRating(), ratingDTO.getVehicleRating(), ratingDTO.getText());
    }

    private RegisteredUser checkIfUserWasPartOfTheDriving(String username, Long drivingId) throws NoUserFoundException, CostumNotFoundException {
        RegisteredUser ru = registeredUserRepository.findByUsername(username).orElse(null);
        if (ru == null)
            throw new NoUserFoundException("No user found");
        List<User> passengers = passengersService.getPassengerByDrivingId(drivingId);
        for (User passenger : passengers){
            if (passenger.equals(ru)){
                return ru;
            }
        }
        throw new CostumNotFoundException("User was not passenger in driving");
    }
}
