package kts.nwt.ktsnwt.service.feedback.interfaces;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.exception.AlreadyInUse;
import kts.nwt.ktsnwt.exception.NoMatchingException;
import kts.nwt.ktsnwt.exception.NotInTimeException;
import kts.nwt.ktsnwt.exception.NotValidException;

public interface RatingService {
    double driversRating(long driverId);
    double vehicleRating(long driverId);
    void saveRating(Driving driving, RegisteredUser user, int driverRating, int vehicleRating, String commentar) throws NotInTimeException, AlreadyInUse, NotValidException, NoMatchingException;
}
