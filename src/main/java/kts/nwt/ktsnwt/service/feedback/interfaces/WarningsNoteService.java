package kts.nwt.ktsnwt.service.feedback.interfaces;

import kts.nwt.ktsnwt.beans.feedback.WarningNote;
import kts.nwt.ktsnwt.exception.NoUserFoundException;

import java.util.List;

public interface WarningsNoteService {
    List<WarningNote> getWarningsNoteByUsername(String username) throws NoUserFoundException;

    void addWarningNot(String adminUsername, String username, String warningNoteText) throws NoUserFoundException;
}
