package kts.nwt.ktsnwt.service.feedback;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.feedback.Rating;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.exception.AlreadyInUse;
import kts.nwt.ktsnwt.exception.NoMatchingException;
import kts.nwt.ktsnwt.exception.NotInTimeException;
import kts.nwt.ktsnwt.exception.NotValidException;
import kts.nwt.ktsnwt.repository.RatingRepository;
import kts.nwt.ktsnwt.service.feedback.interfaces.RatingByUserAndDrivingService;
import kts.nwt.ktsnwt.service.feedback.interfaces.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RatingServiceImpl implements RatingService {
    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private RatingByUserAndDrivingService ratingByUserAndDrivingService;

    public double driversRating(long driverId){
        return ratingRepository.getRatingOfDriver(driverId).orElse((double) 0);
    }

    public double vehicleRating(long driverId){
        return ratingRepository.getRatingOfVehicle(driverId).orElse((double) 0);
    }

    @Override
    public void saveRating(Driving driving, RegisteredUser user, int driverRating, int vehicleRating, String commentar) throws NotInTimeException, AlreadyInUse, NotValidException, NoMatchingException {
        if (driverRating == 0 && vehicleRating == 0)
            throw new NoMatchingException("Rating can not be saved without ratings");
        if (!driving.happened())
            throw new NotValidException("Driving never happened, you can not comment a driving like that");
        if (!driving.canClientLeaveRating(user))
            throw new NotInTimeException("You can not leave a rating");
        Rating ratingExc = ratingByUserAndDrivingService.findRatingByUserToDriving(user.getId(), driving.getId());
        if (ratingExc != null ){
            throw new AlreadyInUse("Already rated driving");
        }
        Rating rating = new Rating();
        rating.setDriverRating(driverRating);
        rating.setVehicleRating(vehicleRating);
        rating.setClient(user);
        rating.setDriving(driving);
        rating.setText(commentar);
        ratingRepository.save(rating);
    }

}
