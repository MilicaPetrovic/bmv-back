package kts.nwt.ktsnwt.service.user.interfaces;

import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.dto.admin.UserDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface RegisteredUserService {
    List<RegisteredUser> getActivatedUsers(PageRequest of);

    int getActivatedUsersCount();

    RegisteredUser findByUsername(String username) throws NoUserFoundException;

    void save(RegisteredUser user);

    boolean addTokens(String username, int tokens) throws NoUserFoundException;

    List<UserDTO> getClients();
}
