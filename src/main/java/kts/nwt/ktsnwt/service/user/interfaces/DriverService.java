package kts.nwt.ktsnwt.service.user.interfaces;

import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.dto.location.SimulationVehicleDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.exception.StillHasActiveDrivingException;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;

public interface DriverService {
    ArrayList<Driver> getActiveDrivers();
    ArrayList<Driver> getDrivingDrivers();
    ArrayList<Driver> getInactiveDrivers();
    Driver findDriverById(Long id);
    List<Driver> getDrivers(PageRequest of);

    void save(Driver driver);

    Driver findByUsername(String username) throws NoUserFoundException;
    void ifUserIsDriverItBecomesActive(User user);

    void ifUserIsDriverItBecomesInactive(String username) throws StillHasActiveDrivingException;

    boolean getIfActive(String username) throws NoUserFoundException;

    Boolean changeStatus(String username) throws StillHasActiveDrivingException, NoUserFoundException;

    List<SimulationVehicleDTO> getAllDriverAndVehicles();
}
