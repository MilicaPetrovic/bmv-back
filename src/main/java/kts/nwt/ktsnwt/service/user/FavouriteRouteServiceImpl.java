package kts.nwt.ktsnwt.service.user;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.location.Address;
import kts.nwt.ktsnwt.beans.location.Route;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.dto.location.MapPointDTO;
import kts.nwt.ktsnwt.dto.location.RouteDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotValidException;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.user.interfaces.FavouriteRouteService;
import kts.nwt.ktsnwt.service.user.interfaces.RegisteredUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class FavouriteRouteServiceImpl implements FavouriteRouteService {

    @Autowired
    private RegisteredUserService registeredUserService;

    @Autowired
    private DrivingService drivingService;

    @Override
    public void toggleFavouriteRoute(String username, int drivingId) throws NoUserFoundException, NotValidException {
        RegisteredUser user = registeredUserService.findByUsername(username);
        Driving driving = drivingService.getDrivingById(drivingId);
        if (!driving.hasPassenger(user)){
            throw new NotValidException("User can not set as favourite");
        }
        user.changeFavouriteRoute(driving.getRoute());
        registeredUserService.save(user);
    }

    @Override
    public ArrayList<RouteDTO> getFavouriteRoutesForUsername(String username) throws NoUserFoundException {
        RegisteredUser registeredUser = this.registeredUserService.findByUsername(username);
        List<Route> routes = registeredUser.getFavouriteRoute();
        ArrayList<RouteDTO> favouriteRoutes= new ArrayList<>();
        for(Route r: routes){
            List<Address> routePoints = r.getRoute();

            ArrayList<MapPointDTO> points = new ArrayList<MapPointDTO>();
            for(Address a : routePoints){
                points.add(new MapPointDTO(a.getLatitude(), a.getLongitude()));
            }

            List<Address> path = r.getPath().getStations();
            ArrayList<MapPointDTO> pathPoints = new ArrayList<>();
            for(Address a : path){
                pathPoints.add(new MapPointDTO(a.getLatitude(), a.getLongitude(), a.getName()));
            }
            RouteDTO routeDTO = new RouteDTO(pathPoints, points, r.getTime(), r.getDistance(), r.getDistance()*120);
            favouriteRoutes.add(routeDTO);
        }
        return favouriteRoutes;
    }
}
