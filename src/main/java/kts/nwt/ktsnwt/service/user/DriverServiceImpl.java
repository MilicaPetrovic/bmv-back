package kts.nwt.ktsnwt.service.user;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.driving.DrivingState;
import kts.nwt.ktsnwt.beans.driving.TimePeriod;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.dto.VehicleDTO;
import kts.nwt.ktsnwt.dto.admin.UserDTO;
import kts.nwt.ktsnwt.dto.location.SimulationVehicleDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.beans.users.DriverState;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.exception.StillHasActiveDrivingException;
import kts.nwt.ktsnwt.repository.DriverRepository;
import kts.nwt.ktsnwt.repository.DrivingRepository;
import kts.nwt.ktsnwt.repository.TimePeriodRepository;
import kts.nwt.ktsnwt.service.driving.interfaces.DriverAssignmentService;
import kts.nwt.ktsnwt.service.user.interfaces.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DriverServiceImpl implements DriverService {
    @Autowired
    private DriverRepository driverRepository;

    @Autowired
    private DrivingRepository drivingRepository;

    @Autowired
    private DriverAssignmentService driverAssignmentService;

    @Autowired
    private TimePeriodRepository timePeriodRepository;

    @Override
    public ArrayList<Driver> getActiveDrivers(){
        return (ArrayList<Driver>) driverRepository.getActiveDrivers();
    }
    @Override
    public ArrayList<Driver> getDrivingDrivers() {

        return (ArrayList<Driver>) driverRepository.getDrivingDrivers();
    }

    @Override
    public ArrayList<Driver> getInactiveDrivers() {

        return (ArrayList<Driver>) driverRepository.getInactiveDrivers();
    }

    @Override
    public Driver findDriverById(Long id){
        return driverRepository.findById(id).orElse(null);
    }

    @Override
    public List<Driver> getDrivers(PageRequest of) {
        return driverRepository.findAll(of);
    }

    @Override
    public void save(Driver driver) {
        driverRepository.save(driver);
    }

    @Override
    public Driver findByUsername(String username) throws NoUserFoundException {
        Driver driver = driverRepository.findByUsername(username).orElse(null);
        if (driver == null)
            throw new NoUserFoundException("No driver found");
        return driver;
    }

    public void ifUserIsDriverItBecomesActive(User user) {
        if (user.isDriver()){
            Driver d = (Driver) user;

            //provera da li ima više od 8h
            d.setActiveHours(driverRepository.findWorkingPeriodsByUsername(d.getUsername()));
            double activeMinutes = driverAssignmentService.getActiveMinutesInThePast24Hours(d);
            if (activeMinutes >= 480) {
                if (!d.isInactive()){
                    d.setDriverState(DriverState.INACTIVE);
                    this.driverRepository.save(d);
                }
                return;
            }

            d.setDriverState(DriverState.ACTIVE);
            TimePeriod tp = new TimePeriod();
            tp.setStartDateTime(LocalDateTime.now());
            tp.setEstimated(false);
            tp.setActiveHour(d);
            d.addActiveHour(tp);
            //timePeriodRepository.save(tp);
            //d.addActiveHour(tp);
            this.driverRepository.save(d);
        }
    }

    @Override
    public void ifUserIsDriverItBecomesInactive(String username) throws StillHasActiveDrivingException {
        try{
            Driver d = findByUsername(username);


            // da li ima active driving?
            List<Driving> activeDriving = drivingRepository.getActiveDrivingsByDriversUsername(username);
            for (Driving driving: activeDriving){
                if (!driving.isStillActive()){
                    driving.setState(DrivingState.REFUSED);
                    drivingRepository.save(driving);
                }
                else {
                    throw new StillHasActiveDrivingException("Driver still has active drivings");
                }
            }


            d.setDriverState(DriverState.INACTIVE);
            d.endLastActiveHour();
            driverRepository.save(d);
        }catch (StillHasActiveDrivingException e){
            throw new StillHasActiveDrivingException("Driver still has active drivings");
        }catch (Exception e){

        }
    }

    @Override
    public boolean getIfActive(String username) throws NoUserFoundException {
        Driver driver = findByUsername(username);
        return !driver.isInactive();
    }

    public int getDriversCount() {
        return driverRepository.getDriversCount();
    }

    public VehicleDTO getVehicle(String username) {
        return new VehicleDTO(driverRepository.findVehicleByUsername(username));
    }

    @Override
    public Boolean changeStatus(String username) throws StillHasActiveDrivingException, NoUserFoundException {
        Driver d = findByUsername(username);
        if (d.isInactive()){
            ifUserIsDriverItBecomesActive(d);
        }else{
            ifUserIsDriverItBecomesInactive(username);
        }
        return true;
    }

    @Override
    public List<SimulationVehicleDTO> getAllDriverAndVehicles() {
        List<Driver> drivers = this.driverRepository.findAll();
        ArrayList<SimulationVehicleDTO> simulationVehicleDTOS = new ArrayList<>();
        for(Driver driver : drivers){
            simulationVehicleDTOS.add(new SimulationVehicleDTO(driver.getVehicle().getId(), driver.getVehicle().getLabel(),
                    driver.getCurrentAddress().getLatitude(), driver.getCurrentAddress().getLongitude(), driver.getId()));
        }
        return simulationVehicleDTOS;
    }

    public List<UserDTO> getDrivers() {
        List<Driver> registeredUsers = driverRepository.findAll();
        List<UserDTO> result = new ArrayList<>();
        for (Driver ru : registeredUsers){
            result.add(new UserDTO(ru));
        }
        return result;
    }
}
