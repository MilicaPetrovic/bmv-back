package kts.nwt.ktsnwt.service.user.interfaces;

import kts.nwt.ktsnwt.dto.location.RouteDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotValidException;

import java.util.ArrayList;

public interface FavouriteRouteService {
    void toggleFavouriteRoute(String username, int drivingId) throws NoUserFoundException, NotValidException;
    ArrayList<RouteDTO> getFavouriteRoutesForUsername(String username) throws NoUserFoundException;

}
