package kts.nwt.ktsnwt.service.user;

import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.dto.admin.UserDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.repository.RegisteredUserRepository;
import kts.nwt.ktsnwt.service.user.interfaces.RegisteredUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RegisteredUserServiceImpl implements RegisteredUserService {
    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Override
    public List<RegisteredUser> getActivatedUsers(PageRequest of) {
        return registeredUserRepository.findByActivated(true, of);
    }

    @Override
    public int getActivatedUsersCount() {
        return (int) registeredUserRepository.countByActivated(true);
    }

    @Override
    public RegisteredUser findByUsername(String username) throws NoUserFoundException {
        RegisteredUser ru = registeredUserRepository.findByUsername(username).orElse(null);
        if (ru == null)
            throw new NoUserFoundException("No registered user found");
        return ru;
    }

    @Override
    public void save(RegisteredUser user) {
        registeredUserRepository.save(user);
    }

    @Override
    public boolean addTokens(String username, int tokens) throws NoUserFoundException {
        RegisteredUser user = findByUsername(username);
        user.setTokens(user.getTokens() + tokens);
        save(user);
        return true;
    }

    @Override
    public List<UserDTO> getClients() {
        List<RegisteredUser> registeredUsers = registeredUserRepository.findAll();
        List<UserDTO> result = new ArrayList<>();
        for (RegisteredUser ru : registeredUsers){
            result.add(new UserDTO(ru));
        }
        return result;
    }

}
