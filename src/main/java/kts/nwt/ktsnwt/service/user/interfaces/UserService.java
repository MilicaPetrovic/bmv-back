package kts.nwt.ktsnwt.service.user.interfaces;

import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.user.*;
import kts.nwt.ktsnwt.exception.NoProfileChangeRequestFoundException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface UserService {
    RegisteredUser saveClient(UserRegisterRequest registerRequest);
    User findById(Long userId);
    void toggleBlockForUser(String username) throws NoUserFoundException;
    User getSocialUser(SocialUserDTO socialUserDTO);
    User findByUsername(String username) throws NoUserFoundException;
    User findByUsernameNotBlocked(String username) throws NoUserFoundException;
    User save(User user);
    Driver findByDriverId(Long driverId, Long driverRole);
    UserProfileDTO getUserProfileDTO(User user);
    String changeProfilePhoto(MultipartFile[] multiPartFiles, String username) throws NoUserFoundException, IOException;
    boolean updateUser(UserProfileDTO dto, User user);
    ArrayList<String> getAllRegisteredUsers(String loggedInUsername);
    List<User> getAdmins();
    boolean updateDriver(ProfileChangeRequestDTO dto) throws NoUserFoundException, NoProfileChangeRequestFoundException;
    List<ProfileChangeRequestDTO> getProfileChangeRequests(PageRequest of);

    int getProfileChangeRequestCount();

    boolean saveDriver(DriverRegisterRequest registerRequest);

    User findByEmail(String credential);
}
