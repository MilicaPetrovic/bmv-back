package kts.nwt.ktsnwt.service.user;

import kts.nwt.ktsnwt.beans.driving.Vehicle;
import kts.nwt.ktsnwt.beans.driving.VehicleType;
import kts.nwt.ktsnwt.beans.location.City;
import kts.nwt.ktsnwt.beans.requests.ProfileChangeRequest;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.Provider;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.user.*;
import kts.nwt.ktsnwt.exception.NoProfileChangeRequestFoundException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.repository.ProfileChangeRequestRepository;
import kts.nwt.ktsnwt.repository.RoleRepository;
import kts.nwt.ktsnwt.repository.UserRepository;
import kts.nwt.ktsnwt.service.picture.interfaces.PhotoService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Value("${app.role.registered.user}")
    private long REGISTERED_USER_ID;

    @Value("${app.role.driver}")
    private long DRIVER_ID;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private ProfileChangeRequestRepository profileChangeRequestRepository;

    final static String PHOTOS_PATH = "bmv-back/src/main/resources/static/photos/";

    @Override
    public RegisteredUser saveClient(UserRegisterRequest registerRequest) {
        RegisteredUser user = new RegisteredUser();
        user.setUsername(registerRequest.getUsername());
        user.setName(registerRequest.getName());
        user.setSurname(registerRequest.getSurname());
        user.setEmail(registerRequest.getEmail());
        //adresa
        user.setCity(new City(registerRequest.getCity(), Integer.parseInt(registerRequest.getPostNumber())));
        user.setPhone(registerRequest.getPhoneNumber());
        user.setBlocked(false);
        user.setRole(roleRepository.findById(REGISTERED_USER_ID).orElse(null));
        user.setPassword(encoder.encode(registerRequest.getPassword()));
        user.setLastPasswordResetDate(new Timestamp((new Date()).getTime()));
        user.setActivated(false);
        user.setSocialAccount(false);
        user.setProvider(Provider.NO_PROVIDER);
        String randomCode = RandomString.make(64);
        user.setVerificationCode(randomCode);
        userRepository.save(user);
        return user;
    }

    @Override
    public User findById(Long userId) {
        return userRepository.findById(userId).orElse(null);
    }

    @Override
    public User getSocialUser(SocialUserDTO socialUserDTO) {
        RegisteredUser user = (RegisteredUser) userRepository.findByEmail(socialUserDTO.getEmail());
        if (user == null)
            user = this.createSocialUser(socialUserDTO);

        else if (!user.getProvider().name().equalsIgnoreCase(socialUserDTO.getProvider())) {
            user.setImage(socialUserDTO.getPhotoUrl());
            user.setName(socialUserDTO.getFirstName());
            user.setSurname(socialUserDTO.getLastName());
            user.setUsername(this.generateRandomUsername(socialUserDTO.getFirstName()));
        }
        return user;
    }

    private RegisteredUser createSocialUser(SocialUserDTO socialUserDTO) {
        RegisteredUser user = new RegisteredUser();
        user.setEmail(socialUserDTO.getEmail());
        user.setName(socialUserDTO.getFirstName());
        user.setSurname(socialUserDTO.getLastName());
        user.setUsername(this.generateRandomUsername(socialUserDTO.getFirstName()));
        user.setRole(roleRepository.findById(REGISTERED_USER_ID).orElse(null));
        user.setSocialAccount(true);
        user.setImage(socialUserDTO.getPhotoUrl());
        user.setBlocked(false);
        user.setActivated(true);
        user.setProvider(Provider.valueOf(socialUserDTO.getProvider()));
        userRepository.save(user);
        return user;
    }

    public void toggleBlockForUser(String username) throws NoUserFoundException {
        User user = findByUsername(username);
        user.setBlocked(!user.isBlocked());
        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) throws NoUserFoundException {
        User user = userRepository.findByUsername(username).orElse(null);
        if (user == null)
            throw new NoUserFoundException("No user found with username: " + username);
        return user;
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public Driver findByDriverId(Long driverId, Long driverRole) {
        return userRepository.findByDriverId(driverId, driverRole);
    }

    @Override
    public UserProfileDTO getUserProfileDTO(User user) {
        UserProfileDTO dto = new UserProfileDTO(user.getEmail(),
                user.getUsername(),
                user.getName(),
                user.getSurname(),
                user.getPhone(),
                user.getCity(),
                user.getProvider());
        if (user instanceof RegisteredUser)
            dto.setTokens(((RegisteredUser) user).getTokens());
        return dto;
    }

    @Override
    public String changeProfilePhoto(MultipartFile[] multiPartFiles, String username) throws NoUserFoundException, IOException {
        User user = findByUsername(username);
        String role = user.getRole().getName().toLowerCase(Locale.ROOT);
        role = role.substring(5);
        photoService.tryDeletePhoto(user.getImage());
        String path = photoService.addPhoto(user.getId(), PHOTOS_PATH, multiPartFiles, role);
        user.setImage(path);
        System.out.println(path);
        userRepository.save(user);
        return user.getImage();
    }

    @Override
    public boolean updateUser(UserProfileDTO dto, User user) {
        if (user.getRole().isDriver()) {
            ProfileChangeRequest request = this.makeProfileChangeRequest(dto, user);
            profileChangeRequestRepository.save(request);
            return false;
        }
        user.setName(dto.getName());
        user.setSurname(dto.getSurname());
        if (user.getCity() == null)
            user.setCity(new City());
        user.getCity().setName(dto.getCity().getName());
        user.getCity().setPostNumber(dto.getCity().getPostNumber());
        user.setPhone(dto.getPhone());
        this.save(user);
        return true;
    }

    private ProfileChangeRequest makeProfileChangeRequest(UserProfileDTO dto, User user) {
        ProfileChangeRequest request = new ProfileChangeRequest();
        if (user.getCity() == null) {
            user.setCity(new City(""));
        }
        request.setUsername(user.getUsername());
        request.setName(dto.getName());
        request.setSurname(dto.getSurname());
        request.setPhone(dto.getPhone());
        request.setCity(new City());
        request.getCity().setName(dto.getCity().getName());
        request.getCity().setPostNumber(dto.getCity().getPostNumber());
        request.setDone(false);
        List<String> changes = this.getChangedFields(request, user);
        request.setChanges(changes);
        return request;
    }

    private List<String> getChangedFields(ProfileChangeRequest request, User user) {
        List<String> changes = new ArrayList<>();
        if (!user.getName().equals(request.getName()))
            changes.add("name");
        if (!user.getSurname().equals(request.getSurname()))
            changes.add("surname");
        if (!user.getPhone().equals(request.getPhone()))
            changes.add("phone");
        if (!user.getCity().getName().equals(request.getCity().getName()))
            changes.add("city");
        if (user.getCity().getPostNumber() != request.getCity().getPostNumber())
            changes.add("postNumber");
        return changes;
    }

    private String generateRandomUsername(String name) {
        String postfix = UUID.randomUUID()
                .toString()
                .substring(0, 8);
        return name + "_" + postfix;
    }
    @Override
    public ArrayList<String> getAllRegisteredUsers(String loggedInUsername){
         ArrayList<String> allRegisteredUsers =  (ArrayList<String>) userRepository.findAllRegisteredUsers();
         allRegisteredUsers.remove(loggedInUsername);
         return allRegisteredUsers;
    }


    public List<User> getAdmins() {
        return userRepository.findByRole_Id(1L);
    }

    @Override
    public boolean updateDriver(ProfileChangeRequestDTO dto) throws NoUserFoundException, NoProfileChangeRequestFoundException {
        Optional<ProfileChangeRequest> requestOptional = profileChangeRequestRepository.findById(dto.getId());
        ProfileChangeRequest request = requestOptional.orElse(null);
        if (request == null)
            throw new NoProfileChangeRequestFoundException("There is no such profile change request.");
        if (!dto.isApproved()) {
            request.setDone(true);
            profileChangeRequestRepository.save(request);
            return false;
        }
        Driver driver = (Driver) findByUsername(dto.getUsername());
        driver.setName(dto.getName());
        driver.setSurname(dto.getSurname());
        driver.setPhone(dto.getPhone());
        if (driver.getCity() == null)
            driver.setCity(new City());
        driver.getCity().setName(dto.getCity().getName());
        driver.getCity().setPostNumber(dto.getCity().getPostNumber());
        request.setDone(true);
        userRepository.save(driver);
        profileChangeRequestRepository.save(request);
        return true;
    }

    @Override
    public List<ProfileChangeRequestDTO> getProfileChangeRequests(PageRequest of) {
        List<ProfileChangeRequest> profileChangeRequests = profileChangeRequestRepository.findAll(of);
        List<ProfileChangeRequestDTO> dtoList = new ArrayList<>();
        for (ProfileChangeRequest r : profileChangeRequests) {
            if (!r.isDone()) {
                dtoList.add(new ProfileChangeRequestDTO(r.getId(), r.getUsername(), r.getName(), r.getSurname(),
                            r.getCity(), r.getPhone(), false, r.getChanges(), false));
            }
        }
        return dtoList;
    }

    @Override
    public int getProfileChangeRequestCount() {
        return profileChangeRequestRepository.getProfileChangeRequestCount();
    }

    @Override
    public boolean saveDriver(DriverRegisterRequest registerRequest) {
        Driver driver = new Driver();
        driver.setUsername(registerRequest.getUsername());
        driver.setName(registerRequest.getName());
        driver.setSurname(registerRequest.getSurname());
        driver.setEmail(registerRequest.getEmail());
        driver.setCity(new City(registerRequest.getCity(), Integer.parseInt(registerRequest.getPostNumber())));
        driver.setPhone(registerRequest.getPhoneNumber());
        driver.setBlocked(false);
        driver.setRole(roleRepository.findById(DRIVER_ID).orElse(null));
        driver.setPassword(encoder.encode(registerRequest.getPassword()));
        driver.setLastPasswordResetDate(new Timestamp((new Date()).getTime()));
        driver.setProvider(Provider.NO_PROVIDER);
        Vehicle vehicle = new Vehicle();
        vehicle.setDriver(driver);
        vehicle.setName(registerRequest.getVehicleName());
        vehicle.setColor(registerRequest.getColor());
        vehicle.setLabel(registerRequest.getLabel());
        vehicle.setNumberOfSeats(registerRequest.getNumberOfSeats());
        vehicle.setBabyFriendly(registerRequest.isBabyFriendly());
        vehicle.setPetFriendly(registerRequest.isPetFriendly());
        vehicle.setType(new VehicleType(registerRequest.getVehicleType(), registerRequest.getPrice()));
        driver.setVehicle(vehicle);
        userRepository.save(driver);
        return true;
    }

    @Override
    public User findByEmail(String credential) {
        return userRepository.findByEmail(credential);
    }

    @Override
    public User findByUsernameNotBlocked(String username) throws NoUserFoundException {
        return userRepository.findByUsernameNotBlocked(username).orElseThrow(() -> {
            throw new NoUserFoundException("No not blocked user was found");
        });
    }

}
