package kts.nwt.ktsnwt.service.notification;

import kts.nwt.ktsnwt.beans.communication.Notification;
import kts.nwt.ktsnwt.beans.communication.NotificationType;
import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.driving.DrivingPayment;
import kts.nwt.ktsnwt.dto.notification.*;
import kts.nwt.ktsnwt.exception.CostumNotFoundException;
import kts.nwt.ktsnwt.repository.NotificationRepository;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class NotificationServiceImpl implements NotificationService{

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private DrivingService drivingService;

    @Autowired
    private SimpMessagingTemplate template;

    public List<NotificationDTO> getNotificationsFor(String username, int interval) {
        LocalDateTime date = LocalDate.now().minusDays(interval).atStartOfDay();
        List<Notification> notifications = notificationRepository.findByClient_UsernameAndDateIsGreaterThanEqual(username, date);
        return convertNotificationsToNotificationsDTO(notifications);
    }

    private List<NotificationDTO> convertNotificationsToNotificationsDTO(List<Notification> notifications) {
        List<NotificationDTO> res = new ArrayList<>();
        for (Notification n : notifications){
            res.add(new NotificationDTO(n));
        }
        return res;
    }

    public void saveRatingNotifications(Driving driving) {
        for (DrivingPayment dp : driving.getPassengers()){
            Notification notification = new Notification();
            notification.setDate(LocalDateTime.now());
            notification.setType(NotificationType.RATE_DRIVING);
            notification.setDriving(driving);
            notification.setClient(dp.getRegisteredUser());
            notificationRepository.save(notification);
        }
    }

    private Notification addSplitFareConfirmationNotification(Driving driving, DrivingPayment drivingPayment) {
        Notification notification = new Notification();
        notification.setDate(driving.getOrderTime());
        notification.setType(NotificationType.SPLIT_FARE_APPROVAL);
        notification.setDriving(driving);
        notification.setClient(drivingPayment.getRegisteredUser());
        return notificationRepository.save(notification);
    }

    @Override
    public DetailedDrivingNotificationDTO getSplitFareNotification(long notificationId) {
        Notification notification = notificationRepository.findById(notificationId).orElseThrow(() -> {
                throw new CostumNotFoundException("Notification not found");
        });
        return new DetailedDrivingNotificationDTO(notification);
    }

    @Override
    public List<DetailedDrivingNotificationDTO> makeDriverNotFoundNotification(Driving driving) {
        List<DetailedDrivingNotificationDTO> res = new ArrayList<>();
        for (DrivingPayment dp : driving.getPassengers()){
            Notification notification = new Notification();
            notification.setClient(dp.getRegisteredUser());
            notification.setDriving(dp.getDriving());
            notification.setType(NotificationType.REFUSED_DRIVING);
            res.add(new DetailedDrivingNotificationDTO(notification));
        }
        return res;
    }

    @Override
    public List<DetailedDrivingNotificationDTO> makeDriverFoundNotification(Driving driving) {
        List<DetailedDrivingNotificationDTO> res = new ArrayList<>();
        for (DrivingPayment dp : driving.getPassengers()){
            Notification notification = new Notification();
            notification.setClient(dp.getRegisteredUser());
            notification.setDriving(dp.getDriving());
            notification.setType(NotificationType.APPROVED_DRIVING);
            res.add(new DetailedDrivingNotificationDTO(notification));
        }
        return res;
    }

    @Override
    public DriverNotificationDTO sendDrivingStartedNotification(long drivingId) {
        Driving driving = drivingService.getDrivingById(drivingId);
        if (driving == null)
            throw new NullPointerException("Driving is null");
        Notification notification = new Notification();
        notification.setClient(null);
        notification.setDriving(driving);
        notification.setDate(LocalDateTime.now());
        notification.setType(NotificationType.ADDED_DRIVING);
        notification = notificationRepository.save(notification);
        return new DriverNotificationDTO(notification);
    }

    @Override
    public List<DriverNotificationDTO> getNotificationsForDriver(String username, int interval) {
        LocalDateTime date = LocalDate.now().minusDays(interval).atStartOfDay();
        List<Notification> notifications = notificationRepository.findByDriver_UsernameAndDateIsGreaterThanEqual(username, date);
        return convertNotificationsToDriverNotificationsDTO(notifications);
    }

    @Override
    public DriverNotificationDTO getDriverNotification(long notificationId) {
        Notification notification = notificationRepository.findById(notificationId).orElseThrow(() -> {
            throw new CostumNotFoundException("Notification not found");
        });
        return new DriverNotificationDTO(notification);
    }

    private List<DriverNotificationDTO> convertNotificationsToDriverNotificationsDTO(List<Notification> notifications) {
        List<DriverNotificationDTO> res = new ArrayList<>();
        for (Notification n : notifications){
            res.add(new DriverNotificationDTO(n));
        }
        return res;
    }

    @Override
    public List<Notification> createSplitFareConfirmationNotification(Driving drivingWithPassengers) {
        List<Notification> result = new ArrayList<>();
        for (DrivingPayment dp : drivingWithPassengers.getPassengers()){
            if (!dp.isConfirmed())
                result.add(addSplitFareConfirmationNotification(drivingWithPassengers, dp));
        }
        return result;
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<NewNotificationDTO> createDriverArrivedNotificationForDriving(long drivingId) {
        Driving driving = drivingService.getDrivingById(drivingId);
        List<NewNotificationDTO> result = new ArrayList<>();
        System.out.println("servis putnici: " + driving.getPassengers().size());
        for (DrivingPayment dp : driving.getPassengers()){
            Notification notification = addDriverArrivedNotification(driving, dp);
            result.add(new NewNotificationDTO(notification));
        }
        return result;
    }


    public Notification addDriverArrivedNotification(Driving driving, DrivingPayment drivingPayment) {
        Notification notification = new Notification();
        try {
            notification.setDate(driving.getOrderTime());
            notification.setType(NotificationType.ARRIVED_DRIVER);
            notification.setDriving(driving);
            notification.setClient(drivingPayment.getRegisteredUser());
            Notification savedNotification = notificationRepository.save(notification);

          //  System.out.println("saved not: " + savedNotification);
          //  System.out.println(savedNotification.getDrivingOrderTime());
          //  System.out.println(savedNotification.getDriverFullname());
            return notification;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Scheduled(initialDelayString = "${future.notification.initialdelay}", fixedRateString = "${future.notification.fixedrate}")
    public void notifyUserForFutureDriving() {
        System.out.println("********** Notify future driving **************");
        LocalDateTime start = LocalDateTime.now().plusMinutes(1);
        LocalDateTime end = LocalDateTime.now().plusMinutes(18);
        //notificationsForMin(start, end, 1);
        List<Driving> drivings = drivingService.getFutureDrivingFromPeriod(true, start, end);
        System.out.println(drivings.size());
        for (Driving driving : drivings) {
            long time = Duration.between(driving.getDrivingStartTime(), LocalDateTime.now()).toMinutes();
            time = Math.abs(time);
            if (time < 17 && time > 13)
                createDrivingReminderNotification(driving, 1);
            else if (time < 12 && time > 8){
                createDrivingReminderNotification(driving, 2);
            }else if (time < 5){
                createDrivingReminderNotification(driving, 3);
            }
        }
    }

    private void createDrivingReminderNotification(Driving driving, int notificationNumber) {
        if (driving.getNotificationSent() == notificationNumber)
            return;
        for (DrivingPayment drivingPayment : driving.getPassengers()){
            Notification notification = new Notification();
            notification.setDate(driving.getOrderTime());
            notification.setType(NotificationType.DRIVING_REMINDER);
            notification.setDriving(driving);
            notification.setClient(drivingPayment.getRegisteredUser());
            Notification savedNotification = notificationRepository.save(notification);
            notification.setId(savedNotification.getId());
            String dest = "/topic/driving/" + notification.getClientUsername();
            template.convertAndSend(dest, new DrivingReminderNotification(notification));
        }
        driving.addNotificationSent(1);
        drivingService.save(driving);

    }


    @Override
    public void createDrivingRefusedNotification(Driving driving) {
        for (DrivingPayment drivingPayment : driving.getPassengers()){
            Notification notification = new Notification();
            notification.setDate(driving.getOrderTime());
            notification.setType(NotificationType.REFUSED_DRIVING);
            notification.setDriving(driving);
            notification.setClient(drivingPayment.getRegisteredUser());
            Notification savedNotification = notificationRepository.save(notification);
            String dest = "/topic/driving/" + notification.getClientUsername();
            template.convertAndSend(dest, new DetailedDrivingNotificationDTO(savedNotification));
        }
    }
}
