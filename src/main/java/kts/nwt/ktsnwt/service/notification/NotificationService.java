package kts.nwt.ktsnwt.service.notification;

import kts.nwt.ktsnwt.beans.communication.Notification;
import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.dto.notification.DriverNotificationDTO;
import kts.nwt.ktsnwt.dto.notification.NewNotificationDTO;
import kts.nwt.ktsnwt.dto.notification.NotificationDTO;
import kts.nwt.ktsnwt.dto.notification.DetailedDrivingNotificationDTO;

import java.util.List;

public interface NotificationService {
    List<NotificationDTO> getNotificationsFor(String username, int interval);
    void saveRatingNotifications(Driving driving);

    DetailedDrivingNotificationDTO getSplitFareNotification(long notificationId);

    List<DetailedDrivingNotificationDTO> makeDriverNotFoundNotification(Driving driving);
    List<DetailedDrivingNotificationDTO> makeDriverFoundNotification(Driving driving);

    DriverNotificationDTO sendDrivingStartedNotification(long drivingId);

    List<DriverNotificationDTO> getNotificationsForDriver(String username, int interval);

    DriverNotificationDTO getDriverNotification(long notificationId);

    List<Notification> createSplitFareConfirmationNotification(Driving drivingWithPassengers);

    List<NewNotificationDTO> createDriverArrivedNotificationForDriving(long drivingId);

    void createDrivingRefusedNotification(Driving d);
}
