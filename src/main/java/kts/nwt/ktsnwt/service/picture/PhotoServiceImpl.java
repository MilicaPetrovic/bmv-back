package kts.nwt.ktsnwt.service.picture;

import kts.nwt.ktsnwt.service.picture.interfaces.PhotoService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class PhotoServiceImpl implements PhotoService {


    @Override
    public String addPhoto(Long id, String directory, MultipartFile[] multipartFiles, String role) throws IOException {
        if (multipartFiles == null) {
            return "";
        }
        Path path = Paths.get(directory + role + "_" + id);
        return savePicturesOnPath(multipartFiles, path);
    }

    private String savePicturesOnPath(MultipartFile[] multipartFiles, Path path) throws IOException {
        if (!Files.exists(path)) {
            Files.createDirectories(path);
        }
        MultipartFile mpf = multipartFiles[0];
        String fileName = mpf.getOriginalFilename();
        Path filePath;
        try (InputStream inputStream = mpf.getInputStream()) {
            filePath = path.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {
            throw new IOException("Could not save image file: " + fileName, ioe);
        }
        return filePath.toString();
    }

    @Override
    public void tryDeletePhoto(String photoPath) {
        try {
            Path path = Paths.get(photoPath);
            Files.delete(path);
        }
        catch (Exception ignored) {

        }
    }
}
