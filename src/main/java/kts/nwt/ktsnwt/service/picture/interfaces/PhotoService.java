package kts.nwt.ktsnwt.service.picture.interfaces;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface PhotoService {

    String addPhoto(Long id,String directory, MultipartFile[] multipartFiles, String role) throws IOException;
    void tryDeletePhoto(String photoPath);
}
