package kts.nwt.ktsnwt.service.mail;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.service.mail.interfaces.VerificationMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class VerificationMailServiceImpl implements VerificationMailService {

    @Value("${app.basic.url}")
    private String basicURL;

    @Value("${app.sendgrid.account.verification.templateId}")
    private String templateId;

    @Autowired
    private SendGrid sendGrid;

    @Override
    public void sendVerificationMailToUser(RegisteredUser user) {

        Mail mail = new Mail();
        Email fromEmail = new Email();
        fromEmail.setEmail("student.kts.nvt.bmv@outlook.com");
        mail.setFrom(fromEmail);

        Email to = new Email();
        to.setEmail(user.getEmail());

        Personalization personalization = new Personalization();
        personalization.addDynamicTemplateData("verificationURL", basicURL + "registered/verify/" + user.getUsername() + "/" + user.getVerificationCode());

        personalization.addTo(to);
        mail.addPersonalization(personalization);
        mail.setTemplateId(templateId);


        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        try {
            request.setBody(mail.build());
            Response response = sendGrid.api(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
