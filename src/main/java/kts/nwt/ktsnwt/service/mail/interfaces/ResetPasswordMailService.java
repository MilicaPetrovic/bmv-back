package kts.nwt.ktsnwt.service.mail.interfaces;

import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.UserNotEnabledException;

public interface ResetPasswordMailService {

    void sendMailForUserToResetPassword(String username) throws UserNotEnabledException, NoUserFoundException;
}
