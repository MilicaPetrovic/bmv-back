package kts.nwt.ktsnwt.service.mail.interfaces;

import kts.nwt.ktsnwt.beans.users.RegisteredUser;

public interface VerificationMailService {
    void sendVerificationMailToUser(RegisteredUser user);
}
