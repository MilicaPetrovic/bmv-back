package kts.nwt.ktsnwt.service.mail;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.UserNotEnabledException;
import kts.nwt.ktsnwt.repository.UserRepository;
import kts.nwt.ktsnwt.service.mail.interfaces.ResetPasswordMailService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;

@Service
public class ResetPasswordMailServiceImpl implements ResetPasswordMailService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SendGrid sendGrid;

    @Value("${app.sendgrid.templateId}")
    private String templateId;

    @Value("${app.basic.url}")
    private String basicURL;

    public void sendMailForUserToResetPassword(String email) throws UserNotEnabledException, NoUserFoundException {
        User u = userRepository.findByEmail(email);

        if (u == null){
            throw new NoUserFoundException("User: " + email + " not found");
        }

        sendMailForPasswordReset(u);
    }

    private void sendMailForPasswordReset(User u) throws UserNotEnabledException {
        Mail mail = new Mail();
        Email fromEmail = new Email();
        fromEmail.setEmail("student.kts.nvt.bmv@outlook.com");
        mail.setFrom(fromEmail);

        Email to = new Email();
        to.setEmail(u.getEmail());

        Personalization personalization = new Personalization();
        personalization.addDynamicTemplateData("costumURL", basicURL + "profile/reset/password/" + u.getUsername() + "/" + generateResetPasswordCode(u));

        personalization.addTo(to);
        mail.addPersonalization(personalization);
        mail.setTemplateId(templateId);


        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        try {
            request.setBody(mail.build());
            Response response = sendGrid.api(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String generateResetPasswordCode(User u) throws UserNotEnabledException {
        if (!u.isEnabled())
            throw new UserNotEnabledException("User: " + u.getUsername() + " is not enabled");
        String randomCode = RandomString.make(64);
        u.setResetPasswordCode(randomCode);
        u.setResetPasswordTime(LocalDateTime.now());
        userRepository.save(u);
        return randomCode;
    }
}
