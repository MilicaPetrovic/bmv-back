package kts.nwt.ktsnwt.service.report;

import kts.nwt.ktsnwt.beans.driving.Driving;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReportUtils {
    public List<Integer> getNumberOfDrivingForDays(List<LocalDate> labels, List<Driving> drivings) {
        List<Integer> result = new ArrayList<>();
        for (LocalDate localDate : labels){
            System.out.println("--------");
            List<Driving> thatDayList = findDrivingsInThatDay(localDate, drivings);
            System.out.println(localDate);
            System.out.println(thatDayList.size());
            result.add(thatDayList.size());
        }
        return result;
    }

    public List<Double> getDistancesForDays(List<LocalDate> labels, List<Driving> drivings) {
        List<Double> result = new ArrayList<>();
        for (LocalDate localDate : labels){
            List<Driving> thatDayList = findDrivingsInThatDay(localDate, drivings);

            result.add(countDistance(thatDayList));
        }
        return result;
    }
    public List<Double> getMoneySpentForDaysByClient(List<LocalDate> labels, List<Driving> drivings, String username) {
        List<Double> result = new ArrayList<>();
        for (LocalDate localDate : labels){
            List<Driving> thatDayList = findDrivingsInThatDay(localDate, drivings);
            result.add(countMoneyByClient(thatDayList, username));
        }
        return result;
    }

    private Double countMoneyByClient(List<Driving> thatDayList, String username) {
        double res = 0;
        for (Driving d : thatDayList){
            res += d.getMoneySpentByPassenger(username);
        }
        return res;
    }

    private Double countDistance(List<Driving> thatDayList) {
        double res = 0;
        for (Driving d : thatDayList){
            res += d.getDistance();
        }
        return res;
    }

    private List<Driving> findDrivingsInThatDay(LocalDate day, List<Driving> drivings) {
        List<Driving> result = new ArrayList<>();
        for (Driving d: drivings){
            System.out.println("-*-*");
            System.out.println(d.getDrivingStartTime());
            System.out.println(day);
            System.out.println("-*-*");
            if (d.getDrivingStartTime().toLocalDate().isEqual(day))
                result.add(d);
            if (d.getDrivingStartTime().toLocalDate().isAfter(day))
                break;
        }
        return result;
    }

    public List<LocalDate> getLabelsForDays(LocalDate start, LocalDate end) {
        List<LocalDate> labels = new ArrayList<>();
        while (start.isBefore(end)){
            labels.add(start);
            start = start.plusDays(1);
        }
        return labels;
    }


    public List<Double> getMoneyEarnedForDays(List<LocalDate> labels, List<Driving> drivings) {
        List<Double> result = new ArrayList<>();
        for (LocalDate localDate : labels){
            List<Driving> thatDayList = findDrivingsInThatDay(localDate, drivings);
            result.add(countMoneyForDriver(thatDayList));
        }
        return result;
    }

    private Double countMoneyForDriver(List<Driving> thatDayList) {
        double res = 0;
        for (Driving d : thatDayList){
            res += d.getPrice();
        }
        return res;
    }
}
