package kts.nwt.ktsnwt.service.report;

import kts.nwt.ktsnwt.dto.report.InconsistencyDTO;
import kts.nwt.ktsnwt.dto.report.ReportDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public interface ReportService {

    ReportDTO getReportByClient(String username, LocalDate start, LocalDate end);

    ReportDTO getReportByDriver(String username, LocalDate start, LocalDate end);

    ReportDTO getReportForAll(LocalDate start, LocalDate end);

    void reportInconsistency(InconsistencyDTO inconsistencyDTO) throws NoUserFoundException;
}
