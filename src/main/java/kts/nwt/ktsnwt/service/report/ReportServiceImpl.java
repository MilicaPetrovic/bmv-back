package kts.nwt.ktsnwt.service.report;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.driving.DrivingState;
import kts.nwt.ktsnwt.beans.feedback.Report;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.dto.report.InconsistencyDTO;
import kts.nwt.ktsnwt.dto.report.ReportDTO;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class ReportServiceImpl implements ReportService{

    @Autowired
    private DrivingRepository drivingRepository;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private ReportUtils reportUtils;

    @Override
    public ReportDTO getReportByClient(String username, LocalDate start, LocalDate end) {
        List<Driving> drivings = drivingRepository.getDrivingsByClientsUsernameInPeriod(username, DrivingState.SUCCESSFUL, start.atStartOfDay(), end.atStartOfDay());
        System.out.println("Driving size: " + drivings.size());
        for (Driving d : drivings){
            System.out.println(d.getId());
        }
        List<LocalDate> labels = reportUtils.getLabelsForDays(start, end);
        System.out.println("Label size: " + labels.size());
        //za svaki dan
        //broj voznji
        List<Integer> numberOfDrivings = reportUtils.getNumberOfDrivingForDays(labels, drivings);
        //broj predenih kilometara
        List<Double> distancesForDays = reportUtils.getDistancesForDays(labels, drivings);
        //kolicina novca
        List<Double> moneyForDays = reportUtils.getMoneySpentForDaysByClient(labels, drivings, username);

        //ukupna suma i prosek
        //broj voznji
        return new ReportDTO(numberOfDrivings, distancesForDays, moneyForDays, labels);
    }

    @Override
    public ReportDTO getReportByDriver(String username, LocalDate start, LocalDate end) {
        List<Driving> drivings = drivingRepository.getDrivingsByDriverInPeriod(username, DrivingState.SUCCESSFUL, start.atStartOfDay(), end.atStartOfDay());
        System.out.println("Driving size: " + drivings.size());
        List<LocalDate> labels = reportUtils.getLabelsForDays(start, end);
        System.out.println("Label size: " + labels.size());
        List<Integer> numberOfDrivings = reportUtils.getNumberOfDrivingForDays(labels, drivings);
        List<Double> distancesForDays = reportUtils.getDistancesForDays(labels, drivings);
        List<Double> moneyForDays = reportUtils.getMoneyEarnedForDays(labels, drivings);

        return new ReportDTO(numberOfDrivings, distancesForDays, moneyForDays, labels);
    }

    @Override
    public ReportDTO getReportForAll(LocalDate start, LocalDate end) {
        List<Driving> drivings = drivingRepository.getDrivingsByInPeriod(DrivingState.SUCCESSFUL, start.atStartOfDay(), end.atStartOfDay().plusDays(2));
        System.out.println("Driving size: " + drivings.size());
        List<LocalDate> labels = reportUtils.getLabelsForDays(start, end);
        System.out.println("Label size: " + labels.size());
        List<Integer> numberOfDrivings = reportUtils.getNumberOfDrivingForDays(labels, drivings);
        List<Double> distancesForDays = reportUtils.getDistancesForDays(labels, drivings);
        List<Double> moneyForDays = reportUtils.getMoneyEarnedForDays(labels, drivings);

        return new ReportDTO(numberOfDrivings, distancesForDays, moneyForDays, labels);
    }

    @Override
    public void reportInconsistency(InconsistencyDTO inconsistencyDTO) throws NoUserFoundException {
        RegisteredUser registeredUser = registeredUserRepository.findById(inconsistencyDTO.getRegisteredUserId()).orElse(null);;
        Driving driving = drivingRepository.findDrivingById(inconsistencyDTO.getDrivingId()).orElse(null);
        if(driving == null){
            throw new NoUserFoundException("Nije pronadjena voznja");
        } else if (registeredUser == null) {
            throw new NoUserFoundException("Nije pronadjena klijent");
        }
        Report report = new Report();
        report.setDriving(driving);
        report.setDriver(driving.getDriver());
        report.setClient(registeredUser);
        this.reportRepository.save(report);
    }

}
