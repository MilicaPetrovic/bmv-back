package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CanNotMakeNewDrivingException extends Exception implements CustomException {
    private int code = 14;
    private String message;

    public CanNotMakeNewDrivingException(String message) {
        this.message = message;
    }
}
