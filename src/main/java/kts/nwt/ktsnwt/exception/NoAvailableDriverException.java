package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoAvailableDriverException extends Exception implements CustomException {
    private int code = 17;
    private String message;

    public NoAvailableDriverException(String message) {
        this.message = message;
    }
}
