package kts.nwt.ktsnwt.exception;

public interface CustomException {
    int code = -1;
    String message = "";
    int getCode();
    String getMessage();
}
