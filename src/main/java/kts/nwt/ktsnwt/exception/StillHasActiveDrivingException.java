package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StillHasActiveDrivingException  extends Exception implements CustomException {
    private int code = 18;
    private String message;
    public StillHasActiveDrivingException(String message) {
        this.message = message;
    }
}