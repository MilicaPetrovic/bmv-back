package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InvalidDrivingStateException extends RuntimeException implements CustomException{
    private int code = 21;
    private String message;
    public InvalidDrivingStateException(String message) {
        this.message = message;
    }
}
