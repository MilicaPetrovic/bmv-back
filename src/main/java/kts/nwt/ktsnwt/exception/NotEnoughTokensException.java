package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotEnoughTokensException extends Exception implements CustomException{

    private int code = 15;
    private String message;

    public NotEnoughTokensException(String message){
        this.message = message;
    }
}
