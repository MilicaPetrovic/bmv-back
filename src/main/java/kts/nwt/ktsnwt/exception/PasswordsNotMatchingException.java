package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PasswordsNotMatchingException extends Exception implements CustomException {

    private int code = 10;

    private String message;

    public PasswordsNotMatchingException(String message) {
        this.message = message;
    }

}
