package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserNotEnabledException extends Exception implements CustomException {
    private int code = 6;
    private String message;
    public UserNotEnabledException(String message) {
        this.message = message;
    }
}
