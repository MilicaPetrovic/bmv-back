package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotInTimeException extends Exception implements CustomException {
    private int code = 5;
    private String message;
    public NotInTimeException(String message) {
        this.message = message;
    }
}
