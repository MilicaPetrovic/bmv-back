package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AlreadyInUse extends Exception implements CustomException {
    private int code = 4;
    private String message;
    public AlreadyInUse(String message) {
        this.message = message;
    }
}
