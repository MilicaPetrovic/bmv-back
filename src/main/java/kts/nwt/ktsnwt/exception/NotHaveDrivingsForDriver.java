package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotHaveDrivingsForDriver  extends Exception implements CustomException {
    private int code = 22;
    private String message;
    public NotHaveDrivingsForDriver(String message) {
        this.message = message;
    }
}
