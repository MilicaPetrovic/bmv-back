package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InvalidFutureTimeException extends Exception implements CustomException {
    private int code = 20;
    private String message;
    public InvalidFutureTimeException(String message) {
        this.message = message;
    }
}
