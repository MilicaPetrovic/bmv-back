package kts.nwt.ktsnwt.exception;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UnauthorizedRequest extends Exception implements CustomException {
    private int code = 8;
    private String message;
    public UnauthorizedRequest(String message) {
        this.message = message;
    }
}
