package kts.nwt.ktsnwt.exception;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoProfileChangeRequestFoundException extends Exception implements CustomException{

    private int code = 11;
    private String message;

    public NoProfileChangeRequestFoundException(String message){
        this.message = message;
    }

}
