package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoUserFoundException extends RuntimeException implements CustomException {
    private int code = 1;
    private String message;

    public NoUserFoundException(String message){
        this.message = message;
    }
}
