package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotAllUsersConfirmedException extends Exception implements CustomException{

    private int code = 16;
    private String message;

    public NotAllUsersConfirmedException(String message){
        this.message = message;
    }
}
