package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CostumNotFoundException extends RuntimeException implements CustomException {
    private int code = 9;
    private String message;
    public CostumNotFoundException(String message) {
        this.message = message;
    }
}
