package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AlreadyEndedException extends Exception implements CustomException {
    private int code = 13;
    private String message;
    public AlreadyEndedException(String message) {
        this.message = message;
    }

}
