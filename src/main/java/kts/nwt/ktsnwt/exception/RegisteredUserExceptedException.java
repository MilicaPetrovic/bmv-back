package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisteredUserExceptedException extends Exception implements CustomException {
    private int code = 2;
    private String message;

    public RegisteredUserExceptedException(String message){
        this.message = message;
    }
}
