package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UnexpectedException extends Exception implements CustomException {
    private int code = 3;
    private String message = "Something unexpected happened";

    public UnexpectedException(String message){
        this.message = message;
    }

    public UnexpectedException(){}
}
