package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoMatchingException extends Exception implements CustomException {
    private int code = 8;
    private String message;
    public NoMatchingException(String message) {
        this.message = message;
    }
}
