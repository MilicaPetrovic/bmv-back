package kts.nwt.ktsnwt.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShouldNotHappenException extends Exception implements CustomException {
    private int code = 12;
    private String message;

    public ShouldNotHappenException(String message){
        this.message = message;
    }
}
