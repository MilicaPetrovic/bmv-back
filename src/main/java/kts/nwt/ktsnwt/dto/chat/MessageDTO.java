package kts.nwt.ktsnwt.dto.chat;

import kts.nwt.ktsnwt.beans.communication.Message;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class MessageDTO {
    private String message;
    private LocalDateTime dateSent;
    private String fromId;
    private String toId;

    public MessageDTO(Message message) {
        this.message = message.getText();
        this.dateSent = message.getDateTime();
        this.fromId = message.getSender().getMessageUsername();
        if (message.getReceiver() == null)
            this.toId = "admin";
        else
            this.toId = message.getReceiver().getMessageUsername();
    }
}
