package kts.nwt.ktsnwt.dto.chat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NotifyMessageReceivedDTO {
    private FriendsListDTO user;
    private MessageDTO message;
}
