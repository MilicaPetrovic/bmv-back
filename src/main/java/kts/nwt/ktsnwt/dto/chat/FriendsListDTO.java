package kts.nwt.ktsnwt.dto.chat;

import kts.nwt.ktsnwt.beans.users.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FriendsListDTO {
    private String username;
    private String displayName;
    private long userType;

    private String profilePhoto;

    public FriendsListDTO(User u) {
        if (u.isAdmin()) {
            this.username = "admin";
            this.displayName= "Admin";
            return;
        }
        this.username = u.getUsername();
        this.displayName = u.getFullname();
        this.userType = u.getRoleId();
        this.profilePhoto = u.getProfilePhotoLink();

    }
}
