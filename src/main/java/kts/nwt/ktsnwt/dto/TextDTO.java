package kts.nwt.ktsnwt.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TextDTO {
    private String text;
}
