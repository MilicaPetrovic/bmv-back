package kts.nwt.ktsnwt.dto.notification;

import kts.nwt.ktsnwt.beans.communication.Notification;
import kts.nwt.ktsnwt.beans.driving.Driving;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class DetailedDrivingNotificationDTO {

    private String username;

    private String type;
    private String orderedUsername;
    private String orderedFullname;
    private LocalDateTime orderTime;
    private String departure;
    private String arrival;
    private double price;
    private double partialPrice;

    private long drivingId;
    public DetailedDrivingNotificationDTO(Notification notification){
        this.type = notification.getType().toString();
        this.username = notification.getClient().getUsername();
        this.orderedUsername = notification.getOrderedUsername();
        this.orderedFullname = notification.getOrderedFullname();
        Driving d = notification.getDriving();
        this.orderTime = d.getOrderTime();
        this.departure = d.getDeparture();
        this.arrival = d.getArrival();
        this.price = d.getPrice();
        this.partialPrice = notification.getMyPrice();
        this.drivingId = d.getId();
    }
}
