package kts.nwt.ktsnwt.dto.notification;

import kts.nwt.ktsnwt.beans.communication.Notification;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NotificationDTO {
    private long id;
    private long drivingId;
    private LocalDateTime date;
    private String type;
    private LocalDateTime orderTime;
    private boolean driverAssigned;
    private String driverName;
    private boolean canMakeAnAction;

    public NotificationDTO(Notification notification) {
        this.id = notification.getId();
        this.drivingId = notification.getDriving().getId();
        this.date = notification.getDate();
        this.type = notification.getType().toString();
        this.orderTime = notification.getDrivingOrderTime();
        this.driverAssigned = notification.isDriverAssigned();
        this.driverName = notification.getDriverFullname();
        this.canMakeAnAction = notification.canMakeAnAction();
    }
}
