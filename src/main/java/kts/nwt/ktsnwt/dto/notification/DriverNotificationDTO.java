package kts.nwt.ktsnwt.dto.notification;

import kts.nwt.ktsnwt.beans.communication.Notification;
import kts.nwt.ktsnwt.beans.driving.Driving;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class DriverNotificationDTO {

    private long id;
    private long drivingId;
    private LocalDateTime date;
    private String type;
    private boolean canMakeAnAction;

    private String departure;

    private String arrival;

    private LocalDateTime orderTime;

    public DriverNotificationDTO(Notification notification) {
        this.id = notification.getId();
        this.date = LocalDateTime.now();
        this.type = notification.getType().name();
        this.canMakeAnAction = notification.canMakeAnAction();
        Driving d = notification.getDriving();
        this.drivingId = d.getId();
        this.departure = d.getDeparture();
        this.arrival = d.getArrival();
        this.orderTime = d.getOrderTime();
    }
}
