package kts.nwt.ktsnwt.dto.notification;

import kts.nwt.ktsnwt.beans.communication.Notification;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Duration;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DrivingReminderNotification {
    private String client;

    private long drivingId;
    private LocalDateTime date;
    private String type;
    private LocalDateTime orderTime;
    private boolean driverAssigned;
    private String driverName;
    private String departure;
    private String arrival;
    private boolean canMakeAnAction;

    private int time;

    public DrivingReminderNotification(Notification notification) {
        this.client = notification.getClientUsername();
        this.drivingId = notification.getDriving().getId();
        this.date = notification.getDate(); // start - 15
        this.type = notification.getType().toString();
        this.orderTime = notification.getDrivingOrderTime();
        this.driverAssigned = notification.isDriverAssigned();
        this.driverName = notification.getDriverFullname();
        this.canMakeAnAction = notification.canMakeAnAction();
        this.departure = notification.getDriving().getDeparture();
        this.arrival = notification.getDriving().getArrival();
        this.time = (int) Duration.between(notification.getDriving().getDrivingStartTime(), LocalDateTime.now()).toMinutes();
        this.time = Math.abs(this.time);
    }
}
