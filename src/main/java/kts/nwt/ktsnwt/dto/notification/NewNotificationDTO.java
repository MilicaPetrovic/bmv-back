package kts.nwt.ktsnwt.dto.notification;

import kts.nwt.ktsnwt.beans.communication.Notification;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class NewNotificationDTO {
    private String client;

    private long drivingId;
    private LocalDateTime date;
    private String type;
    private LocalDateTime orderTime;
    private boolean driverAssigned;
    private String driverName;

    private boolean canMakeAnAction;

    public NewNotificationDTO(Notification notification) {
        this.client = notification.getClientUsername();
        this.drivingId = notification.getDriving().getId();
        this.date = notification.getDate();
        this.type = notification.getType().toString();
        this.orderTime = notification.getDrivingOrderTime();
        this.driverAssigned = notification.isDriverAssigned();
        this.driverName = notification.getDriverFullname();
        this.canMakeAnAction = notification.canMakeAnAction();
    }
}
