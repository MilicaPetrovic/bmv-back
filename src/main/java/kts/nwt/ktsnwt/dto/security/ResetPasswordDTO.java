package kts.nwt.ktsnwt.dto.security;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ResetPasswordDTO {
    private String username;
    private String resetCode;
    private String newPassword;
}
