package kts.nwt.ktsnwt.dto.driving;

import kts.nwt.ktsnwt.dto.location.MapPointDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class MakeDrivingDTO {

    //stations
    private ArrayList<MapPointDTO> stations;
    //podaci za path
    private ArrayList<MapPointDTO> coordinates;

    private String applicantUsername;
    private List<String> linkedUsers;
    private long vehicleTypeId;
    private boolean petFriendly;
    private boolean babyFriendly;
    private double distance;
    private double time;
    private double price;
    private boolean favouriteRoute;
    private boolean future;
    private int hour;
    private int minute;
}
