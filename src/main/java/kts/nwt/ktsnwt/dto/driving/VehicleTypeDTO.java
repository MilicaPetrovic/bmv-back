package kts.nwt.ktsnwt.dto.driving;

import kts.nwt.ktsnwt.beans.driving.VehicleType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class VehicleTypeDTO {
    private Long id;
    private String name;
    private double price;

    public VehicleTypeDTO(VehicleType vehicleType){
        this.id = vehicleType.getId();
        this.name = vehicleType.getName();
        this.price = vehicleType.getPrice();
    }

}
