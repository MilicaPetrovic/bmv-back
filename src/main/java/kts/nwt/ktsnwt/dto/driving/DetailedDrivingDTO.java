package kts.nwt.ktsnwt.dto.driving;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.driving.DrivingPayment;
import kts.nwt.ktsnwt.beans.feedback.Rating;
import kts.nwt.ktsnwt.beans.feedback.Report;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DetailedDrivingDTO {
    private double price;
    private String state;

    private boolean rejected;
    private String rejectionText;
    private LocalDateTime orderTime;
    private boolean finished;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    private DrivingDriverDTO driver;
    private List<DrivingPassengerDTO> passengers;

    private boolean babyFriendly;
    private boolean petFriendly;
    private String vehicleType;

    public DetailedDrivingDTO(Driving driving){
        fillDrivingData(driving);
    }

    public DetailedDrivingDTO(Driving driving, double driverRating, double vehicleRating){
        fillDrivingData(driving);
        this.driver.setDriverRating(Math.ceil(driverRating));
        this.driver.setVehicleRating(Math.ceil(vehicleRating));
        if (driverRating != 0 | vehicleRating != 0)
            this.driver.setRated(true);
    }

    public DetailedDrivingDTO(DetailedDrivingDTO detailedDrivingDTO) {
        this.price = detailedDrivingDTO.getPrice();
        this.state = detailedDrivingDTO.getState();
        this.rejected = detailedDrivingDTO.isRejected();
        this.rejectionText = detailedDrivingDTO.getRejectionText();
        this.orderTime = detailedDrivingDTO.getOrderTime();
        this.finished = detailedDrivingDTO.isFinished();
        this.startTime = detailedDrivingDTO.getStartTime();
        this.endTime = detailedDrivingDTO.getEndTime();
        this.driver = detailedDrivingDTO.getDriver();
        this.passengers = detailedDrivingDTO.getPassengers();
        this.vehicleType = detailedDrivingDTO.getVehicleType();
        this.babyFriendly = detailedDrivingDTO.isBabyFriendly();
        this.petFriendly = detailedDrivingDTO.isPetFriendly();
    }

    private void fillDrivingData(Driving driving) {
        this.price = driving.getPrice();
        this.state = driving.getState().toString();
        this.rejected = driving.isRejected();
        this.rejectionText = driving.getRejectionText();
        this.orderTime = driving.getOrderTime();
        this.babyFriendly = driving.isBabyFriendlyDriving();
        this.petFriendly = driving.isPetFriendlyDriving();
        this.vehicleType = driving.getVehicleTypeName();
        if (driving.getDrivingTime() != null){
            this.finished = driving.isFinished();
            this.startTime = driving.getRealDrivingStartTime();
            this.endTime = driving.getRealDrivingEndTime();
        }
        this.driver = new DrivingDriverDTO(driving.getDriver());
        this.passengers = new ArrayList<>();
        for (DrivingPayment dp : driving.getPassengers()){
            Rating rating = null;
            Report report = null;
            for (Rating r : driving.getRatings()){
                if (r.getClient() == dp.getRegisteredUser()) {
                    rating = r;
                    break;
                }
            }
            for (Report r: driving.getReports()){
                if (r.getClient() == dp.getRegisteredUser()) {
                    report = r;
                    break;
                }
            }
            this.passengers.add(new DrivingPassengerDTO(dp, rating, report));
        }
    }

}
