package kts.nwt.ktsnwt.dto.driving;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RejectDrivingDTO {

    private long drivingId;
    private String reason;
}
