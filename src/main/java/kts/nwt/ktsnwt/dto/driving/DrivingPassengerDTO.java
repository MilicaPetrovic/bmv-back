package kts.nwt.ktsnwt.dto.driving;

import kts.nwt.ktsnwt.beans.driving.DrivingPayment;
import kts.nwt.ktsnwt.beans.feedback.Rating;
import kts.nwt.ktsnwt.beans.feedback.Report;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DrivingPassengerDTO {
    private String username;
    private String fullName;

    private int  driverRating;
    private int vehicleRating;
    private String text;

    private boolean paid;
    private double price;
    private boolean reported;

    private boolean rated;

    public DrivingPassengerDTO(DrivingPayment dp, Rating rating, Report report) {
        this.username = dp.getPassangerUsername();
        this.fullName = dp.getPassangerFullname();
        if (rating != null) {
            this.driverRating = rating.getDriverRating();
            this.vehicleRating = rating.getVehicleRating();
            this.text = rating.getText();
            this.rated = true;
        }else{
            this.rated = false;
        }
        this.price = dp.getPrice();
        this.reported = report != null;
        this.paid = dp.isPaid();
    }
}
