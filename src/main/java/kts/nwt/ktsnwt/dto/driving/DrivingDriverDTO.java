package kts.nwt.ktsnwt.dto.driving;

import kts.nwt.ktsnwt.beans.users.Driver;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DrivingDriverDTO {
    private String username;
    private String fullName;
    private double driverRating;
    private double vehicleRating;
    private boolean rated;


    public DrivingDriverDTO(Driver driver) {
        if (driver == null)
            return;
        this.username = driver.getUsername();
        this.fullName = driver.getFullname();
    }

}
