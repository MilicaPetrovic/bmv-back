package kts.nwt.ktsnwt.dto.driving;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DrivingStateDTO {
    private boolean start = false;
    private boolean finish = false;

    private long drivingId = -1;

    public DrivingStateDTO(boolean start, boolean finish, long drivingId){
        this.start = start;
        this.finish = finish;
        this.drivingId = drivingId;
    }
}
