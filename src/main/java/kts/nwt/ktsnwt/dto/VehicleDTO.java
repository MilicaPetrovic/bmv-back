package kts.nwt.ktsnwt.dto;

import kts.nwt.ktsnwt.beans.driving.Vehicle;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
    public class VehicleDTO {
        private String name;
        private String vehicleType;
        private int vehiclePrice;
        private String color;
        private int yearOfProduction;
        private String label;
        private boolean babyFriendly;
        private boolean petFriendly;
        private int numberOfSeats;

    public VehicleDTO(Vehicle vehicle) {
        this.name = vehicle.getName();
        this.vehicleType = vehicle.getVehicleTypeName();
        this.vehiclePrice = vehicle.getVehicleTypePrice();
        this.color = vehicle.getColor();
        this.yearOfProduction = vehicle.getYearOfProduction();
        this.label = vehicle.getLabel();
        this.babyFriendly = vehicle.isBabyFriendly();
        this.petFriendly = vehicle.isPetFriendly();
        this.numberOfSeats = vehicle.getNumberOfSeats();
    }
}
