package kts.nwt.ktsnwt.dto.location;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LatLngDTO {
    private double latitude;
    private double longitude;

}
