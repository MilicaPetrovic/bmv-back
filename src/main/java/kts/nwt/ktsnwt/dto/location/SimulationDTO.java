package kts.nwt.ktsnwt.dto.location;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;

@Data
@AllArgsConstructor
public class SimulationDTO {
    Long id;
    ArrayList<MapPointDTO> coordinates;
    SimulationVehicleDTO simulationVehicleDTO;
    Integer drivingState;
    Boolean isPaid;

    ArrayList<MapPointDTO> stations;

    public SimulationDTO( Long id, ArrayList<MapPointDTO> coordinates, Boolean isPaid, ArrayList<MapPointDTO> stations){
        this.id = id;
        this.coordinates = coordinates;
        this.isPaid = isPaid;
        this.stations = stations;
    }

    @Override
    public String toString() {
        return "SimulationDTO{" +
                "id=" + id +
                ", coordinates=" + coordinates +
                ", simulationVehicleDTO=" + simulationVehicleDTO +
                ", drivingState=" + drivingState +
                ", isPaid=" + isPaid +
                ", stations=" + stations +
                '}';
    }
}
