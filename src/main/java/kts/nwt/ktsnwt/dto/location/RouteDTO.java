package kts.nwt.ktsnwt.dto.location;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
public class RouteDTO {
    private ArrayList<MapPointDTO> stations;
    private ArrayList<MapPointDTO> coordinates;

    private double totalTime;
    private double totalDistance;
    private double price;

    public RouteDTO(ArrayList<MapPointDTO> stations, ArrayList<MapPointDTO> coordinates, double totalTime, double totalDistance, double price){
        this.stations = stations;
        this.coordinates = coordinates;
        this.totalTime = totalTime;
        this.totalDistance = totalDistance;
        this.price = price;
    }
}
