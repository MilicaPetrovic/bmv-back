package kts.nwt.ktsnwt.dto.location;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MapPointDTO {
    private double lng;
    private double lat;
    private String name;

    public MapPointDTO(double longitude, double latitude) {
        this.lng = longitude;
        this.lat = latitude;
        this.name="";
    }

    public MapPointDTO(double longitude, double latitude, String name) {
        this.lng = longitude;
        this.lat = latitude;
        this.name= name;
    }

}
