package kts.nwt.ktsnwt.dto.location;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SimulationVehicleDTO {
    private long id;
    private String label;
    private double latitude;
    private double longitude;
    private long driverId;
}
