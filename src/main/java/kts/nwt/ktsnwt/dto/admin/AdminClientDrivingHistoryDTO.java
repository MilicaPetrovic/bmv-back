package kts.nwt.ktsnwt.dto.admin;

import kts.nwt.ktsnwt.beans.driving.Driving;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class AdminClientDrivingHistoryDTO {
    private Long id;
    private String drivingState;
    private double price;
    private LocalDateTime orderTime;
    private String departure;
    private String arrival;
    private String driverName;
    private boolean reported;

    public AdminClientDrivingHistoryDTO(Driving d) {
        this.id = d.getId();
        this.drivingState = d.getState().toString();
        this.price = d.getPrice();
        this.orderTime = d.getOrderTime();
        this.departure = d.getDeparture();
        this.arrival = d.getArrival();
        if (d.getDriver() != null)
            this.driverName = d.getDriver().getFullname();
        this.reported = d.isReported();
    }
}
