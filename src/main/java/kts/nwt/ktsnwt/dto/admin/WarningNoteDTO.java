package kts.nwt.ktsnwt.dto.admin;

import kts.nwt.ktsnwt.beans.feedback.WarningNote;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WarningNoteDTO {
    private String warningText;
    private String adminName;

    public WarningNoteDTO(WarningNote warningNote){
        this.warningText = warningNote.getText();
        this.adminName = warningNote.getAdmin().getFullname();
    }
}
