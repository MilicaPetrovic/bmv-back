package kts.nwt.ktsnwt.dto.admin;

import kts.nwt.ktsnwt.beans.users.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AdminUsersDTO {
    private String name;
    private String surname;
    private String email;
    private String username;
    private String phone;
    //image
    private boolean blocked;

    public AdminUsersDTO(User user){
        this.name = user.getName();
        this.surname = user.getSurname();
        this.email = user.getEmail();
        this.username = user.getUsername();
        this.phone = user.getPhone();
        this.blocked = user.isBlocked();
    }
}
