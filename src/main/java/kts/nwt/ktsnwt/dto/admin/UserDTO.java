package kts.nwt.ktsnwt.dto.admin;

import kts.nwt.ktsnwt.beans.users.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO {
    private String username;
    private String fullname;
    private String profile;

    public UserDTO(User user){
        this.username = user.getUsername();
        this.fullname = user.getFullname();
        this.profile = user.getProfilePhotoLink();
    }
}
