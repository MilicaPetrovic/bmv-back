package kts.nwt.ktsnwt.dto.admin;

import kts.nwt.ktsnwt.beans.driving.Driving;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AdminDriverDrivingHistoryDTO {
    private Long id;
    private String state;
    private double price;
    private LocalDateTime orderTime;
    private String departure;
    private String arrival;
    private boolean reported;
    private String rejectionText;

    public AdminDriverDrivingHistoryDTO(Driving driving) {
        this.id = driving.getId();
        this.state = driving.getState().toString();
        this.price = driving.getPrice();
        this.orderTime = driving.getOrderTime();
        this.departure = driving.getDeparture();
        this.arrival = driving.getArrival();
        this.reported = driving.isReported();
        this.rejectionText = driving.getRejectionText();
    }
}
