package kts.nwt.ktsnwt.dto.driving_history;

import kts.nwt.ktsnwt.beans.driving.Driving;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class DriverDrivingHistoryDTO {
    private String state;
    private Long drivingId;
    private String departure;
    private String arrival;
    private double price;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;

    public DriverDrivingHistoryDTO(Driving d) {
        this.state = d.getState().toString();
        this.drivingId = d.getId();
        this.price = d.getPrice();
        this.startDateTime = d.getRealDrivingStartTime();
        this.endDateTime = d.getRealDrivingEndTime();
        this.departure = d.getDeparture();
        this.arrival = d.getArrival();
    }
}
