package kts.nwt.ktsnwt.dto.driving_history;

import kts.nwt.ktsnwt.beans.driving.Driving;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class ClientDrivingHistoryDTO {
    private Long drivingId;
    private String departure;
    private String arrival;
    private double price;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private LocalDateTime orderTime;

    public ClientDrivingHistoryDTO(Driving d) {
        this.drivingId = d.getId();
        this.price = d.getPrice();
        this.orderTime = d.getOrderTime();
        this.startDateTime = d.getRealDrivingStartTime();
        this.endDateTime = d.getRealDrivingEndTime();
        this.departure = d.getDeparture();
        this.arrival =  d.getArrival();
    }
}
