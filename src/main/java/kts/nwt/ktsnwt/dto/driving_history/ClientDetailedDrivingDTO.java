package kts.nwt.ktsnwt.dto.driving_history;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.dto.driving.DetailedDrivingDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ClientDetailedDrivingDTO extends DetailedDrivingDTO {
    private boolean clientCanLeaveRating;
    private boolean favourite;

    public ClientDetailedDrivingDTO(DetailedDrivingDTO detailedDrivingDTO, Driving driving, User user){
        super(detailedDrivingDTO);
        this.clientCanLeaveRating = driving.canClientLeaveRating(user);
        this.favourite = driving.isFavouriteForUser(user);
    }
}
