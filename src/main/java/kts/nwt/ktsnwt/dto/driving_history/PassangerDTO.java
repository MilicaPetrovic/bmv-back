package kts.nwt.ktsnwt.dto.driving_history;

import kts.nwt.ktsnwt.beans.users.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PassangerDTO {
    private String driverName;
    private String driverSurname;
    private String driverUsername;

    public PassangerDTO(User user){
        this.driverName = user.getName();
        this.driverSurname = user.getSurname();
        this.driverUsername = user.getUsername();
    }
}
