package kts.nwt.ktsnwt.dto.driving_history;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.users.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class DriverDrivingHistoryDetailedDTO {
    //map
    private List<String> path;
    //basic info
    private double price;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    //podaci o putnicima
    private List<PassangerDTO> passangers;
    //flags
    private boolean reported;
    private boolean rejected;
    private String rejectionText;


    public DriverDrivingHistoryDetailedDTO(Driving driving, List<User> passengers){
        this.price = driving.getPrice();
        this.startDateTime = driving.getDrivingStartTime();
        this.endDateTime = driving.getDrivingEndTime();
        this.passangers = createPassengers(passengers);
        this.rejected = driving.isRejected();
        this.reported = driving.isReported();
        this.rejectionText = driving.getRejectionText();
    }

    private List<PassangerDTO> createPassengers(List<User> passengers) {
        this.passangers = new ArrayList<>();
        for (User u : passengers) {
            this.passangers.add(new PassangerDTO(u));
        }
        return this.passangers;
    }
}
