package kts.nwt.ktsnwt.dto.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserRegisterRequest {
    private String username;

    private String name;

    private String surname;

    private String email;

    private String password;

    private String city;

    private String phoneNumber;

    private String postNumber;

}
