package kts.nwt.ktsnwt.dto.user;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DriverRegisterRequest extends UserRegisterRequest {

    private String vehicleName;
    private String vehicleType;
    private String color;
    private String label;
    private boolean petFriendly;
    private boolean babyFriendly;
    private int yearOfProduction;
    private int numberOfSeats;
    private int price;
}
