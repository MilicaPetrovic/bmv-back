package kts.nwt.ktsnwt.dto.user;

import kts.nwt.ktsnwt.beans.location.City;
import kts.nwt.ktsnwt.beans.users.Provider;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserProfileDTO {

    private String email;
    private String username;
    private String name;
    private String surname;
    private String phone;
    private City city;
    private Provider provider;
    private Double tokens;

    public UserProfileDTO(String email, String username, String name, String surname, String phone, City city, Provider provider) {
        this.email = email;
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.city = city;
        this.provider = provider;
    }

}
