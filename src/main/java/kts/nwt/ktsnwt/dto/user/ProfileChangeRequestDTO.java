package kts.nwt.ktsnwt.dto.user;


import kts.nwt.ktsnwt.beans.location.City;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProfileChangeRequestDTO {

    private Long id;

    private String username;

    private String name;

    private String surname;

    private City city;

    private String phone;

    private boolean approved;

    private List<String> changes;

    private boolean done;
}
