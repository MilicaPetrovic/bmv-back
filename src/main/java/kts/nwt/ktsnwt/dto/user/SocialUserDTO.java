package kts.nwt.ktsnwt.dto.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SocialUserDTO {

    private String email;
    private String firstName;
    private String lastName;
    private String photoUrl;
    private String provider;

}
