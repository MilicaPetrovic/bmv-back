package kts.nwt.ktsnwt.dto.report;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InconsistencyDTO {
   private long driverId;
   private long drivingId;
   private long registeredUserId;
   private String registeredUsername;
}
