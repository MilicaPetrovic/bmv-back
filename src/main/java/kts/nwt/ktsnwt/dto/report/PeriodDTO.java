package kts.nwt.ktsnwt.dto.report;

import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@NoArgsConstructor
public class PeriodDTO {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX");
    private LocalDate start;
    private LocalDate end;

    private String startString;
    private String endString;

    public LocalDate getStart() {
        if (start == null){
            this.start = LocalDate.parse(startString, formatter);
            this.start = this.start.plusDays(1);
        }
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        if (end == null){
            this.end = LocalDate.parse(endString, formatter);
            this.end = this.end.plusDays(1);
        }
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public String getStartString() {
        return startString;
    }

    public void setStartString(String startString) {
        this.startString = startString;
    }

    public String getEndString() {
        return endString;
    }

    public void setEndString(String endString) {
        this.endString = endString;
    }
}
