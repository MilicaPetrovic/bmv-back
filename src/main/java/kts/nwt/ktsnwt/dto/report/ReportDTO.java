package kts.nwt.ktsnwt.dto.report;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ReportDTO {
    private List<LocalDate> labels;
    private List<Integer> numberOfDrivings;
    private int sumOfDrivings;
    private int averageOfDrivings;

    private List<Double> distancesForDays;
    private double sumOfDistances;
    private double averageOfDistances;
    private List<Double> moneyForDays;
    private double sumOfMoney;
    private double averageOfMoney;


    public ReportDTO(List<Integer> numberOfDrivings, List<Double> distancesForDays, List<Double> moneyForDays, List<LocalDate> labels) {
        this.labels = labels;
        this.numberOfDrivings = numberOfDrivings;
        this.distancesForDays = distancesForDays;
        this.moneyForDays = moneyForDays;

        this.sumOfDrivings = getSumInteger(numberOfDrivings);
        this.averageOfDrivings = this.sumOfDrivings/numberOfDrivings.size();

        this.sumOfDistances = getSumDouble(distancesForDays);
        this.averageOfDistances = this.sumOfDistances/distancesForDays.size();

        this.sumOfMoney = getSumDouble(moneyForDays);
        this.averageOfMoney = this.sumOfMoney/moneyForDays.size();
    }

    private Double getSumDouble(List<Double> doubles) {
        double res = 0;
        for (double d : doubles){
            res += d;
        }
        return res;
    }

    private Integer getSumInteger(List<Integer> doubles) {
        int res = 0;
        for (double d : doubles){
            res += d;
        }
        return res;
    }
}
