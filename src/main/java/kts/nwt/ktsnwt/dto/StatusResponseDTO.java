package kts.nwt.ktsnwt.dto;

import kts.nwt.ktsnwt.exception.CustomException;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatusResponseDTO {
    private boolean successful;
    private String exceptionMessage;
    private int exceptionCode;

    public StatusResponseDTO(Exception e, int code) {
        this.successful = false;
        this.exceptionCode = code;
        this.exceptionMessage = e.getMessage();
    }

    public StatusResponseDTO(boolean successful) {
        this.successful = successful;
    }

    public StatusResponseDTO(boolean successful, String message) {
        this.successful = successful;
        this.exceptionMessage = message;
    }

    public StatusResponseDTO(CustomException e) {
        this.successful = false;
        this.exceptionCode = e.getCode();
        this.exceptionMessage = e.getMessage();
    }

    @Override
    public String toString() {
        return "StatusResponseDTO{" +
                "successful=" + successful +
                ", exceptionMessage='" + exceptionMessage + '\'' +
                ", exceptionCode=" + exceptionCode +
                '}';
    }
}
