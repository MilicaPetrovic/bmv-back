package kts.nwt.ktsnwt.dto.feedback;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ClientDrivingRatingDTO {
    //private String username;
    private Long drivingId;
    private int driverRating;
    private int vehicleRating;
    private String text;
}
