package kts.nwt.ktsnwt.beans.users;

public enum DriverState {
    ACTIVE(0),
    DRIVING(1),
    INACTIVE(2);

    private final int value;

    DriverState(int value) {
        this.value = value;
    }

    public static DriverState valueOf(int value) {
        switch (value) {
            case 2:
                return DriverState.INACTIVE;
            case 1:
                return DriverState.DRIVING;
            default:
                return DriverState.ACTIVE;
        }
    }

    public int getValue() {
        return value;
    }
}


