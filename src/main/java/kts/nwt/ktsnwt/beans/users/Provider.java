package kts.nwt.ktsnwt.beans.users;

public enum Provider {

    NO_PROVIDER,
    GOOGLE,
    FACEBOOK
}
