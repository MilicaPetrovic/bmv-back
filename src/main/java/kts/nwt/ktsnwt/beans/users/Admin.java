package kts.nwt.ktsnwt.beans.users;

import kts.nwt.ktsnwt.beans.feedback.WarningNote;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@DiscriminatorValue("A")
@NoArgsConstructor
@Getter
@Setter
public class Admin extends User {

    @OneToMany(mappedBy = "admin", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<WarningNote> warningNotes;

}
