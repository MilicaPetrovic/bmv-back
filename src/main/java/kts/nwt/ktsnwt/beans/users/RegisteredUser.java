package kts.nwt.ktsnwt.beans.users;

import kts.nwt.ktsnwt.beans.communication.Notification;
import kts.nwt.ktsnwt.beans.driving.DrivingPayment;
import kts.nwt.ktsnwt.beans.feedback.Rating;
import kts.nwt.ktsnwt.beans.feedback.Report;
import kts.nwt.ktsnwt.beans.location.Route;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("RU")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class RegisteredUser extends User {

    private boolean activated;

    private double tokens;

    private boolean socialAccount;

    //@OneToMany(mappedBy = "favouriteForUser", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "favourite_route", joinColumns=@JoinColumn(name = "client_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "route_id", referencedColumnName = "id"))
    //@ManyToMany(mappedBy = "favouriteForUser", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Route> favouriteRoute;

    @OneToMany(mappedBy = "registeredUser", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<DrivingPayment> drivingHistory;

    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Rating> ratings;

    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Report> madeReports;

    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Notification> notifications;

    public void changeFavouriteRoute(Route route) {
        if (favouriteRoute.contains(route)) {
            favouriteRoute.remove(route);
        } else {
            favouriteRoute.add(route);
        }
    }

    public boolean hasEnoughToken(double price) {
        return (this.tokens >= price);
    }

    public RegisteredUser(String username, double tokens, Role role) {
        this.setUsername(username);
        this.tokens = tokens;
        setRole(role);
    }

    public void addFavouriteRoute(Route route) {
        if (favouriteRoute == null)
            favouriteRoute = new ArrayList<>();
        favouriteRoute.add(route);
    }

    public RegisteredUser(Long id, String username, Role role) {
        this.setId(id);
        this.setUsername(username);
        setRole(role);
    }

    public RegisteredUser(Long id, String name, String surname, String username, Role role, String phone, boolean blocked, String email) {
        this.setId(id);
        this.setUsername(username);
        this.setRole(role);
        this.setName(name);
        this.setSurname(surname);
        this.setPhone(phone);
        this.setBlocked(blocked);
        this.setEmail(email);
    }
}
