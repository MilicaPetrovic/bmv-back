package kts.nwt.ktsnwt.beans.users;

import kts.nwt.ktsnwt.beans.communication.Message;
import kts.nwt.ktsnwt.beans.feedback.WarningNote;
import kts.nwt.ktsnwt.beans.location.City;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

import static javax.persistence.DiscriminatorType.STRING;
import static javax.persistence.InheritanceType.SINGLE_TABLE;

@Entity
@Table(name="users")
@Inheritance(strategy=SINGLE_TABLE)
@DiscriminatorColumn(name="type", discriminatorType=STRING)
@NoArgsConstructor
@Data
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable=false)
    private String name;

    @Column(nullable=false)
    private String surname;

    @Column(nullable=false, unique = true)
    private String email;

    @Column(nullable=true)
    private String password;

    @Column(nullable=false, unique = true)
    private String username;

    @Column(nullable=true)
    private String phone;
    
    private String image;

    @Column(nullable=false)
    private boolean blocked;

    private Date lastPasswordResetDate;
    private String resetPasswordCode;
    private LocalDateTime resetPasswordTime;
    private String verificationCode;
    private Provider provider;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private City city;

    @Version
    @Column(nullable=false)
    private int version;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Role role;

    //@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    //@JoinColumn(name = "address", referencedColumnName = "id")
    //private Address address;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<WarningNote> warningNotes;

    @OneToMany(mappedBy = "sender", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Message> sentMessages;

    @OneToMany(mappedBy = "receiver", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Message> receivedMessages;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(this.role);
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public boolean isRegisteredUser() {
        return this.role.isRegisteredUser();
    }

    public boolean isDriver() {
        return this.role.isDriver();
    }

    public boolean isAdmin() {
        return this.role.isAdmin();
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User user = (User) obj;
        return user.getUsername().equals(this.username);
    }

    public String getFullname() {
        return this.name + " " + this.surname;
    }

    public Long getRoleId() {
        return this.role.getId();
    }

    public String getMessageUsername(){
        if (isAdmin())
            return "admin";
        return username;
    }

    public String getProfilePhotoLink() {
        if (provider == null || Objects.requireNonNull(getProvider()) == Provider.NO_PROVIDER) {
            try {
                if (image == null || image.equals("")) {
                    return null;
                }else {
                    String basicURL = "https://localhost:8080/uber/resources/";
                    String[] elements = image.split("\\\\");
                    return basicURL + elements[elements.length - 2] + "/" + elements[elements.length-1];
                }
            } catch (Exception e) {
                return null;
            }
        } else {
            return image;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }
}
