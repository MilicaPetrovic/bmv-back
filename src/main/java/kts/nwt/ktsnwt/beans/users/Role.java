package kts.nwt.ktsnwt.beans.users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Table(name="role")
@Setter
public class Role implements GrantedAuthority {

    @Transient
    private int ADMIN_ID = 1;
    @Transient
    private int REGISTERED_USER_ID = 2;
    @Transient
    private int DRIVER_ID = 3;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name="name")
    private String name;

    public String getName() {
        return name;
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public Role(String name) {
        this.name = name;
    }
    public Role(Long id, String name) {
        this.name = name;
        this.id = id;
    }

    @JsonIgnore
    @Override
    public String getAuthority() {
        return name;
    }

    @Override
    public String toString() {
        return "Role{" +
                "name='" + name + '\'' +
                '}';
    }

    public boolean isRegisteredUser() {
        return this.id == REGISTERED_USER_ID;
    }

    public boolean isAdmin() {
        return this.id == ADMIN_ID;
    }

    public boolean isDriver() {
        return this.id == DRIVER_ID;
    }
}
