package kts.nwt.ktsnwt.beans.users;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.driving.TimePeriod;
import kts.nwt.ktsnwt.beans.driving.Vehicle;
import kts.nwt.ktsnwt.beans.feedback.Report;
import kts.nwt.ktsnwt.beans.location.Address;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("D")
@NoArgsConstructor
@Getter
@Setter
public class Driver extends User {


    @Column(nullable=true)
    private DriverState driverState;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "vehicle", referencedColumnName = "id")
    private Vehicle vehicle;

    @OneToMany(mappedBy = "activeHour", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<TimePeriod> activeHours;

    @OneToMany(mappedBy = "driver", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Driving> drivingHistory;

    @OneToMany(mappedBy = "driver", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Report> reports;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "currentAddress", referencedColumnName = "id")
    private Address currentAddress;

    public Driver(String username, Vehicle vehicle) {
        this.setUsername(username);
        this.vehicle = vehicle;
    }


    public boolean isInactive() {
        return driverState == DriverState.INACTIVE;
    }

    public void addActiveHour(TimePeriod tp) {
        if (activeHours == null)
            activeHours = new ArrayList<>();
        else{
            for (TimePeriod t : activeHours){
                if (t.getEndDateTime() == null)
                    t.setEndDateTime(LocalDateTime.now());
            }
        }
        activeHours.add(tp);
    }

    public void endLastActiveHour() {
        for (TimePeriod t : activeHours) {
            if (t.getEndDateTime() == null)
                t.setEndDateTime(LocalDateTime.now());
        }
    }

    public Driver(Long id, String name, String surname, boolean blocked, DriverState state, Address current_address, Vehicle vehicle, Role role){
        this.setId(id);
        this.setName(name);
        this.setSurname(surname);
        this.setBlocked(blocked);
        this.driverState = state;
        this.currentAddress = current_address;
        this.vehicle = vehicle;
        this.setRole(role);
        this.drivingHistory = new ArrayList<>();
    }
}
