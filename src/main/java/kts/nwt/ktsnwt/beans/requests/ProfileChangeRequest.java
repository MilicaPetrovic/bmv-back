package kts.nwt.ktsnwt.beans.requests;

import kts.nwt.ktsnwt.beans.location.City;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProfileChangeRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String name;

    private String surname;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private City city;

    private String phone;

    private boolean done;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> changes;


}
