package kts.nwt.ktsnwt.beans.communication;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.driving.DrivingPayment;
import kts.nwt.ktsnwt.beans.driving.DrivingState;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable=false)
    private LocalDateTime date;

    @Enumerated
    private NotificationType type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="drivingId")
    private Driving driving;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="clientId")
    private RegisteredUser client;


    public LocalDateTime getDrivingOrderTime() {
        return this.driving.getOrderTime();
    }

    public boolean isDriverAssigned() {
        return driving.isDriverAssigned();
    }

    public String getDriverFullname() {
        return driving.getDriversFullname();
    }

    public boolean canMakeAnAction() {
        switch (type) {
            case RATE_DRIVING -> {
                return driving.canClientLeaveRating(client);
            }
            case SPLIT_FARE_APPROVAL -> {
                for (DrivingPayment dp : driving.getPassengers()) {
                    if (dp.getRegisteredUser() == client)
                        return !dp.isConfirmed();
                }
                return false;
            }
            case ARRIVED_DRIVER -> { return false; }
            case DRIVING_REMINDER -> { return false; }
            case ADDED_DRIVING -> {
                if (driving.isRejected() && driving.getState() != DrivingState.PAID)
                    return false;
                return true;
            }
            default -> {
                return false;
            }
        }
    }

    public String getClientUsername() {
        return client.getUsername();
    }

    public String getOrderedUsername() {
        return driving.getOrderedUser().getUsername();
    }

    public String getOrderedFullname() {
        return driving.getOrderedUser().getFullname();
    }

    public double getMyPrice() {
        for (DrivingPayment dp : driving.getPassengers()){
            if (dp.getRegisteredUser() == client)
                return dp.getPrice();
        }
        return -1;
    }

    public Notification(Long id, LocalDateTime date, NotificationType type, Driving driving, RegisteredUser client) {
        this.id = id;
        this.date = date;
        this.type = type;
        this.driving = driving;
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notification that = (Notification) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
