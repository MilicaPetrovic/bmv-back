package kts.nwt.ktsnwt.beans.communication;

public enum NotificationType {
    SPLIT_FARE_APPROVAL(0),
    APPROVED_DRIVING(1),//?
    ARRIVED_DRIVER(2),//?
    DRIVING_REMINDER(3),
    RATE_DRIVING(4),
    REFUSED_DRIVING(5),//?
    ADDED_DRIVING(6);

    private final int value;

    NotificationType(int value) {
        this.value = value;
    }

    public static NotificationType valueOf(int value) {
        return switch (value) {
            case 0 -> NotificationType.SPLIT_FARE_APPROVAL;
            case 1 -> NotificationType.APPROVED_DRIVING;
            case 2 -> NotificationType.ARRIVED_DRIVER;
            case 4 -> NotificationType.RATE_DRIVING;
            case 5 -> NotificationType.REFUSED_DRIVING;
            case 6 -> NotificationType.ADDED_DRIVING;
            default -> NotificationType.DRIVING_REMINDER;
        };
    }


    public int getValue() {
        return value;
    }
}
