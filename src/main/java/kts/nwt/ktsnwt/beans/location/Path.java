package kts.nwt.ktsnwt.beans.location;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Path {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "path", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Address> stations;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "route", referencedColumnName = "id")
    private Route route;

    public String getDeparture() {
        try {
            return stations.get(0).getName();
        }catch (IndexOutOfBoundsException e){
            return "Subotica";
        }
    }

    public String getArrival() {
        try {
            return stations.get(stations.size() - 1).getName();
        }catch (IndexOutOfBoundsException e){
            return "Novi Sad";
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Path path = (Path) o;
        return Objects.equals(stations, path.stations) && Objects.equals(route, path.route);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stations, route);
    }
}
