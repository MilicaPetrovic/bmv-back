package kts.nwt.ktsnwt.beans.location;

import kts.nwt.ktsnwt.beans.users.Driver;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import java.util.Objects;


@Entity
@Getter
@Setter
@NoArgsConstructor
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="routeId")
    private Route route;

    @Column(nullable = false)
    private double latitude;

    @Column(nullable = false)
    private double longitude;

    @Column(nullable = true)
    private String name;

    @ManyToOne
    @JoinColumn(name="pathId")
    private Path path;

    @OneToOne(mappedBy = "currentAddress", fetch = FetchType.LAZY)
    private Driver currentAddress;

    public Address (double latitude, double longitude, String name){
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }

    public Address (double latitude, double longitude, String name, Path path){
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Double.compare(address.latitude, latitude) == 0 && Double.compare(address.longitude, longitude) == 0 && Objects.equals(name, address.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude, name);
    }
}
