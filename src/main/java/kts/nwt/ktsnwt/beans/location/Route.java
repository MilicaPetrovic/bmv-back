package kts.nwt.ktsnwt.beans.location;

import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.beans.users.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //@ManyToOne(fetch = FetchType.LAZY)
    @ManyToMany(mappedBy = "favouriteRoute", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    //@JoinColumn(name="clientId")
    //@ManyToMany(fetch = FetchType.LAZY)
    //@JoinTable(name = "favouriteRoute", joinColumns=@JoinColumn(name = "route_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "client_id", referencedColumnName = "id"))

    private List<RegisteredUser> favouriteForUser;

    @OneToMany(mappedBy = "route", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Address> route;

    @OneToOne(mappedBy = "route", fetch = FetchType.LAZY)
    private Path path;

    @Column
    private double distance;

    @Column
    private double time;

    public boolean isUserFavourited(User user) {
        for (RegisteredUser ru : favouriteForUser){
            if (ru.getUsername().equals(user.getUsername()))
                return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route = (Route) o;
        return Objects.equals(id, route.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Route(List<Address> route, double distance, double time) {
        this.route = route;
        this.distance = distance;
        this.time = time;
    }
}
