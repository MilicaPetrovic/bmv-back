package kts.nwt.ktsnwt.beans.location;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable=false)
    private String name;

    @Column(nullable=false)
    private int postNumber;

    public City(String name) {
        this.name = name;
    }

    public City(String name, int postNumber) {
        this.name = name;
        this.postNumber = postNumber;
    }

 /*   @OneToMany(mappedBy = "city", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Address> addresses;  */

}
