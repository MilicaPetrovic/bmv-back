package kts.nwt.ktsnwt.beans.feedback;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable=false)
    private int driverRating;

    @Column(nullable=false)
    private int vehicleRating;

    @Column(nullable=true)
    private String text;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="clientId")
    private RegisteredUser client;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="drivingId")
    private Driving driving;

    public Rating(){
        this.driverRating = -1;
        this.vehicleRating = -1;
        this.text = "";
    }
}
