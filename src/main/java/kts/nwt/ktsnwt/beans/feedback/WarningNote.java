package kts.nwt.ktsnwt.beans.feedback;

import kts.nwt.ktsnwt.beans.users.Admin;
import kts.nwt.ktsnwt.beans.users.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class WarningNote {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable=false)
    private String text;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="adminId")
    private Admin admin;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="userId")
    private User user;

}
