package kts.nwt.ktsnwt.beans.driving;

import kts.nwt.ktsnwt.beans.users.Driver;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class TimePeriod {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable=false)
    private LocalDateTime startDateTime;

    @Column(nullable=true)
    private LocalDateTime endDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "activeHour", referencedColumnName = "id")
    private Driver activeHour;

    @Column(nullable = true)
    private boolean estimated;

    public TimePeriod(LocalDateTime startDateTime, LocalDateTime endDateTime, Boolean estimated){
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.estimated = estimated;
    }

    public TimePeriod(LocalDateTime startDateTime, LocalDateTime endDateTime) {
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimePeriod that = (TimePeriod) o;
        return Objects.equals(startDateTime, that.startDateTime) && Objects.equals(endDateTime, that.endDateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startDateTime, endDateTime, estimated);
    }
}
