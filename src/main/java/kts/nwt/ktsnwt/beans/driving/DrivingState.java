package kts.nwt.ktsnwt.beans.driving;

public enum DrivingState {
    MADE(0),
    ACCEPTED(1),
    CURRENT(2),
    REJECTED(3),
    SUCCESSFUL(4),
    REFUSED(5),
    PAID(6);


    private final int value;

    DrivingState(int value) {
        this.value = value;
    }

    public static DrivingState valueOf(int value) {
        switch (value) {
            case 0:
                return DrivingState.MADE;
            case 1:
                return DrivingState.ACCEPTED;
            case 2:
                return DrivingState.CURRENT;
            case 3:
                return DrivingState.REJECTED;
            case 5:
                return DrivingState.REFUSED;
            case 6:
                return DrivingState.PAID;
            default:
                return DrivingState.SUCCESSFUL;
        }
    }


    public int getValue() {
        return value;
    }
}
