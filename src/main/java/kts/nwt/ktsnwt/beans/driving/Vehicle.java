package kts.nwt.ktsnwt.beans.driving;

import kts.nwt.ktsnwt.beans.users.Driver;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable=false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="typeId")
    private VehicleType type;

    @Column(nullable=false)
    private String color;

    @Column(nullable=false)
    private int yearOfProduction;

    @Column(nullable=false)
    private String label;

    @Column(nullable=false)
    private boolean petFriendly;

    @Column(nullable=false)
    private boolean babyFriendly;

    @OneToOne(mappedBy = "vehicle", fetch = FetchType.LAZY)
    private Driver driver;

    private int numberOfSeats;

    public String getVehicleTypeName() {
        return this.type.getName();
    }

    public int getVehicleTypePrice() {
        return this.type.getPrice();
    }

    public Vehicle(Long id, String name, VehicleType type, String color, int yearOfProduction, String label, boolean petFriendly, boolean babyFriendly, int numberOfSeats) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.color = color;
        this.yearOfProduction = yearOfProduction;
        this.label = label;
        this.petFriendly = petFriendly;
        this.babyFriendly = babyFriendly;
        this.numberOfSeats = numberOfSeats;
    }

    public Vehicle(Long id, String name, String label, VehicleType type, boolean petFriendly, boolean babyFriendly, int numberOfSeats){
        this.id = id;
        this.name = name;
        this.label = label;
        this.type = type;
        this.petFriendly = petFriendly;
        this.babyFriendly = babyFriendly;
        this.numberOfSeats = numberOfSeats;
    }
}
