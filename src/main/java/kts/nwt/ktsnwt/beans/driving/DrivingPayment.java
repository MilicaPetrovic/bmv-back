package kts.nwt.ktsnwt.beans.driving;

import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.beans.users.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class DrivingPayment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable =true)
    private double price;

    @Column(nullable = false)
    private boolean confirmed;

    @Column(nullable =false)
    private boolean paid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="drivingId")
    private Driving driving;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="clientId")
    private RegisteredUser registeredUser;

    public DrivingPayment(double price, boolean confirmed, boolean paid, Driving driving, RegisteredUser registeredUser) {
        this.price = price;
        this.confirmed = confirmed;
        this.paid = paid;
        this.driving = driving;
        this.registeredUser = registeredUser;
    }

    public boolean hasAsPassanger(User u) {
        if (this.registeredUser.getUsername().equals(u.getUsername())){
            return true;
        }
        return false;
    }

    public String getPassangerName() {
        return this.registeredUser.getName();
    }

    public String getPassangerSurname() {
        return this.registeredUser.getSurname();
    }

    public String getPassangerUsername() {
        return this.registeredUser.getUsername();
    }

    public String getPassangerFullname() {
        return this.registeredUser.getFullname();
    }

    public boolean hasAsPassanger(String username) {
        return this.registeredUser.getUsername().equals(username);
    }

    public void returnMoneyToUser() {
        this.registeredUser.setTokens(this.registeredUser.getTokens() + this.price);
        this.paid = false;
    }
    public long getPassengerId(){return this.registeredUser.getId(); }

    public void returnTokens() {
        registeredUser.setTokens(registeredUser.getTokens() + this.price);
    }
}
