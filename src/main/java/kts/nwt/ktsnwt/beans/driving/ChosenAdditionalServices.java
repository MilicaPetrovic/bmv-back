package kts.nwt.ktsnwt.beans.driving;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class ChosenAdditionalServices {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="type_id")
    private VehicleType type;

    private boolean petFriendly;

    private boolean babyFriendly;

    public ChosenAdditionalServices(VehicleType type, boolean petFriendly, boolean babyFriendly) {
        this.type = type;
        this.petFriendly = petFriendly;
        this.babyFriendly = babyFriendly;
    }
    public ChosenAdditionalServices(Long id, VehicleType type, boolean petFriendly, boolean babyFriendly) {
        this.id = id;
        this.type = type;
        this.petFriendly = petFriendly;
        this.babyFriendly = babyFriendly;
    }

    public ChosenAdditionalServices(ChosenAdditionalServices chosenAdditionalServices) {
        this.id = chosenAdditionalServices.id;
        this.babyFriendly = chosenAdditionalServices.isBabyFriendly();
        this.petFriendly = chosenAdditionalServices.isPetFriendly();
        this.type = chosenAdditionalServices.getType();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChosenAdditionalServices that = (ChosenAdditionalServices) o;
        return petFriendly == that.petFriendly && babyFriendly == that.babyFriendly && this.type.getId().equals(that.getType().getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, petFriendly, babyFriendly);
    }

}
