package kts.nwt.ktsnwt.beans.driving;

import kts.nwt.ktsnwt.beans.feedback.Rating;
import kts.nwt.ktsnwt.beans.feedback.Report;
import kts.nwt.ktsnwt.beans.location.Path;
import kts.nwt.ktsnwt.beans.location.Route;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.exception.AlreadyEndedException;
import kts.nwt.ktsnwt.exception.ShouldNotHappenException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Driving {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "pathId", referencedColumnName = "id")
    private Path path;

    @Column(nullable = false)
    private double price;

    @Enumerated
    private DrivingState state;

    @Column(nullable = true)
    private String rejectionText;

    @Column(nullable = false)
    private LocalDateTime orderTime;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "drivingTimeId", referencedColumnName = "id")
    private TimePeriod drivingTime;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "driverId")
    private Driver driver;

    @OneToMany(mappedBy = "driving", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<DrivingPayment> passengers;

    @OneToMany(mappedBy = "driving", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Rating> ratings;

    @OneToMany(mappedBy = "driving", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Report> reports;

    @OneToOne
    @JoinColumn(name = "chosen_additional_services_id")
    private ChosenAdditionalServices chosenAdditionalServices;

    @Column(nullable = false)
    boolean future;

    @Column(nullable = true)
    int notificationSent;

    public Driving(Clock clock, double price, DrivingState state, ChosenAdditionalServices chosenAdditionalServices, Path path, TimePeriod drivingTime, boolean future) {
        this.price = price;
        this.state = state;
        this.chosenAdditionalServices = chosenAdditionalServices;
        this.orderTime = LocalDateTime.now(clock);// nesto sa time zonama
        this.path = path;
        System.out.println("----------");
        System.out.println(drivingTime);
        this.drivingTime = drivingTime;
        this.future = future;
    }

    public Driving(Driving driving) {
        this.price = driving.getPrice();
        this.state = driving.getState();
        this.chosenAdditionalServices = driving.getChosenAdditionalServices();
        this.orderTime = driving.getOrderTime();// nesto sa time zonama
        this.path = driving.getPath();
        this.drivingTime = driving.getDrivingTime();
        this.future = driving.isFuture();
        this.passengers = driving.getPassengers();
    }

    public boolean hasPassenger(User u) {
        for (DrivingPayment payment : passengers) {
            if (payment.hasAsPassanger(u)) {
                return true;
            }
        }
        return false;
    }

    public String getDeparture() {
        if (this.getPath() != null)
            return this.getPath().getDeparture();
        return "Novi Sad";
    }

    public String getArrival() {
        if (this.getPath() != null)
            return this.getPath().getArrival();
        return "Subotica";
    }

    public LocalDateTime getDrivingStartTime() {
        if (drivingTime == null)
            return null;
        else
            return drivingTime.getStartDateTime();
    }

    public LocalDateTime getDrivingEndTime() {
        if (drivingTime == null)
            return null;
        else
            return drivingTime.getEndDateTime();
    }

    public String getDriversName() {
        return driver.getName();
    }


    public boolean canClientLeaveRating(User user) {
        if (rejectionText != null)
            return false;
        if (drivingTime == null)
            return false;
        if (state != DrivingState.SUCCESSFUL)
            return false;
        for (Rating r : ratings) {
            if (user == r.getClient())
                return false;
        }
        return LocalDateTime.now().isBefore(getDrivingEndTime().plusDays(3));
    }

    public boolean isRejected() {
        return rejectionText != null;
    }

    public boolean isReported() {
        return !reports.isEmpty();
    }

    public boolean happened() {
        if (rejectionText != null)
            return false;
        return drivingTime != null;
    }

    public boolean isDriverAssigned() {
        return driver != null;
    }

    public String getDriversFullname() {
        if (isDriverAssigned())
            return driver.getFullname();
        return "";
    }

    public boolean isFavouriteForUser(User user) {
        try {
            return this.getPath().getRoute().isUserFavourited(user);
        } catch (Exception e) {
            return false;
        }
    }

    public Route getRoute() {
        if (path == null)
            return null;
        return path.getRoute();
    }

    public boolean isBabyFriendlyDriving() {
        return chosenAdditionalServices.isBabyFriendly();
    }

    public boolean isPetFriendlyDriving() {
        return chosenAdditionalServices.isPetFriendly();
    }

    public String getVehicleTypeName() {
        return chosenAdditionalServices.getType().getName();
    }

    public boolean hasAsDriver(User u) {
        return driver == u;
    }

    public boolean canEnded() throws ShouldNotHappenException, AlreadyEndedException {
        if (state == DrivingState.CURRENT) {
            if (this.drivingTime == null)
                throw new ShouldNotHappenException("Current driving should have a driving time");
            else if (drivingTime.getStartDateTime() == null)
                throw new ShouldNotHappenException("Current driving should have a start time");
            else if (drivingTime.getEndDateTime() != null && !drivingTime.isEstimated()) //TODO: && not estimated!!!!
                throw new AlreadyEndedException("Driving already ended");
            return true;
        }
        return false;
    }

    public void setEndTime(LocalDateTime now) {
        this.drivingTime.setEndDateTime(now);
    }

    public void ended(LocalDateTime endTime) {
        drivingTime.setEndDateTime(endTime);
        state = DrivingState.SUCCESSFUL;
        drivingTime.setEstimated(false);
    }

    public void addPassenger(DrivingPayment passenger) {
        if (this.passengers == null)
            this.passengers = new ArrayList<>();
        this.passengers.add(passenger);
    }

    public RegisteredUser getOrderedUser() {
        return this.passengers.get(0).getRegisteredUser();
    }

    public DrivingPayment confirmPassenger(User user) {
        for (DrivingPayment payment : passengers) {
            if (payment.hasAsPassanger(user)) {
                payment.setConfirmed(true);
                return payment;
            }
        }
        return null;
    }

    public boolean everybodyHasConfirmed() {
        for (DrivingPayment payment : passengers) {
            if (!payment.isConfirmed()) {
                return false;
            }
        }
        return true;
    }

    public boolean isStillActive() {
        return !orderTime.plusHours(6).isBefore(LocalDateTime.now());
    }

    public LocalDateTime getRealDrivingStartTime() {
        if (drivingTime == null || drivingTime.isEstimated())
            return null;
        else
            return drivingTime.getStartDateTime();
    }


    public LocalDateTime getRealDrivingEndTime() {
        if (drivingTime == null || drivingTime.isEstimated())
            return null;
        else
            return drivingTime.getEndDateTime();
    }

    public boolean isFinished() {
        return drivingTime != null && !drivingTime.isEstimated();
    }

    public double getDistance() {
        try {
            return path.getRoute().getDistance();
        } catch (Exception e) {
            return 0;
        }
    }

    public double getTime() {
        try {
            return path.getRoute().getTime();
        } catch (Exception e) {
            return 0;
        }
    }

    public double getMoneySpentByPassenger(String username) {
        for (DrivingPayment dp : passengers) {
            if (dp.hasAsPassanger(username) && dp.isPaid())
                return dp.getPrice();
            else
                return 0;
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Driving driving = (Driving) o;
        return id.equals(driving.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void addNotificationSent(int i) {
        this.notificationSent += i;
    }

    /*
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Driving driving = (Driving) o;
        return Double.compare(driving.price, price) == 0 && Objects.equals(path, driving.path)
                && state == driving.state && Objects.equals(rejectionText, driving.rejectionText)
                && Objects.equals(orderTime, driving.orderTime) && Objects.equals(drivingTime, driving.drivingTime)
                && Objects.equals(driver, driving.driver) && Objects.equals(passengers, driving.passengers)
                && Objects.equals(ratings, driving.ratings) && Objects.equals(reports, driving.reports)
                && Objects.equals(chosenAdditionalServices, driving.chosenAdditionalServices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path, price, state, rejectionText, orderTime, drivingTime, driver, passengers, ratings, reports, chosenAdditionalServices);
    }*/
}
