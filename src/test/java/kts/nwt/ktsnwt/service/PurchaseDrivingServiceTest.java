package kts.nwt.ktsnwt.service;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.driving.DrivingPayment;
import kts.nwt.ktsnwt.beans.driving.DrivingState;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.DriverState;
import kts.nwt.ktsnwt.data.MockData;
import kts.nwt.ktsnwt.dto.driving.PurchaseDrivingDTO;
import kts.nwt.ktsnwt.exception.NoAvailableDriverException;
import kts.nwt.ktsnwt.exception.NotAllUsersConfirmedException;
import kts.nwt.ktsnwt.exception.NotEnoughTokensException;
import kts.nwt.ktsnwt.service.driving.PurchaseDrivingServiceImpl;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.user.interfaces.DriverService;
import kts.nwt.ktsnwt.service.user.interfaces.RegisteredUserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PurchaseDrivingServiceTest {

    @Mock
    private DrivingService drivingService;

    @Mock
    private RegisteredUserService registeredUserService;

    @Mock
    private DriverService driverService;

    @InjectMocks
    private PurchaseDrivingServiceImpl purchaseDrivingService;

    private Driving drivingWithPassenger;
    private Driving drivingWithMultiplePassenger;

    @Autowired
    private MockData mockData;

    @BeforeAll
    void setDriving(){
        drivingWithPassenger = mockData.createDrivingWithPassenger();
        drivingWithMultiplePassenger = mockData.createDrivingWithMultiplePassenger();
    }

    @Test
    void shouldBeOkPurchaseDriving() throws NotAllUsersConfirmedException, NotEnoughTokensException, NoAvailableDriverException {
        Driving driving = mockData.createDrivingWithPassenger();
        when(drivingService.getDrivingById(1L)).thenReturn(driving);
        driving.setDriver(new Driver());
        boolean purchased = purchaseDrivingService.purchaseDriving(new PurchaseDrivingDTO(1L, ""));
        Assertions.assertTrue(purchased);
        for (DrivingPayment dp : driving.getPassengers()){
            Assertions.assertTrue(dp.isPaid());
        }
        Assertions.assertEquals(DrivingState.PAID, driving.getState());
        Assertions.assertEquals(DriverState.DRIVING, driving.getDriver().getDriverState());
    }

    @Test
    void shouldThrowNotAllUsersConfirmedException(){
        when(drivingService.getDrivingById(1L)).thenReturn(drivingWithMultiplePassenger);
        Assertions.assertThrows(NotAllUsersConfirmedException.class, () -> {
            purchaseDrivingService.purchaseDriving(new PurchaseDrivingDTO(1L, ""));
        });
    }

    @Test
    void shouldThrowNoAvailableDriverException(){
        when(drivingService.getDrivingById(1L)).thenReturn(drivingWithPassenger);
        Assertions.assertThrows(NoAvailableDriverException.class, () -> {
            purchaseDrivingService.purchaseDriving(new PurchaseDrivingDTO(1L, ""));
        });
    }
    @Test
    void shouldThrowNotEnoughTokensException(){
        Driving driving = new Driving(drivingWithPassenger);
        driving.getPassengers().get(0).getRegisteredUser().setTokens(0);
        driving.setDriver(new Driver());
        when(drivingService.getDrivingById(1L)).thenReturn(driving);
        Assertions.assertThrows(NotEnoughTokensException.class, () -> {
            purchaseDrivingService.purchaseDriving(new PurchaseDrivingDTO(1L, ""));
        });
    }

}
