package kts.nwt.ktsnwt.service;

import kts.nwt.ktsnwt.beans.driving.*;
import kts.nwt.ktsnwt.beans.location.Address;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.DriverState;
import kts.nwt.ktsnwt.data.MockData;
import kts.nwt.ktsnwt.exception.NoAvailableDriverException;
import kts.nwt.ktsnwt.repository.DriverRepository;
import kts.nwt.ktsnwt.repository.DrivingRepository;
import kts.nwt.ktsnwt.service.driving.DriverAssignmentServiceImpl;
import kts.nwt.ktsnwt.service.driving.interfaces.DriverAssignmentService;
import kts.nwt.ktsnwt.service.notification.NotificationService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DriverAssignmentServiceTest {

    @MockBean
    private DriverRepository driverRepository;

    @MockBean
    private DrivingRepository drivingRepository;

    @MockBean
    private NotificationService notificationService;

    @Autowired
    @InjectMocks
    private DriverAssignmentServiceImpl driverAssignmentService;

    /*
    driver
    driving
    active hours
    current address
    drivings --> time period
    chosen additional services
    path - bar prvi za najblizeg

     */

    @Autowired
    private MockData mockData;

    private Driving driving;


    @BeforeAll
    public void beforeTests(){
        driving = mockData.createDrivingWithPassenger();
        driving.setId(1L);
    }

    @Test
    public void shouldBeAssignDriver() throws NoAvailableDriverException {
        when(drivingRepository.getFutureDrivingFromPeriodWithoutDriver(true, LocalDateTime.now(mockData.fixedClock), LocalDateTime.now(mockData.fixedClock).plusMinutes(10)))
                .thenReturn(new ArrayList<>());
        Driver driver = mockData.createDriverWithEveryFriendly();
        driver.setActiveHours(new ArrayList<>());
        when(driverRepository.getActiveDrivers()).thenReturn(List.of(driver));
        Driving handledDriverDriving = driverAssignmentService.handleDriver(mockData.fixedClock, driving);
        Assertions.assertEquals(handledDriverDriving.getDriver().getUsername(), driver.getUsername());
        Assertions.assertEquals(DrivingState.ACCEPTED, handledDriverDriving.getState());
        Assertions.assertEquals(DriverState.DRIVING, handledDriverDriving.getDriver().getDriverState());
    }

    //nijedan aktivan vozač
    @Test
    public void shouldBeNoActiveDriver() throws NoAvailableDriverException {
        when(drivingRepository.getFutureDrivingFromPeriodWithoutDriver(true, LocalDateTime.now(mockData.fixedClock), LocalDateTime.now(mockData.fixedClock).plusMinutes(10)))
                .thenReturn(new ArrayList<>());
        when(driverRepository.getActiveDrivers()).thenReturn(new ArrayList<>());
        Assertions.assertThrows(NoAvailableDriverException.class, () -> {
            driverAssignmentService.handleDriver(mockData.fixedClock, driving);
        });
    }

    // chosen additional services -- !
    @Test
    public void shouldFallOnBabyFriendly() throws NoAvailableDriverException {
        Driving needsEveryThingFriendly = new Driving(driving);
        needsEveryThingFriendly.setChosenAdditionalServices(mockData.createChosenAdditionalServicesBabys(mockData.createVehicle()));
        when(drivingRepository.getFutureDrivingFromPeriodWithoutDriver(true, LocalDateTime.now(mockData.fixedClock), LocalDateTime.now(mockData.fixedClock).plusMinutes(10)))
                .thenReturn(new ArrayList<>());
        Driver driver = mockData.createDriverWithNothingFriendly();
        driver.setActiveHours(new ArrayList<>());
        when(driverRepository.getActiveDrivers()).thenReturn(List.of(driver));
        Assertions.assertThrows(NoAvailableDriverException.class, () -> {
            driverAssignmentService.handleDriver(mockData.fixedClock, needsEveryThingFriendly);
        });
    }

    @Test
    void shouldFallOnPetFriendly() {
        Driving needsEverythingFriendly = new Driving(driving);
        needsEverythingFriendly.setChosenAdditionalServices(mockData.createChosenAdditionalServicesPets(mockData.createVehicle()));
        when(drivingRepository.getFutureDrivingFromPeriodWithoutDriver(true, LocalDateTime.now(mockData.fixedClock), LocalDateTime.now(mockData.fixedClock).plusMinutes(10)))
                .thenReturn(new ArrayList<>());
        Driver driver = mockData.createDriverWithNothingFriendly();
        driver.setActiveHours(new ArrayList<>());
        when(driverRepository.getActiveDrivers()).thenReturn(List.of(driver));
        Assertions.assertThrows(NoAvailableDriverException.class, () -> {
            driverAssignmentService.handleDriver(mockData.fixedClock, needsEverythingFriendly);
        });
    }

    @Test
    void shouldFallOnVehicleType() {
        Driving drivingWithOtherVehicleType = new Driving(driving);
        VehicleType vt = new VehicleType(100L, "limo", 1000);
        drivingWithOtherVehicleType.setChosenAdditionalServices(mockData.createChosenAdditionalServicesPets(vt));
        when(drivingRepository.getFutureDrivingFromPeriodWithoutDriver(true, LocalDateTime.now(mockData.fixedClock), LocalDateTime.now(mockData.fixedClock).plusMinutes(10)))
                .thenReturn(new ArrayList<>());
        Driver driver = mockData.createDriverWithNothingFriendly();
        driver.setActiveHours(new ArrayList<>());
        when(driverRepository.getActiveDrivers()).thenReturn(List.of(driver));
        Assertions.assertThrows(NoAvailableDriverException.class, () -> {
            driverAssignmentService.handleDriver(mockData.fixedClock, drivingWithOtherVehicleType);
        });
    }

    @Test
    void shouldFallOnNumberOfSeats() {
        Driving multiplePassengerDriving = mockData.createDrivingWithMultiplePassenger();
        //drivingWithOtherVehicleType.setChosenAdditionalServices(mockData.createChosenAdditionalServicesPets(mockData.));
        when(drivingRepository.getFutureDrivingFromPeriodWithoutDriver(true, LocalDateTime.now(mockData.fixedClock), LocalDateTime.now(mockData.fixedClock).plusMinutes(10)))
                .thenReturn(new ArrayList<>());
        Driver driver = mockData.createDriverWithNothingFriendly();
        driver.getVehicle().setNumberOfSeats(1);
        driver.setActiveHours(new ArrayList<>());
        when(driverRepository.getActiveDrivers()).thenReturn(List.of(driver));
        Assertions.assertThrows(NoAvailableDriverException.class, () -> {
            driverAssignmentService.handleDriver(mockData.fixedClock, multiplePassengerDriving);
        });
    }

    // broj radnih sati
    @Test
    void shouldFallOnWorkingHours() throws NoAvailableDriverException {
        when(drivingRepository.getFutureDrivingFromPeriodWithoutDriver(true, LocalDateTime.now(mockData.fixedClock), LocalDateTime.now(mockData.fixedClock).plusMinutes(10)))
                .thenReturn(new ArrayList<>());
        Driver driver = mockData.createDriverWithEveryFriendly();
        TimePeriod timePeriod1 = new TimePeriod(LocalDateTime.now().minusHours(12), LocalDateTime.now().minusHours(6));
        TimePeriod timePeriod2 = new TimePeriod(LocalDateTime.now().minusHours(5), LocalDateTime.now().minusHours(2));
        driver.setActiveHours(List.of(timePeriod1, timePeriod2));
        when(driverRepository.getActiveDrivers()).thenReturn(List.of(driver));
        Assertions.assertThrows(NoAvailableDriverException.class, () -> {
            driverAssignmentService.handleDriver(mockData.fixedClock, driving);
        });
    }


    //najbliži od aktivnih vozača
    @Test
    void shouldBeOkNearestDriver() throws NoAvailableDriverException {
        driving.setPath(mockData.createPath());
        when(drivingRepository.getFutureDrivingFromPeriodWithoutDriver(true, LocalDateTime.now(mockData.fixedClock), LocalDateTime.now(mockData.fixedClock).plusMinutes(10)))
                .thenReturn(new ArrayList<>());
        Driver driver1 = mockData.createDriverWithEveryFriendly();
        driver1.setCurrentAddress(new Address(45.2464537, 19.8342636,"Gogoljeva 22 Novi Sad"));
        Driver driver2 = mockData.createDriverWithEveryFriendly();
        driver2.setCurrentAddress(new Address(45.3464537, 19.9342636,"Na Marsu"));
        driver1.setActiveHours(new ArrayList<>());
        driver2.setActiveHours(new ArrayList<>());
        when(driverRepository.getActiveDrivers()).thenReturn(List.of(driver1, driver2));
        Driving handledDriverDriving = driverAssignmentService.handleDriver(mockData.fixedClock, driving);
        Assertions.assertEquals(handledDriverDriving.getDriver().getUsername(), driver1.getUsername());
        Assertions.assertEquals( DrivingState.ACCEPTED, handledDriverDriving.getState());
        Assertions.assertEquals(DriverState.DRIVING, handledDriverDriving.getDriver().getDriverState());
    }

    // zauzeti -- najbliži koji će da završi
    @Test
    void shouldBeOkNearestToEndDriver() throws NoAvailableDriverException {
        driving.setPath(mockData.createPath());
        when(drivingRepository.getFutureDrivingFromPeriodWithoutDriver(true, LocalDateTime.now(mockData.fixedClock), LocalDateTime.now(mockData.fixedClock).plusMinutes(10)))
                .thenReturn(new ArrayList<>());
        Driver driver1 = mockData.createDriverWithEveryFriendly();
        Driver driver2 = mockData.createDriverWithEveryFriendly();
        driver2.setUsername("simon");
        driver1.setActiveHours(new ArrayList<>());
        driver2.setActiveHours(new ArrayList<>());
        Driving drivingForDriving1LaterToEnd = mockData.createDriving();
        drivingForDriving1LaterToEnd.setDrivingTime(new TimePeriod(LocalDateTime.now(), LocalDateTime.now().plusMinutes(10)));
        driver1.setDrivingHistory(List.of(drivingForDriving1LaterToEnd));
        Driving drivingForDriver2FasterToEnd = mockData.createDriving();
        drivingForDriver2FasterToEnd.setDrivingTime(new TimePeriod(LocalDateTime.now(), LocalDateTime.now().plusMinutes(2)));
        driver2.setDrivingHistory(List.of(drivingForDriver2FasterToEnd));
        when(driverRepository.getActiveDrivers()).thenReturn(new ArrayList<>());
        when(driverRepository.getDrivingDrivers()).thenReturn(List.of(driver1, driver2));
        System.out.println(driving.isFuture());
        Driving handledDriverDriving = driverAssignmentService.handleDriver(mockData.fixedClock, driving);
        Assertions.assertEquals(handledDriverDriving.getDriver().getUsername(), driver2.getUsername());
        Assertions.assertEquals(DrivingState.ACCEPTED,handledDriverDriving.getState());
        Assertions.assertEquals(DriverState.DRIVING, handledDriverDriving.getDriver().getDriverState());
    }


    // imaju buduću vožnju
    @Test
    void shouldFallOnFutureDrivings() throws NoAvailableDriverException {
        when(drivingRepository.getFutureDrivingFromPeriodWithoutDriver(true, LocalDateTime.now(mockData.fixedClock), LocalDateTime.now(mockData.fixedClock).plusMinutes(10)))
                .thenReturn(List.of(driving));
        Driver driver = mockData.createDriverWithEveryFriendly();
        driver.setActiveHours(List.of());
        when(driverRepository.getActiveDrivers()).thenReturn(List.of(driver));
        Assertions.assertThrows(NoAvailableDriverException.class, () -> {
            driverAssignmentService.handleDriver(mockData.fixedClock, driving);
        });
    }

    // nema slobodnih +  imaju buduću vožnju
    @Test
    void shouldFallOnNoActiveFutureDrivings() throws NoAvailableDriverException {
        when(drivingRepository.getFutureDrivingFromPeriodWithoutDriver(true, LocalDateTime.now(mockData.fixedClock), LocalDateTime.now(mockData.fixedClock).plusMinutes(10)))
                .thenReturn(List.of(driving));
        when(driverRepository.getActiveDrivers()).thenReturn(List.of());
        Assertions.assertThrows(NoAvailableDriverException.class, () -> {
            driverAssignmentService.handleDriver(mockData.fixedClock, driving);
        });
    }

    @Test
    void testAssignDriverToFutureDriving(){
        Driving futureDriving = new Driving(driving);
        futureDriving.setFuture(true);
        futureDriving.setChosenAdditionalServices(mockData.createChosenAdditionalServices(mockData.createVehicle()));
        when(drivingRepository.getFutureDrivingFromPeriodWithoutDriver(any(Boolean.class), any(LocalDateTime.class), any(LocalDateTime.class)))
                .thenReturn(List.of(futureDriving));
        Driver driver = mockData.createDriverWithEveryFriendly();
        driver.setActiveHours(new ArrayList<>());
        when(driverRepository.getActiveDrivers()).thenReturn(List.of(driver));

        driverAssignmentService.assignDriverToFutureDriving();
        Assertions.assertNotEquals(null, futureDriving.getDriver());
    }

    @Test
    void shouldNotAssignDriverToFutureDriving(){
        Driving futureDriving = new Driving(driving);
        futureDriving.setFuture(true);
        futureDriving.setChosenAdditionalServices(mockData.createChosenAdditionalServices(mockData.createVehicle()));
        when(drivingRepository.getFutureDrivingFromPeriodWithoutDriver(any(Boolean.class), any(LocalDateTime.class), any(LocalDateTime.class)))
                .thenReturn(List.of(futureDriving));
        when(driverRepository.getActiveDrivers()).thenReturn(List.of());

        driverAssignmentService.assignDriverToFutureDriving();
       Assertions.assertEquals(DrivingState.REFUSED, futureDriving.getState());
       for (DrivingPayment dp: futureDriving.getPassengers()){
           Assertions.assertFalse(dp.isPaid());
       }

    }
}
