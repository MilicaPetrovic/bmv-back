package kts.nwt.ktsnwt.service;

import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.driving.DrivingPayment;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.data.MockData;
import kts.nwt.ktsnwt.dto.notification.DetailedDrivingNotificationDTO;
import kts.nwt.ktsnwt.exception.*;
import kts.nwt.ktsnwt.repository.DrivingPaymentRepository;
import kts.nwt.ktsnwt.service.driving.PassengersServiceImpl;
import kts.nwt.ktsnwt.service.driving.interfaces.DriverAssignmentService;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.notification.NotificationService;
import kts.nwt.ktsnwt.service.user.interfaces.UserService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PassengerServiceTest {

    @Mock
    private UserService userService;

    @Mock
    private DrivingService drivingService;

    @Mock
    private DriverAssignmentService driverAssignmentService;

    @Mock
    private NotificationService notificationService;

    @Mock
    private DrivingPaymentRepository drivingPaymentRepository;

    //@Autowired // ako stavim i autowired stubovanje ne radi --- he?
    @InjectMocks
    private PassengersServiceImpl passengersService;

    @Autowired
    private MockData mockData;

    private Driving driving;

    @BeforeEach
    public void prepareData(){
        driving = mockData.createDriving();
    }

    @Test
    public void shouldBeOkWithOnePassenger() throws NotEnoughTokensException, CanNotMakeNewDrivingException, RegisteredUserExceptedException {
        RegisteredUser ru = new RegisteredUser("jake", 1000, mockData.registeredUserRole);
        when(userService.findByUsernameNotBlocked("jake")).thenReturn(ru);
        when(drivingService.getActiveDrivingByClient(ru.getUsername())).thenReturn(new ArrayList<>());
        Driving result = passengersService.linkUsersInDriving("jake", new ArrayList<>(), driving);
        Assertions.assertEquals(1, result.getPassengers().size());
        Assertions.assertEquals(result.getPassengers().get(0).getRegisteredUser(), ru);
        Assertions.assertEquals(result.getPassengers().get(0).getPrice(), driving.getPrice());
    }

    @Test
    void shouldBeOkWithMorePassenger() throws NotEnoughTokensException, CanNotMakeNewDrivingException, RegisteredUserExceptedException {
        RegisteredUser jake = new RegisteredUser("jake", 1000, mockData.registeredUserRole);
        RegisteredUser neytiri = new RegisteredUser("neytiri", 1000, mockData.registeredUserRole);
        when(userService.findByUsernameNotBlocked(jake.getUsername())).thenReturn(jake);
        when(userService.findByUsernameNotBlocked(neytiri.getUsername())).thenReturn(neytiri);
        when(drivingService.getActiveDrivingByClient(jake.getUsername())).thenReturn(new ArrayList<>());
        when(drivingService.getActiveDrivingByClient(neytiri.getUsername())).thenReturn(new ArrayList<>());
        Driving result = passengersService.linkUsersInDriving("jake", new ArrayList<>(List.of(neytiri.getUsername())), driving);
        Assertions.assertEquals(2, result.getPassengers().size());
        Assertions.assertEquals(result.getPassengers().get(0).getRegisteredUser(), jake);
        Assertions.assertEquals(result.getPassengers().get(0).getPrice(), driving.getPrice()/2);
        Assertions.assertEquals(result.getPassengers().get(1).getRegisteredUser(), neytiri);
        Assertions.assertEquals(result.getPassengers().get(1).getPrice(), driving.getPrice()/2);
    }

    @Test
    void shouldBeNotFoundUser() throws NotEnoughTokensException, CanNotMakeNewDrivingException, RegisteredUserExceptedException {
        when(userService.findByUsernameNotBlocked("jake")).thenThrow(new NoUserFoundException(""));
        Assertions.assertThrows(NoUserFoundException.class, ()-> {
            passengersService.linkUsersInDriving("jake", new ArrayList<>(), driving);
        });
    }

    @Test
    void shouldBeNotEnoughTokenException() throws NotEnoughTokensException, CanNotMakeNewDrivingException, RegisteredUserExceptedException {
        RegisteredUser ru = new RegisteredUser("jake", driving.getPrice() - 10, mockData.registeredUserRole);
        when(userService.findByUsernameNotBlocked("jake")).thenReturn(ru);
        Assertions.assertThrows(NotEnoughTokensException.class, ()->{
            passengersService.linkUsersInDriving("jake", new ArrayList<>(), driving);
        });
    }
    @Test
    void shouldBeCanNotMakeNewDrivingException() throws NotEnoughTokensException, CanNotMakeNewDrivingException, RegisteredUserExceptedException {
        RegisteredUser ru = new RegisteredUser("jake", 1000, mockData.registeredUserRole);
        when(userService.findByUsernameNotBlocked("jake")).thenReturn(ru);
        when(drivingService.getActiveDrivingByClient(ru.getUsername())).thenReturn(new ArrayList<>(List.of(driving, mockData.createCurrentDriving())));
        Assertions.assertThrows(CanNotMakeNewDrivingException.class, () -> {
            passengersService.linkUsersInDriving("jake", new ArrayList<>(), driving);
        });
    }

    @Test
    void shouldConfirmDrivingInSplitFare() throws NotValidException, NoAvailableDriverException {
        Driving drivingWith2Passengers = mockData.createDrivingWithMultiplePassenger();
        when(drivingService.getDrivingById(1L)).thenReturn(drivingWith2Passengers);
        RegisteredUser ru = mockData.createRegisteredNeytiri();
        when(userService.findByUsername(ru.getUsername())).thenReturn(ru);
        List<DetailedDrivingNotificationDTO> notifications = passengersService.confirmDriving(1L, ru.getUsername());
        verify(driverAssignmentService).handleDriver(any(Clock.class), any(Driving.class));
        verify(notificationService).makeDriverFoundNotification(any(Driving.class));
    }

    @Test
    void shouldThrowNotValidNotAPassenger() throws NotValidException, NoAvailableDriverException {
        Driving drivingWith2Passengers = mockData.createDrivingWithMultiplePassenger();
        when(drivingService.getDrivingById(1L)).thenReturn(drivingWith2Passengers);
        RegisteredUser ru = new RegisteredUser("lucas", 1000, mockData.registeredUserRole);
        when(userService.findByUsername(ru.getUsername())).thenReturn(ru);
        Assertions.assertThrows(NotValidException.class, ()-> {
            passengersService.confirmDriving(1L, ru.getUsername());
        });
        verify(driverAssignmentService, times(0)).handleDriver(any(Clock.class), any(Driving.class));
        verify(notificationService, times(0)).makeDriverFoundNotification(any(Driving.class));
    }

    @Test
    void shouldConfirmDrivingInSplitFareNoAvailable() throws NotValidException, NoAvailableDriverException {
        Driving drivingWith2Passengers = mockData.createDrivingWithMultiplePassenger();
        when(drivingService.getDrivingById(1L)).thenReturn(drivingWith2Passengers);
        RegisteredUser ru = mockData.createRegisteredNeytiri();
        when(userService.findByUsername(ru.getUsername())).thenReturn(ru);
        when(driverAssignmentService.handleDriver(any(Clock.class), any(Driving.class))).thenThrow(NoAvailableDriverException.class);
        passengersService.confirmDriving(1L, ru.getUsername());
        verify(driverAssignmentService).handleDriver(any(Clock.class), any(Driving.class));
        verify(notificationService, never()).makeDriverFoundNotification(any(Driving.class));
        verify(notificationService).makeDriverNotFoundNotification(any(Driving.class));
    }
    @Test
    void shouldConfirmDrivingInSplitNotEverybodyConfirmed() throws NotValidException, NoAvailableDriverException {
        Driving drivingWith3Passengers = mockData.createDrivingWithMultiplePassenger();
        RegisteredUser ru = new RegisteredUser("philip", 1000, mockData.registeredUserRole);
        DrivingPayment dp = new DrivingPayment(drivingWith3Passengers.getPrice()/2, false, false, drivingWith3Passengers, ru);
        drivingWith3Passengers.addPassenger(dp);
        when(drivingService.getDrivingById(1L)).thenReturn(drivingWith3Passengers);
        when(userService.findByUsername(ru.getUsername())).thenReturn(ru);
        passengersService.confirmDriving(1L, ru.getUsername());
        verify(driverAssignmentService, never()).handleDriver(any(Clock.class), any(Driving.class));
        verify(notificationService, never()).makeDriverFoundNotification(any(Driving.class));
        verify(notificationService, never()).makeDriverNotFoundNotification(any(Driving.class));
    }
    @Test
    void shouldBeConfirmButDrivingNull() throws NotValidException, NoAvailableDriverException {
        //Driving drivingWith2Passengers = mockData.createDrivingWithMultiplePassenger();
        when(drivingService.getDrivingById(1L)).thenReturn(null);
        Assertions.assertThrows(NullPointerException.class, ()-> {
            passengersService.confirmDriving(1L, "");
        });
        verify(driverAssignmentService, times(0)).handleDriver(any(Clock.class), any(Driving.class));
        verify(notificationService, times(0)).makeDriverFoundNotification(any(Driving.class));
    }
    @Test
    void shouldBeConfirmButUserNotFound() throws NotValidException, NoAvailableDriverException {
        when(drivingService.getDrivingById(1L)).thenReturn(driving);
        when(userService.findByUsername("")).thenThrow(UsernameNotFoundException.class);
        Assertions.assertThrows(UsernameNotFoundException.class, ()-> {
            passengersService.confirmDriving(1L, "");
        });
        verify(driverAssignmentService, times(0)).handleDriver(any(Clock.class), any(Driving.class));
        verify(notificationService, times(0)).makeDriverFoundNotification(any(Driving.class));
    }
}
