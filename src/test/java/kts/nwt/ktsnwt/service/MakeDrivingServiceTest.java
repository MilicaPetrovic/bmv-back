package kts.nwt.ktsnwt.service;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.communication.Notification;
import kts.nwt.ktsnwt.beans.driving.ChosenAdditionalServices;
import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.driving.Vehicle;
import kts.nwt.ktsnwt.beans.driving.VehicleType;
import kts.nwt.ktsnwt.beans.location.Path;
import kts.nwt.ktsnwt.data.MockData;
import kts.nwt.ktsnwt.dto.driving.MakeDrivingDTO;
import kts.nwt.ktsnwt.exception.*;
import kts.nwt.ktsnwt.service.driving.MakeDrivingServiceImpl;
import kts.nwt.ktsnwt.service.driving.interfaces.*;
import kts.nwt.ktsnwt.service.notification.NotificationService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MakeDrivingServiceTest {

    @Mock
    private ChosenAdditionalServicesService chosenAdditionalServices;

    @Mock
    private PathService pathService;
    @Mock
    private PassengersService passengersService;

    @Mock
    private DrivingService drivingService;

    @Mock
    private NotificationService notificationService;

    @Mock
    private DriverAssignmentService driverAssignmentService;

    @InjectMocks
    private MakeDrivingServiceImpl makeDrivingService;

    @Autowired
    private MockData mockData;

    private Driving driving;
    private Driving drivingWithMultiplePassanger;
    @BeforeAll
    void drivingSetup(){
        driving = mockData.createDriving();
        drivingWithMultiplePassanger = mockData.createDrivingWithMultiplePassenger();
    }

    @Test
    void shouldSearchForDriver() throws NotEnoughTokensException, InvalidFutureTimeException, NotFoundException, CanNotMakeNewDrivingException, RegisteredUserExceptedException, NoAvailableDriverException, NoPathGivenException {
        //when(chosenAdditionalServices.makeChosenAdditionalServices(anyLong(), anyBoolean(), anyBoolean())).thenReturn(new ChosenAdditionalServices());
        //when(pathService.save(any(ArrayList.class),(Object.class), anyDouble(), anyDouble(), anyBoolean(), anyString())).thenReturn(new Path());
        driving.setId(1L);
        driving.setFuture(false);
        when(drivingService.save(any(Clock.class), eq(null), eq(null), anyDouble(), anyDouble(),anyBoolean(), anyInt(),anyInt())).thenReturn(driving);
        when(notificationService.createSplitFareConfirmationNotification(any())).thenReturn(new ArrayList<>());
        makeDrivingService.makeDrivingAndReturnNotificationsForPassengers(new MakeDrivingDTO());
        verify(driverAssignmentService).handleDriver(any(Clock.class), any(Driving.class));
    }

    @Test
    void shouldNotSearchForDriverFuture() throws NotEnoughTokensException, InvalidFutureTimeException, NotFoundException, CanNotMakeNewDrivingException, RegisteredUserExceptedException, NoAvailableDriverException, NoPathGivenException {
        driving.setId(1L);
        driving.setFuture(true);
        when(drivingService.save(any(Clock.class), eq(null), eq(null), anyDouble(), anyDouble(),anyBoolean(), anyInt(),anyInt())).thenReturn(driving);
        when(notificationService.createSplitFareConfirmationNotification(any())).thenReturn(new ArrayList<>());
        makeDrivingService.makeDrivingAndReturnNotificationsForPassengers(new MakeDrivingDTO());
        verify(driverAssignmentService, never()).handleDriver(any(Clock.class), any(Driving.class));
    }

    @Test
    void shouldNotSearchForDriverSplitFare() throws NotEnoughTokensException, InvalidFutureTimeException, NotFoundException, CanNotMakeNewDrivingException, RegisteredUserExceptedException, NoAvailableDriverException, NoPathGivenException {
        driving.setId(1L);
        driving.setFuture(false);
        when(drivingService.save(any(Clock.class), eq(null), eq(null), anyDouble(), anyDouble(),anyBoolean(), anyInt(),anyInt())).thenReturn(driving);
        when(notificationService.createSplitFareConfirmationNotification(any())).thenReturn(List.of(new Notification()));
        makeDrivingService.makeDrivingAndReturnNotificationsForPassengers(new MakeDrivingDTO());
        verify(driverAssignmentService, never()).handleDriver(any(Clock.class), any(Driving.class));
    }
}
