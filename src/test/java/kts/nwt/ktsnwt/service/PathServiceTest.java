package kts.nwt.ktsnwt.service;

import kts.nwt.ktsnwt.beans.communication.Notification;
import kts.nwt.ktsnwt.beans.location.Path;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.data.MockData;
import kts.nwt.ktsnwt.dto.location.MapPointDTO;
import kts.nwt.ktsnwt.exception.NoPathGivenException;
import kts.nwt.ktsnwt.repository.PathRepository;
import kts.nwt.ktsnwt.repository.RegisteredUserRepository;
import kts.nwt.ktsnwt.service.driving.PathServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PathServiceTest {
    @Mock
    private PathRepository pathRepository;

    @Mock
    private RegisteredUserRepository registeredUserRepository;

    @InjectMocks
    private PathServiceImpl pathService;

    @Autowired
    private MockData mockData;

    private ArrayList<MapPointDTO> coordinates;
    private ArrayList<MapPointDTO> chosenStations;

    private Path path;

    @BeforeAll
    public void createChosenStations(){
        chosenStations = new ArrayList<>();
        chosenStations.add(new MapPointDTO(45.2464547, 19.8342626,"Gogoljeva 22 Novi Sad"));
        chosenStations.add(new MapPointDTO(45.2537745, 19.8411572,"Jevrejska 10 Novi Sad"));
    }
    @BeforeAll
    public void createCoordinates(){
        coordinates = new ArrayList<>();
        coordinates.add(new MapPointDTO(45.24638, 19.8342));
        coordinates.add(new MapPointDTO(45.24611, 19.8348));
        coordinates.add(new MapPointDTO(45.24591, 19.8354));
        coordinates.add(new MapPointDTO(45.24591, 19.8354));
    }

    @BeforeAll
    public void createPath(){
        path = mockData.createPath();
    }

    @Test
    void shouldBeOkSavedPath() throws NoPathGivenException {
        Mockito.when(pathRepository.save(any(Path.class))).thenReturn(path);

        Path returnedPath = pathService.save(chosenStations, coordinates, mockData.DISTANCE, mockData.ROUTE_TIME, false, "");

        Assertions.assertEquals(returnedPath.getRoute().getTime(), path.getRoute().getTime());
        Assertions.assertEquals(returnedPath.getRoute().getDistance(), path.getRoute().getDistance());
        Assertions.assertEquals(returnedPath.getStations().size(), chosenStations.size());
        Assertions.assertIterableEquals(returnedPath.getRoute().getRoute(), path.getRoute().getRoute());

        Mockito.verify(pathRepository, times(1)).save(any(Path.class));
        Mockito.verify(registeredUserRepository, never()).findByUsername(anyString());
    }

    @Test
    void shouldBeOkFavouriteIt() throws NoPathGivenException {
        Mockito.when(pathRepository.save(any(Path.class))).thenReturn(path);
        RegisteredUser registeredUser = mockData.createRegisteredJake();
        Mockito.when(registeredUserRepository.findByUsername(registeredUser.getUsername())).thenReturn(Optional.of(registeredUser));
        Mockito.when(registeredUserRepository.save(any(RegisteredUser.class))).thenReturn(registeredUser);

        Path returnedPath = pathService.save(chosenStations, coordinates, mockData.DISTANCE, mockData.ROUTE_TIME, true, registeredUser.getUsername());

        Assertions.assertEquals(returnedPath.getRoute().getTime(), path.getRoute().getTime());
        Assertions.assertEquals(returnedPath.getRoute().getDistance(), path.getRoute().getDistance());
        Assertions.assertEquals(returnedPath.getStations().size(), chosenStations.size());
        Assertions.assertIterableEquals(returnedPath.getRoute().getRoute(), path.getRoute().getRoute());
        Assertions.assertEquals(1, registeredUser.getFavouriteRoute().size());

        Mockito.verify(pathRepository, atLeast(1)).save(any(Path.class));
        Mockito.verify(registeredUserRepository, atLeast(1)).findByUsername(registeredUser.getUsername());
    }

    @Test
    void shouldFallOnUserNull(){
        Mockito.when(pathRepository.save(any(Path.class))).thenReturn(path);
        RegisteredUser registeredUser = mockData.createRegisteredJake();
        Mockito.when(registeredUserRepository.findByUsername(registeredUser.getUsername())).thenReturn(Optional.ofNullable(null));

        Assertions.assertThrows(NullPointerException.class, () -> {
            pathService.save(chosenStations, coordinates, mockData.DISTANCE, mockData.ROUTE_TIME, true, registeredUser.getUsername());
        });

        Mockito.verify(pathRepository, times(1)).save(any(Path.class));
        Mockito.verify(registeredUserRepository, times(1)).findByUsername(registeredUser.getUsername());
        Mockito.verify(registeredUserRepository, times(0)).save(any(RegisteredUser.class));
    }
    @Test
    void shouldBeNoPathExceptionNoChosenPath(){
        Assertions.assertThrows(NoPathGivenException.class, ()->{
            pathService.save(new ArrayList<>(), coordinates, mockData.DISTANCE, mockData.ROUTE_TIME,false, "");
        });
        Mockito.verify(pathRepository, times(0)).save(path);
    }

    @Test
    void shouldBeNoPathExceptionNoCoordinates(){
        Assertions.assertThrows(NoPathGivenException.class, ()->{
            pathService.save(chosenStations, new ArrayList<>(), mockData.DISTANCE, mockData.ROUTE_TIME, false, "");
        });
        Mockito.verify(pathRepository, times(0)).save(path);
    }
}
