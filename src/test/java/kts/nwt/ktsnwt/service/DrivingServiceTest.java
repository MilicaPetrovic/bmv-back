package kts.nwt.ktsnwt.service;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.driving.*;
import kts.nwt.ktsnwt.beans.location.Address;
import kts.nwt.ktsnwt.beans.location.Path;
import kts.nwt.ktsnwt.beans.location.Route;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.DriverState;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.data.MockData;
import kts.nwt.ktsnwt.dto.driving.RejectDrivingDTO;
import kts.nwt.ktsnwt.exception.InvalidFutureTimeException;
import kts.nwt.ktsnwt.repository.DriverRepository;
import kts.nwt.ktsnwt.repository.DrivingPaymentRepository;
import kts.nwt.ktsnwt.repository.DrivingRepository;
import kts.nwt.ktsnwt.repository.UserRepository;
import kts.nwt.ktsnwt.service.driving.DrivingServiceImpl;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.service.notification.NotificationService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DrivingServiceTest {

    @MockBean
    private DrivingRepository drivingRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private DrivingPaymentRepository drivingPaymentRepository;

    @MockBean
    private  DriverRepository driverRepository;

    @MockBean
    private NotificationService notificationService;

    @Autowired
    @InjectMocks
    private DrivingServiceImpl drivingService;

    private ChosenAdditionalServices chosenAdditionalServices;
    private Path path;

    @Autowired
    private MockData mockData;

    private Driving driving;

    private Driving paidDriving;

    @BeforeAll
    public void createChosenAdditionalServices(){
        chosenAdditionalServices = mockData.createChosenAdditionalServices(mockData.createVehicle());
        path = mockData.createPath();
        driving = mockData.createDriving(chosenAdditionalServices, path);
        paidDriving = mockData.createPaidDriving();
    }

    @Test
    public void shouldBeSaveDriving() throws InvalidFutureTimeException {
        driving.setId(1L);
        lenient().when(drivingRepository.save(any(Driving.class))).thenReturn(driving);
        lenient().when(drivingRepository.save(driving)).thenReturn(driving);
        Driving d = drivingService.save(mockData.fixedClock, chosenAdditionalServices, path, mockData.PRICE, mockData.DRIVING_TIME, false, 0, 0);
        Assertions.assertEquals(driving.getOrderTime(), d.getOrderTime());//jel uopste konzistentno
        Assertions.assertEquals(d.getPath(), driving.getPath());
        Assertions.assertEquals(d.getDrivingTime(), driving.getDrivingTime());
        verify(drivingRepository).save(any(Driving.class));
    }

    @Test
    public void shouldBeSaveDrivingForFuture() throws InvalidFutureTimeException {
        System.out.println(path);
        driving.setId(1L);
        when(drivingRepository.save(any(Driving.class))).thenReturn(driving);
        Driving d = drivingService.save(mockData.fixedClock, chosenAdditionalServices, path, mockData.PRICE, mockData.DRIVING_TIME, true, LocalDateTime.now().plusHours(4).getHour(), 0);
        Assertions.assertEquals(driving.getOrderTime(), d.getOrderTime());//jel uopste konzistentno
        Assertions.assertEquals(d.getPath(), driving.getPath());
        Assertions.assertEquals(d.getDrivingTime(), driving.getDrivingTime());
        verify(drivingRepository).save(any(Driving.class));
    }

    @Test
    public void shouldGetInvalidFutureTimeExceptionAbove5Hours() throws InvalidFutureTimeException {
        driving.setId(1L);
        Assertions.assertThrows(InvalidFutureTimeException.class, () -> {
            drivingService.save(mockData.fixedClock, chosenAdditionalServices, path, mockData.PRICE, mockData.DRIVING_TIME, true, LocalDateTime.now().plusHours(6).getHour(), 0);
        });
    }

    @Test
    public void shouldGetInvalidFutureTimeExceptionInPast() throws InvalidFutureTimeException {
        driving.setId(1L);
        Assertions.assertThrows(InvalidFutureTimeException.class, () -> {
            drivingService.save(mockData.fixedClock, chosenAdditionalServices, path, mockData.PRICE, mockData.DRIVING_TIME, true, LocalDateTime.now().minusHours(2).getHour(), 0);
        });
    }

    @Test
    @DisplayName("Reject driving test - driving not found")
    public void shouldGetNotFoundException() {
        RejectDrivingDTO dto = new RejectDrivingDTO(99L, "Because");

        when(drivingRepository.findById(99L)).thenReturn(Optional.empty());

        Assertions.assertThrows(NotFoundException.class, () -> {
            drivingService.rejectDriving(dto);
        });
    }

    @Test
    @DisplayName("Reject driving test - driving is rejected")
    public void shouldRejectDriving() throws NotFoundException {
        Driver driver = mockData.createDrivingDriver1(); // ima PAID driving
        paidDriving.setDriver(driver);
        RejectDrivingDTO dto = new RejectDrivingDTO(50L, "Because");

        when(drivingRepository.findById(50L)).thenReturn(Optional.ofNullable(paidDriving));

        drivingService.rejectDriving(dto);
        Assertions.assertEquals(DriverState.ACTIVE, driver.getDriverState());
        Assertions.assertEquals(DrivingState.REJECTED, paidDriving.getState());
        Assertions.assertEquals(dto.getReason(), paidDriving.getRejectionText());

    }

    @Test
    @DisplayName("Reject driving test - tokens are returned to user")
    public void shouldReturnMoney() throws NotFoundException {
        Driver driver = mockData.createDrivingDriver1(); // ima PAID driving
        paidDriving.setDriver(driver);
        RejectDrivingDTO dto = new RejectDrivingDTO(50L, "Because");

        when(drivingRepository.findById(50L)).thenReturn(Optional.ofNullable(paidDriving));

        DrivingPayment drivingPayment1 = paidDriving.getPassengers().get(0);
        DrivingPayment drivingPayment2 = paidDriving.getPassengers().get(1);
        RegisteredUser passenger1 = drivingPayment1.getRegisteredUser();
        RegisteredUser passenger2 = drivingPayment2.getRegisteredUser();
        double oldTokens1 = passenger1.getTokens();
        double oldTokens2 = passenger2.getTokens();

        drivingService.rejectDriving(dto);

        Assertions.assertEquals(oldTokens1 + drivingPayment1.getPrice(), passenger1.getTokens());
        Assertions.assertEquals(oldTokens2 + drivingPayment2.getPrice(), passenger2.getTokens());

    }
}
