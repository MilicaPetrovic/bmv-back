package kts.nwt.ktsnwt.service;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.driving.ChosenAdditionalServices;
import kts.nwt.ktsnwt.beans.driving.VehicleType;
import kts.nwt.ktsnwt.data.MockData;
import kts.nwt.ktsnwt.repository.ChosenAdditionalServicesRepository;
import kts.nwt.ktsnwt.repository.VehicleTypeRepository;
import kts.nwt.ktsnwt.service.driving.ChosenAdditionalServicesServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ChosenAdditionalServicesServiceTest {

    @Mock
    private VehicleTypeRepository vehicleTypeRepository;

    @Mock
    private ChosenAdditionalServicesRepository chosenAdditionalServicesRepository;

    @Autowired
    @InjectMocks
    private ChosenAdditionalServicesServiceImpl chosenAdditionalServicesService;

    @Autowired
    private MockData mockData;

    @Test
    public void shouldBeNotFoundException(){
        when(vehicleTypeRepository.findByIdIs(1L)).thenReturn(Optional.ofNullable(null));

        Assertions.assertThrows(NotFoundException.class, ()->{
            chosenAdditionalServicesService.makeChosenAdditionalServices(1L, false, false);
        });

        verify(vehicleTypeRepository, times(1)).findByIdIs(1L);
    }

    @Test
    public void shouldBeSaveChosenAdditionalServices() throws NotFoundException {
        VehicleType vt = mockData.createVehicle();//
        when(vehicleTypeRepository.findByIdIs(1L)).thenReturn(Optional.of(vt));
        ChosenAdditionalServices chosenAdditionalServices = mockData.createChosenAdditionalServices(vt);//
        when(chosenAdditionalServicesRepository.save(chosenAdditionalServices)).thenReturn(new ChosenAdditionalServices(chosenAdditionalServices));

        ChosenAdditionalServices ca = chosenAdditionalServicesService.makeChosenAdditionalServices(1L, false, false);

        Assertions.assertFalse(ca.isBabyFriendly());
        Assertions.assertFalse(ca.isPetFriendly());
        Assertions.assertEquals(vt.getId(), ca.getType().getId());
        
        verify(vehicleTypeRepository, times(1)).findByIdIs(1L);
        verify(chosenAdditionalServicesRepository, times(1)).save(chosenAdditionalServices);
    }
}
