package kts.nwt.ktsnwt.service;

import javassist.NotFoundException;
import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.beans.driving.DrivingState;
import kts.nwt.ktsnwt.beans.location.Address;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.beans.users.User;
import kts.nwt.ktsnwt.data.MockData;
import kts.nwt.ktsnwt.dto.driving.DrivingStateDTO;
import kts.nwt.ktsnwt.dto.location.SimulationDTO;
import kts.nwt.ktsnwt.dto.location.SimulationVehicleDTO;
import kts.nwt.ktsnwt.dto.report.InconsistencyDTO;
import kts.nwt.ktsnwt.exception.InvalidDrivingStateException;
import kts.nwt.ktsnwt.exception.NoUserFoundException;
import kts.nwt.ktsnwt.exception.NotHaveDrivingsForDriver;
import kts.nwt.ktsnwt.repository.DriverRepository;
import kts.nwt.ktsnwt.repository.DrivingRepository;
import kts.nwt.ktsnwt.repository.UserRepository;
import kts.nwt.ktsnwt.repository.VehicleTypeRepository;
import kts.nwt.ktsnwt.service.driving.DrivingServiceImpl;
import kts.nwt.ktsnwt.service.driving.VehicleTypeServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SimulationDrivingTest {

    @MockBean
    private DrivingRepository drivingRepository;

    @MockBean
    private DriverRepository driverRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private VehicleTypeRepository vehicleTypeRepository;

    @Autowired
    @InjectMocks
    private DrivingServiceImpl drivingService;

    @Autowired
    @InjectMocks
    private VehicleTypeServiceImpl vehicleTypeService;

    @Autowired
    private MockData mockData;

    private Driving driving;
    private Driver driver;

    @Test
    public void shouldBeOkGetLastCurrentPaidDrivingForDriver() {

        driver = mockData.createDrivingDriver1();
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);
        System.out.println("stub driving : " + driving.getId() + " " + driving.getPath().getRoute().getRoute().size());
        System.out.println("stub driver : " + driver.getId() + " " + driver.getName());

        System.out.println(drivingRepository);

        Mockito.when(drivingRepository.getPaidOrCurrentDrivingByDriverId(driver.getId())).thenReturn(Optional.ofNullable(driving));
        Mockito.when(drivingRepository.findDrivingById(driving.getId())).thenReturn(Optional.ofNullable(driving));

        SimulationDTO simulation = drivingService.getLastCurrentPaidDrivingForDriver(driver.getId());
        System.out.println("simulation: " + simulation);
        assertEquals(simulation.getId(), driving.getId());
        verify(drivingRepository, times(1)).getPaidOrCurrentDrivingByDriverId(driver.getId());
        verify(drivingRepository, times(1)).findDrivingById(driving.getId());
    }

    @Test
    public void shouldBeNullGetLastCurrentPaidDrivingForDriver() {
        driver = mockData.createActiveDriver();

        Mockito.when(drivingRepository.getPaidOrCurrentDrivingByDriverId(driving.getId())).thenReturn(null);

        SimulationDTO simulationDTO = drivingService.getLastCurrentPaidDrivingForDriver(driver.getId());
        assertEquals(null, simulationDTO);
        verify(drivingRepository, times(1)).getPaidOrCurrentDrivingByDriverId(driver.getId());
    }


    @Test
    public void shouldBeOkGetDriverByDrivingId() throws NotFoundException {
        driver = mockData.createDrivingDriver2();
        driving = driver.getDrivingHistory().get(0);
        driver.setUsername("neki username");
        driving.setDriver(driver);

        Mockito.when(drivingRepository.findDrivingById(driving.getId())).thenReturn(Optional.ofNullable(driving));

        String driverUsername = this.drivingService.getDriverByDrivingId(driving.getId());

        System.out.println(driverUsername);
        assertEquals(driver.getUsername(), driverUsername);
        verify(drivingRepository, times(1)).findDrivingById(driving.getId());
    }

    @Test
    public void shouldBeNullDriverGetDriverByDrivingId() throws NotFoundException {
        driving = mockData.createAcceptedDriving();

        Mockito.when(drivingRepository.findDrivingById(driving.getId())).thenReturn(Optional.ofNullable(driving));
        String driverUsername = null;

        driverUsername = this.drivingService.getDriverByDrivingId(driving.getId());

        assertEquals(null, driverUsername);
        verify(drivingRepository, times(1)).findDrivingById(driving.getId());
    }

    @Test
    public void shouldBeNullDrivingGetDriverByDrivingId() {
        driving = mockData.createAcceptedDriving();

        Mockito.when(drivingRepository.findDrivingById(driving.getId())).thenReturn(Optional.ofNullable(null));
        Assertions.assertThrows(NotFoundException.class, () -> {
            drivingService.getDriverByDrivingId(driving.getId());
        });
    }


    @Test
    public void shouldBeOkGetSimulationDriving() {
        driver = mockData.createDrivingDriver2();
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);


        Mockito.when(drivingRepository.findDrivingById(driving.getId())).thenReturn(Optional.ofNullable(driving));

        SimulationDTO simulationDTO = this.drivingService.getSimulationDriving(driving.getId());
        assertEquals(simulationDTO.getId(), driving.getId());
        verify(drivingRepository, times(1)).findDrivingById(driving.getId());

    }

    @Test
    public void shouldBeNullGetSimulationDriving() {
        driver = mockData.createDrivingDriver2();
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);

        Mockito.when(drivingRepository.findDrivingById(driving.getId())).thenReturn(Optional.ofNullable(null));

        Assertions.assertThrows(NullPointerException.class, () -> {
            drivingService.getSimulationDriving(driving.getId());
        });
        verify(drivingRepository, times(1)).findDrivingById(driving.getId());
    }

    @Test
    public void shouldBeStartGetStateOfLastDriving() throws NotHaveDrivingsForDriver {
        driver = mockData.createDrivingDriver1();
        driver.setUsername("username");
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);

        Mockito.when(driverRepository.findByUsername(driver.getUsername())).thenReturn(Optional.ofNullable(driver));
        Mockito.when(drivingRepository.getDrivingsByDriverId(driver.getId())).thenReturn(List.of(driving));

        DrivingStateDTO drivingStateDTO = drivingService.getStateOfLastDriving(driver.getUsername());

        assertEquals(true, drivingStateDTO.isStart());
        assertEquals(false, drivingStateDTO.isFinish());
        assertEquals(driving.getId(), drivingStateDTO.getDrivingId());

        verify(drivingRepository, times(1)).getDrivingsByDriverId(driver.getId());
        verify(driverRepository, times(1)).findByUsername(driver.getUsername());
    }

    @Test
    public void shouldBeFinishGetStateOfLastDriving() throws NotHaveDrivingsForDriver {
        driver = mockData.createDrivingDriver2();
        driver.setUsername("username");
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);
        for (Address a : driving.getPath().getRoute().getRoute()) {
            System.out.println("lat: " + a.getLatitude() + "   long:  " + a.getLongitude());
        }

        Mockito.when(driverRepository.findByUsername(driver.getUsername())).thenReturn(Optional.ofNullable(driver));
        Mockito.when(drivingRepository.getDrivingsByDriverId(driver.getId())).thenReturn(List.of(driving));

        DrivingStateDTO drivingStateDTO = drivingService.getStateOfLastDriving(driver.getUsername());

        assertEquals(false, drivingStateDTO.isStart());
        assertEquals(true, drivingStateDTO.isFinish());
        assertEquals(driving.getId(), drivingStateDTO.getDrivingId());

        verify(drivingRepository, times(1)).getDrivingsByDriverId(driver.getId());
        verify(driverRepository, times(1)).findByUsername(driver.getUsername());
    }

    @Test
    public void noUserFoundExceptionGetStateOfLastDriving(){
        Mockito.when(driverRepository.findByUsername("username")).thenReturn(Optional.ofNullable(null));

        Assertions.assertThrows(NoUserFoundException.class, () -> {
            drivingService.getStateOfLastDriving("username");
        });
        verify(driverRepository, times(1)).findByUsername("username");
    }

    @Test
    public void notHaveDrivingsForDriver(){
        driver = mockData.createActiveDriver();
        driver.setUsername("aleksa");

        Mockito.when(driverRepository.findByUsername(driver.getUsername())).thenReturn(Optional.ofNullable(driver));
        Mockito.when(drivingRepository.getDrivingsByDriverId(driver.getId())).thenReturn(List.of());
        Assertions.assertThrows(NotHaveDrivingsForDriver.class, () -> {
            drivingService.getStateOfLastDriving(driver.getUsername());
        });
        verify(drivingRepository, times(1)).getDrivingsByDriverId(driver.getId());
        verify(driverRepository, times(1)).findByUsername(driver.getUsername());
    }


    @Test
    public void shouldBeOkDriverGetSimulationIfExist() throws NotHaveDrivingsForDriver {
        driver = mockData.createDrivingDriver2();
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);
        driver.setUsername("pera");
        System.out.println(driver.getUsername() + "    " + driver);

        Mockito.when(userRepository.findByUsername(driver.getUsername())).thenReturn(Optional.ofNullable(driver));
        Mockito.when(drivingRepository.getDrivingsByDriverId(driver.getId())).thenReturn(List.of(driving));
        Mockito.when(drivingRepository.findDrivingById(driving.getId())).thenReturn(Optional.ofNullable(driving));

        SimulationDTO simulationDTO = drivingService.getSimulationIfExist(driver.getUsername());
        assertEquals(simulationDTO.getId(), driving.getId());
        assertEquals(simulationDTO.getIsPaid(), false);

        verify(drivingRepository, times(1)).getDrivingsByDriverId(driver.getId());
        verify(userRepository, times(1)).findByUsername(driver.getUsername());
    }

    @Test  // ovo pogledaj  !!!!!!!!!!!!!!
    public void shouldBeOkRegUserGetSimulationIfExist() throws NotHaveDrivingsForDriver {
        driver = mockData.createDrivingDriver2();
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);
        User registeredUser = driving.getPassengers().get(0).getRegisteredUser();

        if(registeredUser instanceof User) {
            System.out.println(registeredUser.getUsername() + "  " + registeredUser);
        }

        Mockito.when(userRepository.findByUsername(registeredUser.getUsername())).thenReturn(Optional.of(registeredUser));
        Mockito.when(drivingRepository.getActiveDrivingsByClientsUsername(registeredUser.getUsername())).thenReturn(List.of(driving));
        Mockito.when(drivingRepository.findDrivingById(driving.getId())).thenReturn(Optional.ofNullable(driving));

        SimulationDTO simulationDTO = drivingService.getSimulationIfExist(registeredUser.getUsername());
        assertEquals(simulationDTO.getId(), driving.getId());
        assertEquals(simulationDTO.getIsPaid(), false);

        verify(drivingRepository, times(1)).findDrivingById(driving.getId());
        verify(drivingRepository, times(1)).getActiveDrivingsByClientsUsername(registeredUser.getUsername());
        verify(userRepository, times(1)).findByUsername(registeredUser.getUsername());
    }

    @Test
    public void noUserFoundExceptionGetSimulationIfExist(){
        Mockito.when(userRepository.findByUsername("username")).thenReturn(Optional.ofNullable(null));
        Assertions.assertThrows(NoUserFoundException.class, () -> {
            drivingService.getSimulationIfExist("username");
        });
        verify(userRepository, times(1)).findByUsername("username");
    }

    @Test
    public void shouldBeOkUpdateVehicleLocation() {
        driver = mockData.createDrivingDriver2();
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);

        Mockito.when(driverRepository.findDriverById(driver.getId())).thenReturn(Optional.ofNullable(driver));

        SimulationVehicleDTO simulationVehicleDTO = this.vehicleTypeService.updateVehicleLocation(driver.getId(), 12, 12);
        assertEquals(simulationVehicleDTO.getLatitude(), 12);
        assertEquals(simulationVehicleDTO.getLongitude(), 12);
        verify(driverRepository, times(1)).findDriverById(driver.getId());
    }

    @Test
    public void shouldBeExceptionUpdateVehicleLocation() {
        Mockito.when(driverRepository.findDriverById(6L)).thenReturn(Optional.ofNullable(null));
        Assertions.assertThrows(NoUserFoundException.class, () -> {
            vehicleTypeService.updateVehicleLocation(6L, 12, 12);
        });
        verify(driverRepository, times(1)).findDriverById(6L);
    }


    @Test
    public void checkConsistencyPaidDriving() {
        driver = mockData.createDrivingDriver1();  // paid
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);

        Mockito.when(drivingRepository.findDrivingById(driving.getId())).thenReturn(Optional.ofNullable(driving));

        boolean isConsistent = vehicleTypeService.checkConsistency(driver.getId(), driving.getId(), 10, 10);
        assertEquals(isConsistent, true);
        verify(drivingRepository, times(1)).findDrivingById(driving.getId());
    }

    @Test
    public void checkConsistencyCurrentDrivingInConsistent() {
        driver = mockData.createDrivingDriver1();  // paid
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);

        Mockito.when(drivingRepository.findDrivingById(driving.getId())).thenReturn(Optional.ofNullable(driving));

        boolean isConsistent = vehicleTypeService.checkConsistency(driver.getId(), driving.getId(), 10, 10);
        assertEquals(isConsistent, true);

        driving.setState(DrivingState.CURRENT);

        boolean isConsistent2 = vehicleTypeService.checkConsistency(driver.getId(), driving.getId(), 10, 10);
        assertEquals(isConsistent2, false);

        verify(drivingRepository, times(2)).findDrivingById(driving.getId());
    }

    @Test
    public void checkConsistencyCurrentDrivingConsistent() {
        driver = mockData.createDrivingDriver1();  // paid
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);

        Mockito.when(drivingRepository.findDrivingById(driving.getId())).thenReturn(Optional.ofNullable(driving));

        boolean isConsistent = vehicleTypeService.checkConsistency(driver.getId(), driving.getId(), 10, 10);
        assertEquals(isConsistent, true);

        driving.setState(DrivingState.CURRENT);

        boolean isConsistent2 = vehicleTypeService.checkConsistency(driver.getId(), driving.getId(), 45.24591, 19.8354);
        assertEquals(isConsistent2, true);

        verify(drivingRepository, times(2)).findDrivingById(driving.getId());
    }

    @Test
    public void shouldBeOkGetPassenger() throws NotFoundException {
        driver = mockData.createDrivingDriver2();  // current
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);

        Mockito.when(drivingRepository.findDrivingById(driving.getId())).thenReturn(Optional.ofNullable(driving));

        List<InconsistencyDTO> inconsistencyDTOs = vehicleTypeService.getPassenger(driver.getId(), driving.getId());
        assertEquals(2, inconsistencyDTOs.size());

        verify(drivingRepository, times(1)).findDrivingById(driving.getId());
    }

    @Test
    public void drivingNullGetPassenger() {
        driver = mockData.createDrivingDriver2();  // current
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);

        Mockito.when(drivingRepository.findDrivingById(driving.getId())).thenReturn(Optional.ofNullable(null));

        Assertions.assertThrows(NotFoundException.class, () -> {
            vehicleTypeService.getPassenger(driver.getId(), driving.getId());
        });
        verify(drivingRepository, times(1)).findDrivingById(driving.getId());
    }


    @Test
    public void shouldBeOkStartDriving() throws NotFoundException {
        driver = mockData.createDrivingDriver1();
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);

        Mockito.when(drivingRepository.findById(driving.getId())).thenReturn(Optional.ofNullable(driving));

        Driving started = drivingService.startDriving(driving.getId());
        assertEquals(started.getState(), DrivingState.CURRENT);
        verify(drivingRepository, times(1)).findById(driving.getId());
    }


    @Test
    public void drivingNullStartDriving() {
        driver = mockData.createDrivingDriver1();
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);

        Mockito.when(drivingRepository.findById(driving.getId())).thenReturn(Optional.ofNullable(null));
        Assertions.assertThrows(NotFoundException.class, () -> {
            drivingService.startDriving(driving.getId());
        });
        verify(drivingRepository, times(1)).findById(driving.getId());
    }

    @Test
    public void drivingInappropriateStateStartDriving() {
        driver = mockData.createDrivingDriver1();
        driving = driver.getDrivingHistory().get(0);
        driving.setDriver(driver);

        driving.setState(DrivingState.ACCEPTED);
        Mockito.when(drivingRepository.findById(driving.getId())).thenReturn(Optional.ofNullable(driving));

        Assertions.assertThrows(InvalidDrivingStateException.class, () -> {
            drivingService.startDriving(driving.getId());
        });
        verify(drivingRepository, times(1)).findById(driving.getId());
    }
}
