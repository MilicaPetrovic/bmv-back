package kts.nwt.ktsnwt.service;


import kts.nwt.ktsnwt.beans.communication.Notification;
import kts.nwt.ktsnwt.beans.communication.NotificationType;
import kts.nwt.ktsnwt.beans.driving.Driving;
import kts.nwt.ktsnwt.data.MockData;
import kts.nwt.ktsnwt.dto.notification.DetailedDrivingNotificationDTO;
import kts.nwt.ktsnwt.dto.notification.DriverNotificationDTO;
import kts.nwt.ktsnwt.dto.notification.DrivingReminderNotification;
import kts.nwt.ktsnwt.repository.NotificationRepository;
import kts.nwt.ktsnwt.service.driving.interfaces.DrivingService;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.dto.notification.NewNotificationDTO;
import kts.nwt.ktsnwt.service.notification.NotificationServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class NotificationServiceTest {

    @Mock
    private NotificationRepository notificationRepository;


    @Mock
    private DrivingService drivingService;

    @Mock
    private SimpMessagingTemplate template;

    @InjectMocks
    private NotificationServiceImpl notificationService;

    @Autowired
    private MockData mockData;

    private Driving drivingWithPassenger;
    private Driving drivingWithMultiplePassengers;
    private Driver driver;

    @BeforeAll
    void createDrivings(){
        drivingWithPassenger = mockData.createDrivingWithPassenger();
        drivingWithMultiplePassengers = mockData.createDrivingWithMultiplePassenger();
    }

    @Test
    void shouldBeOkWithOnePassenger(){
        List<Notification> res = notificationService.createSplitFareConfirmationNotification(drivingWithPassenger);
        Assertions.assertTrue(res.isEmpty());
    }


    @Test
    void shouldBeOkWithMultiplePassenger(){
        when(notificationRepository.save(any(Notification.class))).thenReturn(new Notification());
        List<Notification> res = notificationService.createSplitFareConfirmationNotification(drivingWithMultiplePassengers);
        Assertions.assertEquals(1, res.size());
    }

    @Test
    void shouldSendDrivingStartedNotificationShouldBeOk(){
        drivingWithPassenger.setId(1L);
        when(drivingService.getDrivingById(1L)).thenReturn(drivingWithPassenger);
        Notification notification = new Notification(1L, LocalDateTime.now(), NotificationType.ADDED_DRIVING, drivingWithPassenger, null);
        when(notificationRepository.save(any(Notification.class))).thenReturn(notification);
        DriverNotificationDTO dto = notificationService.sendDrivingStartedNotification(1L);
        Assertions.assertEquals(NotificationType.ADDED_DRIVING.toString(), dto.getType());
        Assertions.assertEquals(dto.getDrivingId(), drivingWithPassenger.getId());
    }

    @Test
    void shouldSendDrivingStartedNotificationShouldBeNullPointer(){
        drivingWithPassenger.setId(1L);
        when(drivingService.getDrivingById(1L)).thenReturn(null);
        Assertions.assertThrows(NullPointerException.class, () -> {
            notificationService.sendDrivingStartedNotification(1L);
        });
    }

    @Test
    void shouldMakeDriverNotFoundNotification(){
        Driving driving = mockData.createDrivingWithMultiplePassenger();
        driving.setId(1L);
        List<DetailedDrivingNotificationDTO> res = notificationService.makeDriverNotFoundNotification(driving);
        Assertions.assertEquals(driving.getPassengers().size(), res.size());
        for (DetailedDrivingNotificationDTO d : res)
            Assertions.assertEquals(NotificationType.REFUSED_DRIVING.toString(), d.getType());
    }

    @Test
    void shouldMakeDriverFoundNotification() {
        Driving driving = mockData.createDrivingWithMultiplePassenger();
        driving.setId(1L);
        List<DetailedDrivingNotificationDTO> res = notificationService.makeDriverFoundNotification(driving);
        Assertions.assertEquals(driving.getPassengers().size(), res.size());
        for (DetailedDrivingNotificationDTO d : res)
            Assertions.assertEquals(NotificationType.APPROVED_DRIVING.toString(), d.getType());
    }

    @Test
    void shouldBeOkCreateDriverArrivedNotificationForDriving(){
        driver = mockData.createDrivingDriver1();
        drivingWithMultiplePassengers = driver.getDrivingHistory().get(0);
        drivingWithMultiplePassengers.setDriver(driver);

        System.out.println("putnici : " + drivingWithMultiplePassengers.getPassengers().size());

        Mockito.when(drivingService.getDrivingById(drivingWithMultiplePassengers.getId())).thenReturn(drivingWithMultiplePassengers);

        List<NewNotificationDTO> result = this.notificationService.createDriverArrivedNotificationForDriving(drivingWithMultiplePassengers.getId());
        for(NewNotificationDTO n : result){
            System.out.println(n.getClient() + " " + n.getDrivingId() + " "+ n.getType()) ;
        }
        assertEquals(2, result.size());
        verify(drivingService, times(1)).getDrivingById(drivingWithMultiplePassengers.getId());
    }

    @Test
    void shouldBeNotifyFutureDrivingFor15Min16(){
        Driving driving = drivingWithPassenger;//mockData.createDriving();
        driving.setNotificationSent(0);
        driving.setId(1L);
        driving.getDrivingTime().setStartDateTime(LocalDateTime.now().plusMinutes(16));
        when(drivingService.getFutureDrivingFromPeriod(anyBoolean(), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(List.of(driving));
        when(notificationRepository.save(any(Notification.class))).thenReturn(new Notification());

        notificationService.notifyUserForFutureDriving();
        Assertions.assertEquals(1, driving.getNotificationSent());
        verify(template, times(driving.getPassengers().size())).convertAndSend(anyString(), any(DrivingReminderNotification.class));

    }

    @Test
    void shouldBeNotifyFutureDrivingFor15Min14(){
        Driving driving = drivingWithPassenger;//mockData.createDriving();
        driving.setNotificationSent(0);
        driving.setId(1L);
        driving.getDrivingTime().setStartDateTime(LocalDateTime.now().plusMinutes(14));
        when(drivingService.getFutureDrivingFromPeriod(anyBoolean(), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(List.of(driving));
        when(notificationRepository.save(any(Notification.class))).thenReturn(new Notification());

        notificationService.notifyUserForFutureDriving();
        Assertions.assertEquals(1, driving.getNotificationSent());
        verify(template, times(driving.getPassengers().size())).convertAndSend(anyString(), any(DrivingReminderNotification.class));
    }

    @Test
    void shouldBeNotNotifyFutureDrivingFor15Min18(){
        Driving driving = drivingWithPassenger;//mockData.createDriving();
        driving.setNotificationSent(0);
        driving.setId(1L);
        driving.getDrivingTime().setStartDateTime(LocalDateTime.now().plusMinutes(18));
        when(drivingService.getFutureDrivingFromPeriod(anyBoolean(), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(List.of(driving));

        notificationService.notifyUserForFutureDriving();
        Assertions.assertEquals(0, driving.getNotificationSent());
        verify(template, never()).convertAndSend(anyString(), any(DrivingReminderNotification.class));

    }

    @Test
    void shouldNoNotifyFutureDrivingFor15Min12(){
        Driving driving = drivingWithPassenger;//mockData.createDriving();
        driving.setNotificationSent(0);
        driving.setId(1L);
        driving.getDrivingTime().setStartDateTime(LocalDateTime.now().plusMinutes(12));
        when(drivingService.getFutureDrivingFromPeriod(anyBoolean(), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(List.of(driving));

        notificationService.notifyUserForFutureDriving();
        Assertions.assertEquals(0, driving.getNotificationSent());
        verify(template, never()).convertAndSend(anyString(), any(DrivingReminderNotification.class));
    }

    @Test
    void shouldBeNotNotifyFutureDrivingFor15MinAlreadyNotified(){
        Driving driving = drivingWithPassenger;//mockData.createDriving();
        driving.setNotificationSent(1);
        driving.setId(1L);
        driving.getDrivingTime().setStartDateTime(LocalDateTime.now().plusMinutes(15));
        when(drivingService.getFutureDrivingFromPeriod(anyBoolean(), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(List.of(driving));

        notificationService.notifyUserForFutureDriving();
        Assertions.assertEquals(1, driving.getNotificationSent());
        verify(template, never()).convertAndSend(anyString(), any(DrivingReminderNotification.class));
    }


    @Test
    void shouldBeNotifyFutureDrivingFor10Min11(){
        Driving driving = drivingWithPassenger;//mockData.createDriving();
        driving.setNotificationSent(1);
        driving.setId(1L);
        driving.getDrivingTime().setStartDateTime(LocalDateTime.now().plusMinutes(11));
        when(drivingService.getFutureDrivingFromPeriod(anyBoolean(), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(List.of(driving));
        when(notificationRepository.save(any(Notification.class))).thenReturn(new Notification());

        notificationService.notifyUserForFutureDriving();
        Assertions.assertEquals(2, driving.getNotificationSent());
        verify(template, times(driving.getPassengers().size())).convertAndSend(anyString(), any(DrivingReminderNotification.class));

    }

    @Test
    void shouldBeNotifyFutureDrivingFor10Min9(){
        Driving driving = drivingWithPassenger;//mockData.createDriving();
        driving.setNotificationSent(1);
        driving.setId(1L);
        driving.getDrivingTime().setStartDateTime(LocalDateTime.now().plusMinutes(9));
        when(drivingService.getFutureDrivingFromPeriod(anyBoolean(), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(List.of(driving));
        when(notificationRepository.save(any(Notification.class))).thenReturn(new Notification());

        notificationService.notifyUserForFutureDriving();
        Assertions.assertEquals(2, driving.getNotificationSent());
        verify(template, times(driving.getPassengers().size())).convertAndSend(anyString(), any(DrivingReminderNotification.class));
    }

    @Test
    void shouldBeNotNotifyFutureDrivingFor10in13(){
        Driving driving = drivingWithPassenger;//mockData.createDriving();
        driving.setNotificationSent(1);
        driving.setId(1L);
        driving.getDrivingTime().setStartDateTime(LocalDateTime.now().plusMinutes(13));
        when(drivingService.getFutureDrivingFromPeriod(anyBoolean(), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(List.of(driving));

        notificationService.notifyUserForFutureDriving();
        Assertions.assertEquals(1, driving.getNotificationSent());
        verify(template, never()).convertAndSend(anyString(), any(DrivingReminderNotification.class));

    }

    @Test
    void shouldNoNotifyFutureDrivingFor10Min7(){
        Driving driving = drivingWithPassenger;//mockData.createDriving();
        driving.setNotificationSent(0);
        driving.setId(1L);
        driving.getDrivingTime().setStartDateTime(LocalDateTime.now().plusMinutes(7));
        when(drivingService.getFutureDrivingFromPeriod(anyBoolean(), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(List.of(driving));

        notificationService.notifyUserForFutureDriving();
        Assertions.assertEquals(0, driving.getNotificationSent());
        verify(template, never()).convertAndSend(anyString(), any(DrivingReminderNotification.class));
    }

    @Test
    void shouldBeNotNotifyFutureDrivingFor10MinAlreadyNotified(){
        Driving driving = drivingWithPassenger;//mockData.createDriving();
        driving.setNotificationSent(2);
        driving.setId(1L);
        driving.getDrivingTime().setStartDateTime(LocalDateTime.now().plusMinutes(10));
        when(drivingService.getFutureDrivingFromPeriod(anyBoolean(), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(List.of(driving));

        notificationService.notifyUserForFutureDriving();
        Assertions.assertEquals(2, driving.getNotificationSent());
        verify(template, never()).convertAndSend(anyString(), any(DrivingReminderNotification.class));
    }

    @Test
    void shouldBeNotifyFutureDrivingFor5Min4(){
        Driving driving = drivingWithPassenger;//mockData.createDriving();
        driving.setNotificationSent(2);
        driving.setId(1L);
        driving.getDrivingTime().setStartDateTime(LocalDateTime.now().plusMinutes(4));
        when(drivingService.getFutureDrivingFromPeriod(anyBoolean(), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(List.of(driving));
        when(notificationRepository.save(any(Notification.class))).thenReturn(new Notification());

        notificationService.notifyUserForFutureDriving();
        Assertions.assertEquals(3, driving.getNotificationSent());
        verify(template, times(driving.getPassengers().size())).convertAndSend(anyString(), any(DrivingReminderNotification.class));
    }

    @Test
    void shouldBeNotNotifyFutureDrivingFor5Min6(){
        Driving driving = drivingWithPassenger;//mockData.createDriving();
        driving.setNotificationSent(2);
        driving.setId(1L);
        driving.getDrivingTime().setStartDateTime(LocalDateTime.now().plusMinutes(6));
        when(drivingService.getFutureDrivingFromPeriod(anyBoolean(), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(List.of(driving));

        notificationService.notifyUserForFutureDriving();
        Assertions.assertEquals(2, driving.getNotificationSent());
        verify(template, never()).convertAndSend(anyString(), any(DrivingReminderNotification.class));

    }

    @Test
    void shouldBeNotNotifyFutureDrivingFor5MinAlreadyNotified(){
        Driving driving = drivingWithPassenger;//mockData.createDriving();
        driving.setNotificationSent(3);
        driving.setId(1L);
        driving.getDrivingTime().setStartDateTime(LocalDateTime.now().plusMinutes(5));
        when(drivingService.getFutureDrivingFromPeriod(anyBoolean(), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(List.of(driving));

        notificationService.notifyUserForFutureDriving();
        Assertions.assertEquals(3, driving.getNotificationSent());
        verify(template, never()).convertAndSend(anyString(), any(DrivingReminderNotification.class));
    }
}
