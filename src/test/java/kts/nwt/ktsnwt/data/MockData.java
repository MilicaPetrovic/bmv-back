package kts.nwt.ktsnwt.data;

import kts.nwt.ktsnwt.beans.driving.*;
import kts.nwt.ktsnwt.beans.location.Address;
import kts.nwt.ktsnwt.beans.location.Path;
import kts.nwt.ktsnwt.beans.location.Route;
import kts.nwt.ktsnwt.beans.users.Driver;
import kts.nwt.ktsnwt.beans.users.DriverState;
import kts.nwt.ktsnwt.beans.users.RegisteredUser;
import kts.nwt.ktsnwt.beans.users.Role;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class MockData {
    public final double DISTANCE = 10.31;
    public final double ROUTE_TIME = 10;
    public final double DRIVING_TIME = 7522;

    public final double PRICE = 700;

    public Clock fixedClock = Clock.fixed(Instant.now(), Clock.systemDefaultZone().getZone());
    ;
    public VehicleType vt = new VehicleType(1L, "karavan", 500);
    public Role registeredUserRole = new Role(2L, "REGISTERED_USER");
    public Role driverRole = new Role(3L, "DRIVER");


    public Path createPath() {
        Path path = new Path();

        Address a1 = new Address(45.2464547, 19.8342626, "Gogoljeva 22 Novi Sad", path);
        Address a2 = new Address(45.2537745, 19.8411572, "Jevrejska 10 Novi Sad", path);
        ArrayList<Address> chosenStations = new ArrayList<>(List.of(a1, a2));

        Address a3 = new Address(45.24638, 19.8342, "", path);
        Address a4 = new Address(45.24611, 19.8348, "", path);
        Address a5 = new Address(45.24591, 19.8354, "", path);
        Address a6 = new Address(45.24591, 19.8354, "", path);
        Route route = new Route();
        route.setRoute(List.of(a3, a4, a5, a6));
        route.setTime(ROUTE_TIME);
        route.setDistance(DISTANCE);

        path.setRoute(route);
        path.setStations(chosenStations);

        return path;
    }


    public Driving createDriving(ChosenAdditionalServices chosenAdditionalServices, Path path) {
        System.out.println("------------- create driving -------------");
        System.out.println(fixedClock);
        TimePeriod drivingTime = new TimePeriod(LocalDateTime.now(fixedClock), LocalDateTime.now(fixedClock).plusMinutes((long) DRIVING_TIME), true);
        return new Driving(fixedClock, PRICE, DrivingState.MADE, chosenAdditionalServices, path, drivingTime, false);
    }

    public Driving createDriving() {
        Path path = createPath();
        ChosenAdditionalServices chosenAdditionalServices = new ChosenAdditionalServices(1L, vt, false, false);
        TimePeriod drivingTime = new TimePeriod(LocalDateTime.now(fixedClock), LocalDateTime.now(fixedClock).plusMinutes((long) DRIVING_TIME/60), true);
        return new Driving(fixedClock, PRICE, DrivingState.MADE, chosenAdditionalServices, path, drivingTime, false);
    }

    public Driving createCurrentDriving() {
        Path path = createPath();
        ChosenAdditionalServices chosenAdditionalServices = new ChosenAdditionalServices(1L, vt, false, false);
        TimePeriod drivingTime = new TimePeriod(LocalDateTime.now(), LocalDateTime.now().plusMinutes((long) DRIVING_TIME / 60), true);
        return new Driving(fixedClock, PRICE, DrivingState.CURRENT, chosenAdditionalServices, path, drivingTime, false);
    }

    public Driving createDrivingWithPassenger() {
        Driving d = createDriving();
        RegisteredUser ru = new RegisteredUser("jake", 1000, registeredUserRole);
        DrivingPayment dp = new DrivingPayment(d.getPrice(), true, false, d, ru);
        d.addPassenger(dp);
        return d;
    }

    public Driving createDrivingWithMultiplePassenger() {
        Driving d = createDriving();
        RegisteredUser ru = new RegisteredUser("jake", 1000, registeredUserRole);
        DrivingPayment dp = new DrivingPayment(d.getPrice() / 2, true, false, d, ru);
        d.addPassenger(dp);
        RegisteredUser neytiri = new RegisteredUser("neytiri", 1000, registeredUserRole);
        DrivingPayment dp2 = new DrivingPayment(d.getPrice() / 2, false, false, d, neytiri);
        d.addPassenger(dp2);
        return d;
    }

    public Driver createDrivingDriver1() {
        Address current_address = new Address(45.2464547, 19.8342626, "");

        Vehicle vehicle = new Vehicle(1L, "Skoda", "NS 012 FT", vt, true,
                true, 3);

        Driver driver = new Driver(1L, "Petar", "Petrovic", false, DriverState.DRIVING,
                current_address, vehicle, driverRole);

        Driving driving = createPaidDriving();
        driver.getDrivingHistory().add(driving);

        return driver;
    }

    public Driver createDrivingDriver2(){
        Address current_address = new Address(45.2537745, 19.8411572,  "");
        Vehicle vehicle = new Vehicle(2L, "Reno", "NS 014 FF", vt, true,
                true, 3);

        Driver driver = new Driver(2L, "Aleksa", "Aleksic", false, DriverState.DRIVING,
                current_address, vehicle, driverRole);

        Driving driving = createCurrentDrivingWithPassengers();
        driver.getDrivingHistory().add(driving);
        return driver;
    }

    public Driver createActiveDriver(){
        Address current_address = new Address(45.24611, 19.8348,  "");
        Vehicle vehicle = new Vehicle(3L, "Fiat", "NS 015 DI", vt, true,
                true, 2);

        Driver driver = new Driver(2L, "Aleksa", "Aleksic", false, DriverState.ACTIVE,
                current_address, vehicle, driverRole);


        return driver;
    }

    public Driving createPaidDriving() {
        Driving d = createDriving();
        RegisteredUser ru = new RegisteredUser(30L,"mika", "mika", "jake", registeredUserRole, "+442342", false, "jake@asd.com");
        DrivingPayment dp = new DrivingPayment(d.getPrice() / 2, true, true, d, ru);
        d.addPassenger(dp);
        ru.setDrivingHistory(List.of(dp));
        RegisteredUser neytiri = new RegisteredUser(31L,"mika", "mika", "neytiri", registeredUserRole, "+442342", false, "jake@asd.com");
        DrivingPayment dp2 = new DrivingPayment(d.getPrice() / 2, true, true, d, neytiri);
        d.addPassenger(dp2);
        neytiri.setDrivingHistory(List.of(dp2));
        d.setState(DrivingState.PAID);
        d.setId(50L);
        return d;
    }

    public Driving createCurrentDrivingWithPassengers(){
        Driving d = createDriving();
        RegisteredUser joca = new RegisteredUser(32L,"joca", registeredUserRole);
        DrivingPayment dp = new DrivingPayment(d.getPrice() / 2, true, true, d, joca);
        d.addPassenger(dp);
        joca.setDrivingHistory(List.of(dp));
        RegisteredUser neca = new RegisteredUser(33L,"neca", registeredUserRole);
        DrivingPayment dp2 = new DrivingPayment(d.getPrice() / 2, true, true, d, neca);
        d.addPassenger(dp2);
        neca.setDrivingHistory(List.of(dp2));
        d.setState(DrivingState.CURRENT);
        d.setId(51L);
        return d;
    }

    public VehicleType createVehicle() {
        return new VehicleType(1L, "karavan", 500);
    }

    public ChosenAdditionalServices createChosenAdditionalServices(VehicleType vt) {
        return new ChosenAdditionalServices(1L, vt, false, false);
    }

    public RegisteredUser createRegisteredJake() {
        return new RegisteredUser("jake", 1000, registeredUserRole);
    }

    public RegisteredUser createRegisteredNeytiri() {
        return new RegisteredUser("neytiri", 1000, registeredUserRole);
    }

    public Driver createDriverWithEveryFriendly() {
        //VehicleType vt = new VehicleType(1L, "karavan", 150);
        Vehicle v = new Vehicle(1L, "Passat", vt, "hupi lila", 2000, "NS 001 AA", true, true, 5);
        return new Driver("wille", v);
    }

    public Driver createDriverWithNothingFriendly() {
        //VehicleType vt = new VehicleType(1L, "karavan", 150);
        Vehicle v = new Vehicle(1L, "Passat", vt, "hupi lila", 2000, "NS 001 AA", false, false, 5);
        return new Driver("wille", v);
    }

    public ChosenAdditionalServices createChosenAdditionalServicesBabys(VehicleType vt) {
        return new ChosenAdditionalServices(1L, vt, false, true);
    }


    public ChosenAdditionalServices createChosenAdditionalServicesPets(VehicleType vt) {
        return new ChosenAdditionalServices(1L, vt, true, false);
    }

    public Driving createAcceptedDriving(){
        Driving driving = createDriving();
        RegisteredUser mika = new RegisteredUser(34L,"mika", "mika", "mika", registeredUserRole, "+442342", false, "jake@asd.com");
        DrivingPayment dp = new DrivingPayment(driving.getPrice() / 2, true, false, driving, mika);
        driving.addPassenger(dp);
        driving.setState(DrivingState.ACCEPTED);
        driving.setId(52L);
        return driving;
    }

    public RegisteredUser createRegisteredUser(){
        RegisteredUser arsa = new RegisteredUser(35L,"arsa", registeredUserRole);
        return arsa;
    }
}
