package kts.nwt.ktsnwt.controller;

import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.dto.driving.MakeDrivingDTO;
import kts.nwt.ktsnwt.dto.notification.DetailedDrivingNotificationDTO;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@AutoConfigureWebMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
public class NotificationControllerTest extends LoginLogoutHelper {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldConfirmSplitFare() {
        HttpHeaders headers = login("peralta", "123", restTemplate);
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(new MakeDrivingDTO(), headers);
        ResponseEntity<Object> responseEntity = restTemplate.exchange(
                "/notification//split/fare/confirm/" + 7,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString,StatusResponseDTO.class);
        System.out.println(res);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(res.isSuccessful());
    }

    @Test
    public void shouldConfirmSplitFareFallOnNotAPassenger() {
        HttpHeaders headers = login("rosa", "123", restTemplate);
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(new MakeDrivingDTO(), headers);
        ResponseEntity<Object> responseEntity = restTemplate.exchange(
                "/notification//split/fare/confirm/" + 6,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString,StatusResponseDTO.class);
        System.out.println(res);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(res.isSuccessful());
        assertEquals(7, res.getExceptionCode());
    }

    @Test
    public void shouldGetSplitFareNotification() {
        HttpHeaders headers = login("peralta", "123", restTemplate);
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(new MakeDrivingDTO(), headers);
        ResponseEntity<DetailedDrivingNotificationDTO> responseEntity = restTemplate.exchange(
                "/notification/split/fare/" + 1,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<DetailedDrivingNotificationDTO>() {
                });
        DetailedDrivingNotificationDTO res = responseEntity.getBody();
        System.out.println(res);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("peralta", res.getUsername());
    }

    @Test
    public void shouldGetNotFoundOnGetSplitFareNotification() {
        HttpHeaders headers = login("peralta", "123", restTemplate);
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(new MakeDrivingDTO(), headers);
        ResponseEntity<Object> responseEntity = restTemplate.exchange(
                "/notification/split/fare/" + 100,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

}
