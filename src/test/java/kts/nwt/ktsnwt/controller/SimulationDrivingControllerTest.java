package kts.nwt.ktsnwt.controller;

import kts.nwt.ktsnwt.dto.driving.DrivingStateDTO;
import kts.nwt.ktsnwt.dto.location.SimulationDTO;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;

import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@AutoConfigureWebMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)


public class SimulationDrivingControllerTest extends HeaderHelper {

    @LocalServerPort
    int randomServerPort;

    @Autowired
    private TestRestTemplate myRestTemplate;

    @Test
    public void shouldBeOkGetCurrentPaidDrivingForDriver() {

        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/simulation/driver/14",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        SimulationDTO res = gson.fromJson(jsonString, SimulationDTO.class);

        System.out.println(res);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(11, res.getId());
        assertEquals(6, res.getDrivingState());  // paid
        assertEquals(2, res.getStations().size());
        assertEquals(7, res.getCoordinates().size());
        assertEquals(3, res.getSimulationVehicleDTO().getId());
        assertEquals("NS 016 AA", res.getSimulationVehicleDTO().getLabel());
    }


    @Test
    public void shouldBeNullDriverGetCurrentPaidDrivingForDriver() {
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/simulation/driver/100",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        SimulationDTO res = gson.fromJson(jsonString, SimulationDTO.class);
        assertEquals(null, res);
        System.out.println(res);
    }


    @Test
    public void shouldBeNullDrivingGetCurrentPaidDrivingForDriver() {
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/simulation/driver/16",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        SimulationDTO res = gson.fromJson(jsonString, SimulationDTO.class);
        assertEquals(null, res);
        System.out.println(res);

    }

    @Test
    public void shouldBePaidStateGetStateForDriving() {
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/simulation/state/11",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        SimulationDTO res = gson.fromJson(jsonString, SimulationDTO.class);
        assertEquals(6, res.getDrivingState());
        assertEquals(14, res.getSimulationVehicleDTO().getDriverId());
        System.out.println(res);
    }

    @Test
    public void shouldBeCurrentStateGetStateForDriving() {
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/simulation/state/12",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        SimulationDTO res = gson.fromJson(jsonString, SimulationDTO.class);
        assertEquals(2, res.getDrivingState());
        assertEquals(17, res.getSimulationVehicleDTO().getDriverId());
    }

    @Test
    public void shouldBeNullGetStateForDriving() {
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/simulation/state/1000",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        SimulationDTO res = gson.fromJson(jsonString, SimulationDTO.class);
        assertEquals(null, res);
    }


    @Test
    public void startGetStateOfLastDriving() {
        headers = login("nn@gmail.com", "123", myRestTemplate);
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/simulation/state/last/driving",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        DrivingStateDTO res = gson.fromJson(jsonString, DrivingStateDTO.class);
        assertEquals(true, res.isStart());
        assertEquals(false, res.isFinish());
        assertEquals(11, res.getDrivingId());
    }

    @Test
    public void finishGetStateOfLastDriving() {
        headers = login("dj@gmail.com", "123", myRestTemplate);
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/simulation/state/last/driving",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        DrivingStateDTO res = gson.fromJson(jsonString, DrivingStateDTO.class);
        assertEquals(false, res.isStart());
        assertEquals(true, res.isFinish());
        assertEquals(12, res.getDrivingId());
    }

    @Test
    @Order(100)
    public void noDrivingGetStateOfLastDriving() {
        headers = login("zz@gmail.com", "123", myRestTemplate);
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/simulation/state/last/driving",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        DrivingStateDTO res = gson.fromJson(jsonString, DrivingStateDTO.class);
        assertEquals(false, res.isStart());
        assertEquals(false, res.isFinish());
        assertEquals(-1, res.getDrivingId());
    }


    @Test
    public void noStartNoFinishDrivingGetStateOfLastDriving() {
        headers = login("dusan@gmail.com", "123", myRestTemplate);
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/simulation/state/last/driving",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        DrivingStateDTO res = gson.fromJson(jsonString, DrivingStateDTO.class);
        assertEquals(false, res.isStart());
        assertEquals(false, res.isFinish());
        assertEquals(13, res.getDrivingId());
    }


    @Test
    public void drawForDriver() {
        headers = login("nn@gmail.com", "123", myRestTemplate);
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/simulation/draw",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        SimulationDTO res = gson.fromJson(jsonString, SimulationDTO.class);

        System.out.println(res);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(11, res.getId());
        assertEquals(6, res.getDrivingState());  // paid
        assertEquals(2, res.getStations().size());
        assertEquals(7, res.getCoordinates().size());
        assertEquals(3, res.getSimulationVehicleDTO().getId());
        assertEquals("NS 016 AA", res.getSimulationVehicleDTO().getLabel());
    }

    @Test
    @Order(1)
    public void drawDriverNoDriving(){
        headers = login("zz@gmail.com", "123", myRestTemplate);
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/simulation/draw",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        SimulationDTO res = gson.fromJson(jsonString, SimulationDTO.class);
        System.out.println(res);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(null, res);
    }

    @Test
    public void drawForRegUser() {
        headers = login("andjela@gmail.com", "123", myRestTemplate);
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/simulation/draw",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        SimulationDTO res = gson.fromJson(jsonString, SimulationDTO.class);

        System.out.println(res);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(11, res.getId());
        assertEquals(6, res.getDrivingState());  // paid
        assertEquals(2, res.getStations().size());
        assertEquals(7, res.getCoordinates().size());
        assertEquals(3, res.getSimulationVehicleDTO().getId());
        assertEquals("NS 016 AA", res.getSimulationVehicleDTO().getLabel());
    }

    @Test
    public void drawForRegUserNoDriving() {
        headers = login("lana@gmail.com", "123", myRestTemplate);
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/simulation/draw",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        SimulationDTO res = gson.fromJson(jsonString, SimulationDTO.class);

        System.out.println(res);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(null, res);
    }

}
