package kts.nwt.ktsnwt.controller;

import kts.nwt.ktsnwt.dto.driving.VehicleTypeDTO;
import kts.nwt.ktsnwt.dto.location.LatLngDTO;
import kts.nwt.ktsnwt.dto.location.SimulationVehicleDTO;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@AutoConfigureWebMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)

public class VehicleTypeControllerTest extends HeaderHelper {
    @LocalServerPort
    int randomServerPort;

    @Autowired
    private TestRestTemplate myRestTemplate;


    @Test
    public void getAllVehicleTypes() {
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/vehicle/type/all",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), List.class);
        System.out.println(jsonString);
        List<VehicleTypeDTO> res = gson.fromJson(jsonString, ArrayList.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(2, res.size());
    }

    @Test
    public void updateVehicleLocationOfDriver() {
        LatLngDTO latLngDTO = new LatLngDTO(10, 10);
        HttpEntity<LatLngDTO> entity = new HttpEntity<>(latLngDTO, headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/vehicle/type/14/11",
                HttpMethod.PUT,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        SimulationVehicleDTO res = gson.fromJson(jsonString, SimulationVehicleDTO.class);
        assertEquals(10, res.getLatitude());
        assertEquals(10, res.getLongitude());
        assertEquals(14, res.getDriverId());
        assertEquals("NS 016 AA", res.getLabel());
        System.out.println(res);
    }

    @Test
    public void getAllVehicles() {
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/vehicle/type/vehicle/all",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), List.class);
        List<SimulationVehicleDTO> res = gson.fromJson(jsonString, ArrayList.class);
        assertEquals(6, res.size());
    }
}
