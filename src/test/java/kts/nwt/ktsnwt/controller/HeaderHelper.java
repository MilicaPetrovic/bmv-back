package kts.nwt.ktsnwt.controller;

import com.google.gson.Gson;
import kts.nwt.ktsnwt.dto.UserTokenState;
import kts.nwt.ktsnwt.dto.security.JwtAuthenticationRequest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HeaderHelper {

    protected HttpHeaders headers;
    protected Gson gson;
    protected WebSocketStompClient webSocketStompClient;

    @BeforeAll
    public void initializeGson(){
        gson = new Gson();
        headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Accept", "application/json");
    }

    @BeforeEach
    void setup() {
        this.webSocketStompClient = new WebSocketStompClient(new SockJsClient(
                List.of(new WebSocketTransport(new StandardWebSocketClient()))));
    }

    HttpHeaders login(String username, String password, TestRestTemplate restTemplate) {
        JwtAuthenticationRequest login = new JwtAuthenticationRequest(username, password);
        HttpEntity<JwtAuthenticationRequest> entity = new HttpEntity(login);
        ResponseEntity<UserTokenState> responseEntity = restTemplate.exchange(
                "/auth/login",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<UserTokenState>() {
                });

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + responseEntity.getBody().getAccessToken());
        headers.set("Content-Type", "application/json");
        headers.set("Accept", "application/json");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        return headers;
    }
}
