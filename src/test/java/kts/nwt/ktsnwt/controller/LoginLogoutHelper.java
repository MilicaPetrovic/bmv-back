package kts.nwt.ktsnwt.controller;

import com.google.gson.Gson;
import kts.nwt.ktsnwt.dto.UserTokenState;
import kts.nwt.ktsnwt.dto.security.JwtAuthenticationRequest;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LoginLogoutHelper {

    protected HttpHeaders headers;

    protected Gson gson;

    @BeforeAll
    void initializeGson(){
        gson = new Gson();
    }

    HttpHeaders login(String username, String password, TestRestTemplate restTemplate){
        JwtAuthenticationRequest login = new JwtAuthenticationRequest(username, password);
        HttpEntity<JwtAuthenticationRequest> entity = new HttpEntity(login);
        ResponseEntity<UserTokenState> responseEntity = restTemplate.exchange(
                "/auth/login",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<UserTokenState>() {
                });

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + responseEntity.getBody().getAccessToken());
        headers.set("Content-Type", "application/json");
        headers.set("Accept", "application/json");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        return headers;
    }

    //@AfterEach
    void logout(TestRestTemplate restTemplate){
        //JwtAuthenticationRequest login = new JwtAuthenticationRequest("my@email.rs", "123");
        HttpEntity entity = new HttpEntity(headers);
        ResponseEntity<UserTokenState> responseEntity = restTemplate.exchange("/auth/logout",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<UserTokenState>() {
                });

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

}
