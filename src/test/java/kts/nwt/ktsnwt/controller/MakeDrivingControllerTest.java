package kts.nwt.ktsnwt.controller;

import com.google.gson.Gson;
import kts.nwt.ktsnwt.dto.StatusResponseDTO;
import kts.nwt.ktsnwt.dto.UserTokenState;
import kts.nwt.ktsnwt.dto.driving.MakeDrivingDTO;
import kts.nwt.ktsnwt.dto.driving.PurchaseDrivingDTO;
import kts.nwt.ktsnwt.dto.driving.RejectDrivingDTO;
import kts.nwt.ktsnwt.dto.location.MapPointDTO;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@AutoConfigureWebMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
public class MakeDrivingControllerTest extends LoginLogoutHelper {

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    int randomServerPort;

    @Autowired
    private TestRestTemplate myRestTemplate;

    private ArrayList<MapPointDTO> coordinates;
    private ArrayList<MapPointDTO> chosenStations;

    @BeforeAll
    public void createChosenStations() {
        chosenStations = new ArrayList<>();
        chosenStations.add(new MapPointDTO(45.2464547, 19.8342626, "Gogoljeva 22 Novi Sad"));
        chosenStations.add(new MapPointDTO(45.2537745, 19.8411572, "Jevrejska 10 Novi Sad"));
    }

    @BeforeAll
    public void createCoordinates() {
        coordinates = new ArrayList<>();
        coordinates.add(new MapPointDTO(45.24638, 19.8342));
        coordinates.add(new MapPointDTO(45.24611, 19.8348));
        coordinates.add(new MapPointDTO(45.24591, 19.8354));
        coordinates.add(new MapPointDTO(45.24591, 19.8354));
    }

    @Test
    public void shouldSaveFallOnVehicleType() {
        HttpHeaders headers = login("leti", "123", restTemplate);
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(new MakeDrivingDTO(), headers);
        ResponseEntity<Object> responseEntity = restTemplate.exchange("/make/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertEquals(null, responseEntity.getBody());
    }

    @Test
    public void shouldSaveFallOnChosenStations() {
        HttpHeaders headers = login("leti", "123", restTemplate);
        MakeDrivingDTO makeDrivingDTO = new MakeDrivingDTO();
        makeDrivingDTO.setVehicleTypeId(1L);
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(makeDrivingDTO, headers);

        ResponseEntity<Object> responseEntity = restTemplate.exchange("/make/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        System.out.println(responseEntity);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(null, responseEntity.getBody());
    }

    @Test
    public void shouldSaveFallOnChosenStationNoPath() {
        HttpHeaders headers = login("leti", "123", restTemplate);
        ;
        MakeDrivingDTO makeDrivingDTO = new MakeDrivingDTO();
        makeDrivingDTO.setVehicleTypeId(1L);
        makeDrivingDTO.setStations(new ArrayList<>());
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(makeDrivingDTO, headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/make/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(res.isSuccessful());
        assertEquals(8, res.getExceptionCode());
    }

    @Test
    public void shouldSaveFallOnCoordinatesNullPointer() {
        HttpHeaders headers = login("leti", "123", restTemplate);
        ;
        MakeDrivingDTO makeDrivingDTO = new MakeDrivingDTO();
        makeDrivingDTO.setVehicleTypeId(1L);
        makeDrivingDTO.setStations(chosenStations);
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(makeDrivingDTO, headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/make/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(null, responseEntity.getBody());
    }

    @Test
    public void shouldSaveFallOnCoordinatesNoPath() {
        HttpHeaders headers = login("leti", "123", restTemplate);
        ;
        MakeDrivingDTO makeDrivingDTO = new MakeDrivingDTO();
        makeDrivingDTO.setVehicleTypeId(1L);
        makeDrivingDTO.setStations(chosenStations);
        makeDrivingDTO.setCoordinates(new ArrayList<>());
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(makeDrivingDTO, headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/make/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(res.isSuccessful());
        assertEquals(8, res.getExceptionCode());
    }

    @Test
    public void shouldFallOnNotAvailableDriver() {
        HttpHeaders headers = login("leti", "123", restTemplate);
        ;
        MakeDrivingDTO makeDrivingDTO = new MakeDrivingDTO();
        makeDrivingDTO.setVehicleTypeId(2L);
        makeDrivingDTO.setStations(chosenStations);
        makeDrivingDTO.setCoordinates(coordinates);
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(makeDrivingDTO, headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/make/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);

        System.out.println(res.toString());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(res.isSuccessful());
        assertEquals(17, res.getExceptionCode());
    }

    @Test
    public void shouldFallOnNotEnoughToken() {
        HttpHeaders headers = login("leti", "123", restTemplate);
        ;
        MakeDrivingDTO makeDrivingDTO = new MakeDrivingDTO();
        makeDrivingDTO.setVehicleTypeId(1L);
        makeDrivingDTO.setStations(chosenStations);
        makeDrivingDTO.setCoordinates(coordinates);
        makeDrivingDTO.setPrice(1000);
        makeDrivingDTO.setLinkedUsers(List.of("milly"));
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(makeDrivingDTO, headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/make/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);

        System.out.println(res.toString());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(res.isSuccessful());
        assertEquals(15, res.getExceptionCode());
    }

    @Test
    public void shouldSave() {
        HttpHeaders headers = login("mm", "123", restTemplate);
        ;
        MakeDrivingDTO makeDrivingDTO = new MakeDrivingDTO();
        makeDrivingDTO.setVehicleTypeId(1L);
        makeDrivingDTO.setStations(chosenStations);
        makeDrivingDTO.setCoordinates(coordinates);
        makeDrivingDTO.setLinkedUsers(List.of("cappucino"));
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(makeDrivingDTO, headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/make/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);

        System.out.println(res.toString());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(res.isSuccessful());
    }

    @Test
    public void shouldFallOnCanNotOrder() {
        HttpHeaders headers = login("leti", "123", restTemplate);
        ;
        MakeDrivingDTO makeDrivingDTO = new MakeDrivingDTO();
        makeDrivingDTO.setVehicleTypeId(1L);
        makeDrivingDTO.setStations(chosenStations);
        makeDrivingDTO.setCoordinates(coordinates);
        makeDrivingDTO.setPrice(1000);
        makeDrivingDTO.setLinkedUsers(List.of("ted"));
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(makeDrivingDTO, headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/make/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);

        System.out.println(res.toString());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(res.isSuccessful());
        assertEquals(14, res.getExceptionCode());
    }

    @Test
    public void shouldFallOnNoUserFound() {
        HttpHeaders headers = login("leti", "123", restTemplate);
        ;
        MakeDrivingDTO makeDrivingDTO = new MakeDrivingDTO();
        makeDrivingDTO.setVehicleTypeId(1L);
        makeDrivingDTO.setStations(chosenStations);
        makeDrivingDTO.setCoordinates(coordinates);
        makeDrivingDTO.setPrice(1000);
        makeDrivingDTO.setLinkedUsers(List.of("muki"));
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(makeDrivingDTO, headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/make/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);

        System.out.println(res.toString());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(res.isSuccessful());
        assertEquals(1, res.getExceptionCode());
    }

    @Test
    public void shouldFallOnRegisteredUserExpectedException() {
        HttpHeaders headers = login("leti", "123", restTemplate);
        MakeDrivingDTO makeDrivingDTO = new MakeDrivingDTO();
        makeDrivingDTO.setVehicleTypeId(1L);
        makeDrivingDTO.setStations(chosenStations);
        makeDrivingDTO.setCoordinates(coordinates);
        makeDrivingDTO.setPrice(1000);
        makeDrivingDTO.setLinkedUsers(List.of("philip"));
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(makeDrivingDTO, headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/make/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);

        System.out.println(res.toString());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(res.isSuccessful());
        assertEquals(2, res.getExceptionCode());
    }

    @Test
    public void shouldFallOnInvalidFutureTime() {
        HttpHeaders headers = login("leti", "123", restTemplate);
        ;
        MakeDrivingDTO makeDrivingDTO = new MakeDrivingDTO();
        makeDrivingDTO.setVehicleTypeId(1L);
        makeDrivingDTO.setStations(chosenStations);
        makeDrivingDTO.setCoordinates(coordinates);
        makeDrivingDTO.setPrice(1000);
        makeDrivingDTO.setTime(LocalDateTime.now().plusHours(20).getHour());
        makeDrivingDTO.setMinute(0);
        makeDrivingDTO.setFuture(true);
        makeDrivingDTO.setLinkedUsers(List.of("philip"));
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(makeDrivingDTO, headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/make/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);

        System.out.println(res.toString());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(res.isSuccessful());
        assertEquals(20, res.getExceptionCode());
    }

    @Test
    public void shouldSaveFutureTime() {
        HttpHeaders headers = login("mokka", "123", restTemplate);
        ;
        MakeDrivingDTO makeDrivingDTO = new MakeDrivingDTO();
        makeDrivingDTO.setVehicleTypeId(1L);
        makeDrivingDTO.setStations(chosenStations);
        makeDrivingDTO.setCoordinates(coordinates);
        makeDrivingDTO.setPrice(1000);
        makeDrivingDTO.setHour(LocalDateTime.now().plusHours(2).getHour());
        //System.out.println(makeDrivingDTO.getTime());
        makeDrivingDTO.setMinute(0);
        makeDrivingDTO.setFuture(true);
        HttpEntity<MakeDrivingDTO> entity = new HttpEntity<>(makeDrivingDTO, headers);
        ResponseEntity<Object> responseEntity = myRestTemplate.exchange(
                "/make/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);

        System.out.println(res.toString());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(res.isSuccessful());
    }

    @Test
    public void shouldPurchaseFallOnNoDrivingId() {
        HttpHeaders headers = login("terry", "123", restTemplate);
        HttpEntity<PurchaseDrivingDTO> entity = new HttpEntity<>(new PurchaseDrivingDTO(), headers);
        ResponseEntity<Object> responseEntity = restTemplate.exchange("/purchase/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertFalse(res.isSuccessful());
    }

    @Test
    public void shouldPurchase() {
        HttpHeaders headers = login("terry", "123", restTemplate);
        PurchaseDrivingDTO p = new PurchaseDrivingDTO();
        p.setDrivingId(5L);
        HttpEntity<PurchaseDrivingDTO> entity = new HttpEntity<>(p, headers);
        ResponseEntity<Object> responseEntity = restTemplate.exchange("/purchase/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(res.isSuccessful());
    }

    @Test
    public void shouldFuturePurchase() {
        HttpHeaders headers = login("terry", "123", restTemplate);
        PurchaseDrivingDTO p = new PurchaseDrivingDTO();
        p.setDrivingId(8L);
        HttpEntity<PurchaseDrivingDTO> entity = new HttpEntity<>(p, headers);
        ResponseEntity<Object> responseEntity = restTemplate.exchange("/purchase/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(res.isSuccessful());
    }

    @Test
    public void shouldFallOnNotEverybodyConfirmed() {
        HttpHeaders headers = login("rosa", "123", restTemplate);
        PurchaseDrivingDTO p = new PurchaseDrivingDTO();
        p.setDrivingId(4L);
        HttpEntity<PurchaseDrivingDTO> entity = new HttpEntity<>(p, headers);
        ResponseEntity<Object> responseEntity = restTemplate.exchange("/purchase/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(res.isSuccessful());
        assertEquals(16, res.getExceptionCode());
    }

    @Test
    public void shouldFallOnNoAvailableDriver() {
        HttpHeaders headers = login("rosa", "123", restTemplate);
        PurchaseDrivingDTO p = new PurchaseDrivingDTO();
        p.setDrivingId(3L);
        HttpEntity<PurchaseDrivingDTO> entity = new HttpEntity<>(p, headers);
        ResponseEntity<Object> responseEntity = restTemplate.exchange("/purchase/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(res.isSuccessful());
        assertEquals(17, res.getExceptionCode());
    }

    @Test
    public void shouldPurchaseFallOnNotEnoughToken() {
        HttpHeaders headers = login("milly", "123", restTemplate);
        PurchaseDrivingDTO p = new PurchaseDrivingDTO();
        p.setDrivingId(6L);
        HttpEntity<PurchaseDrivingDTO> entity = new HttpEntity<>(p, headers);
        ResponseEntity<Object> responseEntity = restTemplate.exchange("/purchase/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Object>() {
                });
        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(res.isSuccessful());
        assertEquals(15, res.getExceptionCode());
    }

    @Test
    public void shouldGetErrorRejectDriving_incorrectDrivingID() {
        HttpHeaders headers = login("gina@gmail.com", "123", restTemplate);
        RejectDrivingDTO dto = new RejectDrivingDTO(11111L, "Ne zelim da vozim");
        HttpEntity<RejectDrivingDTO> entity = new HttpEntity<>(dto, headers);

        ResponseEntity<Object> responseEntity = restTemplate.exchange("/reject/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<>() {
                });

        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertNotNull(res);
        assertFalse(res.isSuccessful());
        assertEquals(-100, res.getExceptionCode());
    }

    @Test
    public void shouldRejectDriving() {
        HttpHeaders headers = login("gina@gmail.com", "123", restTemplate);
        RejectDrivingDTO dto = new RejectDrivingDTO(10L, "Ne zelim da vozim");
        HttpEntity<RejectDrivingDTO> entity = new HttpEntity<>(dto, headers);

        ResponseEntity<Object> responseEntity = restTemplate.exchange("/reject/driving",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<>() {
                });

        String jsonString = gson.toJson(responseEntity.getBody(), Map.class);
        StatusResponseDTO res = gson.fromJson(jsonString, StatusResponseDTO.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(res);
        assertFalse(res.isSuccessful());
    }

}
