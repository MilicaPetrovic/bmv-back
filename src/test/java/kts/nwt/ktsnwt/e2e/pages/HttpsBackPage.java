package kts.nwt.ktsnwt.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HttpsBackPage {
    private WebDriver driver;

    private String PAGE_URL = "https://localhost:8080/uber/h2";
    @FindBy(how = How.XPATH, using = "//button[contains(text(), 'Advanced')]")
    WebElement advancedButton;

    @FindBy(how = How.XPATH, using = "//a[contains(text(), 'Proceed to localhost (unsafe)')]")
    WebElement link;


    @FindBy(how = How.XPATH, using = "//a[contains(text(), 'Continue to localhost (unsafe)')]")
    WebElement edgeLink;

    public HttpsBackPage(WebDriver driver) {
        this.driver = driver;
        driver.get(PAGE_URL);
        PageFactory.initElements(driver, this);
    }

    public void setHttps() {
        if (driver instanceof ChromeDriver) {
            if (advancedButton.isDisplayed()) {
                advancedButton.click();
                link.click();
            }
        } else if (driver instanceof EdgeDriver) {
            if (advancedButton.isDisplayed()) {
                advancedButton.click();
                edgeLink.click();
            }
        }
    }

}
