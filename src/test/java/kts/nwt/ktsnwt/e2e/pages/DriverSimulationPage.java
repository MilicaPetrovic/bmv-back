package kts.nwt.ktsnwt.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverSimulationPage {
    private WebDriver driver;
    @FindBy(how = How.ID, using = "startButton")
    WebElement startButton;

    @FindBy(how = How.ID, using = "noPassengerButton")
    WebElement noPassengerButton;

    @FindBy(how = How.ID, using = "endButton")
    WebElement endButton;

    @FindBy(how = How.XPATH, using = "//img[@ptooltip='Obaveštenje']")
    WebElement bell;

    @FindBy(how = How.TAG_NAME, using = "textarea")
    WebElement rejectTextarea;
    @FindBy(how = How.ID, using = "rejectButton")
    WebElement rejectButton;

    @FindBy(how = How.CLASS_NAME, using = "mat-card-content")
    WebElement notificationElement;

    @FindBy(how = How.XPATH, using = "//div[contains(text() ,'odbili vožnju')]")
    WebElement alreadyRejectedDrivingMessage;


    public DriverSimulationPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickOnNoPassengerReject() {
        new WebDriverWait(driver, 180)
                .until(ExpectedConditions.elementToBeClickable(noPassengerButton));
        noPassengerButton.click();
    }

    public void openBell(){
        bell.click();
    }

    public void clickOnNotificationElement(){
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOf(notificationElement));
        notificationElement.click();
    }

    public void addRejectionText(String text){
        rejectTextarea.clear();
        rejectTextarea.sendKeys(text);
    }

    public void clickRejectButton(){
        rejectButton.click();
    }

    public boolean checkIfGotAlreadyRejectedMessage() {
        return (new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOf(alreadyRejectedDrivingMessage))) != null;
    }

    public void clickStart() {
        new WebDriverWait(driver, 180)
                .until(ExpectedConditions.elementToBeClickable(startButton));
        startButton.click();
    }

    public void clickEnd() {
        new WebDriverWait(driver, 1000)
                .until(ExpectedConditions.elementToBeClickable(endButton));
        endButton.click();
    }

    public void refresh() {
        driver.navigate().refresh();
    }
}
