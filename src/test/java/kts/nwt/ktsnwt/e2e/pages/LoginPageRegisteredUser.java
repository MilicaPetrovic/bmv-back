package kts.nwt.ktsnwt.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPageRegisteredUser {
    private WebDriver driver;

    private String PAGE_URL = "https://localhost:4200/auth/login";

    @FindBy(id = "email")
    WebElement inputEmail;

    @FindBy(id = "pass")
    WebElement inputPass;

    @FindBy(id = "subBtn")
    WebElement loginBtn;

    @FindBy(how = How.TAG_NAME, using = "h1")
    WebElement orderTitle;

    @FindBy(how = How.TAG_NAME, using = "h1")
    WebElement loginTitle;

    @FindBy(how = How.XPATH, using = "//button[contains(text(), 'Advanced')]")
    WebElement advancedButton;

    @FindBy(how = How.XPATH, using = "//a[contains(text(), 'Proceed to localhost (unsafe)')]")
    WebElement link;

    @FindBy(how = How.XPATH, using = "//a[contains(text(), 'Continue to localhost (unsafe)')]")
    WebElement edgeLink;

    public LoginPageRegisteredUser(WebDriver driver) {
        this.driver = driver;
        driver.get(PAGE_URL);
        PageFactory.initElements(driver, this);
    }

    public void filLoginForm(String email, String password) {
        this.inputEmail.clear();
        this.inputEmail.sendKeys(email);

        this.inputPass.clear();
        this.inputPass.sendKeys(password);

        this.loginBtn.click();
    }

    private void setHttps() {
        if (driver instanceof ChromeDriver) {
            if (advancedButton.isDisplayed()) {
                advancedButton.click();
                link.click();
            }
        } else if (driver instanceof EdgeDriver) {
            if (advancedButton.isDisplayed()) {
                advancedButton.click();
                edgeLink.click();
            }
        }
    }

    public boolean isOpenedLoginPage() {
        setHttps();
        boolean isOppend = (new WebDriverWait(driver, 5))
                .until(ExpectedConditions.textToBePresentInElement(loginTitle, "Ulogovanje"));
        return isOppend;
    }

    public boolean isOpenedOrderPage() {
        boolean isOppend = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.textToBePresentInElement(orderTitle, "Poručivanje vožnje"));
        return isOppend;
    }


}
