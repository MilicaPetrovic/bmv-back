package kts.nwt.ktsnwt.e2e.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class PaymentPage {
    private WebDriver chromeDriver;

    @FindBy(id = "payBtn")
    WebElement payBtn;

    @FindBy(how = How.CLASS_NAME, using = "p-toast-summary")
    WebElement paymentMessageSummary;

    @FindBy(how = How.CLASS_NAME, using = "p-toast-detail")
    WebElement paymentMessageDetailed;

    public PaymentPage(WebDriver driver) {
        this.chromeDriver = driver;
        PageFactory.initElements(driver, this);
    }

    public void pay() {
        payBtn.click();
    }

    public boolean isGoodMessage() {
        try {
            List<WebElement> allToasts = chromeDriver.findElements(By.className("p-toast-summary"));
            System.out.println("detektovani tostovi: " + allToasts.size());
            if(allToasts.size() == 2){
                return (new WebDriverWait(chromeDriver, 10))
                        .until(ExpectedConditions.textToBePresentInElement(allToasts.get(1), "Uspešno plaćanje vožnje"));
            }else {
                return (new WebDriverWait(chromeDriver, 10))
                        .until(ExpectedConditions.textToBePresentInElement(paymentMessageSummary, "Uspešno plaćanje vožnje"));
            }
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isNotAcceptedDrivingMessage() {
        //Neko od putnika nije potvrdio vožnju.
        try {
            boolean isOppend = (new WebDriverWait(chromeDriver, 10))
                    .until(ExpectedConditions.textToBePresentInElement(paymentMessageDetailed, "Neko od putnika nije potvrdio vožnju."));
            return isOppend;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isNotEnoughTokens(){
        try {
            boolean isOppend = (new WebDriverWait(chromeDriver, 10))
                    .until(ExpectedConditions.textToBePresentInElement(paymentMessageDetailed, "Neko od putnika nema dovoljno tokena za ovu vožnju. Molimo kupite tokene."));
            return isOppend;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isAcceptNotification() {
        try {
            boolean isOppend = (new WebDriverWait(chromeDriver, 10))
                    .until(ExpectedConditions.textToBePresentInElement(paymentMessageDetailed, "Uspešno smo Vam napravili vožnju"));
            return isOppend;
        } catch (Exception e) {
            return false;
        }

    }
}
