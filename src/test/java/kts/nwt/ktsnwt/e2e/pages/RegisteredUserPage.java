package kts.nwt.ktsnwt.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class RegisteredUserPage {
    private WebDriver driver;

    //p-toast-detail
    ///Ne možete više da ocenite vožnju
    @FindBy(how = How.CLASS_NAME, using = "p-toast-detail")
    List<WebElement> messages;
    @FindBy(how = How.XPATH, using = "//div[contains(text(), 'Ne možete više da ocenite vožnju')]")
    WebElement noMoreRatingMessage;
    @FindBy(how = How.XPATH, using = "//div[contains(text() ,'Nažalost nismo bili u stanju da Vam napravimo vožnju ili vozač više nije u mogućnosti da vozi.')]")
    WebElement rejectedOrRefusedDrivingMessage;

    @FindBy(how = How.XPATH, using = "//img[@ptooltip='Obaveštenje']")
    WebElement bell;

    @FindBy(how = How.XPATH, using = "//img[@ptooltip='Poručivanje vožnje']")
    WebElement order;
    @FindBy(how = How.CLASS_NAME, using = "mat-card-content")
    WebElement notificationElement;

    @FindBy(how = How.XPATH, using = "//ngx-stars[@class='driver']/div/div")
    List<WebElement> driverStars;

    @FindBy(how = How.XPATH, using = "//ngx-stars[@class='vehicle']/div/div")
    List<WebElement> vehicleStars;

    @FindBy(how = How.ID, using = "leaveRatingButton")
    WebElement ratingButton;

    @FindBy(how = How.XPATH, using = "//div[contains(text() ,'Uspešno sačuvana recenzija')]")
    WebElement ratingSuccessfulMessage;
    public RegisteredUserPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean checkIfRegisteredUserGotRefusedMessage(){
        return (new WebDriverWait(driver, 20)
                .until(ExpectedConditions.visibilityOf(rejectedOrRefusedDrivingMessage))) != null;
    }

    public void openBell() {
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(bell));
        bell.click();
    }

    public void clickOnNotificationElement(){
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOf(notificationElement));
        notificationElement.click();
    }

    public void rateDriver(int i) {
        WebElement rating = driverStars.get(i-1);
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOf(rating));
        Actions a = new Actions(driver);
        a.moveToElement(rating).click().perform();
    }

    public void rateVehicle(int i) {
        WebElement rating = vehicleStars.get(i-1);
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOf(rating));
        Actions a = new Actions(driver);
        a.moveToElement(rating).click().perform();
    }

    public void sendRating() {
        ratingButton.click();
    }

    public void checkMessages() {
        System.out.println("*****************");
        for (WebElement e : messages){
            System.out.println(e);
            System.out.println(e.getText());
        }
    }

    public boolean checkIfRatingSuccessfulMessage(){
        return (new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOf(ratingSuccessfulMessage))) != null;
    }

    public boolean checkIfNoMoreRatingCanBeGiven(){
        return (new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOf(noMoreRatingMessage))) != null;
    }

    public void refresh(){
        driver.navigate().refresh();
    }

    public void openOrder() {
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(bell));
        order.click();

    }

    private final String PAGE_URL = "https://localhost:4200/registered/step/order";
    public void navigateToOrderDriving() {
        driver.get(PAGE_URL);
    }
}
