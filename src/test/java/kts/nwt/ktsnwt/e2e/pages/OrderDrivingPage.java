package kts.nwt.ktsnwt.e2e.pages;

import kts.nwt.ktsnwt.e2e.helper.ViewPortHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class OrderDrivingPage {

    private WebDriver chromeDriver;

  //  @FindBy(how=How.XPATH, using="//input[@name = 'address']")
    @FindBy(id = "name")
    WebElement inputAddress;

    @FindBy(how = How.CLASS_NAME, using = "foundAddress")
    WebElement foundAddress1;

    @FindBy(how = How.CLASS_NAME, using = "foundAddress")
    WebElement foundAddress2;

    @FindBy(id = "showPath")
    WebElement showBtn;

    @FindBy(how = How.XPATH, using ="//div[contains(text() ,' Izaberi način izbora putanje ')]")
  //  @FindBy(how = How.CLASS_NAME, using = "p-treeselect-label p-placeholder")
    WebElement selectPath;

    @FindBy(how = How.XPATH, using = "//span[contains(text() ,'po vremenu')]")
    WebElement optimalByTime;

    @FindBy(how = How.XPATH, using = "//span[contains(text() ,'po razdaljini')]")
    WebElement optimalByDistance;

    @FindBy(how = How.XPATH, using = "//span[contains(text() ,'po ceni')]")
    WebElement optimalByPrice;

    @FindBy(how = How.XPATH, using ="//div[contains(text() ,' Omiljene putanje ')]")
    WebElement favouritePathsSelect;

    @FindBy(how = How.CLASS_NAME, using = "p-treenode-selectable")
    List<WebElement> favouritePaths;

  //  @FindBy(how = How.CLASS_NAME, using = "leaflet-interactive")
    @FindBy(how = How.TAG_NAME, using = "path")
    List<WebElement> routes;

    @FindBy(id="chosenRouteColor")
    WebElement chosenRouteColor;

    @FindBy(id = "dalje")
    WebElement nextBtn;

    @FindBy(how=How.XPATH,  using = "//h1[contains(text() ,'Izaberi dodatne usluge')]")
    WebElement additionalServices;

    @FindBy(how = How.CLASS_NAME, using = "p-toast-summary")
    WebElement orderMessageSummary;

    @FindBy(how = How.CLASS_NAME, using = "p-toast-detail")
    //  @FindBy(how = How.XPATH, using = "//div[class = 'p-toast-detail']/")
    WebElement orderMessageDetailed;

    @FindBy(how = How.CLASS_NAME, using = "p-toast-summary")
    WebElement confirmMessage;

    @FindBy(how = How.CLASS_NAME, using = "p-toast-detail")
    WebElement confirmMessageDetailed;


    @FindBy(how = How.XPATH, using = "//img[@ptooltip='Obaveštenje']")
    WebElement bell;

    @FindBy(how = How.CLASS_NAME, using = "mat-card-content")
    WebElement conformationCard;

    @FindBy(id="confirmSplitFair")
    WebElement confirmSplitFairBtn;

    @FindBy(how=How.XPATH, using = "//button[contains(text(), 'Allow')]")
    WebElement allowBtn;

    @FindBy(how = How.XPATH, using = "//h3[ contains(text(), ' Neko hoće da te doda u njihovu vožnju ']")
    WebElement confirmationCardH3;

    private ViewPortHelper viewPortHelper;

    public OrderDrivingPage(WebDriver driver){
        this.chromeDriver = driver;
        PageFactory.initElements(driver, this);
        viewPortHelper = new ViewPortHelper();
    }

    public void selectAddress(String address){

        new WebDriverWait(chromeDriver, 10)
                .until(ExpectedConditions.visibilityOfElementLocated(By.id("name")));

        inputAddress.clear();
        inputAddress.sendKeys(address);

        String[] words =  address.split(" ");
        String firstWord = words[0];
        String upper = firstWord.substring(0,1).toUpperCase();
        String lower = firstWord.substring(1).toLowerCase();
        String word = upper+lower;
        System.out.println("first: " + word);

        new WebDriverWait(chromeDriver, 10)
                .until(ExpectedConditions.textToBePresentInElement(foundAddress1, word));

        viewPortHelper.waitToBeMovable(chromeDriver, 0, foundAddress1);
        Actions a1 = new Actions(chromeDriver);
        a1.moveToElement(foundAddress1).click().perform();
    }


    public void showRoutes(){
        viewPortHelper.waitToBeMovable(chromeDriver, 0, showBtn);
        showBtn.click();

        new WebDriverWait(chromeDriver, 10)
                .until(ExpectedConditions.numberOfElementsToBeMoreThan(By.tagName("path"), 3));
    }

    public void choseOptimalByTime(){
        viewPortHelper.waitToBeMovable(chromeDriver, 0, nextBtn);
        Actions a = new Actions(chromeDriver);
        a.moveToElement(selectPath).click().perform();
        a.moveToElement(optimalByTime).click().perform();
    }

    public void choseOptimalByDistance(){
        viewPortHelper.waitToBeMovable(chromeDriver, 0, nextBtn);
        Actions a = new Actions(chromeDriver);
        a.moveToElement(selectPath).click().perform();
        a.moveToElement(optimalByDistance).click().perform();
    }

    public void choseOptimalByPrice(){
        viewPortHelper.waitToBeMovable(chromeDriver, 0, nextBtn);
        Actions a = new Actions(chromeDriver);
        a.moveToElement(selectPath).click().perform();
        a.moveToElement(optimalByPrice).click().perform();
    }

    public void clickNext(){
        viewPortHelper.waitToBeMovable(chromeDriver, 0, nextBtn);
        nextBtn.click();
    }

    public void clickAllowBtn(){
        allowBtn.click();
    }

    public void clickOnFavouritePathSelect(){
        Actions a = new Actions(chromeDriver);
        a.moveToElement(favouritePathsSelect).click().perform();
        a.moveToElement(favouritePaths.get(0)).click().perform();
        nextBtn.click();
    }

    public boolean choseRoutesByHand(){
        if(routes.size()> 4){
            System.out.println(routes);
            System.out.println(routes.get(1));

            routes.get(1).click();

            boolean isChosen = new WebDriverWait(chromeDriver, 10)
                    .until(ExpectedConditions.textToBePresentInElement(chosenRouteColor, "Izabrana putanja: zelena"));
            nextBtn.click();
            return isChosen;
        }
        return false;
    }


    public boolean isShownSplitFairConfirmation(){
        try {
            boolean isOppend = (new WebDriverWait(chromeDriver, 5))
                    .until(ExpectedConditions.textToBePresentInElement(confirmMessageDetailed, "Ako ste usaglašeni potvrdite, u slučaju ako niste, samo zanemarite"));
            return isOppend;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean notChosenRoute(){
        try {
            boolean isOppend = (new WebDriverWait(chromeDriver, 5))
                    .until(ExpectedConditions.textToBePresentInElement(confirmMessageDetailed, "Prvo izaberite putanju da biste prešli na sledeći korak."));
            return isOppend;
        } catch (Exception e) {
            return false;
        }
    }

    public void confirmSplitFairToast(){
        confirmMessageDetailed.click();
        confirmSplitFairBtn.click();
    }

    public void openBell(){
        bell.click();
    }

    public void refresh(){
        chromeDriver.navigate().refresh();
    }
    public void confirmSplitFair(){
        new WebDriverWait(chromeDriver, 20)
                .until(ExpectedConditions.elementToBeClickable(conformationCard));
        conformationCard.click();

        new WebDriverWait(chromeDriver, 10)
                .until(ExpectedConditions.elementToBeClickable(confirmSplitFairBtn));
        confirmSplitFairBtn.click();
    }

    public boolean isShownNextPage(){
        boolean isOppend =(new WebDriverWait(chromeDriver, 10))
                .until(ExpectedConditions.textToBePresentInElement(additionalServices, "Izaberi dodatne usluge"));
        return isOppend;
    }
}
