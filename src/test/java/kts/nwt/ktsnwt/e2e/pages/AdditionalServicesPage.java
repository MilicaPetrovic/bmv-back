package kts.nwt.ktsnwt.e2e.pages;

import kts.nwt.ktsnwt.e2e.helper.ViewPortHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdditionalServicesPage {

    private WebDriver driver;

    //   @FindBy(id = "b")
    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'Vožnja sa bebom')]")
    WebElement babyCheckbox;

    //  @FindBy(id = "p")Vožnja sa ljubimcem
    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'Vožnja sa ljubimcem')]")
    WebElement petCheckbox;

    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'karavan')]")
    WebElement type1;

    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'dzip')]")
    WebElement type2;

    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'terenski')]")
    WebElement type3;

    @FindBy(id = "mat-chip-list-input-0")
    WebElement inputFriend;

    // @FindBy(id = "additionPotvrdi")
    @FindBy(how = How.CLASS_NAME, using = "p-button-label")
    WebElement nextBtn;

    @FindBy(how = How.CLASS_NAME, using = "p-toast-summary")
    WebElement drivingAdditionalMessage;

    @FindBy(how = How.CLASS_NAME, using = "p-toast-detail")
    WebElement drivingAdditionalMessageDetailed;
    @FindBy(how = How.XPATH, using = "//h1[contains(text() ,'Plaćanje vožnje')]")
    WebElement payment;

    private ViewPortHelper viewPortHelper;

    public AdditionalServicesPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        viewPortHelper = new ViewPortHelper();
    }

    public void checkBabyCheckbox() {
        if (!babyCheckbox.isSelected()) {
            Actions a = new Actions(driver);
            a.moveToElement(babyCheckbox).click().perform();
        }
    }

    public void checkPetCheckbox() {
        if (!petCheckbox.isSelected()) {
            Actions a = new Actions(driver);
            a.moveToElement(petCheckbox).click().perform();
        }
    }

    public void choseType1() {
        type1.click();
    }

    public void choseType2() {
        type2.click();
    }

    public void choseType3() {
        type3.click();
    }

    public void addFriend(String friend) {
        inputFriend.clear();
        inputFriend.sendKeys(friend);
        inputFriend.sendKeys(Keys.ENTER);
    }

    public void next() {
        //new WebDriverWait(driver, 5)
        //        .until(ExpectedConditions.visibilityOfElementLocated(By.id("additionPotvrdi")));

        viewPortHelper.waitToBeMovable(driver, 0, nextBtn);
        nextBtn.click();
    }

    public boolean isNotEnoughTokens() {
        try {
            boolean isOppend = (new WebDriverWait(driver, 10))
                    .until(ExpectedConditions.textToBePresentInElement(drivingAdditionalMessageDetailed, "Neko od putnika nema dovoljno tokena za ovu vožnju. Molimo kupite tokene."));
            return isOppend;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isPassengerAlreadyHaveDriving() {
        try {
            boolean isOppend = (new WebDriverWait(driver, 10))
                    .until(ExpectedConditions.textToBePresentInElement(drivingAdditionalMessageDetailed, "Nismo uspeli da naručimo vožnju za Vas. Vi ili jedan od drugara već ima poručeno ili aktivno vožnje."));
            return isOppend;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean notAvailableDriver() {
        try {
            boolean isOppend = (new WebDriverWait(driver, 10))
                    .until(ExpectedConditions.textToBePresentInElement(drivingAdditionalMessageDetailed, "Nismo uspeli da naručimo vožnju za Vas. Nemamo vozača na raspolaganju."));
            return isOppend;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isOpenedPayment() {
        boolean isOppend = (new WebDriverWait(driver, 5))
                .until(ExpectedConditions.textToBePresentInElement(payment, "Plaćanje vožnje"));
        return isOppend;
    }

}
