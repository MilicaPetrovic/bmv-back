package kts.nwt.ktsnwt.e2e.pages;

import kts.nwt.ktsnwt.e2e.helper.ViewPortHelper;
import org.junit.jupiter.api.Timeout;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class LoginPage {
    private final WebDriver chromeDriver;

    private final String PAGE_URL = "https://localhost:4200/auth/login";

    private final WebDriverWait wait;

    @FindBy(className = "p-toast-summary")
    private List<WebElement> messages;

    @FindBy(className = "p-toast-icon-close-icon")
    private List<WebElement> messagesClose;

    @FindBy(id = "email")
    WebElement inputEmail;

    @FindBy(id = "pass")
    WebElement inputPass;

    @FindBy(id = "subBtn")
    WebElement loginBtn;

    @FindBy(how = How.TAG_NAME, using = "h1")
    WebElement title;

    @FindBy(className = "error")
    WebElement errorMessage;

    @FindBy(how = How.XPATH, using = "//button[contains(text(), 'Advanced')]")
    WebElement advancedButton;

    @FindBy(how = How.XPATH, using = "//a[contains(text(), 'Proceed to localhost (unsafe)')]")
    WebElement link;

    @FindBy(how = How.XPATH, using = "//a[contains(text(), 'Continue to localhost (unsafe)')]")
    WebElement edgeLink;

    @FindBy(how = How.XPATH, using = "//img[@ptooltip='Odjava']")
    WebElement logout;

    ViewPortHelper viewPortHelper;

    public LoginPage(WebDriver driver) {
        this.chromeDriver = driver;
        driver.get(PAGE_URL);
        this.wait = new WebDriverWait(chromeDriver, 10);
        PageFactory.initElements(driver, this);
        viewPortHelper = new ViewPortHelper();
    }

    public void filLoginForm(String email, String password) {
        this.inputEmail.clear();
        this.inputEmail.sendKeys(email);

        this.inputPass.clear();
        this.inputPass.sendKeys(password);

        this.loginBtn.click();
    }

    public void logout() {
        try {
            for (WebElement msg : messagesClose) {
                System.out.println(msg);
                msg.click();
            }
        } catch (ElementClickInterceptedException e) {
            this.chromeDriver.navigate().refresh();
        }
        System.out.println(logout);
        viewPortHelper.waitToBeMovable(chromeDriver, 0, logout);
        logout.click();
    }

    public void setHttps() {
        if (chromeDriver instanceof ChromeDriver) {
            if (advancedButton.isDisplayed()) {
                advancedButton.click();
                link.click();
            }
        } else if (chromeDriver instanceof EdgeDriver) {
            if (advancedButton.isDisplayed()) {
                advancedButton.click();
                edgeLink.click();
            }
        }
    }

    public boolean isOpenedLoginPage() {
        //setHttps();
        return wait.until(ExpectedConditions.textToBePresentInElement(title, "Ulogovanje"));
    }

    public boolean isOpenedOrderPage() {
        return wait.until(ExpectedConditions.textToBePresentInElement(title, "Poručivanje vožnje"));
    }

    public boolean isOpenedDriverHomePage() {
        return wait.until(ExpectedConditions.textToBePresentInElement(title, "Prikaz vožnje"));
    }

    public boolean isOpenedAdminHomePage() {
        return wait.until(ExpectedConditions.textToBePresentInElement(title, "Korisnici"));
    }

    public boolean isBadCredentialsErrorShowed() {
        return wait.until(ExpectedConditions.textToBePresentInElement(errorMessage, "Neispravan email ili lozinka."));
    }

    public boolean isNotActivatedAccountErrorShowed() {
        return wait.until(ExpectedConditions.textToBePresentInElement(errorMessage, "Vaš nalog još nije aktiviran!"));
    }


}
