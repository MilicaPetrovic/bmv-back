package kts.nwt.ktsnwt.e2e.tests;

import kts.nwt.ktsnwt.e2e.helper.Helper;
import kts.nwt.ktsnwt.e2e.pages.*;
import org.apache.commons.logging.Log;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EndDrivingTest extends BaseTest{

    @Test
    void shouldBeRejectNoPassenger(){
        LoginPage ru = loginAsRegisteredUser(chromeDriver, edgeDriver, "evioletta121@gmail.com", "123");

        LoginPage d = loginAsDriver(edgeDriver, chromeDriver, "nn@gmail.com", "123");

        DriverSimulationPage driverSimulationPage = new DriverSimulationPage(chromeDriver);
        driverSimulationPage.clickOnNoPassengerReject();
        d.logout();
        chromeDriver.manage().window().setPosition(new Point(-2000, 0));

        edgeDriver.manage().window().maximize();
        RegisteredUserPage registeredUserPage = new RegisteredUserPage(edgeDriver);
        assertTrue(registeredUserPage.checkIfRegisteredUserGotRefusedMessage());
        ru.logout();
    }

    @Test
    void shouldBeRejectFromDialog(){
        LoginPage ru = loginAsRegisteredUser(chromeDriver, edgeDriver, "olivia@gmail.com", "123");

        LoginPage d = loginAsDriver(edgeDriver, chromeDriver, "ll@gmail.com", "123");

        DriverSimulationPage driverSimulationPage = new DriverSimulationPage(chromeDriver);
        driverSimulationPage.openBell();
        driverSimulationPage.clickOnNotificationElement();
        driverSimulationPage.addRejectionText("Ne mogu vise hvala lepo");
        driverSimulationPage.clickRejectButton();
        Helper.takeScreenshoot(chromeDriver, "3-xxxxxxxx");
        chromeDriver.manage().window().setPosition(new Point(-2000, 0));

        edgeDriver.manage().window().maximize();
        RegisteredUserPage registeredUserPage = new RegisteredUserPage(edgeDriver);
        Assertions.assertTrue(registeredUserPage.checkIfRegisteredUserGotRefusedMessage());
        ru.logout();
        edgeDriver.manage().window().setPosition(new Point(-2000, 0));

        chromeDriver.manage().window().maximize();
        driverSimulationPage.refresh();
        driverSimulationPage.openBell();
        driverSimulationPage.clickOnNotificationElement();
        Assertions.assertTrue(driverSimulationPage.checkIfGotAlreadyRejectedMessage());
        d.logout();

    }

/*
    @Test
    void shouldBeMessageAlreadyRejected(){

        loginAsD(edgeDriver, chromeDriver, "nn@gmail.com", "123");

        DriverSimulationPage driverSimulationPage = new DriverSimulationPage(chromeDriver);
        driverSimulationPage.openBell();
        driverSimulationPage.clickOnNotificationElement();
        //driverSimulationPage.addRejectionText("Brate pusti me");
        //driverSimulationPage.clickRejectButton();
        Assertions.assertTrue(driverSimulationPage.checkIfGotAlreadyRejectedMessage());
    }
*/

    @Test
    void shouldStartAndEndDrivingWithRating(){
        LoginPage d = loginAsDriver(chromeDriver, edgeDriver, "oo@gmail.com", "123");

        DriverSimulationPage driverSimulationPage = new DriverSimulationPage(edgeDriver);
        driverSimulationPage.clickStart();

        //klijent ima aktivnu voznju --> ne moze da naruci
        LoginPage ru = loginAsRegisteredUser(edgeDriver, chromeDriver, "jamie@gmail.com", "123");
        OrderDrivingPage orderDrivingPage = new OrderDrivingPage(chromeDriver);
        orderDrivingPage.clickOnFavouritePathSelect();
        assertEquals(true, orderDrivingPage.isShownNextPage());
        AdditionalServicesPage additionalServicesPage = new AdditionalServicesPage(chromeDriver);
        additionalServicesPage.next();
        Assertions.assertTrue(additionalServicesPage.isPassengerAlreadyHaveDriving());
        chromeDriver.manage().window().setPosition(new Point(-2000, 0));

        edgeDriver.manage().window().maximize();
        driverSimulationPage.clickEnd();
        d.logout();

        //klijent oceni proslu voznju
        edgeDriver.manage().window().setPosition(new Point(-2000, 0));
        chromeDriver.manage().window().maximize();
        RegisteredUserPage registeredUserPage = new RegisteredUserPage(chromeDriver);
        registeredUserPage.navigateToOrderDriving();
        Helper.takeScreenshoot(chromeDriver, "3-open-bell");
        registeredUserPage.openBell();
        registeredUserPage.clickOnNotificationElement();
        registeredUserPage.rateDriver(3);
        registeredUserPage.rateVehicle(5);
        Helper.takeScreenshoot(chromeDriver, "3-leaveRating");
        registeredUserPage.sendRating();
        Assertions.assertTrue(registeredUserPage.checkIfRatingSuccessfulMessage());
        Helper.takeScreenshoot(chromeDriver, "3-uspesno-sacuvanje");

        //klijent ne moze za istu voznju da ostavi vise recenzija
        registeredUserPage.navigateToOrderDriving();
        Helper.takeScreenshoot(chromeDriver, "3-open-order");
        registeredUserPage.openBell();
        registeredUserPage.clickOnNotificationElement();
        Assertions.assertTrue(registeredUserPage.checkIfNoMoreRatingCanBeGiven());


        //klijent vise nema aktivnu voznju --> moze da naruci
        registeredUserPage.navigateToOrderDriving();
        orderDrivingPage.clickOnFavouritePathSelect();
        assertTrue(orderDrivingPage.isShownNextPage());

        additionalServicesPage.next();
        assertTrue(additionalServicesPage.isOpenedPayment());

        ru.logout();
    }

    private LoginPage loginAsDriver(WebDriver toMinimize, WebDriver toMaximize, String username, String password) {
        toMinimize.manage().window().setPosition(new Point(-2000, 0));
        toMaximize.manage().window().maximize();
        //HttpsBackPage backPage = new HttpsBackPage(toMaximize);
        //backPage.setHttps();

        LoginPage login = new LoginPage(toMaximize);
        assertEquals(true, login.isOpenedLoginPage());

        login.filLoginForm(username, password);
        assertEquals(true, login.isOpenedDriverHomePage());
        return login;
    }

    private LoginPage loginAsRegisteredUser(WebDriver toMinimize, WebDriver toMaximize, String username, String password) {
        toMinimize.manage().window().setPosition(new Point(-2000, 0));
        toMaximize.manage().window().maximize();
        //HttpsBackPage backPage = new HttpsBackPage(toMaximize);
        //backPage.setHttps();

        LoginPage login = new LoginPage(toMaximize);
        assertEquals(true, login.isOpenedLoginPage());

        login.filLoginForm(username, password);
        assertEquals(true, login.isOpenedOrderPage());
        return login;
    }
}
