package kts.nwt.ktsnwt.e2e.tests;

import kts.nwt.ktsnwt.e2e.pages.HttpsBackPage;
import kts.nwt.ktsnwt.e2e.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.openqa.selenium.edge.EdgeDriver;

import java.util.concurrent.TimeUnit;

public class BaseTest {
    public static WebDriver chromeDriver;
    public static WebDriver edgeDriver;

    @BeforeAll
    public static void initializeWebDriver() {
        System.setProperty("webdriver.edge.driver", "D:\\SYSTEM\\Downloads\\edgedriver_win64\\msedgedriver.exe"); //"C:\\Users\\User\\msedgedriver.exe"
        edgeDriver = new EdgeDriver();
        edgeDriver.manage().window().maximize();
        edgeDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        HttpsBackPage backPage = new HttpsBackPage(edgeDriver);
        backPage.setHttps();
        LoginPage loginPage = new LoginPage(edgeDriver);
        loginPage.setHttps();


        System.setProperty("webdriver.chrome.driver", "D:\\SYSTEM\\Downloads\\chromedriver_win32\\chromedriver.exe"); // D:\SYSTEM\Downloads\chromedriver_win32\ // "C:\\Users\\User\\chromedriver2.exe"

        chromeDriver = new ChromeDriver();
        chromeDriver.manage().window().maximize();
        chromeDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        HttpsBackPage backPageChrome = new HttpsBackPage(chromeDriver);
        backPageChrome.setHttps();
        LoginPage loginPageChrome = new LoginPage(chromeDriver);
        loginPageChrome.setHttps();

    }


    @AfterAll
    public static void quitDriver() {
        chromeDriver.quit();
        edgeDriver.quit();
    }
}
