package kts.nwt.ktsnwt.e2e.tests;

import kts.nwt.ktsnwt.e2e.helper.Helper;
import kts.nwt.ktsnwt.e2e.pages.*;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.openqa.selenium.Point;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OrderDrivingTest extends BaseTest {

    @Test
    @Order(1)
    public void shouldBeOkOrderFavouriteRoute() {
        //HttpsBackPage backPage = new HttpsBackPage(chromeDriver);
        //backPage.setHttps();
        LoginPage login = new LoginPage(chromeDriver);
        assertEquals(true,login.isOpenedLoginPage());

        login.filLoginForm("evioletta121@gmail.com", "123");
        assertEquals(true, login.isOpenedOrderPage());

        OrderDrivingPage orderPage = new OrderDrivingPage(chromeDriver);
        orderPage.clickOnFavouritePathSelect();


        assertEquals(true, orderPage.isShownNextPage());
        AdditionalServicesPage additionalServicesPage = new AdditionalServicesPage(chromeDriver);
        additionalServicesPage.next();

        assertEquals(true, additionalServicesPage.isOpenedPayment());

        PaymentPage paymentPage = new PaymentPage(chromeDriver);
        paymentPage.pay();

        Helper.takeScreenshoot(chromeDriver, "screan1");
        assertEquals(true, paymentPage.isGoodMessage());
        login.logout();
        assertEquals(true,login.isOpenedLoginPage());
    }

    @Test
    @Order(2)
    public void shouldBeOkayOrderDrivingTest() {
        //HttpsBackPage backPage = new HttpsBackPage(chromeDriver);
        //backPage.setHttps();
        LoginPage login = new LoginPage(chromeDriver);
        assertEquals(true,login.isOpenedLoginPage());

        login.filLoginForm("ev@gmail.com", "123");
        assertEquals(true, login.isOpenedOrderPage());

        OrderDrivingPage orderPage = new OrderDrivingPage(chromeDriver);
        orderPage.selectAddress("gogoljeva 22 novi sad");
        orderPage.selectAddress("zmaj jovina 10 novi sad");
        orderPage.showRoutes();
        orderPage.choseOptimalByTime();
        orderPage.clickNext();

        assertEquals(true, orderPage.isShownNextPage());

        AdditionalServicesPage additionalServicesPage = new AdditionalServicesPage(chromeDriver);

        additionalServicesPage.next();
        assertEquals(true, additionalServicesPage.isOpenedPayment());

        PaymentPage paymentPage = new PaymentPage(chromeDriver);
        paymentPage.pay();

        Helper.takeScreenshoot(chromeDriver, "screan2");

        assertEquals(true, paymentPage.isGoodMessage());
        login.logout();
        assertEquals(true,login.isOpenedLoginPage());
    }

    @Test
    @Order(3)
    public void userDontHaveTokensOrderDrivingTest() {
        //HttpsBackPage backPage = new HttpsBackPage(chromeDriver);
        //backPage.setHttps();

        LoginPage login = new LoginPage(chromeDriver);
        assertEquals(true,login.isOpenedLoginPage());

        login.filLoginForm("dz@gmail.com", "123");
        assertEquals(true, login.isOpenedOrderPage());

        OrderDrivingPage orderPage = new OrderDrivingPage(chromeDriver);
        orderPage.selectAddress("gogoljeva 22 novi sad");
        orderPage.selectAddress("zmaj jovina 10 novi sad");
        orderPage.showRoutes();

        //  assertEquals(true, orderPage.choseRoutesByHand());

        orderPage.choseOptimalByDistance();
        orderPage.clickNext();

        assertEquals(true, orderPage.isShownNextPage());

        AdditionalServicesPage additionalServicesPage = new AdditionalServicesPage(chromeDriver);

        additionalServicesPage.next();
        Helper.takeScreenshoot(chromeDriver, "screan3");

        assertEquals(true, additionalServicesPage.isNotEnoughTokens());
        login.logout();
        assertEquals(true,login.isOpenedLoginPage());
    }


    @Test
    @Order(4)
    public void splitFairOnePassengerAlreadyHaveDriving() {
        //HttpsBackPage backPage = new HttpsBackPage(chromeDriver);
        //backPage.setHttps();
        LoginPage login = new LoginPage(chromeDriver);
        assertEquals(true,login.isOpenedLoginPage());

        login.filLoginForm("jj@ex.com", "123");
        assertEquals(true, login.isOpenedOrderPage());
        OrderDrivingPage orderPage = new OrderDrivingPage(chromeDriver);
        orderPage.selectAddress("gogoljeva 22 novi sad");
        orderPage.selectAddress("zmaj jovina 10 novi sad");
        orderPage.showRoutes();

        orderPage.choseOptimalByDistance();
        orderPage.clickNext();

        assertEquals(true, orderPage.isShownNextPage());

        AdditionalServicesPage additionalServicesPage = new AdditionalServicesPage(chromeDriver);
        additionalServicesPage.addFriend("mm");
        additionalServicesPage.next();

        Helper.takeScreenshoot(chromeDriver, "screan4");


        assertEquals(true, additionalServicesPage.isPassengerAlreadyHaveDriving());
        login.logout();
        assertEquals(true,login.isOpenedLoginPage());
    }

    @Test
    @Order(5)
    public void splitFairOnePassengerDontHaveEnoughTokens() {
        //HttpsBackPage backPage = new HttpsBackPage(chromeDriver);
        //backPage.setHttps();
        LoginPage login = new LoginPage(chromeDriver);
        assertEquals(true,login.isOpenedLoginPage());

        login.filLoginForm("jj@ex.com", "123");
        assertEquals(true, login.isOpenedOrderPage());

        OrderDrivingPage orderPage = new OrderDrivingPage(chromeDriver);
        orderPage.selectAddress("gogoljeva 22 novi sad");
        orderPage.selectAddress("zmaj jovina 10 novi sad");
        orderPage.showRoutes();

        orderPage.choseOptimalByDistance();
        orderPage.clickNext();

        assertEquals(true, orderPage.isShownNextPage());

        AdditionalServicesPage additionalServicesPage = new AdditionalServicesPage(chromeDriver);
        additionalServicesPage.addFriend("dzoni");
        additionalServicesPage.next();
        Helper.takeScreenshoot(chromeDriver, "screan5");

        assertEquals(true, additionalServicesPage.isNotEnoughTokens());
        login.logout();
        assertEquals(true,login.isOpenedLoginPage());

    }


    @Test
    @Order(6)
    public void splitFairOnePassengerNotConfirmed() {
        //HttpsBackPage backPage = new HttpsBackPage(chromeDriver);
        //backPage.setHttps();
        LoginPage login = new LoginPage(chromeDriver);
        assertEquals(true,login.isOpenedLoginPage());

        login.filLoginForm("jj@ex.com", "123");
        assertEquals(true, login.isOpenedOrderPage());

        OrderDrivingPage orderPage = new OrderDrivingPage(chromeDriver);
        orderPage.selectAddress("gogoljeva 22 novi sad");
        orderPage.selectAddress("zmaj jovina 10 novi sad");
        orderPage.showRoutes();

        orderPage.choseOptimalByPrice();
        orderPage.clickNext();

        assertEquals(true, orderPage.isShownNextPage());

        AdditionalServicesPage additionalServicesPage = new AdditionalServicesPage(chromeDriver);
        additionalServicesPage.addFriend("marsal");

        additionalServicesPage.next();

        assertEquals(true, additionalServicesPage.isOpenedPayment());

        PaymentPage paymentPage = new PaymentPage(chromeDriver);
        paymentPage.pay();


        Helper.takeScreenshoot(chromeDriver, "screan6");

        assertEquals(true, paymentPage.isNotAcceptedDrivingMessage());

        login.logout();
        assertEquals(true,login.isOpenedLoginPage());
    }

    @Test
    @Order(7)
    public void shouldBeOkSplitFairOnePassengerConfirmed() {
        // prijava u edge driver
        chromeDriver.manage().window().setPosition(new Point(-2000, 0));
        edgeDriver.manage().window().maximize();
        //HttpsBackPage backPage = new HttpsBackPage(edgeDriver);
        //backPage.setHttps();

        LoginPage login = new LoginPage(edgeDriver);
        assertEquals(true, login.isOpenedLoginPage());

        login.filLoginForm("robin@gmail.com", "123");
        assertEquals(true, login.isOpenedOrderPage());


        // prijava u chrome
        edgeDriver.manage().window().setPosition(new Point(-2000, 0));
        chromeDriver.manage().window().maximize();
        //HttpsBackPage backPageChrome = new HttpsBackPage(chromeDriver);
        //backPageChrome.setHttps();
        LoginPage loginChrome = new LoginPage(chromeDriver);
        assertEquals(true,loginChrome.isOpenedLoginPage());

        loginChrome.filLoginForm("ted@gmail.com", "123");
        assertEquals(true, loginChrome.isOpenedOrderPage());
        OrderDrivingPage orderPageChrome = new OrderDrivingPage(chromeDriver);
        //orderPageChrome.openBell();


        chromeDriver.manage().window().setPosition(new Point(-2000, 0));

        edgeDriver.manage().window().maximize();
        OrderDrivingPage orderPage = new OrderDrivingPage(edgeDriver);
        orderPage.selectAddress("gogoljeva 22 novi sad");
        orderPage.selectAddress("zmaj jovina 10 novi sad");
        orderPage.showRoutes();

        orderPage.choseOptimalByPrice();
        orderPage.clickNext();

        assertEquals(true, orderPage.isShownNextPage());

        AdditionalServicesPage additionalServicesPage = new AdditionalServicesPage(edgeDriver);
        additionalServicesPage.addFriend("ted");
        additionalServicesPage.next();
        assertEquals(true, additionalServicesPage.isOpenedPayment());

        /// accept notification
        edgeDriver.manage().window().setPosition(new Point(-2000, 0));
        chromeDriver.manage().window().maximize();
        Helper.takeScreenshoot(edgeDriver, "a0");

        orderPageChrome.refresh();
        orderPageChrome.openBell();
        Helper.takeScreenshoot(chromeDriver, "a00");
        orderPageChrome.confirmSplitFair();
        Helper.takeScreenshoot(chromeDriver, "a1");

        chromeDriver.manage().window().setPosition(new Point(-2000, 0));

        edgeDriver.manage().window().maximize();
        Helper.takeScreenshoot(edgeDriver, "a2");

        PaymentPage paymentPage = new PaymentPage(edgeDriver);
        assertEquals(true, paymentPage.isAcceptNotification());

        paymentPage.pay();

        Helper.takeScreenshoot(edgeDriver, "a3");
        assertEquals(true, paymentPage.isGoodMessage());
        login.logout();
        loginChrome.logout();
        assertEquals(true,login.isOpenedLoginPage());
        assertEquals(true,loginChrome.isOpenedLoginPage());
    }


    @Test
    @Order(8)
    public void routeIsNotChosen(){
        //HttpsBackPage backPage = new HttpsBackPage(chromeDriver);
        //backPage.setHttps();
        LoginPage login = new LoginPage(chromeDriver);

        assertEquals(true,login.isOpenedLoginPage());
        login.filLoginForm("jj@ex.com", "123");
        assertEquals(true, login.isOpenedOrderPage());

        OrderDrivingPage orderPage = new OrderDrivingPage(chromeDriver);
        orderPage.selectAddress("gogoljeva 22 novi sad");
        orderPage.selectAddress("zmaj jovina 10 novi sad");
        orderPage.showRoutes();

        orderPage.clickNext();
        assertEquals(true, orderPage.notChosenRoute());
        login.logout();
        assertEquals(true,login.isOpenedLoginPage());
    }

    @Test
    @Order(9)
    public void noAvailableDriverWithAdditionalServices(){
        //HttpsBackPage backPage = new HttpsBackPage(chromeDriver);
        //backPage.setHttps();
        LoginPage login = new LoginPage(chromeDriver);

        assertEquals(true,login.isOpenedLoginPage());
        login.filLoginForm("lili@gmail.com", "123");
        assertEquals(true, login.isOpenedOrderPage());

        OrderDrivingPage orderPage = new OrderDrivingPage(chromeDriver);
        orderPage.selectAddress("gogoljeva 22 novi sad");
        orderPage.selectAddress("zmaj jovina 10 novi sad");
        orderPage.showRoutes();

        orderPage.choseOptimalByPrice();
        orderPage.clickNext();

        assertEquals(true, orderPage.isShownNextPage());
        AdditionalServicesPage additionalServicesPage = new AdditionalServicesPage(chromeDriver);
        additionalServicesPage.choseType2();
        additionalServicesPage.next();

       assertEquals(true, additionalServicesPage.notAvailableDriver());
       login.logout();
        assertEquals(true,login.isOpenedLoginPage());
    }
}

//  Nažalost nismo bili u stanju da Vam napravimo vožnju     - nema vozaca

