package kts.nwt.ktsnwt.e2e.tests;

import kts.nwt.ktsnwt.e2e.pages.HttpsBackPage;
import kts.nwt.ktsnwt.e2e.pages.LoginPage;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class LoginTest extends BaseTest {

    @Test
    @Order(1)
    public void shouldBeOkLoginRegisteredUser() {
        //HttpsBackPage backPage = new HttpsBackPage(chromeDriver);
        //backPage.setHttps();

        LoginPage login = new LoginPage(chromeDriver);
        assertTrue(login.isOpenedLoginPage());

        login.filLoginForm("evioletta121@gmail.com", "123");
        assertTrue(login.isOpenedOrderPage());
        login.logout();
        assertEquals(true,login.isOpenedLoginPage());
    }

    @Test
    @Order(2)
    public void shouldBeOkLoginDriver() {
        //HttpsBackPage backPage = new HttpsBackPage(chromeDriver);
        //backPage.setHttps();

        LoginPage login = new LoginPage(chromeDriver);
        assertTrue(login.isOpenedLoginPage());

        login.filLoginForm("aa@gmail.com", "123");
        assertTrue(login.isOpenedDriverHomePage());
        login.logout();
        assertEquals(true,login.isOpenedLoginPage());
    }

    @Test
    @Order(3)
    public void shouldBeOkLoginAdmin() {
        //HttpsBackPage backPage = new HttpsBackPage(chromeDriver);
        //backPage.setHttps();

        LoginPage login = new LoginPage(chromeDriver);
        assertTrue(login.isOpenedLoginPage());

        login.filLoginForm("jj@gmail.com", "123");
        assertTrue(login.isOpenedAdminHomePage());
        login.logout();
        assertEquals(true,login.isOpenedLoginPage());
    }

    @Test
    @Order(4)
    public void shouldBeErrorLogin_incorrectEmail() {
        //HttpsBackPage backPage = new HttpsBackPage(chromeDriver);
        //backPage.setHttps();

        LoginPage login = new LoginPage(chromeDriver);
        assertTrue(login.isOpenedLoginPage());

        login.filLoginForm("error", "123");
        assertTrue(login.isBadCredentialsErrorShowed());
        assertEquals(true,login.isOpenedLoginPage());
    }

    @Test
    @Order(5)
    public void shouldBeErrorLogin_incorrectPassword() {
        //HttpsBackPage backPage = new HttpsBackPage(chromeDriver);
        //backPage.setHttps();

        LoginPage login = new LoginPage(chromeDriver);
        assertTrue(login.isOpenedLoginPage());

        login.filLoginForm("jj@gmail.com", "asdasdasd");
        assertTrue(login.isBadCredentialsErrorShowed());
        assertEquals(true,login.isOpenedLoginPage());
    }

    @Test
    @Order(6)
    public void shouldBeErrorLogin_accountNotActivated() {
        //HttpsBackPage backPage = new HttpsBackPage(chromeDriver);
        //backPage.setHttps();

        LoginPage login = new LoginPage(chromeDriver);
        assertTrue(login.isOpenedLoginPage());

        login.filLoginForm("gg@gmail.com", "123");
        assertTrue(login.isNotActivatedAccountErrorShowed());
        assertEquals(true,login.isOpenedLoginPage());
    }
}
