package kts.nwt.ktsnwt.e2e.helper;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.MoveTargetOutOfBoundsException;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.function.Function;

public class ViewPortHelper {

    public void waitToBeMovable(WebDriver chromeDriver, int amount, WebElement element){
        ((JavascriptExecutor)chromeDriver).executeScript("scroll(0,"+ amount + ");");
        Wait<WebDriver> wait = new FluentWait<WebDriver>(chromeDriver)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofSeconds(2))
                .ignoring(MoveTargetOutOfBoundsException.class);

        Function<WebDriver, Boolean> function = new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver driver) {
                try {
                    Actions a = new Actions(chromeDriver);
                    a.moveToElement(element).perform();
                    return true;
                }catch (MoveTargetOutOfBoundsException | NoSuchElementException e){
                    return false;
                }
            }
        };

        wait.until(function);
    }
}
