INSERT INTO CITY (NAME, POST_NUMBER) VALUES ('Novi Sad', 21000);

INSERT INTO role (name) VALUES ('ROLE_ADMIN');
INSERT INTO role (name) VALUES ('ROLE_REGISTERED_USER');
INSERT INTO role (name) VALUES ('ROLE_DRIVER');

------ RU 1  U1 --------- 123    id 1
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Jake', 'Sully', 'mm','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'mm', '+381246548', false, true, false, 0, '', 5000, 0);
insert into user_role (user_id, role_id) values (1, 2);

INSERT INTO vehicle_type (name, price) values ('karavan', 120);
INSERT INTO vehicle_type (name, price) values ('dzip', 200);

------ RU 2  U2 --------- 123    not enough token
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Milly', 'Brown', 'milly','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'milly', '+381246548', false, true, false, 0, '', 0, 0);
insert into user_role (user_id, role_id) values (2, 2);

--type vehicle
--INSERT INTO vehicle_type (name, price) values ('karavan', 120);
--INSERT INTO vehicle_type (name, price) values ('dzip', 200);

-- vehicle 1
INSERT INTO vehicle(type_id, name, label, year_of_production, color, baby_friendly, pet_friendly, number_of_seats) values
    (1, 'Mini moris', 'NS 041 JK', 2010, 'rose', true, true, 4);
insert into address (longitude, latitude) values (45.25302, 19.83945);--1
------ D 1  U3 --------- 123
insert into users (type, name, surname, email, password, username, phone, blocked, driver_state, vehicle, current_address, version) values
    ('D', 'Philip', 'Philipia', 'philip@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'philip', '+381246548', false,0, 1, 1, 0);
insert into user_role (user_id, role_id) values (3, 3);
insert into time_period (start_date_time, active_hour, estimated) values
    (PARSEDATETIME('2023-01-28 10:57:58','yyyy-MM-dd HH:mm:ss'), 3, false);   -- 1

------ RU 3  U4 --------- 123    id 1 has active driving
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Ted', 'Mosby', 'ted@email.rs','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'ted', '+381246548', false, true, false, 0, '', 5000, 0);
insert into user_role (user_id, role_id) values (4, 2);

----- driving 1   -- current
insert into time_period (start_date_time, end_date_time, estimated)  -- 2
values (CURRENT_TIMESTAMP(), PARSEDATETIME('2023-01-28 15:24:58', 'yyyy-MM-dd HH:mm:ss'), true);  -- 11
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (1);
insert into address (longitude, latitude, name, path_id) values (45.2464547, 19.8342626,'Gogoljeva 22 Novi Sad', 1);  -- 2
insert into address (longitude, latitude, name, path_id) values (45.245906, 19.8353987,'Gogoljeva 52 Novi Sad', 1);   -- 3

insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, true, 1); --1
insert into driving (price, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (550, 2, 2, CURRENT_TIMESTAMP(), 1, 1, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed)
values (550, true, 1, 4, true);


------ RU 4  U5 --------- 123
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Moso', 'Masa', 'leti','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'leti', '+381246548', false, true, false, 0, '', 5000, 0);
insert into user_role (user_id, role_id) values (5, 2);

------ RU 5  U6 --------- 123
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Inka', 'Manka', 'mokka','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'mokka', '+381246548', false, true, false, 0, '', 5000, 0);
insert into user_role (user_id, role_id) values (6, 2);

------ RU 6  U7 --------- 123
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Inka', 'Manka', 'cappucino','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'cappucino', '+381246548', false, true, false, 0, '', 5000, 0);
insert into user_role (user_id, role_id) values (7, 2);

------- driving 10 --- future
insert into time_period (start_date_time, end_date_time, estimated)   -- 3
values (TIMESTAMPADD('MINUTE', 10, NOW()), PARSEDATETIME('2023-01-22 18:10:58', 'yyyy-MM-dd HH:mm:ss'), true);
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, false, 1);
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (2);--10
insert into address (longitude, latitude, name, path_id) values (45.246479, 19.834240,'Gogoljeva 22 Novi Sad', 2);   -- 4
insert into address (longitude, latitude, name, path_id) values (45.264370, 19.822000,'Bulevar Jase Tomica 12 Novi Sad', 2);  -- 5
insert into address (longitude, latitude, route_id) values (45.264370, 19.822000, 2);  -- 6
insert into driving (price, state, driving_time_id, order_time, chosen_additional_services_id, path_id, future, notification_sent)
values (630, 0, 3, PARSEDATETIME('2023-01-27 14:22:00','yyyy-MM-dd HH:mm:ss'), 2, 2, true, 0);
insert into driving_payment (price, paid,driving_id, client_id,confirmed) values (630, true, 2, 4, true);

------ RU 7  U8 --------- 123
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'jake', 'peralta', 'peralta','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'peralta', '+381246548', false, true, false, 0, '', 5000, 0);
insert into user_role (user_id, role_id) values (8, 2);

------ RU 8  U9 --------- 123
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Rosa', 'Diaz', 'rosa','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'rosa', '+381246548', false, true, false, 0, '', 5000, 0);
insert into user_role (user_id, role_id) values (9, 2);

------ RU 9  U10 --------- 123
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Terry', 'Jeffords', 'terry','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'terry', '+381246548', false, true, false, 0, '', 5000, 0);
insert into user_role (user_id, role_id) values (10, 2);

------ RU 10  U11 --------- 123
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Amy', 'Santiago', 'amy','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'amy', '+381246548', false, true, false, 0, '', 5000, 0);
insert into user_role (user_id, role_id) values (11, 2);

------ RU 11  U12 --------- 123
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Charles', 'Boyle', 'boyle','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'boyle', '+381246548', false, true, false, 0, '', 5000, 0);
insert into user_role (user_id, role_id) values (12, 2);

-- vehicle 2
INSERT INTO vehicle(type_id, name, label, year_of_production, color, baby_friendly, pet_friendly, number_of_seats) values
    (1, 'Mercedes', 'NS 041 JK', 2010, 'rose', true, true, 4);
insert into address (longitude, latitude) values (45.25302, 19.83945);--7
------ D 2  U13 --------- 123
insert into users (type, name, surname, email, password, username, phone, blocked, driver_state, vehicle, current_address, version) values
    ('D', 'Gina', 'Linetti', 'gina@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'gina', '+381246548', false,0, 2, 7,0);
insert into user_role (user_id, role_id) values (13, 3);
insert into time_period (start_date_time, active_hour, estimated) values
    (PARSEDATETIME('2023-01-28 10:57:58','yyyy-MM-dd HH:mm:ss'), 13, false);   -- 1


------------- driving 3
insert into time_period (start_date_time, end_date_time, estimated)  -- 5
values (CURRENT_TIMESTAMP(), PARSEDATETIME('2023-01-28 15:24:58', 'yyyy-MM-dd HH:mm:ss'), true);
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (3);
insert into address (longitude, latitude, name, path_id) values (45.2464547, 19.8342626,'Gogoljeva 22 Novi Sad', 3); --8
insert into address (longitude, latitude, name, path_id) values (45.245906, 19.8353987,'Gogoljeva 52 Novi Sad', 3); --9
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, true, 1); --3
insert into driving (price, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (550, 0, 5, CURRENT_TIMESTAMP(), 3, 3, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed)
values (275, false, 3, 8, true);


------------- driving 4
insert into time_period (start_date_time, end_date_time, estimated)  -- 6
values (CURRENT_TIMESTAMP(), PARSEDATETIME('2023-01-28 15:24:58', 'yyyy-MM-dd HH:mm:ss'), true);
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (4);
insert into address (longitude, latitude, name, path_id) values (45.2464547, 19.8342626,'Gogoljeva 22 Novi Sad', 4);--10
insert into address (longitude, latitude, name, path_id) values (45.245906, 19.8353987,'Gogoljeva 52 Novi Sad', 4);--11
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, true, 1); --3
insert into driving (price, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (1000, 0, 6, CURRENT_TIMESTAMP(), 4, 4, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed)
values (500, false, 4, 9, true);
insert into driving_payment (price, paid,driving_id, client_id, confirmed)
values (500, false, 4, 10, false);


------------- driving 5
insert into time_period (start_date_time, end_date_time, estimated)  -- 7
values (CURRENT_TIMESTAMP(), PARSEDATETIME('2023-01-28 15:24:58', 'yyyy-MM-dd HH:mm:ss'), true);
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (5);
insert into address (longitude, latitude, name, path_id) values (45.2464547, 19.8342626,'Gogoljeva 22 Novi Sad', 5);--12
insert into address (longitude, latitude, name, path_id) values (45.245906, 19.8353987,'Gogoljeva 52 Novi Sad', 5);--13
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, true, 1); --5
insert into driving (price, driver_id, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (700, 13, 0, 7, CURRENT_TIMESTAMP(), 5, 5, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed)
values (700, false, 5, 10, true);


------------- driving 6
insert into time_period (start_date_time, end_date_time, estimated)  -- 8
values (CURRENT_TIMESTAMP(), PARSEDATETIME('2023-01-28 15:24:58', 'yyyy-MM-dd HH:mm:ss'), true);
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (6);
insert into address (longitude, latitude, name, path_id) values (45.2464547, 19.8342626,'Gogoljeva 22 Novi Sad', 6);--14
insert into address (longitude, latitude, name, path_id) values (45.245906, 19.8353987,'Gogoljeva 52 Novi Sad', 6);--15
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, true, 1); --3
insert into driving (price, driver_id, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (550, 13, 0, 8, CURRENT_TIMESTAMP(), 6, 6, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed)
values (550, false, 6, 2, true);

------------- driving 7
insert into time_period (start_date_time, end_date_time, estimated)  -- 9
values (CURRENT_TIMESTAMP(), PARSEDATETIME('2023-01-28 15:24:58', 'yyyy-MM-dd HH:mm:ss'), true);
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (7);
insert into address (longitude, latitude, name, path_id) values (45.2464547, 19.8342626,'Gogoljeva 22 Novi Sad', 7);--16
insert into address (longitude, latitude, name, path_id) values (45.245906, 19.8353987,'Gogoljeva 52 Novi Sad', 7);--17
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, true, 1); --3
insert into driving (price, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (1500, 0, 9, CURRENT_TIMESTAMP(), 7, 7, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed)
values (500, false, 7, 9, true);
insert into driving_payment (price, paid,driving_id, client_id, confirmed)
values (500, false, 7, 8, false);
insert into notification (date, type, driving_id, client_id) values
    (CURRENT_TIMESTAMP(), 0, 7, 8);
insert into driving_payment (price, paid,driving_id, client_id, confirmed)
values (500, false, 7, 10, false);

------------ purchuse -- future
------------- driving 8
insert into time_period (start_date_time, end_date_time, estimated)  -- 10
values (CURRENT_TIMESTAMP(), PARSEDATETIME('2023-01-28 15:24:58', 'yyyy-MM-dd HH:mm:ss'), true);
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (8);
insert into address (longitude, latitude, name, path_id) values (45.2464547, 19.8342626,'Gogoljeva 22 Novi Sad', 8);--18
insert into address (longitude, latitude, name, path_id) values (45.245906, 19.8353987,'Gogoljeva 52 Novi Sad', 8);--19
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, true, 1); --8
insert into driving (price, driver_id, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (700, 13, 0, 10, CURRENT_TIMESTAMP(), 8, 8, true, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed)
values (700, false, 8, 10, true);

------------- driving 9
insert into time_period (start_date_time, end_date_time, estimated)  -- 11
values (CURRENT_TIMESTAMP(), PARSEDATETIME('2023-01-28 15:24:58', 'yyyy-MM-dd HH:mm:ss'), true);
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (9);
insert into address (longitude, latitude, name, path_id) values (45.2464547, 19.8342626,'Gogoljeva 22 Novi Sad', 9);--20
insert into address (longitude, latitude, name, path_id) values (45.245906, 19.8353987,'Gogoljeva 52 Novi Sad', 9);--21
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, true, 1); --9
insert into driving (price, driver_id, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (700, 13, 0, 10, CURRENT_TIMESTAMP(), 9, 9, true, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed)
values (700, false, 8, 10, true);

------------- driving 10
insert into time_period (start_date_time, end_date_time, estimated)  -- 12
values (CURRENT_TIMESTAMP(), PARSEDATETIME('2023-01-28 15:24:58', 'yyyy-MM-dd HH:mm:ss'), true);
insert into route (distance, time) values (5.31, 7);
insert into path (route) values (10);
insert into address (longitude, latitude, name, path_id) values (45.2464547, 19.8342626,'Gogoljeva 22 Novi Sad', 10);--22
insert into address (longitude, latitude, name, path_id) values (45.245906, 19.8353987,'Gogoljeva 52 Novi Sad', 10);--23
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, true, 1); --10
insert into driving (price, driver_id, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (700, 13, 0, 10, CURRENT_TIMESTAMP(), 10, 10, true, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed)
values (700, true, 8, 10, true);




----- testovi za obavljanje voznje
                                                    --45.247220, 19.841160
INSERT INTO address (longitude, latitude) values(45.25373, 19.8412);  -- 24

-- vehicle 3
INSERT INTO vehicle(type_id, name, label, year_of_production, color, baby_friendly, pet_friendly, number_of_seats) values
    (1, 'Audi', 'NS 016 AA', 2020, 'crna', true, true, 4);

------ D 3  U14 --------- 123     driving state
insert into users (type, name, surname, email, password, username, phone, blocked, driver_state, vehicle, current_address, version) values
('D', 'Nenad', 'Nenadic', 'nn@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'nenad','+381246748', false, 1, 3, 24,0);
insert into user_role (user_id, role_id) values (14, 3);
insert into time_period (start_date_time, active_hour, estimated) values
    (PARSEDATETIME('2023-01-30 15:57:58','yyyy-MM-dd HH:mm:ss'), 14, false);   -- 13

------ RU    U15   -- 123
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Andjela', 'Andjela', 'andjela@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'andjela', '+381246548', false, true,false, 0, '', 5000, 0);
insert into user_role (user_id, role_id) values (15, 2);

--- driving 11
insert into time_period (start_date_time, end_date_time, estimated)   -- 14
values (PARSEDATETIME('2023-01-30 15:04:58','yyyy-MM-dd HH:mm:ss'), PARSEDATETIME('2023-01-30 15:24:58', 'yyyy-MM-dd HH:mm:ss'), true);
insert into route (distance, time) values (10.31, 10);
insert into path (route) values (11);
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, false, 1);
insert into address (longitude, latitude, name, path_id) values (45.2537745, 19.8411572,'Jevrejska 10 Novi Sad', 11);--25
insert into address (longitude, latitude, name, path_id) values (45.2531858, 19.8393187,'Jevrejska 26 Novi Sad', 11);--26

insert into address (longitude, latitude, route_id) values (45.25373, 19.8412, 11);--27
insert into address (longitude, latitude, route_id) values (45.25361, 19.84094, 11);--28
insert into address (longitude, latitude, route_id) values (45.25358, 19.84085, 11);--29
insert into address (longitude, latitude, route_id) values (45.25336, 19.8403, 11);--30
insert into address (longitude, latitude, route_id) values (45.25315, 19.83978, 11);--31
insert into address (longitude, latitude, route_id) values (45.25302, 19.83945, 11);--32
insert into address (longitude, latitude, route_id) values (45.25302, 19.83945, 11);--33

insert into driving (driver_id, price, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (14, 900, 6, 14, CURRENT_TIMESTAMP(), 11, 11, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (900, true, 11, 15, true);



----- inactive driver without drivings
INSERT INTO address (longitude, latitude) values(45.247220, 19.841160);  -- 34

-- vehicle 4
INSERT INTO vehicle(type_id, name, label, year_of_production, color, baby_friendly, pet_friendly, number_of_seats) values
    (1, 'Fiat', 'NS 016 BB', 2015, 'siva', true, true, 4);

------ D 4  U16 --------- 123     inactive state
insert into users (type, name, surname, email, password, username, phone, blocked, driver_state, vehicle, current_address, version) values
('D', 'Zika', 'Zikic', 'zz@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'zika','+381246748', false, 2, 4, 34, 0);
insert into user_role (user_id, role_id) values (16, 3);



------- active driver and current driving come to end
INSERT INTO address (longitude, latitude) values(45.25302, 19.83945);  -- 35

-- vehicle 5
INSERT INTO vehicle(type_id, name, label, year_of_production, color, baby_friendly, pet_friendly, number_of_seats) values
    (1, 'Reno', 'NS 016 CC', 2018, 'crvena', true, true, 4);

------ D 5  U17 --------- 123     inactive state
insert into users (type, name, surname, email, password, username, phone, blocked, driver_state, vehicle, current_address, version) values
('D', 'Djordje', 'Djordje', 'dj@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'djordje','+381246748', false, 1, 5, 35, 0);
insert into user_role (user_id, role_id) values (17, 3);
insert into time_period (start_date_time, active_hour, estimated) values
    (PARSEDATETIME('2023-02-01 08:57:58','yyyy-MM-dd HH:mm:ss'), 17, false);  -- 15

------ RU    U18   -- 123
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Milena', 'Milena', 'milena@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
        'milena', '+381246548', false, true,false, 0, '', 5000, 0);
insert into user_role (user_id, role_id) values (18, 2);

--- driving 12
insert into time_period (start_date_time, end_date_time, estimated)   -- 16
values (PARSEDATETIME('2023-01-30 15:04:58','yyyy-MM-dd HH:mm:ss'), PARSEDATETIME('2023-01-30 15:24:58', 'yyyy-MM-dd HH:mm:ss'), true);
insert into route (distance, time) values (10.31, 10);
insert into path (route) values (12);
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, false, 1);
insert into address (longitude, latitude, name, path_id) values (45.2537745, 19.8411572,'Jevrejska 10 Novi Sad', 12);--36
insert into address (longitude, latitude, name, path_id) values (45.2531858, 19.8393187,'Jevrejska 26 Novi Sad', 12);--37

insert into address (longitude, latitude, route_id) values (45.25373, 19.8412, 12);--38
insert into address (longitude, latitude, route_id) values (45.25361, 19.84094, 12);--39
insert into address (longitude, latitude, route_id) values (45.25358, 19.84085, 12);--40
insert into address (longitude, latitude, route_id) values (45.25336, 19.8403, 12);--41
insert into address (longitude, latitude, route_id) values (45.25315, 19.83978, 12);--42
insert into address (longitude, latitude, route_id) values (45.25302, 19.83945, 12);--43

insert into driving (driver_id, price, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (17, 900, 2, 16, CURRENT_TIMESTAMP(), 12, 12, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (900, true, 12, 18, true);



------- active driver and current driving
INSERT INTO address (longitude, latitude) values(45.25358, 19.84085);  -- 44

-- vehicle 6
INSERT INTO vehicle(type_id, name, label, year_of_production, color, baby_friendly, pet_friendly, number_of_seats) values
    (1, 'Citroen', 'NS 016 DD', 2018, 'zuta', true, true, 4);

------ D 6  U19 --------- 123     drivind state no start no end
insert into users (type, name, surname, email, password, username, phone, blocked, driver_state, vehicle, current_address, version) values
('D', 'Dusan', 'Dusan', 'dusan@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'dusan','+381246748', false, 1, 6, 44, 0);
insert into user_role (user_id, role_id) values (19, 3);
insert into time_period (start_date_time, active_hour, estimated) values
    (PARSEDATETIME('2023-02-01 08:57:58','yyyy-MM-dd HH:mm:ss'), 19, false);  -- 17

------ RU    U20   -- 123
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Ivana', 'Ivana', 'ivana@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
            'ivana', '+381246548', false, true,false, 0, '', 5000, 0);
insert into user_role (user_id, role_id) values (20, 2);

--- driving 13
insert into time_period (start_date_time, end_date_time, estimated)   -- 18
values (PARSEDATETIME('2023-01-30 15:04:58','yyyy-MM-dd HH:mm:ss'), PARSEDATETIME('2023-01-30 15:24:58', 'yyyy-MM-dd HH:mm:ss'), true);
insert into route (distance, time) values (10.31, 10);
insert into path (route) values (13);
insert into chosen_additional_services (baby_friendly, pet_friendly, type_id) values (false, false, 1);
insert into address (longitude, latitude, name, path_id) values (45.2537745, 19.8411572,'Jevrejska 10 Novi Sad', 13);--45
insert into address (longitude, latitude, name, path_id) values (45.2531858, 19.8393187,'Jevrejska 26 Novi Sad', 13);--46

insert into address (longitude, latitude, route_id) values (45.25373, 19.8412, 13);--47
insert into address (longitude, latitude, route_id) values (45.25361, 19.84094, 13);--48
insert into address (longitude, latitude, route_id) values (45.25358, 19.84085, 13);--49
insert into address (longitude, latitude, route_id) values (45.25336, 19.8403, 13);--50
insert into address (longitude, latitude, route_id) values (45.25315, 19.83978, 13);--51
insert into address (longitude, latitude, route_id) values (45.25302, 19.83945, 13);--52

insert into driving (driver_id, price, state, driving_time_id, order_time, path_id, chosen_additional_services_id, future, notification_sent)
values (19, 900, 2, 18, CURRENT_TIMESTAMP(), 13, 13, false, 0);
insert into driving_payment (price, paid,driving_id, client_id, confirmed) values (900, true, 13, 20, true);


------ RU    U21   -- 123
insert into users (type, name, surname, email, password, username, phone, blocked, activated, social_account, provider, image, tokens, version)
values ('RU', 'Lana', 'Lana', 'lana@gmail.com','$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra',
            'lana', '+381246548', false, true,false, 0, '', 5000, 0);
insert into user_role (user_id, role_id) values (21, 2);
